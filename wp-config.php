<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wyz_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'edA.}+Y dar}:6K2:jOW}8kRpd7b3fcIWZl$$6%B0DT-&-Z:a9iwZImhG}H9ij%5' );
define( 'SECURE_AUTH_KEY',   '>bfxRc_,2v@eg_#Q:J`@o+VPF8?N4*lM$KS#3_4!Y!Sb0@e5%vPDd-W,`>XI:U|0' );
define( 'LOGGED_IN_KEY',     ':F>-P(6VPxS,K~l#C0^C*B!wnZbS$}qi|`2-DNi)54cV3*vj, o`&+D6b|IAMYc<' );
define( 'NONCE_KEY',         'LPkZk2{tIDDEH/%Ol&^fE{9  Fh?k[Sr|-q4XlueTM|63PuNV-=Hy1&hz}9Dg<bU' );
define( 'AUTH_SALT',         'un{Fi6M|Hg*US_93w6qh1|Lyn;_2)FW]fHx.bz?hJs=YvFAGzbg[8.,3~H*7sEP0' );
define( 'SECURE_AUTH_SALT',  'PG5Azp7%|w,V,5>|*%+*gp)HO$qdnzpH68F:/UUkWu[Y{P`-o]07sC0;Xl=,uS;6' );
define( 'LOGGED_IN_SALT',    '6krne5X7$o}R)YK-OW/^{Rl.7~f9>ee~cO:L:!O0qnZX}mDfnmE/+StS2}VWrpN/' );
define( 'NONCE_SALT',        'ZLQv:j0ZOXWk=R04OM|vE&]tfu9{f6UA2utve/DZ&}L2x,4V,KnF<&A6mPu}#Bp7' );
define( 'WP_CACHE_KEY_SALT', 'Yq9ye;Vgp=U-g747cUMU(tE84MN^Ba+gPPxnr6t-[#Y&+zplFTMq@4b;?DKd;DsU' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_7f7e1d5d72_';

define( 'WP_DEBUG', false );
/* Change WP_MEMORY_LIMIT to increase the memory limit for public pages. */
define('WP_MEMORY_LIMIT', '256M');

/* Uncomment and change WP_MAX_MEMORY_LIMIT to increase the memory limit for admin pages. */
//define('WP_MAX_MEMORY_LIMIT', '256M');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
