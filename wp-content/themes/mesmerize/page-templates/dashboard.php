<?php
/*
 * Template Name: Dashboard Template
 */
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title></title>

  <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/assets/css/bootstrap.min.css">
  <link rel="icon" href="<?php bloginfo('template_url') ?>/assets/assets/images/favicon.png" type="image/png">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
  <link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/assets/assets/images/favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?php bloginfo('template_url') ?>/assets/assets/images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/assets/css/custom.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/assets/css/responsive.css">
</head>
<body>
  <header id="header" class="">
        <nav class="navbar navbar-expand-lg navbar-dark justify-content-center">
          <a class="navbar-brand" href="index.html"><img src="<?php bloginfo('template_url') ?>/assets/assets/images/logo.png" alt="logo"></a>
            <div class="container-fluid">
              <div class="row ml-auto">
                <div class="header_menu">
                  <form>
                    <ul class="navbar-nav ml-auto">
                      <li>
                        <a href="javascript:void();">
                          <div id="filter">Filter</div>
                        </a>
                      </li>
                       <li>
                        <a href="javascript:void();">
                          <div id="calender">
                            <div class="form-group">
                              <div class='input-group date' >
                                  <input type='text' class="form-control" id='datepicker' autocomplete="off"placeholder="Calender" />
                              </div>
                            </div>
                          </div>
                        </a>
                      </li>
                       <li>
                        <a href="javascript:void();">
                          <div id="location">
                            <div class="form-group">
                              <div class='input-group'>
                                <input type='text' class="form-control" id='' placeholder="Clark Quay, Singapore" />
                            </div>
                          </div>
                        </div>
                        </a>
                      </li>
                       <li>
                        <a href="javascript:void();" class="submit-btn">
                          <div id="submit">Order</div>
                        </a>
                      </li>
                    </ul>
                  </form>
                </div>
              </div>
           </div>
           
        </nav>
      </header>

   <!-- /header -->

   <section class="main-page">
     <div class="container-fluid">
       <div class="row">
        <div class="col-lg-3 col-sm-12 p-0">
           <div class="your-match">
             <div class="match-ratio text-center">
               <span>97%</span><p>MATCH WITH<br>YOUR PRIORITIES</p>
               <a href="" class="green-btn buttons">CHANGE PRIORITIES</a>
             </div>
             <div class="order-summary">
               <h3>YOUR ORDER</h3>
               <div class="order_desc">
                 <p><span class="count">(x2)</span><span class="dish-name">Dumplings.............</span></p>
                 <p class="price">14.00</p>
               </div>
               <div class="order_desc">
                 <p><span class="count">(x10)</span><span class="dish-name">Main Dish...........</span></p>
                 <p class="price">100.00</p>
               </div>
               <div class="order_desc">
                 <p><span class="count">(x1)</span><span class="dish-name">Main Dish 2..........</span></p>
                 <p class="price">13.00</p>
               </div>
               <div class="order_desc">
                 <p><span class="deliver">Delivery fee.........</span></p>
                 <p class="price">10.00</p>
               </div>
               <div class="total-bill">
                 <p>TOTAL S$137</p>
               </div>
               <div class="bill-div">
                 <a href="" class="green-btn buttons">GENERATE QUOTE</a>
                 <a href="" class="orange-btn buttons">CHECK OUT</a>
               </div> 
             </div>
             <div class="message">
               <img src="assets/images/gift.png" alt="gift">
               <h3>CONGRATULATIONS!</h3>
               <small>You will earn 12 rewards points!</small>
             </div>
             </div>
           </div>
         <div class="col-lg-9 col-sm-12 p-0">
           <div class="img_banner" style="background: url('<?php bloginfo('template_url') ?>/assets/assets/images/resto_bg.png') no-repeat;background-size: cover;background-position: 100% 40%;">
           </div>
           <div class="resto-info text-center">
             <div class="row">
               <div class="col-lg-4">
                 <div class="kitchen-ratings">
                   <h2>FENG'S KITCHEN</h2>
                   <P></P>
                   <ul class="list-inline">
                     <li class="list-inline-item"><a href="">Chinese</a></li>
                     <li class="list-inline-item"><a href="">Salad</a></li>
                     <li class="list-inline-item"><a href="">Sandwiches</a></li>
                   </ul>
                 </div>
               </div>
               <div class="col-lg-4">
                 <div class="resto-logo">
                   <div class="main-logo">
                     <img class="" alt="">
                   </div>
                   <img src="assets/images/food_choice.png" alt="">
                 </div>
               </div>
               <div class="col-lg-4">
                 <div class="resto-features"></div>
               </div>
             </div>
           </div>
         </div>
         </div>
       </div>
   </section>

  <footer>
    <div class="container">
      <div class="row">
          <div class="col-lg-3 col-12"> 
            <figure>
              <a href="index.html"><img src="assets/images/Logo_Footer.png" class="d-block mx-auto" alt=""></a>
            </figure>
          </div>

          <div class="col-lg-5 col-12">
            <div class="footer_main_links">
              <ul class="list-unstyled list-inline text-center">
                    <li class="list-inline-item"><a class="d-block mx-auto" target="_blank" href="index.html">HOME</a></li>
                    <li class="list-inline-item"><a class="d-block mx-auto" target="_blank" href="plans.html">HOME</a></li>
                    <li class="list-inline-item"><a class="d-block mx-auto" target="_blank" href="price.html">HOME</a></li>
                    <li class="list-inline-item"><a class="d-block mx-auto" target="_blank" href="#">HOME</a></li>
                    <li class="list-inline-item"><a class="d-block mx-auto" target="_blank" href="contact.html">HOME</a></li>
                  </ul>
              </div>
            <div class="social_list">
              <ul class="list-unstyled list-inline text-center">
                <li class="list-inline-item"><a class="d-block mx-auto" target="_blank" href="#"><img  src="assets/images/f-fb-icon.png" alt=""></a></li>
                <li class="list-inline-item"><a class="d-block mx-auto" target="_blank" href="#"><img  src="assets/images/f-yt-icon.png" alt=""></a></li>
                <li class="list-inline-item"><a class="d-block mx-auto" target="_blank" href="#"><img src="assets/images/f-insta-icon.png" alt=""></a></li>
              </ul>
            </div>
            <p class="text-center">&copy;All Rights Reserved.</p>
          </div>

          <div class="col-lg-4 col-12">
            <div class="store_list ">
              <ul class="list-unstyled list-inline text-center">
                <li class="list-inline-item"><a class="d-block mx-auto" target="_blank" href="#"><img  src="assets/images/google_play.png" alt=""></a></li>
                <li class="list-inline-item"><a class="d-block mx-auto" target="_blank" href="#"><img  src="assets/images/app_store.png" alt=""></a></li>
              </ul>
            </div>
          </div>
         
      </div> <!-- row div -->
    </div> <!-- container-fluid div -->
  </footer>
  <section>
    <div class="below-footer"></div>  
  </section>

<script src="../assets/assets/js/jquery.min.js"></script>
<script src="../assets/assets/js/jquery-ui.min.js"></script>
<script src="../assets/assets/js/bootstrap.min.js"></script>
<script src="../assets/assets/js/custom.js"></script>
<script type="text/javascript">
 $( function() {
  $( "#datepicker" ).datepicker({
    dateFormat: "dd-mm-yy"
    , duration: "fast"
  });
} );          
</script>

</body>
</html>