<?php

Class wyz_chef_dish{
	public function __construct() {
		add_action('init',  array($this,'cw_post_type_Dishes'));
		add_action( 'init', array($this,'add_custom_taxonomies') );

		add_shortcode("Restaurant_form", array( $this, 'wp_form_shortcode' ));
		add_shortcode("Restaurant_dashboard_form", array( $this, 'wp_dashboard_form_shortcode' ));
	}

	public static function wp_form_shortcode() {
		wp_enqueue_style('front-css', WYZ_PLUGIN_ROOT_URL .'assets/css/menu.css');
		wp_enqueue_style('front-css', WYZ_PLUGIN_ROOT_URL .'assets/css/custom.css');
	    wp_enqueue_script('front-script', WYZ_PLUGIN_ROOT_URL .'assets/js/menu.js');

        $current_user = wp_get_current_user();
        global $wpdb;
        if(isset($_GET['user_id']) && !empty($_GET['user_id'])){
        $usr_id = $_GET['user_id'];
           if($usr_id != 0) {
                   if ( in_array( 'administrator', (array) $current_user->roles ) ) {
                         $restaurantId = $wpdb->prepare("SELECT ID FROM {$wpdb->posts} WHERE post_author='%d' AND post_type='wp_restaurant' AND post_title !='Auto Draft' AND post_author!='0' ORDER BY ID DESC LIMIT 1", array($usr_id));
		                $rID = $wpdb->get_col($restaurantId);
		                $restaurantId = $rID[0];
                   }
           }
         
        }else{

        global $wpdb;
	    $usrID = wp_get_current_user()->ID;
	    $restaurantId = $wpdb->prepare("SELECT ID FROM {$wpdb->posts} WHERE post_author='%d' AND post_type='wp_restaurant' AND post_title !='Auto Draft' AND post_author!='0' ORDER BY ID DESC LIMIT 1", array($usrID));
        $rID = $wpdb->get_col($restaurantId);
		$restaurantId = $rID[0];
        }
	   



		if (isset($rID[0]) & !empty($rID[0])) { 

		$menus = get_terms([
		  	'taxonomy' => 'restaurant_menu', 
		  	'hide_empty' => false,
            'childless' => true,
		]);

		if(count($menus)) {
			$menusCol = $wpdb->get_results ('
			    SELECT distinct(menu_id) 
			    FROM  '. $wpdb->prefix .'wyz_restaurant_dishes
			        WHERE restaurant_id = '. $restaurantId .' ORDER BY menu_id
			');

			global $post;
			$style = '';
			$submitRestaurantPage = false;
			if(
				$post->post_name == 'submit-restaurant'
				|| $post->post_name == 'submit-restaurant-optimize'
			) {
				$style = '';
				$submitRestaurantPage = true;
			} else {
				$style = 'style="display: none;"';
			}

			$existingMenus = array();
			$menuWrapper = '<div class="menu-list-container"><ul class="menus-wrapper owl-carousel owl-theme" id="owl-demo">';
			$dishTabsContent = '<div id="dishTabsContent" class="tab-content col-md-12">';
            foreach ($menusCol as $key => $menuCol) {
				$activeClass = $key ? '' : 'active';
				$displayBlockStyle = $key ? '' : 'display: block;';
				$menuId = $menuCol->menu_id;
				$existingMenus[] = $menuCol->menu_id;
				$menu = get_term($menuId);
				$imageId = get_term_meta( $menuId, 'wyz-menu-taxonomy-image-id', true );

				if( $imageId ) {
              		$menuImage = wp_get_attachment_thumb_url( $imageId, 'thumbnail' );
            	} else {
            		$menuImage = WYZ_PLUGIN_ROOT_URL . 'assets/images/restaurant.svg';
            	}

            	$menuWrapper .= '
					<li class="menu-wrapper '. $activeClass .'" id="menu-wrapper-'. $menuId .'" data-menu-id="'. $menuId .'">
						<!-- <span class="menu-edit fa fa-pencil-square-o" '. $style .'></span> -->
						<span class="menu-delete fa fa-trash-o" '. $style .'></span>
						<div class="menu-header">
							<img class="menu-icon" src="'. $menuImage .'"><br>
							<span class="menu-title">'. $menu->name .'</span>
						</div>
					</li>
				';

                $dishes = $wpdb->get_results ("
				    SELECT * 
				    FROM  {$wpdb->prefix}wyz_restaurant_dishes
			        WHERE restaurant_id = {$restaurantId} AND menu_id = {$menuId} 
				");

				if (count($dishes)) {
					$dishTabsContent .= '<div class="menu-dishes '. $activeClass .'" id="menu-dishes-'. $menuId .'" data-menu-id="'. $menuId .'">';
					
					foreach ($dishes as $key => $dish) {
                        $dishId = $dish->id;
                        $dish->image = empty($dish->image) ? '' : $dish->image;

						$dishTabsContent .= '
						    <div style="'. $displayBlockStyle .'" id="menu-dish-'. $menuId .''. $dishId .'" class="menu-dish">
						        <div class="hidden-fields hidden-dish-fileds'. $menuId . $dishId .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][menuId]" value="'. $menuId .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][dishId]" value="'. $dishId .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][image]" value="'. $dish->image .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][name]" value="'. $dish->name .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][price]" value="'. $dish->price .'">
                                    <input type="hidden" name="menu['. $menuId .']['. $dishId .'][tax]" value="'. $dish->tax .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][description]" value="'. $dish->description .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][tags]" value="'. $dish->tags .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][min_quantity]" value="'. $dish->min_quantity .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][min_guest_serve]" value="'. $dish->min_guest_serve .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][max_guest_serve]" value="'. $dish->max_guest_serve .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][lead_time]" value="'. $dish->lead_time .'">
                                    <input type="hidden" name="menu['. $menuId .']['. $dishId .'][having_modifiers]" value="'. $dish->having_modifiers .'">
                                </div>
                            ';

                            $modifiers = $wpdb->get_results ("
                                SELECT * 
                                FROM  {$wpdb->prefix}wyz_restaurant_dish_modifiers
                                WHERE dish_id = {$dish->id} 
                            ");
                            
                            $dishTabsContent .= "<div class='hidden-fields hidden-modifier-fields{$menuId}{$dishId}'>";
                            if (count($modifiers)) {
                                foreach ($modifiers as $key => $modifier) {
                                    // echo '<pre>Debug modifier: '; print_r( $modifier ); echo '</pre>';
                                    $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifierIds][]' value='{$modifier->id}'>";
                                    $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][id]' value='{$modifier->id}'>";
                                    $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][modifierType]' value='{$modifier->modifier_type}'>";
                                    $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][promptText]' value='{$modifier->prompt_text}'>";
                                    $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][min]' value='{$modifier->min}'>";
                                    $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][max]' value='{$modifier->max}'>";

                                    $modifierItems = $wpdb->get_results ("
                                        SELECT * 
                                        FROM  {$wpdb->prefix}wyz_restaurant_dish_modifier_items
                                        WHERE modifier_id = {$modifier->id} 
                                    ");

                                    foreach ($modifierItems as $key => $modifierItem) {
                                        // echo '<pre>Debug modifierItem: '; print_r( $modifierItem ); echo '</pre>';
                                        $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][itemId][]' value='{$modifierItem->id}'>";
                                        $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][itemText][]' value='{$modifierItem->text}'>";
                                        $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][itemPrice][]' value='{$modifierItem->price}'>";
                                        $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][itemTax][]' value='{$modifierItem->tax}'>";
                                        $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][itemTotal][]' value='{$modifierItem->total}'>";
                                    }
                                }
                            }
                            $dishTabsContent .= "</div>";

                            $dishTabsContent .= "<div class='hidden-fields hidden-delete-modifier-fields{$menuId}{$dishId}'></div>";

                            $dishTabsContent .= '
						        <div class="headerBox">
						            <div class="row">
						                <div class="col-md-9 col-sm-8">
						                    <h5 class="titleHeader" id="dishTitle'. $menuId .''. $dishId .'"> 
						                    	<span class="">'. htmlspecialchars_decode($dish->name, ENT_QUOTES) .'</span>
					                    	</h5>
						                    <p class="card-text" id="dishDescription'. $menuId .''. $dishId .'"> 
						                    	<span class="">'. htmlspecialchars_decode($dish->description, ENT_QUOTES) .'</span> 
					                    	</p>
						                </div>
						                <div class="col-md-2 col-sm-2">
						                    <div class="dishPrice" id="dishPrice'. $menuId .''. $dishId .'"> 
						                    	<span id="s'. $menuId .''. $dishId .'">$ '. $dish->price .'</span> 
						                    </div>
						                </div>
						                <div class="col-md-1 col-sm-2" '. $style .'>
						                    <div class="dish-edit-wrapper" '. $style .'>
							                    <a href="#" class="dish-edit" onclick="editDish('. $menuId .', '. $dishId .'); return false;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                <a href="#" class="dish-copy" data-menu-id="'. $menuId .'" data-dish-id="'. $dishId .'"><i class="fa fa-copy"></i></a>
							                    <a href="#" class="dish-delete" data-menu-id="'. $menuId .'" data-dish-id="'. $dishId .'"><i class="fa fa-trash-o"></i></a>
						                    </div>
						                </div>
						            </div>
                                </div>
						    </div>
						';
					}

					$dishTabsContent .= '</div>';
				}
			}

			$menuWrapper .= '</ul>';
				
			$dishTabsContent .= '
				<script type="text/javascript">
					var WYZ_PLUGIN_ROOT_URL = "'. WYZ_PLUGIN_ROOT_URL .'";
					jQuery(document).ready(function($){
						'. ($submitRestaurantPage && count($menusCol) ? '$(\'.add-dish\').show();' : '') .'
					})
				</script>
			';
			$dishTabsContent .= '</div>';

    			$form = '
    				<div class="menus-container">
    					'. $menuWrapper .'

    					<div class="add-menu-wrapper" '. $style .'>
    						<select class="menu-list">
    					';

    					$menuOptions = $popMenuOptions = '';
                        foreach ($menus as $menu) {
    						$disabled = '';
    						$imageId = get_term_meta( $menu->term_id, 'wyz-menu-taxonomy-image-id', true );

    						if( $imageId ) {
    		              		$menuImage = wp_get_attachment_thumb_url( $imageId, 'thumbnail' );
    		            	} else {
    		            		$menuImage = WYZ_PLUGIN_ROOT_URL . 'assets/images/restaurant.svg';
    		            	}

    		            	if (in_array($menu->term_id, $existingMenus)) {
    		            		$disabled = 'disabled';
    		            	}
    						$menuOptions .= '<option data-img="'. $menuImage .'"  data-menu-id="'. $menu->term_id .'" value="'. $menu->term_id .'" '. $disabled .'>'. $menu->name .'</option>';
                            $popMenuOptions .= '<option data-img="'. $menuImage .'"  data-menu-id="'. $menu->term_id .'" value="'. $menu->term_id .'" '. (empty($disabled) ? 'disabled' : '') .'>'. $menu->name .'</option>';
    					}
    						
    					$form .= $menuOptions;

                        $form .= '
    						</select><br />
    						<button type="button" class="add-menu modify-button" >Add Menu</button>
    					</div>
                    </div>

					'. $dishTabsContent .'

					<div style="clear: both;"><hr />
					<button type="button" id="openmenumodel" class="btn-model btn btn-info btn-lg add-dish" data-toggle="modal" data-target="#menu-modal">Add New Item</button>
				</div>

				<!-- Modal -->
			  	<div class="" id="modal-container">
			  		<div class="modal fade" id="menu-modal" role="dialog">
				    	<div class="modal-dialog">
					    
					      	<!-- Modal content-->
					      	<div class="modal-content">
							    <div class="modal-body" id="dishbody">
							        <div id="dishTabsContentformodal" class="col-md-12">
							            <div class="" id="categorylink5">
							                <div>
                                                <h3 class="popup-dish-tab active" data-tab-target="popup-general">Item Info</h3>
                                                <h3 class="popup-dish-tab" data-tab-target="popup-modifier">Modifier Info</h3>
							                    <div class="row popup-dish-tab-content popup-general-tab-content" style="line-height: 14px;">
							                        <div id="row1">
							                            <div class="add-item-popup-description">
                                                            <div class="upper-row-content">
                                                                    <p><span id="showMenuFormError1"></span></p>
                                                                    <div class="pop-image">
                                                                        <p>
                                                                            <input class="popup-dish-img" type="file">
                                                                            <small class="p_size">(Prefered size: <strong>225w X 125h</strong> )</small>
                                                                        </p>
                                                                        <div class="popup-dish-preview-container">
                                                                            <div class="iamgePadding popup-dish-preview" id="image-holder1"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="popup-description ">
                                                                        <p class="popup-dish-name-new">
                                                                            <label>Item Name</label>
                                                                            <input class="popup-dish-name" type="text" placeholder="Dish title">
                                                                        </p>
                                                                        <!-- <div class="field required-field pop-up-category"> <span>Category</span>
                                                                            <select name="menu_popup_category" id="menu_popup_category" class="popup-dish-category required-field" required="">
                                                                            ';

                                                                            /*$dishCategories = get_terms([
                                                                                'taxonomy' => 'restaurant_category', 
                                                                                'hide_empty' => false,
                                                                            ]);

                                                                            foreach ($dishCategories as $dishCategory) {
                                                                                $selected = '';
                                                                                if($dishCategory->name==$value->tags){ 
                                                                                    $selected = "selected";
                                                                                }
                                                                                $form.='<option '. $selected .' value="'. $dishCategory->term_id .'">'. $dishCategory->name .'</option>';
                                                                            }*/

                                                                            $form .= '
                                                                            </select>
                                                                        </div> -->
                                                                        <div class="popup-dish-price-new">
                                                                            <div class="popup-price-fields">
                                                                                <label>Base Price</label>
                                                                                <input class="popup-dish-price" type="text" placeholder="Base Price">
                                                                            </div>
                                                                            <div class="popup-price-fields">
                                                                                <label>Include Tax</label>
                                                                                <input class="popup-dish-price-tax" type="text" placeholder="Tax" value="7">
                                                                            </div>
                                                                            <div class="popup-price-fields">
                                                                                <label>Sub Total</label>
                                                                                <input class="popup-dish-price-total" type="text" placeholder="Sub Total">
                                                                            </div>
                                                                        </div>
                                                                        <div class="desc-box">
                                                                            <label>Item Description</label>
                                                                            <textarea class="popup-dish-desc" placeholder="Dish discription..."cols="31" rows="6"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="popup-tags">
                                                                <h3>TAGS</h3>
                                                                    <div class="add-item-popup-description">
                                                                        <p><span id="showMenuFormError1"></span>
                                                                        </p>
                                                                        <div class="popup-description tags-sec">
                                                                            <p>Is this item vegetarian, gluten free or Halal? Add tags to describe it more accurately and help the guest make a more informed choice.</p>
                                                                            <div class="field required-field pop-up-category"> <span>Select tag(s)</span>
                                                                                <select id="menu_popup_tags" class="popup-dish-tags selectpicker" multiple="">
                                                                                ';

                                                                                $dishTags = get_terms([
                                                                                    'taxonomy' => 'restaurant_tag', 
                                                                                    'hide_empty' => false,
                                                                                ]);

                                                                                foreach ($dishTags as $dishTag) {
                                                                                    $selected = '';
                                                                                    // if($dishTag->name==$value->tags){ 
                                                                                    //     $selected = "selected";
                                                                                    // }
                                                                                    $form.='<option '. $selected .' value="'. $dishTag->term_id .'">'. $dishTag->name .'</option>';
                                                                                }

                                                                                $form .= '
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                            </div>
                                                                                                                      
							                            </div>
                                                        <div>
                                                        <div class="add-item-popup-description ">
                                                            <div class="popup-quantity">
                                                                <h3>QUANTITY</h3>
                                                                <p><span id="showMenuFormError1"></span>
                                                                </p>
                                                                <div class="popup-description pop-up-qauntity">
                                                                    <div class="field required-field ">
                                                                        <p>How many guests will this item serve?</p>
                                                                        <p>
                                                                            <label>
                                                                                <b>Min</b>
                                                                                <input type="number" id="minmumquantity" min="1" class="popup-dish-min-serve" />
                                                                            </label>
                                                                            <label>
                                                                                <b>Max</b>
                                                                                <input type="number" id="maxmumquantity" class="popup-dish-max-serve" />
                                                                            </label>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="popup-lead-time">
                                                                    <p><span id="showMenuFormError1"></span>
                                                                    </p>
                                                                   <div class="popup-description pop-up-qauntity lead-time">
                                                                        <div class="field required-field ">
                                                                            <p>What is the minimum quantity that guests are required to order for this particular item.</p>
                                                                            <p>
                                                                                <input type="text" id="exactqauntity" class="popup-dish-min-quantity">
                                                                            </p>
                                                                        </div> 
                                                                </div>    
                                                             </div>
                                                            <div class="popup-change-menu-dropdown">
                                                                <div class="popup-lead-time">
                                                                <h3>LEAD TIME</h3>
                                                                    <p><span id="showMenuFormError1"></span>
                                                                    </p>
                                                                   <div class="field required-field ">
                                                                            <p>How many time do you need to prepare this item?</p>
                                                                                <select id="menu_popup_lead_time" class="popup-dish-lead-time required-field" required="">
                                                                                ';
                                                                                
                                                                                $hourText = "Hour";
                                                                                for ($i=0; $i <= 72; $i+=6) { 
                                                                                    if($i>24) $i+=6;
                                                                                    $form .= "<option value='{$i}'> {$i} {$hourText}</option>";
                                                                                    $hourText = "Hours";
                                                                                }

                                                                                $form .= '
                                                                                </select>
                                                                        </div> 
                                                                    
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row-custom">
                                                        <div class="popup-change-menu-dropdown modifier-box">
                                                         <h3>Menu</h3>
                                                                <div class="popup-dish-menu-container popup-description">
                                                                    <select class="popup-dish-menu">
                                                                        '. $popMenuOptions .'
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
							                        </div>
							                        
							                    </div>
                                                
                                                <div class="row popup-dish-tab-content popup-modifier-tab-content" style="display: none; line-height: 14px;">
                                                    <div class="row-custom">
                                                        <div class="add-item-popup-description modifier-box">
                                                            <div class="modifiers-section">
                                                                 <label><input type="checkbox" class="having-modifiers" /> Having Modifiers</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="add-item-popup-description">
                                                        <button type="button" class="btn btn-info btn-lg add-modifier" data-toggle="modal" data-target="#modifier-modal" style="display: none;">Add New Item</button>
                                                    </div>
                                                    <div class="dish-modifiers-container"></div>
                                                </div>

							                </div>
                                            
							            </div>
							        </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default dish-popup-button" data-dismiss="modal">Cancel</button>
                                        <button type="button" onclick="addDish()" class="btn btn-default dish-add-btn dish-popup-button" data-dismiss="modal">Add Item</button>
                                        <button type="button" onclick="updateDish(this)" class="btn btn-default dish-save-btn" data-menu-id="" data-dish-id="" data-dismiss="modal" style="display: none;">Save Item</button>
                                    </div>
							    </div>
							</div>
						</div>

                        <div class="modal" id="modifier-modal" data-backdrop="static">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                     <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                      <h4 class="modal-title">Add New Item</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p><span id="showMenuFormError1"></span></p>
                                        <div class="modifier-type-container">
                                            <label>Quantity</label>
                                            <select class="modifier-type">
                                                <option value="1">Single Selection, Required</option>
                                                <option value="2">Single Selection, Optional</option>
                                                <option value="3">Multiple Selections, Optional</option>
                                                <option value="4">Custom</option>
                                            </select>
                                        </div>
                                        <div class="modifier-prompt-container">
                                            <label>Modifier Prompt</label>
                                            <input type="text" class="modifier-prompt" />
                                        </div>
                                        <div class="custom-modifier-container" style="display: none;">
                                            <label>Select Minimum And Maximum</label>
                                            <div class="row custom-modifier-items">
                                                <div class="col-md-6"><input type="number" class="modifier-min-items" placeholder="Enter Minimum Number" value="0" step="1" min="0" /></div>
                                                <div class="col-md-6"><input type="number" class="modifier-max-items" placeholder="Enter Maximum Number" value="" /></div>
                                            </div>
                                        </div>
                                        <span class="sk-msg error-msg"></span>
                                        <div class="row modifier-items-container">
                                            <div class="modifier-item-data">
                                                <input type="hidden" class="modifier-item-id" value="" />
                                                <div class="col-md-5"><input type="text" class="modifier-item" placeholder="Name: eg. Pepperoni" /></div>
                                                <div class="col-md-2"><input type="text" class="modifier-price" placeholder="Price: eg. 20" value="0" /></div>
                                                <div class="col-md-2"><input type="text" class="modifier-tax" placeholder="Tax(%): eg. 7" value="7" /></div>
                                                <div class="col-md-2"><input type="text" class="modifier-total" placeholder="Total: eg. 25" value="0" /></div>
                                                <div class="col-md-1">
                                                    <!-- <a href="#delete-modifier-item" class="delete-modifier-item"><i class="fa fa-trash-o"></i></a> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="modal-footer">
                                        <button type="button" class="btn dish-popup-button" data-dismiss="modal">Cancel</button>
                                        <button type="button" onclick="addModifier()" class="btn modifier-add-btn dish-popup-button">Add</button>
                                        <button type="button" onclick="updateModifier(this)" class="btn modifier-save-btn dish-popup-button" data-modifier-id="" style="display: none;">Save Item</button>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
			  </div>
	      	';

      	} else {
      		$form = '<h3>Please create some menus from admin area</h3>';
      	}
    }
		return $form;
    }

	public static function wp_dashboard_form_shortcode() {
		wp_enqueue_style('front-css', WYZ_PLUGIN_ROOT_URL .'assets/css/menu.css');
		wp_enqueue_style('front-css', WYZ_PLUGIN_ROOT_URL .'assets/css/custom.css');
        wp_enqueue_script('front-script', WYZ_PLUGIN_ROOT_URL .'assets/js/menu.js');
        
        $current_user = wp_get_current_user();
        global $wpdb;
        if(isset($_GET['user_id']) && !empty($_GET['user_id'])){
        $usr_id = $_GET['user_id'];
           if($usr_id != 0) {
                if ( in_array( 'administrator', (array) $current_user->roles ) ) {
                        $restaurantId = $wpdb->prepare("SELECT ID FROM {$wpdb->posts} WHERE post_author='%d' AND post_type='wp_restaurant' AND post_title !='Auto Draft' AND post_author!='0' ORDER BY ID DESC LIMIT 1", array($usr_id));
                    $rID = $wpdb->get_col($restaurantId);
                    $restaurantId = $rID[0];
                }
           }         
        } else {
            global $wpdb;
            $usrID = wp_get_current_user()->ID;
            $restaurantId = $wpdb->prepare("SELECT ID FROM {$wpdb->posts} WHERE post_author='%d' AND post_type='wp_restaurant' AND post_title !='Auto Draft' AND post_author!='0' ORDER BY ID DESC LIMIT 1", array($usrID));
            $rID = $wpdb->get_col($restaurantId);
            $restaurantId = $rID[0];
        }

		if (isset($rID[0]) & !empty($rID[0])) { 

		$menus = get_terms([
		  	'taxonomy' => 'restaurant_menu', 
		  	'hide_empty' => false,
        ]);
        
        $formData = '';
		if(count($menus)) {
			$menusCol = $wpdb->get_results ('
			    SELECT distinct(menu_id) 
			    FROM  '. $wpdb->prefix .'wyz_restaurant_dishes
			        WHERE restaurant_id = '. $restaurantId .' ORDER BY menu_id
			');

			$existingMenus = array();
			$menuWrapper = '<div class="menu-list-container"><ul class="menus-wrapper owl-carousel owl-theme" id="owl-demo">';
			$dishTabsContent = '<div id="dishTabsContent" class="tab-content col-md-12">';
            foreach ($menusCol as $key => $menuCol) {
				$activeClass = $key ? '' : 'active';
				$displayBlockStyle = $key ? '' : 'display: block;';
				$menuId = $menuCol->menu_id;
				$existingMenus[] = $menuCol->menu_id;
				$menu = get_term($menuId);
				$imageId = get_term_meta( $menuId, 'wyz-menu-taxonomy-image-id', true );

				if( $imageId ) {
              		$menuImage = wp_get_attachment_thumb_url( $imageId, 'thumbnail' );
            	} else {
            		$menuImage = WYZ_PLUGIN_ROOT_URL . 'assets/images/restaurant.svg';
            	}

            	$menuWrapper .= '
					<li class="menu-wrapper '. $activeClass .'" id="menu-wrapper-'. $menuId .'" data-menu-id="'. $menuId .'">
						<div class="menu-header">
							<img class="menu-icon" src="'. $menuImage .'"><br>
							<span class="menu-title">'. $menu->name .'</span>
						</div>
					</li>
				';

                $dishes = $wpdb->get_results ("
				    SELECT * 
				    FROM  {$wpdb->prefix}wyz_restaurant_dishes
			        WHERE restaurant_id = {$restaurantId} AND menu_id = {$menuId} 
				");

				if (count($dishes)) {
					$dishTabsContent .= '<div class="menu-dishes '. $activeClass .'" id="menu-dishes-'. $menuId .'" data-menu-id="'. $menuId .'">';
					
					foreach ($dishes as $key => $dish) {
                        $dishId = $dish->id;
                        $dish->image = empty($dish->image) ? '' : $dish->image;

						$dishTabsContent .= '
						    <div style="'. $displayBlockStyle .'" id="menu-dish-'. $menuId .''. $dishId .'" data-menuId="'. $menuId .'" data-dishId="'. $dishId .'"  class="menu-dish">
						        <div class="hidden-fields hidden-dish-fileds'. $menuId . $dishId .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][menuId]" value="'. $menuId .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][dishId]" value="'. $dishId .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][image]" value="'. $dish->image .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][name]" value="'. $dish->name .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][price]" value="'. $dish->price .'">
                                    <input type="hidden" name="menu['. $menuId .']['. $dishId .'][tax]" value="'. $dish->tax .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][description]" value="'. $dish->description .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][tags]" value="'. $dish->tags .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][min_quantity]" value="'. $dish->min_quantity .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][min_guest_serve]" value="'. $dish->min_guest_serve .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][max_guest_serve]" value="'. $dish->max_guest_serve .'">
						            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][lead_time]" value="'. $dish->lead_time .'">
                                    <input type="hidden" name="menu['. $menuId .']['. $dishId .'][having_modifiers]" value="'. $dish->having_modifiers .'">
                                </div>
                            ';

                            $modifiers = $wpdb->get_results ("
                                SELECT * 
                                FROM  {$wpdb->prefix}wyz_restaurant_dish_modifiers
                                WHERE dish_id = {$dish->id} 
                            ");
                            
                            $dishTabsContent .= "<div class='hidden-fields hidden-modifier-fields{$menuId}{$dishId}'>";
                            if (count($modifiers)) {
                                foreach ($modifiers as $key => $modifier) {
                                    // echo '<pre>Debug modifier: '; print_r( $modifier ); echo '</pre>';
                                    $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifierIds][]' value='{$modifier->id}'>";
                                    $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][id]' value='{$modifier->id}'>";
                                    $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][modifierType]' value='{$modifier->modifier_type}'>";
                                    $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][promptText]' value='{$modifier->prompt_text}'>";
                                    $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][min]' value='{$modifier->min}'>";
                                    $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][max]' value='{$modifier->max}'>";

                                    $modifierItems = $wpdb->get_results ("
                                        SELECT * 
                                        FROM  {$wpdb->prefix}wyz_restaurant_dish_modifier_items
                                        WHERE modifier_id = {$modifier->id} 
                                    ");

                                    foreach ($modifierItems as $key => $modifierItem) {
                                        $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][itemId][]' value='{$modifierItem->id}'>";
                                        $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][itemText][]' value='{$modifierItem->text}'>";
                                        $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][itemPrice][]' value='{$modifierItem->price}'>";
                                        $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][itemTax][]' value='{$modifierItem->tax}'>";
                                        $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier->id}][itemTotal][]' value='{$modifierItem->total}'>";
                                    }
                                }
                            }
                            $dishTabsContent .= "</div>";

                            $dishTabsContent .= "<div class='hidden-fields hidden-delete-modifier-fields{$menuId}{$dishId}'></div>";

                            $dishTabsContent .= '
						        <div class="headerBox dashboard-dish-box">
						            <div class="row">
						                <div class="col-md-10 col-sm-10">
						                    <h5 class="titleHeader" id="dishTitle'. $menuId .''. $dishId .'"> 
						                    	<span class=""> '. $dish->name .'</span>
					                    	</h5>
						                    <p class="card-text" id="dishDescription'. $menuId .''. $dishId .'"> 
						                    	<span class="">'. $dish->description .'</span> 
					                    	</p>
						                </div>
						                <div class="col-md-2 col-sm-2">
						                    <div class="dishPrice" id="dishPrice'. $menuId .''. $dishId .'"> 
						                    	<span id="s'. $menuId .''. $dishId .'">$ '. $dish->price .'</span> 
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						';
					}

					$dishTabsContent .= '</div>';
				}
			}

			$menuWrapper .= '</ul>';
            $dishTabsContent .= '</div>';
            
            $formData .= '
                <div class="menus-container">
                    '. $menuWrapper .'
                </div>
                '. $dishTabsContent .'
                <div class="modal fade" id="menu-preview-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="preview-modal-dish-name"></h2>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="preview-modal-dish-image-container">
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="dishpreviewpopup">
                                            <div class="dishpreviewpopupdata">
                                                <span class="dish-label">Price: $ </span>
                                                <span class="preview-modal-dish-price"></span>
                                            </div>
                                            <div class="dishpreviewpopupdata">
                                                <span class="dish-label">Tax: </span>
                                                <span class="preview-modal-dish-tax"></span>
                                                <span>%</span>
                                            </div>
                                            <div class="dishpreviewpopupdata">
                                                <span class="dish-label">Tags: </span>
                                                <span class="preview-modal-dish-tags"></span>
                                            </div> 
                                            <div class="dishpreviewpopupdata">
                                                <span class="dish-label">Min Quantity: </span>
                                                <span class="preview-modal-dish-min-qauntity"></span>
                                            </div> 
                                            <div class="dishpreviewpopupdata">
                                                <span class="dish-label">Min Serving: </span>
                                                <span class="preview-modal-dish-min-serve"></span>
                                            </div>  
                                            <div class="dishpreviewpopupdata">
                                                <span class="dish-label">Max Serving: </span>
                                                <span class="preview-modal-dish-max-serve"></span>
                                            </div> 
                                            <div class="dishpreviewpopupdata">
                                                <span class="dish-label">Lead Time: </span>
                                                <span class="preview-modal-dish-lead-time"></span>
                                            </div>                                                 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="preview-modal-dish-description"></p>
                                        
                                        <div class="preview-modal-modifiers-data">
                                            <div class="modifier-data-heading">
                                            <span class="modifier-type">Modifier<br>Type</span>
                                            <span class="modifier-promt-text">Modifier<br>Text</span>
                                            <div class="modifier-item-heading">
                                                <span class="modifier-item-text">Modifier<br>Item</span>
                                                <span class="modifier-item-price">Modifier<br>Price</span>
                                                <span class="modifier-item-tax">Modifier<br>Tax</span>
                                                <span class="modifier-item-total">Modifier<br>Total</span>
                                            </div>
                                        </div>    
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                <script type="text/javascript">
                    jQuery(document).ready(function($){
                        $("#dishTabsContent .menu-dish").on("click", function() {
                            let menuId = $(this).attr("data-menuId");
                            let dishId = $(this).attr("data-dishId");
                            let dishPreviewImage = "";
                            
                            let dishImage = $("input[name=\'menu["+ menuId +"]["+ dishId +"][image]\']").val();
                            // $(".preview-modal-dish-image").html("<img src=\'"+ dishImage +"\' >");
                            let dishName = $("input[name=\'menu["+ menuId +"]["+ dishId +"][name]\']").val();
                            $(".preview-modal-dish-name").text(dishName);
                            let dishPrice = $("input[name=\'menu["+ menuId +"]["+ dishId +"][price]\']").val();
                            $(".preview-modal-dish-price").text(dishPrice);
                            let dishDiscription = $("input[name=\'menu["+ menuId +"]["+ dishId +"][description]\']").val();
                            $(".preview-modal-dish-description").text(dishDiscription);
                            let dishTax = $("input[name=\'menu["+ menuId +"]["+ dishId +"][tax]\']").val();
                            $(".preview-modal-dish-tax").text(dishTax);
                            let dishTags = $("input[name=\'menu["+ menuId +"]["+ dishId +"][tags]\']").val();
                            $(".preview-modal-dish-tags").text(dishTags);
                            let dishQauntity = $("input[name=\'menu["+ menuId +"]["+ dishId +"][min_qauntity]\']").val();
                            $(".preview-modal-dish-min-quantity").text(dishQauntity);
                            let dishMinServe = $("input[name=\'menu["+ menuId +"]["+ dishId +"][min_guest_serve]\']").val();
                            $(".preview-modal-dish-min-serve").text(dishMinServe);
                            let dishMaxServe = $("input[name=\'menu["+ menuId +"]["+ dishId +"][max_guest_serve]\']").val();
                            $(".preview-modal-dish-max-serve").text(dishMaxServe);
                            let dishLeadTime = $("input[name=\'menu["+ menuId +"]["+ dishId +"][lead_time]\']").val();
                            $(".preview-modal-dish-lead-time").text(dishLeadTime);

                            dishPreviewImage += "<div class=\"dishpreviewpopupdata\">";
                            dishPreviewImage += "<p class=\"preview-modal-dish-image\">";
                            dishPreviewImage += "<img src=\' " + dishImage + "\' >";
                            dishPreviewImage += "</p>";
                            dishPreviewImage += "</div>";

                            let modifierIds = $("input[name=\'menu["+ menuId +"]["+ dishId +"][modifierIds][]\']");
                            let modifiersData = "";
                            let modifierTypeText = [
                                "",
                                "Single Selection, Required",
                                "Single Selection, Optional",
                                "Multiple Selections, Optional",
                                "Custom Modifier Item",
                            ];
                            
                            for(let i = 0; i < modifierIds.length; i++) {
                                let modifierId = modifierIds[i].value;
                                let dishModifierType = $("input[name=\'menu["+ menuId +"]["+ dishId +"][modifier]["+ modifierId +"][modifierType]\']").val();
                                let dishModifierPromptText = $("input[name=\'menu["+ menuId +"]["+ dishId +"][modifier]["+ modifierId +"][promptText]\']").val();
                                let dishModifierMin = $("input[name=\'menu["+ menuId +"]["+ dishId +"][modifier]["+ modifierId +"][min]\']").val();
                                let dishModifierMax = $("input[name=\'menu["+ menuId +"]["+ dishId +"][modifier]["+ modifierId +"][max]\']").val();

                                modifiersData += "<div class=\"dish-modifier\">";
                                modifiersData += "<div class=\"dish-modifier-heading\">";    
                                modifiersData += "<p class=\"preview-modal-modifier-type\">"+ modifierTypeText[dishModifierType] +"</p>";
                                modifiersData += "<p class=\"preview-modal-modifier-prompt-text\">"+ dishModifierPromptText +"</p>";
                                modifiersData += "</div>";
                                
                                if(4 == dishModifierType) {
                                    modifiersData += "<div class=\"dish-custom-modifier\">";
                                    modifiersData += "<div class=\"dish-custom-modifier-min\">";
                                    modifiersData += "<span>Min: </span>";
                                    modifiersData += "<span class=\"preview-modal-modifier-min\">"+ dishModifierMin +"</span>";
                                    modifiersData += "</div>";
                                    modifiersData += "<div class=\"dish-custom-modifier-max\">";
                                    modifiersData += "<span>Max: </span>";
                                    modifiersData += "<span class=\"preview-modal-modifier-max\">"+ dishModifierMax +"</span>";
                                    modifiersData += "</div>";
                                    modifiersData += "</div>";
                                }

                                    modifiersData += "<div class=\"modifier-item-heading\">";
                                    modifiersData += "<span class=\"modifier-item-text-heading\">Item-text";
                                    modifiersData += "</span>";
                                    modifiersData += "<span class=\"modifier-item-info-heading\">Item-price";
                                    modifiersData += "</span>";
                                    modifiersData += "<span class=\"modifier-item-info-heading\">Item-tax";
                                    modifiersData += "</span>";
                                    modifiersData += "<span class=\"modifier-item-info-heading\">Item-total";
                                    modifiersData += "</span>";
                                    modifiersData += "</div>";

                                let itemIds = $("input[name=\'menu["+ menuId +"]["+ dishId +"][modifier]["+ modifierId +"][itemId][]\']");
                                for(let i = 0; i < itemIds.length; i++) {                                   
                                    let dishModifierItemId = itemIds[i].value;
                                    // console.log("dishModifierItemId: ", dishModifierItemId);

                                    let dishModifierItemText = $("input[name=\'menu["+ menuId +"]["+ dishId +"][modifier]["+ modifierId +"][itemText][]\']")[i].value;
                                    // console.log("dishModifierItemText: ", dishModifierItemText);
                                    let dishModifierItemPrice = $("input[name=\'menu["+ menuId +"]["+ dishId +"][modifier]["+ modifierId +"][itemPrice][]\']")[i].value;
                                    // console.log("dishModifierItemPrice: ", dishModifierItemPrice);
                                    let dishModifierItemTax = $("input[name=\'menu["+ menuId +"]["+ dishId +"][modifier]["+ modifierId +"][itemTax][]\']")[i].value;
                                    // console.log("dishModifierItemTax: ", dishModifierItemTax);
                                    let dishModifierItemTotal = $("input[name=\'menu["+ menuId +"]["+ dishId +"][modifier]["+ modifierId +"][itemTotal][]\']")[i].value;
                                    // console.log("dishModifierItemTotal: ", dishModifierItemTotal);

                                    
                                    modifiersData += "<div class=\"modifier-item\">";
                                    modifiersData += "<p class=\"preview-modal-modifier-item-text\">"+ dishModifierItemText +"</p>";
                                    modifiersData += "<p class=\"preview-modal-modifier-item-price\">"+ dishModifierItemPrice +"</p>";
                                    modifiersData += "<p class=\"preview-modal-modifier-item-tax\">"+ dishModifierItemTax +"</p>";
                                    modifiersData += "<p class=\"preview-modal-modifier-item-total\">"+ dishModifierItemTotal +"</p>";
                                    modifiersData += "</div>";
                                };

                                modifiersData += "</div>";
                            };


                            $(".preview-modal-modifiers-data").html(modifiersData);                            
                            $(".preview-modal-dish-image-container").html(dishPreviewImage); 
                            $("#menu-preview-modal").modal();
                        });
                    });
                </script>
            ';

    		$formData .= '</div>';
      	} else {
            $formData .= '<h3>Please create some menus from admin area</h3>';
      	}
    }
		return $formData;
	}

	public static function wp_menu_optimize() {
		wp_enqueue_style('front-css', WYZ_PLUGIN_ROOT_URL .'assets/css/menu.css');
		wp_enqueue_style('front-css', WYZ_PLUGIN_ROOT_URL .'assets/css/custom.css');
        wp_enqueue_script('front-script', WYZ_PLUGIN_ROOT_URL .'assets/js/menu.js');
        
        $current_user = wp_get_current_user();
        global $wpdb;
        if(isset($_GET['user_id']) && !empty($_GET['user_id'])){
        $usr_id = $_GET['user_id'];
           if($usr_id != 0) {
                if ( 
                	in_array( 'administrator', (array) $current_user->roles ) || 
                	in_array( 'editor', (array) $current_user->roles ) 
                ) {
                        $restaurantQuery = "SELECT ID FROM {$wpdb->posts} WHERE post_author='{$usr_id}' AND post_type='wp_restaurant' AND post_title !='Auto Draft' AND post_author!='0' ORDER BY ID DESC LIMIT 1";
                    $rID[0] = $wpdb->get_var($restaurantQuery);
                    $restaurantId = $rID[0];
                }
           }         
        } else {
            global $wpdb;
            $usrID = wp_get_current_user()->ID;
            $restaurantQuery = "SELECT ID FROM {$wpdb->posts} WHERE post_author='{$usrID}' AND post_type='wp_restaurant' AND post_title !='Auto Draft' AND post_author!='0' ORDER BY ID DESC LIMIT 1";
            $rID[0] = $wpdb->get_var($restaurantQuery);
            $restaurantId = $rID[0];
        }

		$formData = '';
		if (isset($rID[0]) & !empty($rID[0])) {
			$menus = get_terms([
			  	'taxonomy' => 'restaurant_menu', 
			  	'hide_empty' => false,
            	'childless' => true,
	        ]);
	        
	        if(count($menus)) {
	        	$menuDishesArray['menus'] = array();
				$menusCol = $wpdb->get_results ("
				    SELECT distinct(menu_id) 
				    FROM  {$wpdb->prefix}wyz_restaurant_dishes
				    WHERE restaurant_id = {$restaurantId} ORDER BY menu_id
				", ARRAY_A);

				$existingMenus = array();
				$menuWrapper = '<div class="menu-list-container"><ul class="menus-wrapper owl-carousel owl-theme" id="owl-demo">';
				$dishTabsContent = '<div id="dishTabsContent" class="tab-content col-md-12">';
	            foreach ($menusCol as $key => $menuCol) {
					$activeClass = $key ? '' : 'active';
					$displayBlockStyle = $key ? '' : 'display: block;';
					$menuId = $menuCol['menu_id'];
					$existingMenus[] = $menuCol['menu_id'];
					$menu = get_term($menuId);
					$imageId = get_term_meta( $menuId, 'wyz-menu-taxonomy-image-id', true );

					if( $imageId ) {
	              		$menuImage = wp_get_attachment_thumb_url( $imageId, 'thumbnail' );
	            	} else {
	            		$menuImage = WYZ_PLUGIN_ROOT_URL . 'assets/images/restaurant.svg';
	            	}

	            	$menuWrapper .= '
						<li class="menu-wrapper '. $activeClass .'" id="menu-wrapper-'. $menuId .'" data-menu-id="'. $menuId .'">
							<span class="menu-delete fa fa-trash-o"></span>
							<div class="menu-header">
								<img class="menu-icon" src="'. $menuImage .'"><br>
								<span class="menu-title">'. $menu->name .'</span>
							</div>
						</li>
					';

	                $dishes = $wpdb->get_results ("
					    SELECT * 
					    FROM  {$wpdb->prefix}wyz_restaurant_dishes
				        WHERE restaurant_id = {$restaurantId} AND menu_id = {$menuId} 
					", ARRAY_A);

					if (count($dishes)) {
						$menuDishesArray['menus'][$menuId]['dishes'] = array();
						$dishTabsContent .= '<div class="menu-dishes '. $activeClass .'" id="menu-dishes-'. $menuId .'" data-menu-id="'. $menuId .'">';
						
						foreach ($dishes as $key => $dish) {
							$menuDishesArray['menus'][$menuId]['dishes'][$dish['id']] = $dish;
	                        $dishId = $dish['id'];
	                        $dish['image'] = empty($dish['image']) ? '' : $dish['image'];

							/*$dishTabsContent .= '
							    <div style="'. $displayBlockStyle .'" id="menu-dish-'. $menuId .''. $dishId .'" data-menuId="'. $menuId .'" data-dishId="'. $dishId .'"  class="menu-dish">
							        <div class="hidden-fields hidden-dish-fileds'. $menuId . $dishId .'">
							            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][menuId]" value="'. $menuId .'">
							            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][dishId]" value="'. $dishId .'">
							            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][image]" value="'. $dish['image'] .'">
							            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][name]" value="'. $dish['name'] .'">
							            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][price]" value="'. $dish['price'] .'">
	                                    <input type="hidden" name="menu['. $menuId .']['. $dishId .'][tax]" value="'. $dish['tax'] .'">
							            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][description]" value="'. $dish['description'] .'">
							            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][tags]" value="'. $dish['tags'] .'">
							            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][min_quantity]" value="'. $dish['min_quantity'] .'">
							            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][min_guest_serve]" value="'. $dish['min_guest_serve'] .'">
							            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][max_guest_serve]" value="'. $dish['max_guest_serve'] .'">
							            <input type="hidden" name="menu['. $menuId .']['. $dishId .'][lead_time]" value="'. $dish['lead_time'] .'">
	                                    <input type="hidden" name="menu['. $menuId .']['. $dishId .'][having_modifiers]" value="'. $dish['having_modifiers'] .'">
	                                </div>
	                            ';*/

	                        $dishTabsContent .= '
							    <div style="'. $displayBlockStyle .'" id="menu-dish-'. $menuId .''. $dishId .'" data-menuId="'. $menuId .'" data-dishId="'. $dishId .'"  class="menu-dish">
	                            ';

	                            $modifiers = $wpdb->get_results ("
	                                SELECT * 
	                                FROM  {$wpdb->prefix}wyz_restaurant_dish_modifiers
	                                WHERE dish_id = {$dish[id]} 
	                            ", ARRAY_A);
	                            
	                            // $dishTabsContent .= "<div class='hidden-fields hidden-modifier-fields{$menuId}{$dishId}'>";
	                            if (count($modifiers)) {
	                            	// $menuDishesArray['menus'][$menuId]['dishes'][$dish['id']]['modifiers'] = array();
	                            	// echo '<pre>Debug menuDishesArray: '; print_r( $menuDishesArray ); echo '</pre>'; die;
	                                foreach ($modifiers as $key => $modifier) {
	                            		$menuDishesArray['menus'][$menuId]['dishes'][$dish['id']]['modifiers'][$modifier['id']] = $modifier;
	                                    // $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifierIds][]' value='{$modifier[id]}'>";
	                                    // $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier[id]}][id]' value='{$modifier[id]}'>";
	                                    // $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier[id]}][modifierType]' value='{$modifier->modifier_type}'>";
	                                    // $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier[id]}][promptText]' value='{$modifier->prompt_text}'>";
	                                    // $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier[id]}][min]' value='{$modifier->min}'>";
	                                    // $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier[id]}][max]' value='{$modifier->max}'>";

	                                    $modifierItems = $wpdb->get_results ("
	                                        SELECT * 
	                                        FROM  {$wpdb->prefix}wyz_restaurant_dish_modifier_items
	                                        WHERE modifier_id = {$modifier[id]} 
	                                    ", ARRAY_A);

	                                    if (count($modifierItems)) {
	                                    	// $menuDishesArray['menus'][$menuId]['dishes'][$dish['id']]['modifiers'][$modifier[id]]['items'] = array();
		                                    foreach ($modifierItems as $key => $modifierItem) {
		                                    	$menuDishesArray['menus'][$menuId]['dishes'][$dish['id']]['modifiers'][$modifier[id]]['items'][$modifierItem['id']] = $modifierItem;
		                                        // $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier[id]}][itemId][]' value='{$modifierItem[id]}'>";
		                                        // $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier[id]}][itemText][]' value='{$modifierItem[text]}'>";
		                                        // $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier[id]}][itemPrice][]' value='{$modifierItem[price]}'>";
		                                        // $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier[id]}][itemTax][]' value='{$modifierItem[tax]}'>";
		                                        // $dishTabsContent .= "<input type='hidden' name='menu[{$menuId}][{$dishId}][modifier][{$modifier[id]}][itemTotal][]' value='{$modifierItem[total]}'>";
		                                    }
	                                    }
	                                }
	                            }
	                            // $dishTabsContent .= "</div>";

	                            // $dishTabsContent .= "<div class='hidden-fields hidden-delete-modifier-fields{$menuId}{$dishId}'></div>";

	                            $dishTabsContent .= '
							        <div class="headerBox dashboard-dish-box">
							            <div class="row">
							                <div class="col-md-9 col-sm-8">
							                    <h5 class="titleHeader" id="dishTitle'. $menuId .''. $dishId .'"> 
							                    	<span class=""> '. $dish['name'] .'</span>
						                    	</h5>
							                    <p class="card-text" id="dishDescription'. $menuId .''. $dishId .'"> 
							                    	<span class="">'. $dish['description'] .'</span> 
						                    	</p>
							                </div>
							                <div class="col-md-2 col-sm-2">
							                    <div class="dishPrice" id="dishPrice'. $menuId .''. $dishId .'"> 
							                    	<span id="s'. $menuId .''. $dishId .'">$ '. $dish['price'] .'</span> 
							                    </div>
							                </div>
							                <div class="col-md-1 col-sm-2">
							                    <div class="dish-edit-wrapper">
								                    <a href="#" class="dish-edit" onclick="editDish('. $menuId .', '. $dishId .'); return false;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
	                                                <a href="#" class="dish-copy" data-menu-id="'. $menuId .'" data-dish-id="'. $dishId .'"><i class="fa fa-copy"></i></a>
								                    <a href="#" class="dish-delete" data-menu-id="'. $menuId .'" data-dish-id="'. $dishId .'"><i class="fa fa-trash-o"></i></a>
							                    </div>
							                </div>
							            </div>
							        </div>
							    </div>
							';
						}

						$dishTabsContent .= '</div>';
					}
				}

				$menuWrapper .= '</ul>';
	            $dishTabsContent .= '</div>';
	            
	            $formData .= '
	                <div class="menus-container">
	                    '. $menuWrapper .'

	                    <div class="add-menu-wrapper">
    						<select class="menu-list">
    					';

    					$menuOptions = $popMenuOptions = '';
                        foreach ($menus as $menu) {
    						$disabled = '';
    						$imageId = get_term_meta( $menu->term_id, 'wyz-menu-taxonomy-image-id', true );

    						if( $imageId ) {
    		              		$menuImage = wp_get_attachment_thumb_url( $imageId, 'thumbnail' );
    		            	} else {
    		            		$menuImage = WYZ_PLUGIN_ROOT_URL . 'assets/images/restaurant.svg';
    		            	}

    		            	if (in_array($menu->term_id, $existingMenus)) {
    		            		$disabled = 'disabled';
    		            	}
    						$menuOptions .= '<option data-img="'. $menuImage .'"  data-menu-id="'. $menu->term_id .'" value="'. $menu->term_id .'" '. $disabled .'>'. $menu->name .'</option>';
                            $popMenuOptions .= '<option data-img="'. $menuImage .'"  data-menu-id="'. $menu->term_id .'" value="'. $menu->term_id .'" '. (empty($disabled) ? 'disabled' : '') .'>'. $menu->name .'</option>';
    					}
    						
    					$formData .= $menuOptions;

                        $formData .= '
    						</select><br />
    						<button type="button" class="add-menu modify-button" >Add Menu</button>
    					</div>
	                </div>
	                
	                '. $dishTabsContent .'

					<div style="clear: both;"><hr />
					<button type="button" id="openmenumodel" class="btn-model btn btn-info btn-lg add-dish" onclick="showMenuModal(); return false;">Add New Item</button>

	                <div class="" id="modal-container">
						<div class="modal fade" id="menu-modal" role="dialog">
					    	<div class="modal-dialog">
					        
					          	<!-- Modal content-->
					          	<div class="modal-content">
					    		    <div class="modal-body" id="dishbody">
					    		        <div id="dishTabsContentformodal" class="col-md-12">
					    		            <div class="" id="categorylink5">
					    		                <div>
					                                <h3 class="popup-dish-tab active" data-tab-target="popup-general">Item Info</h3>
					                                <h3 class="popup-dish-tab" data-tab-target="popup-modifier">Modifier Info</h3>
					    		                    <div class="row popup-dish-tab-content popup-general-tab-content" style="line-height: 14px;">
					    		                        <div id="row1">
					    		                            <div class="add-item-popup-description">
					                                            <div class="upper-row-content">
					                                                    <p><span id="showMenuFormError1"></span></p>
					                                                    <div class="pop-image">
					                                                        <p>
					                                                            <input class="popup-dish-img" type="file">
					                                                            <small class="p_size">(Prefered size: <strong>225w X 125h</strong> )</small>
					                                                        </p>
					                                                        <div class="popup-dish-preview-container">
					                                                            <div class="iamgePadding popup-dish-preview" id="image-holder1"></div>
					                                                        </div>
					                                                    </div>
					                                                    <div class="popup-description ">
					                                                        <p class="popup-dish-name-new">
					                                                            <label>Item Name</label>
					                                                            <input class="popup-dish-name" type="text" placeholder="Dish title">
					                                                        </p>
					                                                        <div class="popup-dish-price-new">
					                                                            <div class="popup-price-fields">
					                                                                <label>Base Price</label>
					                                                                <input class="popup-dish-price" type="text" placeholder="Base Price">
					                                                            </div>
					                                                            <div class="popup-price-fields">
					                                                                <label>Include Tax</label>
					                                                                <input class="popup-dish-price-tax" type="text" placeholder="Tax" value="7">
					                                                            </div>
					                                                            <div class="popup-price-fields">
					                                                                <label>Sub Total</label>
					                                                                <input class="popup-dish-price-total" type="text" placeholder="Sub Total">
					                                                            </div>
					                                                        </div>
					                                                        <div class="desc-box">
					                                                            <label>Item Description</label>
					                                                            <textarea class="popup-dish-desc" placeholder="Dish discription..."cols="31" rows="6"></textarea>
					                                                        </div>
					                                                    </div>
					                                                </div>
					                                                <div class="popup-tags">
					                                                <h3>TAGS</h3>
					                                                    <div class="add-item-popup-description">
					                                                        <p><span id="showMenuFormError1"></span>
					                                                        </p>
					                                                        <div class="popup-description tags-sec">
					                                                            <p>Is this item vegetarian, gluten free or Halal? Add tags to describe it more accurately and help the guest make a more informed choice.</p>
					                                                            <div class="field required-field pop-up-category"> <span>Select tag(s)</span>
					                                                                <select id="menu_popup_tags" class="popup-dish-tags selectpicker" multiple="">
					                                                                ';

					                                                                $dishTags = get_terms([
					                                                                    'taxonomy' => 'restaurant_tag', 
					                                                                    'hide_empty' => false,
					                                                                ]);

					                                                                foreach ($dishTags as $dishTag) {
					                                                                    $formData .='<option value="'. $dishTag->term_id .'">'. $dishTag->name .'</option>';
					                                                                }

					                                                                $formData .= '
					                                                                </select>
					                                                            </div>
					                                                        </div>
					                                                    </div>
					                                            </div>
					                                            </div>
					                                                                                                      
					    		                            </div>
					                                        <div>
					                                        <div class="add-item-popup-description ">
					                                            <div class="popup-quantity">
					                                                <h3>QUANTITY</h3>
					                                                <p><span id="showMenuFormError1"></span>
					                                                </p>
					                                                <div class="popup-description pop-up-qauntity">
					                                                    <div class="field required-field ">
					                                                        <p>How many guests will this item serve?</p>
					                                                        <p>
					                                                            <label>
					                                                                <b>Min</b>
					                                                                <input type="number" id="minmumquantity" min="1" class="popup-dish-min-serve" />
					                                                            </label>
					                                                            <label>
					                                                                <b>Max</b>
					                                                                <input type="number" id="maxmumquantity" class="popup-dish-max-serve" />
					                                                            </label>
					                                                        </p>
					                                                    </div>
					                                                </div>
					                                            </div>
					                                            <div class="popup-lead-time">
					                                                    <p><span id="showMenuFormError1"></span>
					                                                    </p>
					                                                   <div class="popup-description pop-up-qauntity lead-time">
					                                                        <div class="field required-field ">
					                                                            <p>What is the minimum quantity that guests are required to order for this particular item.</p>
					                                                            <p>
					                                                                <input type="text" id="exactqauntity" class="popup-dish-min-quantity">
					                                                            </p>
					                                                        </div> 
					                                                </div>    
					                                             </div>
					                                            <div class="popup-change-menu-dropdown">
					                                                <div class="popup-lead-time">
					                                                    <h3>LEAD TIME</h3>
					                                                    <p><span id="showMenuFormError1"></span>
					                                                    </p>
					                                                    <div class="field required-field ">
					                                                        <p>How many time do you need to prepare this item?</p>
					                                                            <select id="menu_popup_lead_time" class="popup-dish-lead-time required-field" required="">
					                                                            ';
					                                                            
					                                                            $hourText = "Hour";
					                                                            for ($i=0; $i <= 72; $i+=6) { 
					                                                                if($i>24) $i+=6;
					                                                                $formData .= "<option value='{$i}'> {$i} {$hourText}</option>";
					                                                                $hourText = "Hours";
					                                                            }

					                                                            $formData .= '
					                                                            </select>
					                                                    </div>
					                                                </div>
					                                            </div>
					                                        </div>
					                                    </div>
					                                    <div class="row-custom">
					                                        <div class="popup-change-menu-dropdown modifier-box">
					                                         <h3>Menu</h3>
					                                                <div class="popup-dish-menu-container popup-description">
					                                                    <select class="popup-dish-menu">
					                                                        '. $popMenuOptions .'
					                                                    </select>
					                                                </div>
					                                            </div>
					                                        </div>
					    		                        </div>
					    		                        
					    		                    </div>
					                                
					                                <div class="row popup-dish-tab-content popup-modifier-tab-content" style="display: none; line-height: 14px;">
					                                    <div class="row-custom">
					                                        <div class="add-item-popup-description modifier-box">
					                                            <div class="modifiers-section">
					                                                 <label><input type="checkbox" class="having-modifiers" /> Having Modifiers</label>
					                                            </div>
					                                        </div>
					                                    </div>
					                                    <div class="add-item-popup-description">
					                                        <button type="button" class="btn btn-info btn-lg add-modifier" onclick="showModifierModal(); return false;" style="display: none;">Add New Item</button>
					                                    </div>
					                                    <div class="dish-modifiers-container"></div>
					                                </div>

					    		                </div>
					                            
					    		            </div>
					    		        </div>
					                    <div class="modal-footer">
					                        <button type="button" class="btn btn-default dish-popup-button" data-dismiss="modal">Cancel</button>
					                        <button type="button" onclick="addDish()" class="btn btn-default dish-add-btn dish-popup-button" data-dismiss="modal">Add Item</button>
					                        <button type="button" onclick="updateDish(this)" class="btn btn-default dish-save-btn" data-menu-id="" data-dish-id="" data-dismiss="modal" style="display: none;">Save Item</button>
					                    </div>
					    		    </div>
					    		</div>
					    	</div>

					        <div class="modal" id="modifier-modal" data-backdrop="static">
					            <div class="modal-dialog">
					                <div class="modal-content">
					                     <div class="modal-header">
					                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					                      <h4 class="modal-title">Add New Item</h4>
					                    </div>
					                    <div class="modal-body">
					                        <p><span id="showMenuFormError1"></span></p>
					                        <div class="modifier-type-container">
					                            <label>Quantity</label>
					                            <select class="modifier-type">
					                                <option value="1">Single Selection, Required</option>
					                                <option value="2">Single Selection, Optional</option>
					                                <option value="3">Multiple Selections, Optional</option>
					                                <option value="4">Custom</option>
					                            </select>
					                        </div>
					                        <div class="modifier-prompt-container">
					                            <label>Modifier Prompt</label>
					                            <input type="text" class="modifier-prompt" />
					                        </div>
					                        <div class="custom-modifier-container" style="display: none;">
					                            <label>Select Minimum And Maximum</label>
					                            <div class="row custom-modifier-items">
					                                <div class="col-md-6"><input type="number" class="modifier-min-items" placeholder="Enter Minimum Number" value="0" step="1" min="0" /></div>
					                                <div class="col-md-6"><input type="number" class="modifier-max-items" placeholder="Enter Maximum Number" value="" /></div>
					                            </div>
					                        </div>
					                        <span class="sk-msg error-msg"></span>
					                        <div class="row modifier-items-container">
					                            <div class="modifier-item-data">
					                                <input type="hidden" class="modifier-item-id" value="" />
					                                <div class="col-md-5"><input type="text" class="modifier-item" placeholder="Name: eg. Pepperoni" /></div>
					                                <div class="col-md-2"><input type="text" class="modifier-price" placeholder="Price: eg. 20" value="0" /></div>
					                                <div class="col-md-2"><input type="text" class="modifier-tax" placeholder="Tax(%): eg. 7" value="7" /></div>
					                                <div class="col-md-2"><input type="text" class="modifier-total" placeholder="Total: eg. 25" value="0" /></div>
					                                <div class="col-md-1">
					                                    <!-- <a href="#delete-modifier-item" class="delete-modifier-item"><i class="fa fa-trash-o"></i></a> -->
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					                   
					                    <div class="modal-footer">
					                        <button type="button" class="btn dish-popup-button" data-dismiss="modal">Cancel</button>
					                        <button type="button" onclick="addModifier()" class="btn modifier-add-btn dish-popup-button">Add</button>
					                        <button type="button" onclick="updateModifier(this)" class="btn modifier-save-btn dish-popup-button" data-modifier-id="" style="display: none;">Save Item</button>
					                    </div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
	            ';

	    		$formData .= '</div>';
	    		// echo '<pre>Debug menuDishesArray: '; print_r( $menuDishesArray ); echo '</pre>'; die;
	    		$menuDishesJSON = json_encode($menuDishesArray, JSON_HEX_APOS | JSON_HEX_QUOT);

	    		// <input type="hidden" name="menu_dishes" value=\''. $menuDishesJSON .'\' />
	    		$formData .= '
	    			<input type="hidden" name="menu_dishes" value=\'\' />
					<script type="text/javascript"> var menuDishes = '. $menuDishesJSON .'; </script>
				';
	      	} else {
	            $formData .= '<h3>Please create some menus from admin area</h3>';
	      	}
	    }

		return $formData;
	}

	function cw_post_type_Dishes() {
		$supports = array(
			'title',
			'editor',
			'thumbnail',
		);

		$labels = array(
			'name' => _x('Dishes', 'plural'),
			'singular_name' => _x('Dishes', 'singular'),
			'menu_name' => _x('Dishes', 'admin menu'),
			'name_admin_bar' => _x('Dishes', 'admin bar'),
			'add_new' => _x('Add Dishes', 'add Dishes'),
			'add_new_item' => __('Add New Dishes'),
			'new_item' => __('New Dishes'),
			'edit_item' => __('Edit Dishes'),
			'view_item' => __('View Dishes'),
			'all_items' => __('All Dishes'),
			'search_items' => __('Search Dishes'),
			'not_found' => __('No Dishes found.'),
		);
		$args = array(
			'supports' => $supports,
			'labels' => $labels,
			'public' => true,
			'query_var' => true,
			'rewrite' => array('slug' => 'Dishes'),
			'taxonomies' => array( 'dish_category' ),
			'has_archive' => true,
			'hierarchical' => false,
		);

		register_post_type('dish', $args);
	}

	function add_custom_taxonomies() {
	   	//Add new "Locations" taxonomy to Posts
	   	register_taxonomy('dish_category', 'dish', array(
	    	'hierarchical' => true,
	    	// This array of options controls the labels displayed in the WordPress Admin UI
	    	'labels' => array(
		      	'name' => _x( 'Dish Category', 'taxonomy general name' ),
		      	'singular_name' => _x( 'Dish Category', 'taxonomy singular name' ),
		      	'search_items' =>  __( 'Search Dish Category' ),
		      	'all_items' => __( 'All Dish Categories' ),
		      	'parent_item' => __( 'Parent Dish Categories' ),
		      	'parent_item_colon' => __( 'Parent Dish Categories:' ),
		      	'edit_item' => __( 'Edit Location' ),
		      	'update_item' => __( 'Update Dish Categories' ),
		      	'add_new_item' => __( 'Add New Dish Categories' ),
		      	'new_item_name' => __( 'New Location Dish Category' ),
		      	'menu_name' => __( 'Dish Categories' ),
		      	'show_admin_column' => false,
		      	'show_ui' => true,
		    ),
	    	// Control the slugs used for this taxonomy
	    	'rewrite' => array(
		      	'slug' => 'Dish_Categories', // This controls the base slug that will display before each term
		      	'with_front' => true, // Don't display the category base before "/locations/"
		      	'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
	    	),
	  	));
	}
}

// add_shortcode("Restaurant_form", array( 'wyz_chef_dish', 'wp_form_shortcode' ));
//include(plugin_dir_path(__FILE__) . 'aj-demo-dishajax-code.js');
//include(plugin_dir_path(__FILE__) . 'wp_add_dishajax.php');
$wp_simple_locations = new wyz_chef_dish;
