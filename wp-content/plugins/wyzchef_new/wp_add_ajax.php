<?php
session_start();
add_action( 'wp_enqueue_scripts', 'aj_enqueue_scripts' );
function aj_enqueue_scripts() {
    wp_enqueue_script( 'aj-demo', WYZ_PLUGIN_ROOT_URL. 'assets/js/aj-demo-ajax-code.js', array('jquery') );
    wp_localize_script( 'aj-demo', 'aj_ajax_demo', array(
                        'ajax_url' => admin_url( 'admin-ajax.php' ),
                        'aj_demo_nonce' => wp_create_nonce('aj-demo-nonce') 
    ));
}
//echo "test";die();
add_action( 'wp_ajax_nopriv_aj_ajax_demo_get_type', 'aj_ajax_demo_type' );
add_action( 'wp_ajax_aj_ajax_demo_get_type', 'aj_ajax_demo_type' ); 
function aj_ajax_demo_type() {
    //print_r($_REQUEST);
    if(isset($_REQUEST['myType'])){
        $category_name = $_REQUEST['myType'];
         $catData =wp_insert_term(
          $category_name, // the term 
          'restaurant_type', // the taxonomy
          array(
            'description'=> '',
            'slug' => '',
            'parent'=> ''  // get numeric term id
          )
        );
         //print_r($id['term_id']);
         if(is_array($catData)){
            $val = $catData['term_id'];
            $name = $_REQUEST['myType'];
            echo "<option value=".$val.">".$name."</option>";
         }
        wp_die();
    }
}
add_action( 'wp_ajax_nopriv_aj_ajax_demo_get_count1', 'aj_ajax_demo_process1' );
add_action( 'wp_ajax_aj_ajax_demo_get_count1', 'aj_ajax_demo_process1' ); 
function aj_ajax_demo_process1() {
    //print_r($_REQUEST);exit;
    if(isset($_REQUEST['myCategory'])){
        $category_name = $_REQUEST['myCategory'];
         $catData =wp_insert_term(
          $category_name, // the term 
          'restaurant_category', // the taxonomy
          array(
            'description'=> '',
            'slug' => '',
            'parent'=> ''  // get numeric term id
          )
        );
         //print_r($id['term_id']);
         if(is_array($catData)){
            $val = $catData['term_id'];
            $name = $_REQUEST['myCategory'];
            echo "<option value=".$val.">".$name."</option>";
         }
        wp_die();
    }
}
add_action( 'wp_ajax_nopriv_aj_ajax_demo_delete', 'aj_ajax_demo_delete' );
add_action( 'wp_ajax_aj_ajax_demo_delete', 'aj_ajax_demo_delete' ); 
function aj_ajax_demo_delete() {
    //print_r($_REQUEST);exit;
    if(isset($_REQUEST['DishID'])){
        $dishid= $_REQUEST['DishID'];
        global $wpdb;
        $query= "DELETE FROM `wp_wyz_restaurant_dishes` WHERE `wp_wyz_restaurant_dishes`.`id` = '$dishid'";
        $wpdb->query($query);
        wp_die();
    }
}


// search dish throw search box in restaurant detail page
add_action('wp_ajax_nopriv_wyz_get_dishes', 'wyz_get_dishes');
add_action( 'wp_ajax_wyz_get_dishes', 'wyz_get_dishes');
function wyz_get_dishes(){
  global $wpdb;
  $table= $wpdb->prefix."wyz_restaurant_dishes";
  $results = $wpdb->get_results("SELECT * FROM $table WHERE `name` LIKE '%".$_POST['dish']."%'");
  if(count($results) > 0){
    foreach ($results as $result) { 
      $dishId = $result->id;
      $i = $dishId;
      ?>
      <div class="col-md-6 col-sm-6 detail-add-popup">
        <div class="row dish-single-item" data-toggle="modal" data-target="#exampleModal<?php echo $i;?>">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <h6><?php echo $result->name?></h6>
            <p class="blurr-text">
              <?php 
              echo wp_trim_words( $result->description, 45, '...' );
              ?></p>   
            </div>                                                          
            <div class="price dish-box-price"><!--<span class="dish-size-label">S</span>-->
              <span class="dish-size">S<?php echo "$". $result->price;?></span></div>
              <!--  <button class="btn btn-primary dish-box-button" data-toggle="modal" data-target="#exampleModal<?php echo $i;?>">Add</button> -->
            </div>
            <div class="modal fade" id="exampleModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="cartModalLabel<?php echo $i;?>" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title cartModalLabel" id="cartModalLabel<?php echo $i;?>"><strong><?php echo $result->name;?></strong></h5>
                    <!-- <input type="hidden" id="itemDetails<?php echo $i;?>" value="<?php echo $result->name?>" /> -->
                    <button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        <?php
                        $image = $result->image;
                        $imageData = base64_encode(file_get_contents($image));
                        $src = 'data: '.mime_content_type($image).';base64,'.$imageData;
                        ?>
                        <img src="<?php echo $src;?>">
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <p class="blurr-text1"><?php echo $result->description;?></p>
                        <label for="quantity<?php echo $i;?>" >Choose Quantity</label>
                        <input type="number" id ="quantity<?php echo $i;?>" class="quantity" min="1" value = "1" name="quantity" />
                        <input type="hidden" id="min_quantity<?php echo $i;?>"name="min_quantity" 
                                value="<?php echo $result->min_quantity;?>">
                                <?php
                                                                if($result->min_quantity > 0){ ?>
                                                                    <div class="min-quantity-msg" style="color:red;">

                                                                        Please Select Min <?php echo $result->min_quantity;?> Quantity Of this dish</div>

                                                                    <?php   }else{?>

                                                                        <div class="min-quantity-msg" style="color:red;">

                                                                        Please Select Min 1 Quantity Of this dish</div>

                                                                    <?php  }
                                                                    $min_guest = $result->min_guest_serve;
                                                                    $max_guest = $result->max_guest_serve;

                                                                    if($min_guest > 0 || $max_guest > 0){

                                                                        echo "<div class='serve-people'>Serves ".$min_guest."-".$max_guest." guests</div>";

                                                                    }

                                                                    ?>        
                      </div>
                    </div>
                    <!-- modifire check condition-->
                    <?php
                    if($result->having_modifiers == '1'){
                      global $wpdb;
                      $table1 = $wpdb->prefix.'wyz_restaurant_dish_modifiers';
                      $table2 = $wpdb->prefix.'wyz_restaurant_dish_modifier_items';
                      $query1 = $wpdb->get_results("SELECT `id`,`prompt_text`, `modifier_type`, `min`, `max` FROM $table1 WHERE `dish_id` = '".$i."'");
                      ?>
                      <div id="modifier-parent-section"> 
                        <?php
                        foreach ($query1 as $key => $value) {
                          $query2 = $wpdb->get_results("SELECT * FROM $table2 WHERE `modifier_id` = '".$value->id."'");
                          ?> 
                          <div class="modifier-section <?php if($value->modifier_type == '1'){ echo "single_required_$i";}?> <?php if($value->modifier_type == '4'){ echo "custom_check_$i";}?>">
                            <span class="menuItemModal-choice-name-<?php echo $value->id;?> choice-name h5"><?php echo $value->prompt_text;?><br></span>
                            <?php
                            if($value->modifier_type == '1'){ ?>
                              <p><span class="required">Required - Please Choose 1.</span> </p>
                            <?php  }elseif($value->modifier_type == '2'){ ?>
                              <p><span class="optional">Optional - Choose one as you like.</span> </p>
                            <?php  }
                            elseif($value->modifier_type == '3'){ ?>
                              <p><span class="optional">Optional - Choose as many as you like.</span> </p>
                            <?php  }
                            elseif($value->modifier_type == '4'){
                              $min = $value->min;
                              $max = $value->max;
                              ?>
                              <input type="hidden" name="min" id="min_<?php echo $i;?>" value="<?php echo $value->min;?>">
                              <input type="hidden" name="max" id="max_<?php echo $i;?>" value="<?php echo $value->max;?>">
                              <p><span class="custom_check">You have to select Minimum <?php echo $min; 
                              if($max > $min){?>
                                and maximum <?php echo $max;?> 

                              </span>


                            <?php }

                            ?> 

                          </p>

                        <?php  }
                            foreach ($query2 as $key => $value1) { 
                              if($value->modifier_type == '1'){ ?>
                                <div class="single-collection">
                                  <input type="radio" class ="radio dish_modifire_new_<?php echo $i;?> single_selection_required" 
                                  name="single_selection_required_<?php echo $value1->modifier_id;?>" value="<?php echo $value1->price+($value1->price*$value1->tax/100); ?>" 
                                  id="<?php echo $value1->modifier_id;?>" class="dish_modifire" data_modifire_type= "<?php echo $value->modifier_type;?>" modifire_gst = "<?php echo ($value1->price*$value1->tax/100);?>" modifire_name= "<?php echo $value1->text; ?>" modifire_parent_name= "<?php echo $value->prompt_text;?>" >
                                  <label for="huey" style="
                                  display: inline-block;
                                  width: 85%;
                                  float: left;
                                  vertical-align: middle;
                                  margin-top: 12px;
                                  margin-left: 5px;
                                  "><?php echo $value1->text; ?><span> + $<?php echo $value1->price+($value1->price*$value1->tax/100); ?></span></label>
                                </div>
                              <?php     }
                              else if($value->modifier_type == '2'){ ?>
                                <div class="single-collection">
                                  <input type="radio" class ="radio dish_modifire_new_<?php echo $i;?>" 
                                  name="single_selection_optional_<?php echo $value1->modifier_id;?>" 
                                  value="<?php echo $value1->price+($value1->price*$value1->tax/100); ?>" 
                                  id="<?php echo $value1->modifier_id;?>" 
                                   modifire_gst = "<?php echo ($value1->price*$value1->tax/100);?>"  
                                                                    modifire_name= "<?php echo $value1->text; ?>"  modifire_parent_name= "<?php echo $value->prompt_text;?>">
                                  <label for="huey" style="
                                  display: inline-block;
                                  width: 85%;
                                  float: left;
                                  vertical-align: middle;
                                  margin-top: 12px;
                                  margin-left: 5px;
                                  "><?php echo $value1->text; ?><span> + $<?php echo $value1->price+($value1->price*$value1->tax/100); ?></span></label>
                                </div>
                              <?php     }
                              else if($value->modifier_type == '3' || $value->modifier_type == '4'){?>
                              <div class="single-collection">
                                <input type="checkbox" name="multiple_selection_optional[]" value="<?php echo $value1->price; ?>"  
                                id="<?php echo $value1->modifier_id;?>" class="dish_modifire" 
                                data_modifire_type= "<?php echo $value->modifier_type;?>" 
                                modifire_gst = "<?php echo ($value1->price*$value1->tax/100);?>" style="
                                display: inline-block;
                                width: 10%;
                                float: left;
                                vertical-align: middle;
                                margin:0;
                                ">
                                <label for="huey" style="
                                display: inline-block;
                                width: 85%;
                                float: left;
                                vertical-align: middle;
                                margin-top: 12px;
                                margin-left: 5px;
                                "><?php echo $value1->text; ?><span> + $<?php echo $value1->price+($value1->price*$value1->tax/100); ?></span></label>
                              </div>
                              <?php }   
                              ?>
                            <?php  }
                            ?>
                          </div>
                        <?php } ?>
                      </div>
                    <?php     }
                    ?>
                    <!-- end of modifire -->   
                  </div>
                  <input type="hidden" id="dish_name<?php echo $i;?>" class="quantity"  value = "<?php echo $result->name?>" />
                  <input type="hidden" id="price<?php echo $i;?>" class="price"  value = "<?php echo $result->price;?>" />
                  <input type="hidden" id="dish_id<?php echo $i;?>" class="dish_id"  value = "<?php echo $result->id;?>" />
                  <input type="hidden" id="dish_tax<?php echo $i;?>" class="dish_tax"  value = "<?php echo $result->tax;?>" />
                  <input type="hidden" id="restro_id<?php echo $i;?>" class="restro_id"  value = "<?php echo $result->restaurant_id;?>" />
                  <div class="modal-footer">
                  <div class="min-quantity" style="display:none;color:red;">
                                                        Please Select Min <?php echo $result->min_quantity;?> Quantity Of this dish</div>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="add-to-cart<?php echo $i;?>" class="add-to-cart" data-id="<?php echo $i;?>" class="btn btn-primary updateCart">Add</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php
        }
    } else {
        echo "No Dishes Found";
    }
    die();
}

//add to cart
add_action('wp_ajax_nopriv_addToCart', 'addToCart');
add_action( 'wp_ajax_addToCart', 'addToCart');
function addToCart(){
    session_start();
    $data = array();
    $key = base64_encode(strtolower($_POST['dish']).'_'.$_POST['dish_id']);
    if(!empty($_SESSION["cart_item"])) {
        if(array_key_exists($key,$_SESSION["cart_item"])) {
            $_SESSION["cart_item"][$key]['quantity'] = $_SESSION["cart_item"][$key]['quantity'] + $_POST['quantity'];
            $_SESSION["cart_item"][$key]['amount'] =  $_POST['price'] * $_SESSION["cart_item"][$key]['quantity'];
            $tax =  $_SESSION["cart_item"][$key]['quantity'] * ($_POST['price']*$_POST['tax']/100);
            $_SESSION["cart_item"][$key]['tax'] =  $tax;
            $_SESSION["cart_item"][$key]['radioValues'] =  $_POST['radioValues'];
            $_SESSION["cart_item"][$key]['radioValue2'] = $_POST['radioValue2'] * $_SESSION["cart_item"][$key]['quantity'];
        }else{
            $tax =  $_POST['quantity'] * ($_POST['price']*$_POST['tax']/100);
            $_SESSION["cart_item"][$key] = $_POST;
            $_SESSION["cart_item"][$key]['amount'] = $_POST['price'] * $_POST['quantity'];
            $_SESSION["cart_item"][$key]['radioValues'] =  $_POST['radioValues'];
            $_SESSION["cart_item"][$key]['radioValue2'] = $_POST['radioValue2'] * $_SESSION["cart_item"][$key]['quantity'];
            $_SESSION["cart_item"][$key]['tax'] =  $tax;
            $_SESSION["cart_item"][$key]['tax_per_dish'] =  $_POST['tax'];
        }
    } else {
        $tax =  $_POST['quantity'] * ($_POST['price']*$_POST['tax']/100);
        $_SESSION["cart_item"][$key] = $_POST;
        $_SESSION["cart_item"][$key]['amount'] = $_POST['price'] * $_POST['quantity'];
        $_SESSION["cart_item"][$key]['radioValue1'] = $_POST['radioValue1'] * $_POST['quantity'];
        $_SESSION["cart_item"][$key]['radioValue2'] = $_POST['radioValue2'] * $_POST['quantity'];
        $_SESSION["cart_item"][$key]['tax'] =  $tax;
        $_SESSION["cart_item"][$key]['radioValues'] =  $_POST['radioValues'];
        $_SESSION["cart_item"][$key]['tax_per_dish'] =  $_POST['tax'];
    }

    // $_SESSION["cart_item"][$key]['total_amount'] = $_SESSION["cart_item"][$key]['amount'];

    if(isset($_SESSION["cart_item"])){
        $data['add_cart_count'] = count($_SESSION["cart_item"]);//mobile count
        $item_total = 0;
        $total_tax =  0;
        $delivery_fee = 0;
        $data['cart_items'] = '
        <table cellpadding="10" cellspacing="1" class="show_table">
        <tbody>';   

        $totalModifierTax = 0;
        foreach ($_SESSION["cart_item"] as $key => $item){ 
    
            $delivery_fee = get_post_meta($item["restro_id"],"wp_restaurant_delivery_fee",true);
            $radioDataValues=[];
            
            $modifireBaseTotal = 0;
            foreach($item["radioValues"] as $key2 => $radioV){
                $radioDataValues= array_merge($radioDataValues, $radioV);
                if (isset($radioDataValues['base_price'])) {
                    // $_SESSION["cart_item"][$key]['total_amount'] += $radioDataValues['base_price'] * $item['quantity'];
                    $modifireBaseTotal += $radioDataValues['base_price'] * $item['quantity'];
                    unset($radioDataValues['base_price']);
                }
            }
            $item["radioValues"]= $radioDataValues;
    
            $checkboxDataValues=[];
            foreach($item["checkboxValues"] as $key3 => $checkboxV){
                $checkboxDataValues= array_merge($checkboxDataValues, $checkboxV);
                if (isset($checkboxDataValues['base_price'])) {
                    // $_SESSION["cart_item"][$key]['total_amount'] += $checkboxDataValues['base_price'] * $item['quantity'];
                    $modifireBaseTotal += $checkboxDataValues['base_price'] * $item['quantity'];
                    unset($checkboxDataValues['base_price']);
                }
            }
            $item["checkboxValues"]= $checkboxDataValues;
            $single_selection_required = array_sum($item["radioValues"]) * $item['quantity'];
            $multiple_selection_optional = array_sum($item["checkboxValues"]) * $item['quantity'];

            $_SESSION["cart_item"][$key]['total_modifier_tax'] = $single_selection_required + $multiple_selection_optional - $modifireBaseTotal;

            $total_tax = $item["tax"];
            $totalModifierTax += $item["tax"] + $_SESSION["cart_item"][$key]['total_modifier_tax'];

            $modifier_gst_total = array_sum($item["modifier_gst"]) * $item['quantity'] ;
            $total_price = $item["amount"] + $single_selection_required + $multiple_selection_optional + $total_tax + $modifier_gst_total;

            $_SESSION["cart_item"][$key]['total_amount'] = $item["amount"] + $modifireBaseTotal;

            $data['cart_items'] .= '
            <tr>
                <td class="item-number" style="width:10px">
                <div class="number">
                    <span class="minus">-</span>
                    <input type="number" class="btnUpdateAction cart-action" value="'.$item["quantity"].'" data-key="'.$key.'" data-price="'.$item["price"].'" data-tax="'.$item["tax_per_dish"].'" data-radioVal-one="'.$item["radioValue1"].  '" data-radioVal-two="'.$item["radioValue2"].'" data-restro="'.$item["restro_id"].'"  min="1">
                    <span class="plus">+</span>
                </div>
            </td>
            <td class="dish-name-td"><strong>'.$item["dish"].'</strong></td>
              <td align=right style="width:65px">$'.$total_price.'</td>
                <td><a href="'.$key.'"  class="btnRemoveAction cart-action"><span class="fa fa-trash"></span></a></td>
            </tr>';
            $item_total += $total_price;
        }
        $item_total = $item_total+$delivery_fee;
        // $total_gst = $total_tax + $modifier_gst_total;
        $total_gst = $totalModifierTax;
        $item_total_exclude_delivery = (int)$item_total-(int)$delivery_fee;
        $data['cart_items'] .= '<tr>
            <td colspan="5" align=right>Delivery Fees:$'.$delivery_fee.'</td>
            </tr>';
        $data['cart_items'] .= '<tr>
            <td colspan="5" align=right>GST (Included in price): $'.$total_gst.'</td>
            </tr>';
        $data['cart_items'] .= '<tr>
            <td colspan="5" align=right><strong>Total: </strong>$'.$item_total.'</td>
            </tr>
        </tbody>
        </table>';
        $minorder = get_post_meta($item["restro_id"],"wp_restaurant_minimum_order_amount",true);
        // if($item_total_exclude_delivery >= $minorder){
        //     $data['cart_items'] .= '<div class="bill-div">
        //                             <a href="'.site_url().'/checkout/" class="orange-btn buttons">CHECK OUT</a>
        //                         </div>';
        // }else{
        //     $data['cart_items'] .=   '<a href="#" class="orange-btn buttons minordercheck">CHECK OUT</a>';
        // }
    
    }else{
        echo "Currently Your Cart Is empty";
    }
    wp_send_json_success( $data );
    wp_die();
}
//remove cart
add_action('wp_ajax_nopriv_cart_item_remove', 'cart_item_remove');
add_action( 'wp_ajax_cart_item_remove', 'cart_item_remove');
function cart_item_remove(){
    session_start();
    $data = array();
    if(!empty($_SESSION["cart_item"])) {
        foreach($_SESSION["cart_item"] as $k => $v) {
            if($_POST["item_key"] == $k) {
                unset($_SESSION["cart_item"][$k]);
            }

            if(empty($_SESSION["cart_item"])) {
                unset($_SESSION["cart_item"]);
                $_SESSION['coupon_applied'] = false;
                $_SESSION['coupon_data'] = null;
                $_SESSION['cart_before_coupon'] = null;
                $_SESSION['coupon_amount'] = null;
            }
        }
    }

  if(isset($_SESSION["cart_item"])){
    $data['cart_count'] = count($_SESSION["cart_item"]);//mobile remove count
    $item_total = 0;
    $data['cart_items'] = 
    '<table cellpadding="10" cellspacing="1" class="show_table">
       <tbody>
        ';
    $restro_id = '';
    $appliedTax = 0;
    foreach ($_SESSION["cart_item"] as $key => $item){ 
      $restro_id = $_SESSION["cart_item"][$key]['restro_id'];
      $delivery_fee = get_post_meta($item["restro_id"],"wp_restaurant_delivery_fee",true);

      // $item_total += $item["amount"];
      // $total_tax += $item["tax"];
      // $single_selection_required += $item["radioValue1"];
      // $single_selection_optional += $item["radioValue2"];

        $radioDataValues=[];
        foreach($item["radioValues"] as $radioV){
            $radioDataValues= array_merge($radioDataValues,$radioV);
            if (isset($radioDataValues['base_price'])) {
                unset($radioDataValues['base_price']);
            }
        }
        $item["radioValues"]= $radioDataValues;

        $checkboxDataValues=[];
        foreach($item["checkboxValues"] as $checkboxV){
            $checkboxDataValues= array_merge($checkboxDataValues,$checkboxV);
            if (isset($checkboxDataValues['base_price'])) {
                unset($checkboxDataValues['base_price']);
            }

        }
        $item["checkboxValues"]= $checkboxDataValues;


        $single_selection_required = array_sum($item["radioValues"]) * $item['quantity'];
        $multiple_selection_optional = array_sum($item["checkboxValues"]) * $item['quantity'];

        $total_tax = $item["tax"];
        $modifier_gst_total = array_sum($item["modifier_gst"]) * $item['quantity'] ;
        $total_price = $item["amount"]+ $single_selection_required+$multiple_selection_optional+$total_tax+$modifier_gst_total;
        $item_total += $total_price;


      $data['cart_items'] .= 
      '<tr>
         <td class="item-number" style="width:10px">
         <div class="number">
             <span class="minus">-</span>
             <input type="number" class="btnUpdateAction cart-action" value="'.$item["quantity"].'" data-key="'.$key.'" data-price="'.$item["price"].'" data-tax="'.$item["tax_per_dish"].'" data-restro="'.$item["restro_id"].'" min="1">
             <span class="plus">+</span>
         </div>
     </td>
     <td class="dish-name-td"><strong>'.$item["dish"].'</strong></td>
     <td align=right style="width:65px">$ '. $total_price .'</td>

         <td><a href="'.$key.'"  class="btnRemoveAction cart-action"><span class="fa fa-trash"></span></a></td>
       </tr>';
       $appliedTax += $item["tax"] + $item['total_modifier_tax'];
    }
    $item_total += $delivery_fee;
    $item_total_exclude_delivery = (int)$item_total-(int)$delivery_fee;
    $data['cart_items'] .= 
        '<tr>
             <td colspan="5" align=right>Delivery Fees: $ '.get_post_meta($restro_id, 'wp_restaurant_delivery_fee', true).'</td>
        </tr>';
        $data['cart_items'] .= 
        '<tr>
            <td colspan="5" align=right>GST (Included in price): $ '.$appliedTax.'</td>
        </tr>';
        $data['cart_items'] .= 
        '<tr>
           <td colspan="5" align=right><strong>Total: </strong>$ '.$item_total.'</td>
        </tr>
      </tbody>
    </table>
    <!-- <div class="bill-div">
    <a href="'.site_url().'/checkout/" class="orange-btn buttons">CHECK OUT</a>
    </div> -->';
  }else{
    echo "Currently Your Cart Is empty";
  }
  wp_send_json_success( $data );
  wp_die();
}

//update cart
add_action('wp_ajax_nopriv_cart_item_update', 'cart_item_update');
add_action( 'wp_ajax_cart_item_update', 'cart_item_update');
function cart_item_update(){
  $data = array();
  if(!empty($_SESSION["cart_item"])) {
    foreach($_SESSION["cart_item"] as $k => $v) {
      if($_POST["item_key"] == $k){
        $_SESSION["cart_item"][$k]['quantity'] = $_POST['quantity'];
        $_SESSION["cart_item"][$k]['amount'] =  $_POST['price'] * $_SESSION["cart_item"][$k]['quantity'];
        $_SESSION["cart_item"][$k]['radioValue1'] = $_POST['radioValue1'] * $_SESSION["cart_item"][$k]['quantity'];
        $_SESSION["cart_item"][$k]['radioValue2'] = $_POST['radioValue2'] * $_SESSION["cart_item"][$k]['quantity'];
        $tax =  $_SESSION["cart_item"][$k]['quantity'] * ($_POST['price']*$_POST['tax']/100);
        $_SESSION["cart_item"][$k]['tax'] =  $tax;
      }

    }
  }

  if(isset($_SESSION["cart_item"])){
    $item_total = 0;
    $appliedTax =  0;
    $data['cart_items'] = 
    '<table cellpadding="10" cellspacing="1" class="show_table">
       <tbody>
        ';  
    $restro_id = '';
    foreach ($_SESSION["cart_item"] as $key => $item){                  
      $restro_id = $_SESSION["cart_item"][$key]['restro_id'];
      $delivery_fee = get_post_meta($item["restro_id"],"wp_restaurant_delivery_fee",true);
      
      $modifireBaseTotal = 0;
      $radioDataValues=[];
      foreach($item["radioValues"] as $radioV){
        $radioDataValues= array_merge($radioDataValues, $radioV);
        if (isset($radioDataValues['base_price'])) {
            $_SESSION["cart_item"][$key]['total_amount'] += $radioDataValues['base_price'] * $item['quantity'];
            $modifireBaseTotal += $radioDataValues['base_price'] * $item['quantity'];
            unset($radioDataValues['base_price']);
        }
      }
      $item["radioValues"]= $radioDataValues;

      $checkboxDataValues=[];
      foreach($item["checkboxValues"] as $checkboxV){
        $checkboxDataValues= array_merge($checkboxDataValues, $checkboxV);
        if (isset($checkboxDataValues['base_price'])) {
            $_SESSION["cart_item"][$key]['total_amount'] += $checkboxDataValues['base_price'] * $item['quantity'];
            $modifireBaseTotal += $checkboxDataValues['base_price'] * $item['quantity'];
            unset($checkboxDataValues['base_price']);
        }

      }
      $item["checkboxValues"]= $checkboxDataValues;

      $single_selection_required = array_sum($item["radioValues"]) * $item['quantity'];
      $multiple_selection_optional = array_sum($item["checkboxValues"]) * $item['quantity'];

      $_SESSION["cart_item"][$key]['total_modifier_tax'] = $single_selection_required + $multiple_selection_optional - $modifireBaseTotal;

      $total_tax = $item["tax"];
      $modifier_gst_total = array_sum($item["modifier_gst"]) * $item['quantity'] ;
      $total_price = $item["amount"]+ $single_selection_required+$multiple_selection_optional+$total_tax+$modifier_gst_total;

      $appliedTax += $total_tax + $_SESSION["cart_item"][$key]['total_modifier_tax'];
      
      $_SESSION["cart_item"][$key]['total_amount'] = $item["amount"] + $modifireBaseTotal;

      $data['cart_items'] .= '
      <tr>
         <td class="item-number" style="width:10px">
         <div class="number">
             <span class="minus">-</span>
             <input type="text" class="btnUpdateAction cart-action" value="'.$item["quantity"].'" data-key="'.$key.'" data-price="'.$item["price"].'" data-tax="'.$item["tax_per_dish"].'" data-radioVal-one="'.$item["radioValue1"].'" data-radioVal-two="'.$item["radioValue2"].'" min="1"> 
             <span class="plus">+</span>
         </div>
     </td>
     <td class="dish-name-td"><strong>'.$item["dish"].'</strong></td>

         <td align=right style="width:65px">$'.$total_price.'</td>
         <td><a href="'.$key.'"  class="btnRemoveAction cart-action"><span class="fa fa-trash"></span></a></td>
      </tr>';
      $item_total += $total_price;
    }
    $item_total = $item_total+$delivery_fee;
    // $total_gst = $total_tax+$modifier_gst_total;
    $total_gst = $appliedTax;
    $item_total_exclude_delivery = (int)$item_total-(int)$delivery_fee;
    $data['cart_items'] .= 
     '<tr>
       <td colspan="5" align=right>Delivery Fees: $'.get_post_meta($restro_id, 'wp_restaurant_delivery_fee', true).'</td>
      </tr>';
    $data['cart_items'] .= 
      '<tr>
        <td colspan="5" align=right>GST (Included in price): $'.$total_gst.'</td>
      </tr>';
    $data['cart_items'] .= 
      '<tr>
         <td colspan="5" align=right><strong>Total: </strong>$'.$item_total.'</td>
       </tr>
       </tbody>
    </table>';
    $minorder = get_post_meta($item["restro_id"],"wp_restaurant_minimum_order_amount",true);
    // if($item_total_exclude_delivery >= $minorder){
    //   $data['cart_items'] .= '<div class="bill-div">

    //   <a href="'.site_url().'/checkout/" class="orange-btn buttons">CHECK OUT</a>
    //   </div>';
    // }else{
    //   $data['cart_items'] .= '<a href="#" class="orange-btn buttons minordercheck">CHECK OUT</a>';
    // }

  }else{
    echo "Currently Your Cart Is empty";
  }
  wp_send_json_success( $data );
  wp_die();
}


//Approve Order
add_action('wp_ajax_nopriv_wyz_approve_order', 'wyz_approve_order');
add_action( 'wp_ajax_wyz_approve_order', 'wyz_approve_order');
function wyz_approve_order()
{
  global $wpdb;
  $table= $wpdb->prefix."wyz_restaurant_order";
  $OrderDetailstable = $wpdb->prefix."wyz_restaurant_order_details";
  $user_info = get_userdata( $_POST['user_id']);
  // Get User Email
  $user_Email = $user_info->data->user_email;

  $data = array(
    'id' => $_POST['id'], 
    'status' => $_POST['status']
  );

  $updated = $wpdb->update( $table, array( 'status' => $_POST['status']), array('id' => $_POST['id']));

  ?>
  <span data-id="'.$order['id'].'" class="approved">Status: Confirmed</span>
  <?php
  if($updated == true){
    if( is_admin() ){
      $admin_email = get_bloginfo('admin_email'); 
      $to = array($admin_email);
      $subject = 'Order Accepted';
      ob_start();
      include(WYZ_PLUGIN_ROOT_PATH . '/frontend/admin-email-acceptance.php');
      $email_content = ob_get_contents();
      ob_end_clean();
      // $headers = array('Content-Type: text/html; charset=UTF-8');
      $headers = array(
          'Content-Type: text/html; charset=UTF-8',
          'From: WYZchef <contact@wyzchef.com>',
      );
      // add_filter('wp_mail_from', 'new_mail_from');
      // add_filter('wp_mail_from_name', 'new_mail_from_name');
      // function new_mail_from($admin_email)  
      // {  
      //   return 'contact@wyzchef.com';
      // }
      // function new_mail_from_name($admin_email)  
      // {  
      //   return 'WYZchef';
      // }
      wp_mail( $to, $subject, $email_content, $headers );
    }
    //for User mail
    if( !empty($user_Email) ){
      $to = $user_Email;
      $subject = 'Order Accepted';
      ob_start();
      include(WYZ_PLUGIN_ROOT_PATH . '/frontend/user-email-acceptance.php');
      $email_content = ob_get_contents();
      ob_end_clean();
      // $headers = array('Content-Type: text/html; charset=UTF-8');
      $headers = array(
          'Content-Type: text/html; charset=UTF-8',
          'From: WYZchef <contact@wyzchef.com>',
      );
      wp_mail( $to, $subject, $email_content, $headers );
    }
  }
  die();

}


//Dicline Order
add_action('wp_ajax_nopriv_wyz_decline_order', 'wyz_decline_order');
add_action( 'wp_ajax_wyz_decline_order', 'wyz_decline_order');
function wyz_decline_order()
{

  global $wpdb;
  $table= $wpdb->prefix."wyz_restaurant_order";
  $OrderDetailstable = $wpdb->prefix."wyz_restaurant_order_details";
  $user_info = get_userdata( $_POST['user_id']);
  // Get User Email
  $user_Email = $user_info->data->user_email;
  $data = array(
    'id' => $_POST['id'], 
    'status' => $_POST['status']
  );
  $updated = $wpdb->update( $table, array( 'status' => $_POST['status']), array('id' => $_POST['id']));
  ?>
  <button type="button" data-id="'.$order['id'].'" class="btn btn-primary reject">Declined</button>
  <?php
  if($updated == true){
    if( is_admin() ){
      $admin_email = get_bloginfo('admin_email'); 
      $to = array($admin_email);
      $subject = 'Order Declined';
      ob_start();
      include(WYZ_PLUGIN_ROOT_PATH . '/frontend/admin-email-decline.php');
      $email_content = ob_get_contents();
      ob_end_clean();
      // $headers = array('Content-Type: text/html; charset=UTF-8');
      $headers = array(
          'Content-Type: text/html; charset=UTF-8',
          'From: WYZchef <contact@wyzchef.com>',
      );
      // add_filter('wp_mail_from', 'new_mail_from');
      // add_filter('wp_mail_from_name', 'new_mail_from_name');
      // function new_mail_from($admin_email)  
      // {  
      //   return 'contact@wyzchef.com';
      // }
      // function new_mail_from_name($admin_email)  
      // {  
      //   return 'WYZchef';
      // }
      wp_mail( $to, $subject, $email_content, $headers );
    }
    //for User mail
    if( !empty($user_Email) ){
      $to = $user_Email;
      $subject = 'Order Declined';
      ob_start();
      include(WYZ_PLUGIN_ROOT_PATH . '/frontend/user-email-decline.php');
      $email_content = ob_get_contents();
      ob_end_clean();
      // $headers = array('Content-Type: text/html; charset=UTF-8');
      $headers = array(
          'Content-Type: text/html; charset=UTF-8',
          'From: WYZchef <contact@wyzchef.com>',
      );
      wp_mail( $to, $subject, $email_content, $headers );
    }
  }
  die();

}


//show dishes throw menu click
add_action('wp_ajax_nopriv_show_dishes_menu', 'show_dishes_menu');
add_action( 'wp_ajax_show_dishes_menu', 'show_dishes_menu');
function show_dishes_menu(){
  global $wpdb;
  $table = $wpdb->prefix.'wyz_restaurant_dishes';
  $results = $wpdb->get_results("SELECT * FROM $table WHERE `menu_id` = '".$_POST['menu_id']."' AND `restaurant_id` = '".$_POST['restro_id']."' ORDER BY id DESC LIMIT 4");

  if(count($results) > 0){
    foreach ($results as $result) { 
      $dishId = $result->id;
      $i = $dishId;
      ?>
      <div class="col-md-6 col-sm-6 detail-add-popup">
        <div class="row dish-single-item" data-toggle="modal" data-target="#exampleModal<?php echo $i;?>">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <h6><?php echo $result->name?></h6>
            <p class="blurr-text">
              <?php 
              echo wp_trim_words( $result->description, 45, '...' );
              ?></p>   

            </div>                                                          
            <div class="price dish-box-price"><!--<span class="dish-size-label">S</span>-->
              <span class="dish-size">S<?php echo "$". $result->price;?></span>
            </div>

          </div>
          <div class="modal fade" id="exampleModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="cartModalLabel<?php echo $i;?>" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title cartModalLabel" id="cartModalLabel<?php echo $i;?>"><strong><?php echo $result->name;?></strong></h5>
                  <button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                      <?php
                      $image = $result->image;
                      $imageData = base64_encode(file_get_contents($image));
                      $src = 'data: '.mime_content_type($image).';base64,'.$imageData;
                      ?>
                      <img src="<?php echo $src;?>">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                      <p class="blurr-text1"><?php echo $result->description;?></p>
                      <label for="quantity<?php echo $i;?>" >Choose Quantity</label>
                      <input type="number" id ="quantity<?php echo $i;?>" class="quantity" min="1" value = "1" name="quantity" />
                      <input type="hidden" id="min_quantity<?php echo $i;?>"name="min_quantity" 
                                value="<?php echo $result->min_quantity;?>">
                                <?php
                                                                if($result->min_quantity > 0){ ?>
                                                                    <div class="min-quantity-msg" style="color:red;">

                                                                        Please Select Min <?php echo $result->min_quantity;?> Quantity Of this dish</div>

                                                                    <?php   }else{?>

                                                                        <div class="min-quantity-msg" style="color:red;">

                                                                        Please Select Min 1 Quantity Of this dish</div>

                                                                    <?php  }
                                                                    $min_guest = $result->min_guest_serve;
                                                                    $max_guest = $result->max_guest_serve;

                                                                    if($min_guest > 0 || $max_guest > 0){

                                                                        echo "<div class='serve-people'>Serves ".$min_guest."-".$max_guest." guests</div>";

                                                                    }

                                                                    ?>          
                    </div>
                  </div>
                  <!-- modifire check condition-->
                  <?php
                  if($result->having_modifiers == '1'){
                    global $wpdb;
                    $table1 = $wpdb->prefix.'wyz_restaurant_dish_modifiers';
                    $table2 = $wpdb->prefix.'wyz_restaurant_dish_modifier_items';
                    $query1 = $wpdb->get_results("SELECT `id`,`prompt_text`, `modifier_type`, `min`, `max` FROM $table1 WHERE `dish_id` = '".$i."'");

                    ?>
                    <div id="modifier-parent-section"> 
                      <?php
                      foreach ($query1 as $key => $value) {
                        $query2 = $wpdb->get_results("SELECT * FROM $table2 WHERE `modifier_id` = '".$value->id."'");
                        ?> 

                        <div class="modifier-section <?php if($value->modifier_type == '1'){ echo "single_required_$i";}?> <?php if($value->modifier_type == '4'){ echo "custom_check_$i";}?>">
                          <span class="menuItemModal-choice-name-<?php echo $value->id;?> choice-name h5"><?php echo $value->prompt_text;?><br></span>
                          <?php
                          if($value->modifier_type == '1'){ ?>
                            <p><span class="required">Required - Please Choose 1.</span> </p>
                          <?php  }elseif($value->modifier_type == '2'){ ?>
                            <p><span class="optional">Optional - Choose one as you like.</span> </p>
                          <?php  }
                          elseif($value->modifier_type == '3'){ ?>
                            <p><span class="optional">Optional - Choose as many as you like.</span> </p>
                          <?php  }
                          elseif($value->modifier_type == '4'){
                            $min = $value->min;
                            $max = $value->max;
                            ?>
                            <input type="hidden" name="min" id="min_<?php echo $i;?>" value="<?php echo $value->min;?>">
                            <input type="hidden" name="max" id="max_<?php echo $i;?>" value="<?php echo $value->max;?>">
                            <p><span class="custom_check">You have to select Minimum <?php echo $min; 
                            if($max > $min){?>
                              and maximum <?php echo $max;?> 

                            </span>


                          <?php }

                          ?> 

                        </p>

                      <?php  }

                          foreach ($query2 as $key => $value1) { 
                            if($value->modifier_type == '1'){ ?>
                              <div class="single-collection">
                                <input type="radio" class ="radio dish_modifire_new_<?php echo $i;?>" 
                                name="single_selection_required_<?php echo $value1->modifier_id;?>" 
                                value="<?php echo $value1->price+($value1->price*$value1->tax/100); ?>" 
                                id="<?php echo $value1->modifier_id;?>" class="dish_modifire" data_modifire_type= "<?php echo $value->modifier_type;?>" modifire_gst = "<?php echo ($value1->price*$value1->tax/100);?>" modifire_name= "<?php echo $value1->text; ?>" modifire_parent_name= "<?php echo $value->prompt_text;?>" >
                                <label for="huey" style="
                                display: inline-block;
                                width: 85%;
                                float: left;
                                vertical-align: middle;
                                margin-top: 10px;
                                margin-left: 5px;
                                "><?php echo $value1->text; ?><span> + $<?php echo $value1->price+($value1->price*$value1->tax/100); ?></span></label>
                              </div>
                            <?php     }
                            else if($value->modifier_type == '2'){ ?>
                              <div class="single-collection">
                                <input type="radio" class ="radio dish_modifire_new_<?php echo $i;?>" 
                                name="single_selection_optional_<?php echo $value1->modifier_id;?>" 
                                value="<?php echo $value1->price+($value1->price*$value1->tax/100); ?>" id="<?php echo $value1->modifier_id;?>" class="dish_modifire" data_modifire_type= "<?php echo $value->modifier_type;?>" 
                                modifire_gst = "<?php echo ($value1->price*$value1->tax/100);?>"  
                                modifire_name= "<?php echo $value1->text; ?>"  modifire_parent_name= "<?php echo $value->prompt_text;?>" >
                                <label for="huey" style="
                                display: inline-block;
                                width: 85%;
                                float: left;
                                vertical-align: middle;
                                margin-top: 10px;
                                margin-left: 5px;
                                "><?php echo $value1->text; ?><span> + $<?php echo $value1->price+($value1->price*$value1->tax/100); ?></span></label>
                              </div>
                            <?php  }
                            else if($value->modifier_type == '3' || $value->modifier_type == '4'){?>
                             <div class="single-collection">
                              <input type="checkbox" name="multiple_selection_optional[]" 
                              value="<?php echo $value1->price+($value1->price*$value1->tax/100); ?>"  
                              id="<?php echo $value1->modifier_id;?>" class="<?php if($value->modifier_type == '4'){ echo "custom_checkbox";}?>" 
                              data_modifire_type= "<?php echo $value->modifier_type;?>" 
                              modifire_gst = "<?php echo ($value1->price*$value1->tax/100);?>" modifire_name= "<?php echo $value1->text; ?>" modifire_parent_name= "<?php echo $value->prompt_text;?>" style="
                              display: inline-block;
                              width: 10%;
                              float: left;
                              vertical-align: middle;
                              margin:0
                              ">
                              <label for="huey" style="
                              display: inline-block;
                              width: 85%;
                              float: left;
                              vertical-align: middle;
                              margin-top: 10px;
                              margin-left: 5px;
                              "><?php echo $value1->text; ?><span> + $<?php echo $value1->price+($value1->price*$value1->tax/100); ?></span></label>
                            </div>
                            <?php }   
                            ?>

                          <?php  }
                          ?>

                        </div>
                        <?php } ?>
                    </div>

                  <?php  }

                  ?>

                  <!-- end of modifire -->              

                </div>
                <input type="hidden" id="dish_name<?php echo $i;?>" class="quantity"  value = "<?php echo $result->name?>" />
                <input type="hidden" id="price<?php echo $i;?>" class="price"  value = "<?php echo $result->price;?>" />
                <input type="hidden" id="dish_id<?php echo $i;?>" class="dish_id"  value = "<?php echo $result->id;?>" />
                <input type="hidden" id="dish_tax<?php echo $i;?>" class="dish_tax"  value = "<?php echo $result->tax;?>" />
                <input type="hidden" id="restro_id<?php echo $i;?>" class="restro_id"  value = "<?php echo $result->restaurant_id;?>" />
                <div class="modal-footer">
                <div class="min-quantity" style="display:none;color:red;">
                                                        Please Select Min <?php echo $result->min_quantity;?> Quantity Of this dish</div>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" id="add-to-cart<?php echo $i;?>" class="add-to-cart" data-id="<?php echo $i;?>" class="btn btn-primary updateCart" >Add</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php     }?>

      <div class="show_more_main" id="show_more_main<?php echo $i;?>">
        <span id="<?php echo $i;?>"  restro_id="<?php echo $_POST['restro_id'];?>" menuID = "<?php echo $_POST['menu_id'];?>"class="show_more" title="Load more posts">Show more</span>
        <span class="loding" style="display: none;">
          <img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/loader.gif"/>
        </span>
      </div>         
<?php  }
die();    
}


//Completed Order
add_action('wp_ajax_nopriv_wyz_complete_order', 'wyz_complete_order');
add_action( 'wp_ajax_wyz_complete_order', 'wyz_complete_order');
function wyz_complete_order()
{
  global $wpdb;
  $table= $wpdb->prefix."wyz_restaurant_order";

  $data = array(
    'id' => $_POST['id'], 
    'status' => $_POST['status']
  );
  $updated = $wpdb->update( $table, array( 'status' => $_POST['status']), array('id' => $_POST['id']));
  ?>
  <span data-id="'.$order['id'].'" class="completed">Status: Completed</span>
  <?php
  die();
  
}


//add ratings for completed orders
add_action('wp_ajax_nopriv_wyz_post_user_review', 'wyz_post_user_review');
add_action( 'wp_ajax_wyz_post_user_review', 'wyz_post_user_review');
function wyz_post_user_review(){
  global $wpdb;
  $orderTable = $wpdb->prefix."wyz_restaurant_order";
  $time = current_time('mysql');
  $json_message = array();
  $data = array(
    'comment_post_ID'       => $_POST['comment_post_ID'],
    'comment_author'        => $_POST['comment_author'],
    'comment_author_email'  => $_POST['review_author_email'],
    'comment_author_url'    => '',
    'comment_content'       => $_POST['comment_content'],
    'comment_type'          => '',
    'comment_parent'        => 0,
    'user_id'               => $_POST['comment_user_id'],
    'comment_author_IP'     => '',
    'comment_agent'         => $_SERVER['HTTP_USER_AGENT'],
    'comment_date'          => $time,
    'comment_approved'      => 0,
  ); 


  $comment_id = wp_insert_comment($data);
  update_comment_meta( $comment_id, 'wyz_rating', $_POST['rating'] );
  update_comment_meta( $comment_id, 'wyz_order_id', $_POST['comment_order_ID'] );
  update_comment_meta( $comment_id, 'wyz_title', $_POST['review_title'] );
  $wpdb->update( $orderTable, array( 'order_rating' => $_POST['rating']), array('id' => $_POST['comment_order_ID'] ));

  $userId = $wpdb->get_var( "SELECT user_id FROM {$orderTable} WHERE id = {$_POST[comment_order_ID]}" );
  $rewardPoints  = floatval(get_user_meta($userId, "reward_points", true ));
  update_user_meta($userId, "reward_points", ($rewardPoints+100) );

  $json_message['POST'] = $_POST;
  $json_message['rewardPoints'] = $rewardPoints;
  $json_message['Response'] = 'success';
  $json_message['class'] = 'alert alert-success';
  $json_message['Msg'] = 'Your Review has been submitted.';
  $json_message['ratings'] = '<p class="show-ratings-'.$_POST['comment_order_ID'].'">';
  $ratings = $_POST['rating'];
  $off_rating = 5 - $ratings;
  for ($i=1; $i <=$ratings ; $i++) { 
    $json_message['ratings'] .= "<span class='fa fa-star'></span>";
  }
  for ( $i = 1; $i <= $off_rating; $i++ ) {
    $a  = $i + $ratings;
    $json_message['ratings'] .= "<span class='fa fa-star-o'></span>";
  }

  $json_message['ratings'] .= '</p>';

  wp_send_json($json_message, true);
  exit;  
}


//Save prefrences for users
add_action('wp_ajax_nopriv_prefrence_save_data', 'prefrence_save_data');
add_action( 'wp_ajax_prefrence_save_data', 'prefrence_save_data');
function prefrence_save_data(){
  global $wpdb;
  $tableName = $wpdb->prefix . 'wyz_user_preferences';
  $user_id = $_POST['user_id'];
  $cuisines = $_POST['cuisines'];
  $services = $_POST['services'];
  $budget = $_POST['budget'];
  $dietary_restriction = $_POST['dietary_restriction'];
  $first_priority = $_POST['first_priority'];
  $second_priority = $_POST['second_priority'];
  $third_priority = $_POST['third_priority'];
  $nationalities = $_POST['nationalities'];
  $cuisines = implode(",",$cuisines);
  $services = implode(",",$services);
  $dietary_restriction = implode(",",$dietary_restriction);
  $nationalities = implode(",",$nationalities);
  $rowcount1 = $wpdb->get_var("SELECT COUNT(*) FROM $tableName WHERE user_id = $user_id");
  if($rowcount1 == 0){
    $success = $wpdb->insert($tableName, array(
      "user_id"        => $user_id,
      "dietary_restriction"        => $dietary_restriction,
      "meals" => '',
      "services"          => $services,
      "budget"         => $budget,
      "restaurant_types"     => $cuisines,
      "first_priority"  => $first_priority,
      "second_priority"  => $second_priority,
      "third_priority"  => $third_priority,
      "nationalities"  => $nationalities
    ));
    if($success) {

      $data = 'preferences Saved Successfully';
      wp_send_json_success( $data );
      exit;
    }
  } else{
    $success = $wpdb->update( $tableName, array(
      "user_id"        => $user_id,
      "dietary_restriction"        => $dietary_restriction,
      "meals" => '',
      "services"          => $services,
      "budget"         => $budget,
      "restaurant_types"     => $cuisines,
      "first_priority"  => $first_priority,
      "second_priority"  => $second_priority,
      "third_priority"  => $third_priority,
      "nationalities"  => $nationalities
    ), 
    array('user_id' => $user_id)
  );
    if($success) {

      $data = 'preferences Updated Successfully';
      wp_send_json_success( $data );
      exit;
    }
  }

}

//load more dishes
add_action('wp_ajax_nopriv_load_more_dishes', 'load_more_dishes');
add_action( 'wp_ajax_load_more_dishes', 'load_more_dishes');
function load_more_dishes(){
    global $wpdb;
    if (isset($_POST['formData']) && $_POST['formData']) {
        parse_str($_POST['formData'], $formData);
    }
    
    $whereQuery = "WHERE restaurant_id = {$formData[restro_id]} AND id > $formData[last_dish_id]";
    if (!empty($formData['tags'])) {
        $whereQuery .= ' AND (';
        foreach ($formData['tags'] as $key => $tag) {
            if (!$key) {
                $whereQuery .= " tags LIKE '%{$tag}%'";
            } else {
                $whereQuery .= " OR tags LIKE '%{$tag}%'";
            }
        }
        $whereQuery .= ') ';
    }

    if (!empty($formData['menuID'])) {
        $whereQuery .= " AND menu_id = {$formData[menuID]}";
    }

    if (!empty($formData['dishSearch'])) {
        $whereQuery .= " AND name LIKE '%{$formData[dishSearch]}%'";
    }

    $limit = $formData['limit'];
    $querycount = "SELECT COUNT(*)  FROM {$wpdb->prefix}wyz_restaurant_dishes {$whereQuery}";
    $queryAll = $wpdb->get_var($querycount);

    $dishQuery = "SELECT id, name, description, price, tax, image, min_quantity, min_guest_serve, max_guest_serve, having_modifiers, tags FROM {$wpdb->prefix}wyz_restaurant_dishes {$whereQuery} ORDER BY id ASC LIMIT {$limit}";
    $dishs = $wpdb->get_results($dishQuery);

    $dataArray = array(
        'lastDishID' => $formData['last_dish_id'],
        // 'formData' => $formData,
        // 'query' => $query,
    );
    if(count($dishs) > 0){
        $dishModifierTable = $wpdb->prefix.'wyz_restaurant_dish_modifiers';
        $modifierItemTable = $wpdb->prefix.'wyz_restaurant_dish_modifier_items';
        foreach ($dishs as  $dish) {
            $dishId = $dish->id;

            if( $dishId > 0 ) {
                $dataArray['dish'][$dishId]['id'] = $dishId;
                $dataArray['dish'][$dishId]['name'] = htmlspecialchars_decode($dish->name, ENT_QUOTES);
                $dataArray['dish'][$dishId]['description'] = htmlspecialchars_decode($dish->description, ENT_QUOTES);
                $dataArray['dish'][$dishId]['price'] = $dish->price;
                $dataArray['dish'][$dishId]['tax'] = $dish->tax;

                $dataArray['dish'][$dishId]['imageSrc'] = $dish->image;
                $dataArray['dish'][$dishId]['min_quantity'] = $dish->min_quantity;
                $dataArray['dish'][$dishId]['min_guest_serve'] = $dish->min_guest_serve;
                $dataArray['dish'][$dishId]['max_guest_serve'] = $dish->max_guest_serve;
                $dataArray['dish'][$dishId]['having_modifiers'] = $dish->having_modifiers;
                $dishTags = array();
                if (!empty($dish->tags) && trim($dish->tags) && ($dish->tags != 'null')) {
                    $dishTags = explode(',', $dish->tags);
                }
                
                $dataArray['dish'][$dishId]['tags'] = array();
                foreach ($dishTags as $tag) {
                    $dataArray['dish'][$dishId]['tags'][] = get_term($tag)->name;
                }

                if($dish->having_modifiers == '1'){
                    $modifiers = $wpdb->get_results("SELECT `id`,`prompt_text`, `modifier_type`, `min`, `max` FROM $dishModifierTable WHERE `dish_id` = '{$dishId}'", ARRAY_A);
                    foreach ($modifiers as $key1 => $modifier) {
                        $dataArray['dish'][$dishId]['modifiers'][$modifier['id']]['modifier'] = $modifier;
                        $modifierItems = $wpdb->get_results("SELECT * FROM {$modifierItemTable} WHERE `modifier_id` = {$modifier[id]}", ARRAY_A);
                        foreach ($modifierItems as $key2 => $modifierItem) {
                            $dataArray['dish'][$dishId]['modifiers'][$modifier['id']]['modifire_items'][$modifierItem['id']] = $modifierItem;
                        }
                    }
                }
            }

        }
        if($queryAll > $limit){ 
            $dataArray['showlimit'] = true;
            $dataArray['msg'] = "Show more";
        } else {
            $dataArray['showlimit'] = false;
            $dataArray['msg'] = "That's All!";
        }
    } else{
        $dataArray['showlimit'] = false;
        $dataArray['msg'] = "No dishes yet..!";
    }

    echo json_encode($dataArray); die;
}


/**
 * Apply coupon code
 */
add_action('wp_ajax_nopriv_wyzchef_apply_coupon', 'wyzchef_apply_coupon');
add_action( 'wp_ajax_wyzchef_apply_coupon', 'wyzchef_apply_coupon');
function wyzchef_apply_coupon() {
    if (isset($_POST['code']) && !empty($_POST['code'])) {
        if (isset($_SESSION['coupon_applied']) && $_SESSION['coupon_applied']) {
            echo json_encode([
                'success' => false,
                'msg' => 'Coupon already applied.'
            ]); die;
        } else {
            session_start();
            global $wpdb;
            $couponCode = htmlspecialchars(str_replace('\\', '', $_POST['code']), ENT_QUOTES);
            $sqlQuery = "
                SELECT *
                FROM {$wpdb->prefix}wyz_restaurant_order_coupons as c  
                WHERE c.code = '{$couponCode}'
            ";

            $couponData = $wpdb->get_row($sqlQuery);

            if ($couponData) {
                if ($couponData->status) {
                    if (new DateTime() > new DateTime("{$couponData->expiry} 23:59:59")) {
                        echo json_encode([
                            'success' => false,
                            'msg' => 'Coupon code has expired.'
                        ]); die;
                    }

                    $oldTotal = $total = 0; 
                    $dishesAmounts = array();
                    $totalDiscountedAmount = 0;
                    // $_SESSION['cart_before_coupon'] = $_SESSION['cart_item'];
                    $cartItems = str_replace("\\", "",$_POST['cartItems']);
                    $_SESSION['cart_before_coupon'] = $cartItems;
                    $cartItems = json_decode($cartItems);
                    // echo'<pre>Debug $cartItems: '; print_r( $cartItems->{31}); echo '</pre>'; die;
                    foreach ($cartItems as $key => $item) {
                        $dishPrice = $item->totalBasePrice;
                        $dishGstPrice = $item->totalGstPrice;
                        if ($couponData->type) {
                            $discountedAmount = ($dishPrice*$couponData->value)/100;
                            $updatedPrice = $dishPrice - $discountedAmount;
                            $totalDiscountedAmount += $discountedAmount;
                        } else {
                            $updatedPrice = $dishPrice - $couponData->value;
                            $totalDiscountedAmount += $couponData->value;
                        }
                        
                        $cartItems->{$key}->totalBasePriceBeforeCoupon = $dishPrice;
                        $cartItems->{$key}->totalGstPriceBeforeCoupon = $dishGstPrice;
                        $cartItems->{$key}->totalBasePrice = $updatedPrice;
                        $cartItems->{$key}->totalGstPrice = $dishGstPrice - $discountedAmount;

                        $dishesAmounts[$key] = $cartItems->{$key}->totalGstPrice;
                        $oldTotal += $dishGstPrice;
                        $total += $cartItems->{$key}->totalGstPrice;
                    }

                    $_SESSION['coupon_applied'] = true;
                    $_SESSION['coupon_data'] = $couponData;
                    $_SESSION['coupon_amount'] = $totalDiscountedAmount;

                    echo json_encode([
                        'success' => true,
                        'cartItems' => $cartItems,
                        'oldTotal' => $oldTotal,
                        'total' => $total,
                        'dishesAmounts' => $dishesAmounts,
                        // 'session' => $_SESSION,
                        'msg' => 'Coupon applied successfully..'
                    ]); die;
                } else {
                    echo json_encode([
                        'success' => false,
                        'msg' => 'Coupon is not available now.'
                    ]); die;
                }
            } else {
                echo json_encode([
                    'success' => false,
                    'msg' => 'Invalid coupon code.',
                    'cartItems' => json_decode(str_replace("\\", "",$_POST['cartItems']))
                ]); die;
            }
        }
    }

    echo json_encode([
        'success' => false,
        'msg' => 'Coupon code is not valid.'
    ]); die;
}


/**
 * Remove coupon code from cart
 */
add_action('wp_ajax_nopriv_wyzchef_remove_coupon', 'wyzchef_remove_coupon');
add_action( 'wp_ajax_wyzchef_remove_coupon', 'wyzchef_remove_coupon');
function wyzchef_remove_coupon() {
    if (isset($_SESSION['coupon_applied']) && $_SESSION['coupon_applied']) {
        session_start();

        $total = 0; 
        $dishesAmounts = array();
        $cartItems = json_decode($_SESSION['cart_before_coupon']);
        foreach ($cartItems as $key => $item) {
            // $restaurantId = $item['restro_id'];
            $dishesAmounts[$key] = $item->totalGstPrice;
            $total += $item->totalGstPrice;
        }

        // $deliveryCharges = get_post_meta($restaurantId, "wp_restaurant_delivery_fee", true);
        // $total += $deliveryCharges;

        $_SESSION['coupon_applied'] = false;
        $_SESSION['coupon_data'] = null;
        $_SESSION['cart_before_coupon'] = null;
        $_SESSION['coupon_amount'] = null;

        echo json_encode([
            'success' => true,
            'total' => $total,
            'dishesAmounts' => $dishesAmounts,
            'cartItems' => $cartItems,
            'msg' => 'Coupon removed..'
        ]); die;
    } else {
        echo json_encode([
            'success' => false,
            'msg' => 'Coupon is not applied yet.'
        ]); die;
    }
}


add_action( 'wp_ajax_nopriv_sendRedeemRequest', 'sendRedeemRequest');
add_action( 'wp_ajax_sendRedeemRequest', 'sendRedeemRequest');
function sendRedeemRequest() {
    $userId = intval( $_POST['userId'] );
    $adminMail = false;
    $msg = 'You are not authorize to perform this action.';
    $emailContent = '';
    $rewardPoints = false;

    // get user info
    $user_info = get_userdata( $userId );

    if ($user_info) {
        $userEmail = $user_info->user_email;
        $userFirstName  = get_user_meta($userId, "first_name", true );
        $userLastName  = get_user_meta($userId, "last_name", true );
        $rewardPoints  = floatval(get_user_meta($userId, "reward_points", true ));

        if ($rewardPoints > 2999) {
            $adminEmail = get_option('admin_email');
            $subject = 'Redeem Request ';

            $emailContent = "
                You received redeem request as follows:
                <br/> &nbsp; &nbsp; User Id: $userId, 
                <br/> &nbsp; &nbsp; User Email: $userEmail, 
                <br/> &nbsp; &nbsp; User First Name: $userFirstName, 
                <br/> &nbsp; &nbsp; User Last Name: $userLastName, 
                <br/> &nbsp; &nbsp; Reward Points: $rewardPoints, 
                <br/> &nbsp; &nbsp; Redeem Request For Points: 3000 
            ";
            
            // $headers = array('Content-Type: text/html; charset=UTF-8');
            // $headers = array("Content-Type: text/html; charset=UTF-8','From: {$userFirstName} {$userLastName} <{$userEmail}>");
            $headers = array(
                "Content-Type: text/html; charset=UTF-8",
                "From: {$userFirstName} {$userLastName} <{$userEmail}>",
            );

            $adminMail = wp_mail( $adminEmail, $subject, $emailContent, $headers );

            $msg = "There is some issue with mail server to process your request.";
            // $rewardPoints = $rewardPoints - 3000;
            // update_user_meta($userId, "reward_points", ($rewardPoints) );
            if ($adminMail) {
                $rewardPoints = $rewardPoints - 3000;
                update_user_meta($userId, "reward_points", $rewardPoints );
                $msg = "Your request to redeem reward points is sent successfully.";
            }
        } else {
            $msg = "Minimum reward points must 3000 or more to redeem.";
        }
    }

    echo json_encode([
        'status' => $adminMail,
        'msg' => $msg,
        'rewardPoints' => $rewardPoints,
        'emailContent' => $emailContent,
    ]);

    wp_die();
}

add_action( 'wp_ajax_orderDishDetails', 'orderDishDetails');
add_action( 'wp_ajax_nopriv_orderDishDetails', 'orderDishDetails');
function orderDishDetails() {
    $htmlmaster = '';
    $order = json_decode(str_replace('\\', '', $_POST['order']));
    $unixTimeStamp = strtotime($order->delivery_date);
    $date = date('D, M d', $unixTimeStamp);
    $time = date('h:i A',$unixTimeStamp);

    global $wpdb;
    $orderDeliveryDetailsTable = "{$wpdb->prefix}wyz_restaurant_order_delivery_details";
    $orderDetailsTable = "{$wpdb->prefix}wyz_restaurant_order_details";
    $dishesTable = "{$wpdb->prefix}wyz_restaurant_dishes";
    $postTable = "{$wpdb->prefix}posts";
    $sql = "
        SELECT d.id, d.name, odd.company, odd.floor, odd.street, odd.city, odd.postal_code, odd.people, od.quantity, od.line_total, od.modifier, p.post_title FROM {$orderDeliveryDetailsTable} AS odd 
        INNER JOIN {$orderDetailsTable} AS od 
        ON od.order_id = odd.order_id 
        INNER JOIN {$dishesTable} AS d 
        ON od.dish_id = d.id 
        INNER JOIN {$postTable} AS p 
        ON d.restaurant_id = p.ID 
        WHERE odd.order_id = {$order->id}
    ";
    $orderDetails = $wpdb->get_results($sql, ARRAY_A);
    
    $orderServicesTable = "{$wpdb->prefix}wyz_restaurant_order_services";
    $servicesTable = "{$wpdb->prefix}wyz_restaurant_services";
    $sql = "
        SELECT s.id, s.question, os.quantity, os.total_price, os.options FROM {$orderServicesTable} AS os 
        INNER JOIN {$servicesTable} AS s
        ON s.id = os.service_id 
        WHERE order_id = {$order->id}
    ";
    $services = $wpdb->get_results($sql, ARRAY_A);
    
    if(count($orderDetails) > 0){
        $htmlmaster .= '
            <div class="order-detail-header">
                <span>Order # '. $order->id .'</span> <span>For delivery on '. $date .' at '. $time .'</span>
            </div>
        ';
        $htmlmaster .= '
            <div class="row three_Year_table">
                <table class="header_table">
                    <tbody>
                      <tr>
                        <td><h4>Order Details</h4></td>
                      </tr>
                      <tr>
                        <th>Vendor\'s Name</th>
                        <th>Date</th>
                      </tr>
                      <tr>
                        <td>'. $orderDetails[0]['post_title'] .'</td>
                        <td>'. $date .'</td>
                      </tr>
                      <tr>
                        <th>Headcount</th>
                        <th>Time</th>
                      </tr>
                      <tr>
                        <td>'. $orderDetails[0]['people'] .'</td>
                        <td>'. $time .'</td>
                      </tr>
                      <tr>
                        <th>Company</th>
                      </tr>
                      <tr>
                        <td>'. $orderDetails[0]['company'] .'</td>
                      </tr>
                      <tr>
                        <th>Address</th>
                      </tr>
                      <tr>
                        <td>'. $orderDetails[0]['floor'] .' '. $orderDetails[0]['street'] .' '. $orderDetails[0]['city'] .' '. $orderDetails[0]['postal_code'] .'</td>
                      </tr>
                    </tbody>
                </table>
            </div>
            <div class="help_box">
                <p>Call +65 8891 3599 if you’re lost / late so we can help!</p>
            </div>
            <div class="three_Year_table" style= "border-color:black; margin-top:100px;">
                <table class="header_table price_table">
                    <tbody>
                      <tr>
                        <th>Qty</th>
                        <th colspan="3">Item</th>
                        <th>Price</th>
                        <th>w/GST</th>
                      </tr>
                    ';

                    foreach ($orderDetails as $orderDetail) {
                        $htmlmaster .=  '<tr>';
                        $htmlmaster .= '<td>'.$orderDetail["quantity"].'</td>';

                        $htmlmaster .= '<td  colspan="3">'.$orderDetail['name'];
                        $modifiers = unserialize($orderDetail['modifier']);
                        if ($modifiers) {
                            $modifierNames = array();
                            foreach ($modifiers as $key => $modifier) {
                                $modifierNames[] = $modifier->itemText;
                            }
                            if (!empty($modifierNames)) {
                                $htmlmaster .= '<br/><sup>('. implode(', ', $modifierNames) .')</sup>';
                            }
                        }
                        $htmlmaster .= '</td>';

                        $htmlmaster .= '<td>SGD</td>';
                        
                        $htmlmaster .= '<td>'.round($orderDetail["line_total"], 2).'</td>';
                        $htmlmaster .=  '</tr>';
                    
                        $item_total += $orderDetail["line_total"];
                    }

                    if(count($services) > 0){
                        foreach ($services as $service) {
                            $htmlmaster .=  '<tr>';
                            $htmlmaster .= '<td>'.$service["quantity"].'</td>';

                            $htmlmaster .= '<td  colspan="3">'.$service['question'];
                            $options = unserialize($service['options']);
                            if ($options) {
                                $optionNames = array();
                                foreach ($options as $key => $option) {
                                    $optionNames[] = $option->name;
                                }
                                if (!empty($optionNames)) {
                                $htmlmaster .= '<br/><sup>('. implode(', ', $optionNames) .')</sup>';
                                }
                            }
                            $htmlmaster .= '</td>';

                            $htmlmaster .= '<td>SGD</td>';
                            
                            $htmlmaster .= '<td>'.round($service["total_price"], 2).'</td>';
                            $htmlmaster .=  '</tr>';
                
                            $item_total += $service["total_price"];
                        }
                    }

                    $htmlmaster .= '
                      <tr>
                        <td></td>
                        <td colspan="3">Tableware<br>Include: Cups</td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3">Sub Total</td>
                        <td>SGD</td>
                        <td>'. round($item_total, 2) .'</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3">Delivery Fee</td>
                        <td>SGD</td>
                        <td>'. $order->delivery_fees .'</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3">Surcharge</td>
                        <td>SGD</td>
                        <td>0</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3">GST (included in price)</td>
                        <td>SGD</td>
                        <td>'. $order->gst .'</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3">Tip</td>
                        <td>SGD</td>
                        <td>0</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3">Total</td>
                        <td>SGD</td>
                        <td>'. $order->total .'</td>
                      </tr>
                    </tbody>
                </table>
            </div>
        ';

        // $htmlmaster .= '<span>Instructions: '.get_delivery_instructions($order->id).'</span><br>';
    } else {
        $htmlmaster .=  "<div class='no-orders'>
            <h1>No Orders Found For This Restaurant</h1>
            <img src='". WYZ_PLUGIN_ROOT_URL."assets/images/Review-catering.svg'>
            <a  href='". home_url('caterers') ."'>Log In</a>
        </div>";
    }
    
    echo json_encode([
        'html' => $htmlmaster
    ]); die;
}

function get_delivery_instructions($orderid) {
  global $wpdb;
  $deliveryTable = $wpdb->prefix . 'wyz_restaurant_order_delivery_details';
  $instructions = $wpdb->get_row("SELECT `instructions` FROM $deliveryTable WHERE order_id = $orderid",ARRAY_A);
  return $instructions['instructions'];
}



/**
 * This function call through AJAX
 * to get restaurant availability according to delivery date
 */
add_action( 'wp_ajax_getRestaurantAvailability', 'getRestaurantAvailability');
add_action( 'wp_ajax_nopriv_getRestaurantAvailability', 'getRestaurantAvailability');
function getRestaurantAvailability() {
    global $wpdb;
    $joinQuery = '';
    $whereQuery = '';
    $dateTime = '';
    $differenceInHours = null;
    $formData = $_REQUEST;
    // $formData = $_POST;

    if (isset($formData['delivery_date']) && !empty($formData['delivery_date'])) {
        $currentDateTime = strtotime("now");
        $dateTime = strtotime($formData['delivery_date']);
        $differenceInHours = ($dateTime - $currentDateTime)/3600;
        $date = date('m/d/Y', $dateTime);

        $dateQuery = '
            INNER JOIN 
            '.$wpdb->prefix.'postmeta as pm1 
            ON (p.ID = pm1.post_id AND pm1.meta_key = "wp_restaurant_closing_date") 
            INNER JOIN 
                '.$wpdb->prefix.'postmeta as pm2 
            ON (p.ID = pm2.post_id AND pm2.meta_key = "wp_restaurant_delivery_days") 
            INNER JOIN 
                '.$wpdb->prefix.'postmeta as pmlt 
            ON (p.ID = pmlt.post_id AND pmlt.meta_key = "wp_restaurant_lead_time") 
        ';

        $whereQuery .= ' AND pm1.meta_value NOT LIKE "%'. $date .'%" ';
        $whereQuery .= ' AND (pmlt.meta_value ="" OR pmlt.meta_value < '. $differenceInHours .')';
    }

    $joinQuery .= $dateQuery;

    $sqlQuery = '
        SELECT 
            p.ID
        FROM '.$wpdb->prefix.'posts as p 
        '. $joinQuery .' 
        WHERE 
            p.id = '. $formData['id'] .' 
            AND p.post_type = "wp_restaurant" 
            AND p.post_status = "publish" 
            '. $whereQuery .'
    '; 

    $restaurant = $wpdb->get_row($sqlQuery);

    echo json_encode([
        // 'formData' => $formData,
        'differenceInHours' => $differenceInHours, 
        'restaurant' => $restaurant ? $restaurant->ID : null, 
        // 'dateTime' => $dateTime, 
        // 'day' => $day, 
        // 'date' => $date, 
        // 'time' => $timeHour, 
        // 'deliveryDays' => unserialize($restaurant->deliveryDays), 
    ]); die;
}