<?php
global $wpdb;
$orderTable    = $wpdb->prefix . 'wyz_restaurant_order';
$deliveryTable = $wpdb->prefix . 'wyz_restaurant_order_delivery_details';
$orderid  = $_POST['id'];
$restro_id  = $_POST['restro_id'];

$results = $wpdb->get_results("SELECT ord.id, ord.user_id, ord.payment_status, ord.total, ord.status, delivery.street, delivery.floor, delivery.company, delivery.postal_code, delivery.city, delivery.delivery_date, delivery.delivery_phone, delivery.people, delivery.email FROM $orderTable AS ord LEFT JOIN $deliveryTable AS delivery ON ord.`id` = delivery.`order_id` WHERE ord.`id` = $orderid");


// Delivery Date
$delivery_date = $results[0]->delivery_date;

//get PAX
$pax = $results[0]->people;

//get first name and last name
$user_info = get_userdata( $results[0]->user_id );
$userFirstName = $user_info->first_name;
$userLastName =  $user_info->last_name;

//get restaurant name
$restaurant_name = get_the_title($restro_id);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>

        .order-email-template{max-width: 980px;width:100%;box-shadow: 0 2px 13px 0px #ccc;margin-top:25px;}
        .mailer-head, .mailer-body, .mailer-footer{padding:10px;}
        .mailer-body h3{font-weight:400;font-size:19px;color:grey;}
        .mailer-body h3 span{font-weight:600;}
        .mailer-body ul li{position:relative;padding:10px;padding-left:50px;list-style: none;}
        .mailer-body ul li img{position: absolute;width:25px;height:25px;left:10px;top:9px;}
        .mailer-footer a{color:#0ab9b1; display: inline-block;border:2px solid #0ab9b1;padding:5px 20px;border-radius:35px;transition: 0.3s all ease-in-out;}
        .mailer-footer a:hover{text-decoration: none;background-color:#0ab9b1;color:#ffffff;}
    </style>
</head>
<body>
    <div class="container">
        <div class="row order-email-template">
            <div class="col-md-12">
                <div class="mailer-head">
                    <a href="https://wyzchef.com/"><img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/wyzchef3.png" alt="WYZchef" id="logo"></a>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="mailer-body">
                    <h3><?php echo $restaurant_name;?> has declined Order # <?php echo $orderid;?></h3>
                    <ul>
                        <li><span class="calender-icon"><img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/calender-icon.png" alt="WYZchef" id="logo"></span><?php echo date('l', strtotime($delivery_date));?> 
                        <span><?php echo date('jS F Y', strtotime($delivery_date));?> at <?php echo date('H:i A', $delivery_date);?> </span></li>
                        <li><span class="map-marker-icon"><img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/maps-and-flags.png" alt="WYZchef" id="logo"></span><span><?Php echo $results[0]->company.", ".$results[0]->street.", ".$results[0]->floor.", ".$results[0]->postal_code.", ".$results[0]->city;?></span></li>
                        <li><span class="dish-icon"><img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/prefrences-icon.png" alt="WYZchef" id="logo"></span><span><?php echo $pax." PAX";?></span></li>
                        <li><span class="invoice-icon"><img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/invoice-icon.png" alt="WYZchef" id="logo"></span><span><?Php echo $results[0]->total;?> SGD</span></li>
                    </ul>
                    <p>The partner is Decline this order.</p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="mailer-footer">
                    <a href="<?php echo site_url();?>/wp-admin/admin.php?page=wyz-chef-orders">Review details</a>
                </div>
            </div>
        </div>
    </div>
</body>
<script
src="https://code.jquery.com/jquery-3.4.1.min.js"
integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
crossorigin="anonymous"></script>
</html>