<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$dir = plugin_dir_url( WYZ_RESTAURANT_PLUGIN_FILE );
wp_enqueue_style( 'front-css',$dir. 'assets/fonts/font-awesome.min.css');
wp_enqueue_script( 'front-script',$dir. 'assets/js/menu.js');

		global $wpdb;
		
		/*Form Submission*/
		if(isset($_POST['postID']) && $_POST['postID'] > 0) {
			// echo '<pre>Debug POST: '; print_r( $_POST ); echo '</pre>'; die;
			$pid = $post_id = $_POST['postID'];

			if (!isset($_POST['wp_restaurant_delivery_everywhere'])) {
				$_POST['wp_restaurant_delivery_everywhere'] = '';
			}

            if (!isset($_POST['terms_conditions'])) {
                $_POST['terms_conditions'] = '1';
            }

			if(isset($_POST['wp_restaurant_email'])) {
				$title     = $_POST['title'];
				$content   = $_POST['description'];
				$post_type = 'wp_restaurant';
				$categoryID = $_POST['wp_restaurant_type'];

				$old_post = array(
					'ID'    => $_POST['postID'],
					'post_title'    => $title,
					'post_content'  => $content,
					// 'post_status'   => 'publish',
                    // 'post_status'   => get_post_status($_POST['postID']),
					'post_type'     => $post_type,
					'post_category' => ($categoryID)
				);

				$result = wp_update_post($old_post);

				for($i=0; $i < sizeof($_FILES['wp_restaurant_gallery']['name']); $i++) {
					$_FILES['image']['name']   = $_FILES['wp_restaurant_gallery']['name'][$i];
					$_FILES['image']['tmp_name']  = $_FILES['wp_restaurant_gallery']['tmp_name'][$i];
					$post_id=$_POST['postID'];
					if($_FILES['image']['name']!='') {
						$upload = wp_upload_bits( $_FILES['image']['name'], null, file_get_contents( $_FILES['image']['tmp_name'] ) );
					    $wp_filetype = wp_check_filetype( basename( $upload['file'] ), null );
					    $wp_upload_dir = wp_upload_dir();
					    $attachment = array(
					        'guid'			 	=> $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $upload['file'] ),
					        'post_mime_type' 	=> $wp_filetype['type'],
					        'post_title' 	 	=> preg_replace('/\.[^.]+$/', '', basename( $upload['file'] )),
					        'post_content' 	 	=> '',
					        'post_status' 	 	=> 'inherit'
					    );

					    $attach_id = wp_insert_attachment( $attachment, $upload['file'], $post_id );
					    require_once(ABSPATH . 'wp-admin/includes/image.php');
					    $attach_data = wp_generate_attachment_metadata( $attach_id, $upload['file'] );
					    wp_update_attachment_metadata( $attach_id, $attach_data );
					    update_post_meta( $post_id, 'wp_restaurant_gallery', $attach_id );
					    $gallarimgID[] = $attach_id;
					}
				}

				$_FILES['image']['name']   = $_FILES['wp_restaurant_logo']['name'];
				$_FILES['image']['tmp_name']  = $_FILES['wp_restaurant_logo']['tmp_name'];
				$post_id=$_POST['postID'];
				if($_FILES['image']['name']!='') {
					$upload = wp_upload_bits( $_FILES['image']['name'], null, file_get_contents( $_FILES['image']['tmp_name'] ) );
				    $wp_filetype = wp_check_filetype( basename( $upload['file'] ), null );
				    $wp_upload_dir = wp_upload_dir();
				    $attachment = array(
				        'guid'			 	=> $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $upload['file'] ),
				        'post_mime_type' 	=> $wp_filetype['type'],
				        'post_title' 	 	=> preg_replace('/\.[^.]+$/', '', basename( $upload['file'] )),
				        'post_content' 	 	=> '',
				        'post_status' 	 	=> 'inherit'
				    );

				    $attach_id = wp_insert_attachment( $attachment, $upload['file'], $post_id );
				    require_once(ABSPATH . 'wp-admin/includes/image.php');
				    $attach_data = wp_generate_attachment_metadata( $attach_id, $upload['file'] );
				    wp_update_attachment_metadata( $attach_id, $attach_data );
				    update_post_meta( $post_id, '_thumbnail_id', $attach_id );
				    $logomgID = $attach_id;
				}
				if($gallarimgID[0]!='') {
					$_POST['wp_restaurant_gallery']= $gallarimgID;
				}
				if($logomgID!='') {
					$_POST['wp_restaurant_logo']= $logomgID;
				}

				$_FILES['image']['name']   = $_FILES['wp_restaurant_list_img']['name'];
				$_FILES['image']['tmp_name']  = $_FILES['wp_restaurant_list_img']['tmp_name'];
				$post_id=$_POST['postID'];
				if($_FILES['image']['name']!='') {
					$upload = wp_upload_bits( $_FILES['image']['name'], null, file_get_contents( $_FILES['image']['tmp_name'] ) );
				    $wp_filetype = wp_check_filetype( basename( $upload['file'] ), null );
				    $wp_upload_dir = wp_upload_dir();
				    $attachment = array(
				        'guid'			 	=> $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $upload['file'] ),
				        'post_mime_type' 	=> $wp_filetype['type'],
				        'post_title' 	 	=> preg_replace('/\.[^.]+$/', '', basename( $upload['file'] )),
				        'post_content' 	 	=> '',
				        'post_status' 	 	=> 'inherit'
				    );

				    $attach_id = wp_insert_attachment( $attachment, $upload['file'], $post_id );
				    require_once(ABSPATH . 'wp-admin/includes/image.php');
				    $attach_data = wp_generate_attachment_metadata( $attach_id, $upload['file'] );
				    wp_update_attachment_metadata( $attach_id, $attach_data );
				    update_post_meta( $post_id, 'wp_restaurant_list_img', $attach_id );
				    $restaurantListImg = $attach_id;
				}
				if(!empty($restaurantListImg)) {
					$_POST['wp_restaurant_list_img']= $restaurantListImg;
				}

				foreach ($_POST as $key => $value) {
					if(
						($key != 'title')
						&& ($key != 'description')
						&& ($key != 'postID')
						&& ($key != 'restaurant_services')
						&& ($key != 'menu')
					) {
					    update_post_meta($pid, $key, $value);
					}
				}

				### Category Post Relation ###
				if(is_array($categoryID)) {
					for($k=0;$k<sizeof($categoryID);$k++) {
						$wpdb->query( $wpdb->prepare("INSERT IGNORE INTO ".$wpdb->prefix."term_relationships (object_id, term_taxonomy_id) VALUES ( %d, %d)",array($pid,$categoryID[$k])));
					}
				}
				wp_set_post_categories($pid, $post_categories, $append );
			}

			// $wpdb->show_errors();
			$deletedOptions = trim($_POST['deletedOptions']);
			if ($deletedOptions) {
				$wpdb->query("
					DELETE FROM {$wpdb->prefix}wyz_restaurant_service_options 
					WHERE id IN($deletedOptions)
				");
			}

			$deletedServices = trim($_POST['deletedServices']);
			if ($deletedServices) {
				$wpdb->query("
					DELETE FROM {$wpdb->prefix}wyz_restaurant_services 
					WHERE id IN($deletedServices)
				");
			}

			$restaurantServices = json_decode(str_replace("\\", "",$_POST['restaurant_services']));
			if ($restaurantServices) {
				foreach ($restaurantServices as $key1 => $service) {
					if (empty($service)) {
						continue;
					}

					if (empty($service->id)) {
						$wpdb->insert(
							"{$wpdb->prefix}wyz_restaurant_services",
							array(
								'type' => $service->type,
								'quantityRequired' => $service->quantityRequired,
								'question' => htmlspecialchars(str_replace('\\', '', $service->question), ENT_QUOTES),
								'restaurant_id' => $pid,
							)
						);

						$serviceId = $wpdb->insert_id;
						// $restaurantServices[$key1]->id = $serviceId;
						
						foreach ($service->options as $key2 => $option) {
							$wpdb->insert(
								"{$wpdb->prefix}wyz_restaurant_service_options",
								array(
									'name' => htmlspecialchars(str_replace('\\', '', $option->name), ENT_QUOTES),
									'price' => $option->price,
									'tax' => $option->tax,
									'total' => $option->total,
									'service_id' => $serviceId,
								)
							);

							// $restaurantServices[$key1]->options[$key2]->id = $wpdb->insert_id;
						}
					} else {
						$wpdb->update(
							"{$wpdb->prefix}wyz_restaurant_services",
							array(
								'type' => $service->type,
								'quantityRequired' => $service->quantityRequired,
								'question' => htmlspecialchars(str_replace('\\', '', $service->question), ENT_QUOTES),
							),
							array(
								'id' => $service->id
							)
						);
						
						foreach ($service->options as $key2 => $option) {
							if (empty($option->id)) {
								$wpdb->insert(
									"{$wpdb->prefix}wyz_restaurant_service_options",
									array(
										'name' => htmlspecialchars(str_replace('\\', '', $option->name), ENT_QUOTES),
										'price' => $option->price,
										'tax' => $option->tax,
										'total' => $option->total,
										'service_id' => $service->id,
									)
								);

								// $restaurantServices[$key1]->options[$key2]->id = $wpdb->insert_id;
							} else {
								$wpdb->update(
									"{$wpdb->prefix}wyz_restaurant_service_options",
									array(
										'name' => htmlspecialchars(str_replace('\\', '', $option->name), ENT_QUOTES),
										'price' => $option->price,
										'tax' => $option->tax,
										'total' => $option->total,
										'service_id' => $service->id,
									),
									array(
										'id' => $option->id
									)
								);
							}
						}
					}
				}
			}

			if (isset($_POST['menu']) && count($_POST['menu'])) {
				foreach ($_POST['menu'] as $menuId => $dishes) {
					foreach ($dishes as $key => $dish) {
						if (isset($dish['deletedishId']) && !empty($dish['deletedishId'])) {
                            $selectQuery = "
                                SELECT dm.id FROM `{$wpdb->prefix}wyz_restaurant_dish_modifiers` dm
                                INNER JOIN `{$wpdb->prefix}wyz_restaurant_dishes` d ON dm.dish_id = d.id
                                WHERE d.id = {$dish[deletedishId]}
                            ";
                            $dishModifierIds = $wpdb->get_col($selectQuery);

                            foreach ($dishModifierIds as $key => $modifierId) {
                                $wpdb->delete(
                                    $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
                                    array(
                                        'modifier_id' => $modifierId,
                                    )
                                );
                            }

                            $wpdb->delete(
                                $wpdb->prefix .'wyz_restaurant_dish_modifiers',
                                array(
                                    'dish_id' => $dish['deletedishId'],
                                )
                            );

							$wpdb->delete(
								$wpdb->prefix .'wyz_restaurant_dishes',
								array(
									'id' => $dish['deletedishId'],
								)
							);
						} elseif (!isset($dish['dishId']) || empty($dish['dishId'])) {
							$wpdb->insert(
								$wpdb->prefix .'wyz_restaurant_dishes',
								array(
									'name' => htmlspecialchars(str_replace('\\', '', $dish['name']), ENT_QUOTES),
									'description' => htmlspecialchars(str_replace('\\', '', $dish['description']), ENT_QUOTES),
									'image' => $dish['image'],
									'price' => $dish['price'],
                                    'tax' => $dish['tax'],
									'min_quantity' => $dish['min_quantity'],
									'min_guest_serve' => $dish['min_guest_serve'],
									'max_guest_serve' => $dish['max_guest_serve'],
									'lead_time' => $dish['lead_time'],
									'tags' => $dish['tags'],
                                    'having_modifiers' => $dish['having_modifiers'],
									'menu_id' => $dish['menuId'],
									'restaurant_id' => $pid,
									'status' => 1,
								)
							);
                            $dishId = $wpdb->insert_id;

                            if (isset($dish['modifier']) && !empty($dish['modifier'])) {
                                foreach ($dish['modifier'] as $key => $modifier) {
                                    $wpdb->insert(
                                        $wpdb->prefix .'wyz_restaurant_dish_modifiers',
                                        array(
                                            'prompt_text' => $modifier['promptText'],
                                            'modifier_type' => $modifier['modifierType'],
                                            'min' => $modifier['min'],
                                            'max' => $modifier['max'],
                                            'dish_id' => $dishId,
                                        )
                                    );
                                    $modifierId = $wpdb->insert_id;

                                    if (isset($modifier['itemText']) && !empty($modifier['itemText'])) {
                                        foreach ($modifier['itemText'] as $key => $itemText) {
                                            $modifierItemId = $wpdb->insert(
                                                $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
                                                array(
                                                    'text' => $itemText,
                                                    'price' => $modifier['itemPrice'][$key],
                                                    'tax' => $modifier['itemTax'][$key],
                                                    'total' => $modifier['itemTotal'][$key],
                                                    'modifier_id' => $modifierId,
                                                )
                                            );
                                        }
                                    }
                                }
                            }
						} else {
							$wpdb->update(
								$wpdb->prefix .'wyz_restaurant_dishes',
								array(
									'name' => htmlspecialchars(str_replace('\\', '', $dish['name']), ENT_QUOTES),
									'description' => htmlspecialchars(str_replace('\\', '', $dish['description']), ENT_QUOTES),
									'image' => $dish['image'],
									'price' => $dish['price'],
                                    'tax' => $dish['tax'],
									'min_quantity' => $dish['min_quantity'],
									'min_guest_serve' => $dish['min_guest_serve'],
									'max_guest_serve' => $dish['max_guest_serve'],
									'lead_time' => $dish['lead_time'],
									'tags' => $dish['tags'],
                                    'having_modifiers' => $dish['having_modifiers'],
									'menu_id' => $dish['menuId'],
									'restaurant_id' => $pid,
									'status' => 1,
								),
								array(
									'id' => $dish['dishId'],
								)
							);

                            /*if (isset($dish['modifier']) && !empty($dish['modifier'])) {
                                foreach ($dish['modifier'] as $key => $modifier) {
                                    $wpdb->insert(
                                        $wpdb->prefix .'wyz_restaurant_dish_modifiers',
                                        array(
                                            'prompt_text' => $modifier['promptText'],
                                            'modifier_type' => $modifier['modifierType'],
                                            'dish_id' => $dish['dishId'],
                                        )
                                    );
                                    $modifierId = $wpdb->insert_id;

                                    if (isset($modifier['itemText']) && !empty($modifier['itemText'])) {
                                        foreach ($modifier['itemText'] as $key => $itemText) {
                                            $modifierItemId = $wpdb->insert(
                                                $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
                                                array(
                                                    'text' => $itemText,
                                                    'price' => $modifier['itemPrice'][$key],
                                                    'tax' => $modifier['itemTax'][$key],
                                                    'total' => $modifier['itemTotal'][$key],
                                                    'modifier_id' => $modifierId,
                                                )
                                            );
                                        }
                                    }
                                }
                            }*/

                            if (isset($dish['deleteModifier']) && !empty($dish['deleteModifier'])) {
                                foreach ($dish['deleteModifier'] as $key => $deleteModifier) {
                                    $wpdb->delete(
                                        $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
                                        array(
                                            'modifier_id' => $deleteModifier,
                                        )
                                    );

                                    $wpdb->delete(
                                        $wpdb->prefix .'wyz_restaurant_dish_modifiers',
                                        array(
                                            'id' => $itemDeleteId,
                                        )
                                    );
                                }
                            }

                            if (isset($dish['modifier']) && !empty($dish['modifier'])) {
                                foreach ($dish['modifier'] as $key => $modifier) {
                                    if (!isset($modifier['id']) || empty($modifier['id'])) {
                                        $wpdb->insert(
                                            $wpdb->prefix .'wyz_restaurant_dish_modifiers',
                                            array(
                                                'prompt_text' => $modifier['promptText'],
                                                'modifier_type' => $modifier['modifierType'],
                                                'min' => $modifier['min'],
                                                'max' => $modifier['max'],
                                                'dish_id' => $dish['dishId'],
                                            )
                                        );
                                        $modifierId = $wpdb->insert_id;
                                    } else {
                                        $wpdb->update(
                                            $wpdb->prefix .'wyz_restaurant_dish_modifiers',
                                            array(
                                                'prompt_text' => $modifier['promptText'],
                                                'modifier_type' => $modifier['modifierType'],
                                                'min' => $modifier['min'],
                                                'max' => $modifier['max'],
                                                // 'dish_id' => $dishId,
                                            ),
                                            array(
                                                'id' => $modifier['id'],
                                            )
                                        );
                                        $modifierId = $modifier['id'];
                                    }

                                    if (isset($modifier['itemDeleteId']) && !empty($modifier['itemDeleteId'])) {
                                        foreach ($modifier['itemDeleteId'] as $key => $itemDeleteId) {
                                            $wpdb->delete(
                                                $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
                                                array(
                                                    'id' => $itemDeleteId,
                                                )
                                            );
                                        }
                                    }

                                    if (isset($modifier['itemText']) && !empty($modifier['itemText'])) {
                                        foreach ($modifier['itemText'] as $key => $itemText) {
                                            if (!isset($modifier['itemId'][$key]) || empty($modifier['itemId'][$key])) {
                                                $wpdb->insert(
                                                    $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
                                                    array(
                                                        'text' => $itemText,
                                                        'price' => $modifier['itemPrice'][$key],
                                                        'tax' => $modifier['itemTax'][$key],
                                                        'total' => $modifier['itemTotal'][$key],
                                                        'modifier_id' => $modifierId,
                                                    )
                                                );
                                            } else {
                                                $wpdb->update(
                                                    $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
                                                    array(
                                                        'text' => $itemText,
                                                        'price' => $modifier['itemPrice'][$key],
                                                        'tax' => $modifier['itemTax'][$key],
                                                        'total' => $modifier['itemTotal'][$key],
                                                        // 'modifier_id' => $modifierId,
                                                    ),
                                                    array(
                                                        'id' => $modifier['itemId'][$key],
                                                    )
                                                );
                                            }
                                        }
                                    }
                                }
                            }
						}
					}
				}
			}

			if (isset($_POST['deleteMenuId']) && count($_POST['deleteMenuId'])) {
				foreach ($_POST['deleteMenuId'] as $key => $menuId) {
                    $selectQuery = "
                        SELECT dm.id FROM `{$wpdb->prefix}wyz_restaurant_dish_modifiers` dm
                        INNER JOIN `{$wpdb->prefix}wyz_restaurant_dishes` d ON dm.dish_id = d.id
                        WHERE d.menu_id = {$menuId} AND d.restaurant_id = {$pid}
                    ";
                    $dishModifierIds = $wpdb->get_col($selectQuery);

                    foreach ($dishModifierIds as $key => $modifierId) {
                        $wpdb->delete(
                            $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
                            array(
                                'modifier_id' => $modifierId,
                            )
                        );
                    }

                    $wpdb->delete(
                        $wpdb->prefix .'wyz_restaurant_dish_modifiers',
                        array(
                            'dish_id' => $dish['deletedishId'],
                        )
                    );

					$wpdb->delete(
						$wpdb->prefix .'wyz_restaurant_dishes',
						array(
							'menu_id' => $menuId,
							'restaurant_id' => $pid,
						)
					);
				}
			}
		} else {
			if(isset($_POST['author_email'])) {
				$subject = "Password setting";
				$userdata = array(
				    'user_login'  =>  $_POST['author_email'],
				    'user_email'  =>  $_POST['author_email'],
				    'user_url'    =>  $website,
				    'user_pass'   =>  NULL
				);
				$user_id = wp_insert_user( $userdata ) ;
				$user = get_user_by('id', $user_id);
				$firstname = $user->first_name;
			    $email = $user->user_email;
			    $adt_rp_key = get_password_reset_key( $user );
			    $user_login = $user->user_login;
				// $headers = array( 'Content-type: text/html' );
				$headers = array(
	                'Content-Type: text/html; charset=UTF-8',
	                'From: WYZchef <contact@wyzchef.com>',
	            );
				if(!is_object($user_id))
					$rp_link = '<a href="'. home_url() .'/reset-password?keyid='.$user_id.'">Password Setting Link</a>';
				$message = 'Hi User please set your password  :'. $rp_link ;

	  			$email = $_POST['author_email'];
				$to = get_option('admin_email');
				$subject = "Password setting";

				$sent=false;
				add_filter('wp_mail_content_type', function( $content_type ) {
	            	return 'text/html';
				});
			 	$sent = wp_mail($email, $subject, $message,$headers);
			  	if($sent) {
					echo "Restaurant Updated Successfully";
					echo $sent."mail sent";
			  	} else {
					echo "Restaurant Updated Successfully";
			  	}
			}
			if(isset($_POST['wp_restaurant_email'])) {
				$title     = $_POST['title'];
				$content   = $_POST['description'];
				$post_type = 'wp_restaurant';
				$categoryID = $_POST['wp_restaurant_type'];
				$new_post = array(
					'post_title'    => $title,
					'post_content'  => $content,
					'post_status'   => 'draft',
					'post_type'     => $post_type,
					'post_category' => ($categoryID),
				);

				$pid = wp_insert_post($new_post);

				for($i=0;$i<sizeof($_FILES['wp_restaurant_gallery']['name']);$i++) {
					$_FILES['image']['name']   = $_FILES['wp_restaurant_gallery']['name'][$i];
					$_FILES['image']['tmp_name']  = $_FILES['wp_restaurant_gallery']['tmp_name'][$i];
					$post_id=$pid;
					if($_FILES['image']['name']!='') {
						$upload = wp_upload_bits( $_FILES['image']['name'], null, file_get_contents( $_FILES['image']['tmp_name'] ) );
					    $wp_filetype = wp_check_filetype( basename( $upload['file'] ), null );
					    $wp_upload_dir = wp_upload_dir();
					    $attachment = array(
					        'guid'			 	=> $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $upload['file'] ),
					        'post_mime_type' 	=> $wp_filetype['type'],
					        'post_title' 	 	=> preg_replace('/\.[^.]+$/', '', basename( $upload['file'] )),
					        'post_content' 	 	=> '',
					        'post_status' 	 	=> 'inherit'
					    );

					    $attach_id = wp_insert_attachment( $attachment, $upload['file'], $post_id );
					    require_once(ABSPATH . 'wp-admin/includes/image.php');
					    $attach_data = wp_generate_attachment_metadata( $attach_id, $upload['file'] );
					    wp_update_attachment_metadata( $attach_id, $attach_data );
					    update_post_meta( $post_id, 'wp_restaurant_gallery', $attach_id );
					    $gallarimgID[] = $attach_id;
					}
				}

				$_FILES['image']['name']   = $_FILES['wp_restaurant_logo']['name'];
				$_FILES['image']['tmp_name']  = $_FILES['wp_restaurant_logo']['tmp_name'];
				$post_id=$pid;
				if($_FILES['image']['name']!='') {
					$upload = wp_upload_bits( $_FILES['image']['name'], null, file_get_contents( $_FILES['image']['tmp_name'] ) );
				    $wp_filetype = wp_check_filetype( basename( $upload['file'] ), null );
				    $wp_upload_dir = wp_upload_dir();
				    $attachment = array(
				        'guid'			 	=> $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $upload['file'] ),
				        'post_mime_type' 	=> $wp_filetype['type'],
				        'post_title' 	 	=> preg_replace('/\.[^.]+$/', '', basename( $upload['file'] )),
				        'post_content' 	 	=> '',
				        'post_status' 	 	=> 'inherit'
				    );

				    $attach_id = wp_insert_attachment( $attachment, $upload['file'], $post_id );
				    require_once(ABSPATH . 'wp-admin/includes/image.php');
				    $attach_data = wp_generate_attachment_metadata( $attach_id, $upload['file'] );
				    wp_update_attachment_metadata( $attach_id, $attach_data );
				    update_post_meta( $post_id, '_thumbnail_id', $attach_id );
				    $logomgID = $attach_id;
				}
				$_POST['wp_restaurant_gallery'] = $gallarimgID;
				$_POST['wp_restaurant_logo'] = $logomgID;

				$_FILES['image']['name']   = $_FILES['wp_restaurant_list_img']['name'];
				$_FILES['image']['tmp_name']  = $_FILES['wp_restaurant_list_img']['tmp_name'];
				$post_id=$pid;
				if($_FILES['image']['name']!='') {
					$upload = wp_upload_bits( $_FILES['image']['name'], null, file_get_contents( $_FILES['image']['tmp_name'] ) );
				    $wp_filetype = wp_check_filetype( basename( $upload['file'] ), null );
				    $wp_upload_dir = wp_upload_dir();
				    $attachment = array(
				        'guid'			 	=> $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $upload['file'] ),
				        'post_mime_type' 	=> $wp_filetype['type'],
				        'post_title' 	 	=> preg_replace('/\.[^.]+$/', '', basename( $upload['file'] )),
				        'post_content' 	 	=> '',
				        'post_status' 	 	=> 'inherit'
				    );

				    $attach_id = wp_insert_attachment( $attachment, $upload['file'], $post_id );
				    require_once(ABSPATH . 'wp-admin/includes/image.php');
				    $attach_data = wp_generate_attachment_metadata( $attach_id, $upload['file'] );
				    wp_update_attachment_metadata( $attach_id, $attach_data );
				    update_post_meta( $post_id, 'wp_restaurant_list_img', $attach_id );
				    $restaurantListImg = $attach_id;
				}
				$_POST['wp_restaurant_list_img'] = $restaurantListImg;

				foreach ($_POST as $key => $value) {
					if(
						($key != 'title')
						&& ($key != 'description')
						&& ($key != 'restaurant_services')
						&& ($key != 'menu')
					) {
						add_post_meta($pid, $key, $value, true);
					}
				}

				/* Category Post Relation */
				if(is_array($categoryID)) {
					for($k=0;$k<sizeof($categoryID);$k++) {
						$wpdb->query( $wpdb->prepare("INSERT INTO  ".$wpdb->prefix."term_relationships (object_id, term_taxonomy_id) VALUES ( %d, %d)",array($pid,$categoryID[$k])));
					}
				}
				wp_set_post_categories($pid, $post_categories, $append );

				$restaurantServices = json_decode(str_replace("\\", "",$_POST['restaurant_services']));
				if ($restaurantServices) {
					foreach ($restaurantServices as $key => $service) {
						if (empty($service)) {
							continue;
						}

						$wpdb->insert(
							"{$wpdb->prefix}wyz_restaurant_services",
							array(
								'type' => $service->type,
								'quantityRequired' => $service->quantityRequired,
								'question' => htmlspecialchars(str_replace('\\', '', $service->question), ENT_QUOTES),
								'restaurant_id' => $pid,
							)
						);

						$serviceId = $wpdb->insert_id;
						
						foreach ($service->options as $key => $option) {
							$wpdb->insert(
								"{$wpdb->prefix}wyz_restaurant_service_options",
								array(
									'name' => htmlspecialchars(str_replace('\\', '', $option->name), ENT_QUOTES),
									'price' => $option->price,
									'tax' => $option->tax,
									'total' => $option->total,
									'service_id' => $serviceId,
								)
							);
						}
					}
				}

				if (isset($_POST['menu']) && count($_POST['menu'])) {
					foreach ($_POST['menu'] as $menuId => $dishes) {
						foreach ($dishes as $key => $dish) {
							$wpdb->insert(
								$wpdb->prefix .'wyz_restaurant_dishes',
								array(
									'name' => htmlspecialchars(str_replace('\\', '', $dish['name']), ENT_QUOTES),
									'description' => htmlspecialchars(str_replace('\\', '', $dish['description']), ENT_QUOTES),
									'image' => $dish['image'],
									'price' => $dish['price'],
                                    'tax' => $dish['tax'],
									'min_quantity' => $dish['min_quantity'],
									'min_guest_serve' => $dish['min_guest_serve'],
									'max_guest_serve' => $dish['max_guest_serve'],
									'lead_time' => $dish['lead_time'],
									'tags' => $dish['tags'],
                                    'having_modifiers' => $dish['having_modifiers'],
									'menu_id' => $dish['menuId'],
									'restaurant_id' => $pid,
									'status' => 1,
								)
							);
                            $dishId = $wpdb->insert_id;

                            if (isset($dish['modifier']) && !empty($dish['modifier'])) {
                                foreach ($dish['modifier'] as $key => $modifier) {
                                    $wpdb->insert(
                                        $wpdb->prefix .'wyz_restaurant_dish_modifiers',
                                        array(
                                            'prompt_text' => $modifier['promptText'],
                                            'modifier_type' => $modifier['modifierType'],
                                            'min' => $modifier['min'],
                                            'max' => $modifier['max'],
                                            'dish_id' => $dishId,
                                        )
                                    );
                                    $modifierId = $wpdb->insert_id;

                                    if (isset($modifier['itemText']) && !empty($modifier['itemText'])) {
                                        foreach ($modifier['itemText'] as $key => $itemText) {
                                            $modifierItemId = $wpdb->insert(
                                                $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
                                                array(
                                                    'text' => $itemText,
                                                    'price' => $modifier['itemPrice'][$key],
                                                    'tax' => $modifier['itemTax'][$key],
                                                    'total' => $modifier['itemTotal'][$key],
                                                    'modifier_id' => $modifierId,
                                                )
                                            );
                                        }
                                    }
                                }
                            }
						}
					}
				}
				add_post_meta($pid, "wyz_restaurant_avg_rating", '');
			}
		}
		/*if (isset($_POST) && count($_POST) > 1) {
			echo '
				<script type="text/javascript">
					window.location.href = "'. site_url('restaurant-dashboard') .'";
				</script>
			';
		}*/
		/*Form Submission*/
		$dataRestaurantType = get_terms( array(
		    'taxonomy' => 'restaurant_type',
		    'hide_empty' => false,
		) );

		$datadataRestaurantCategory = get_terms( array(
		    'taxonomy' => 'restaurant_category',
		    'hide_empty' => false,
		) );
		/*	wp_enqueue_style('custom', WYZ_PLUGIN_ROOT_URL .'assets/css/custom.css');*/
		wp_enqueue_style('select2', WYZ_PLUGIN_ROOT_URL .'assets/css/select-2min.css');
		wp_enqueue_script('select2js', WYZ_PLUGIN_ROOT_URL .'assets/js/select-2min.js');
		?>
		<script type="text/javascript">
			jQuery(document).ready(function() {
		    	jQuery('.selectpicker').select2();
			});
		</script>

		<?php
		/*Form Start*/
		// $menuSection =wyz_chef_dish::wp_form_shortcode();
		$current_user = wp_get_current_user();

		//echo "<pre>";
		//print_r($current_user);
		$htmlmaster = '';
		$html1 = '';
		$html2 = '';
		$htmlmaster.= '
			<style>
                .header-image{display:none;}
                .tabcontent {display: none;}
                .tabcontent.active {display: block;}
            </style>
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARteWOCnYMfmc4hP7xyZpLVulT7fG_Cn4&libraries=places,drawing"></script>
			<form id="submissionForm" action="'. get_the_permalink() . (!empty($_SERVER['QUERY_STRING']) ? "?{$_SERVER[QUERY_STRING]}" : '') .'" method="POST" class="restaurant-listings-form" onsubmit="return validationForm()" enctype="multipart/form-data" novalidate>
			<div class="side-menu-button">
				<span class="one"></span>
				<span class="two"></span>
				<span class="three"></span>
			</div>
		';

		if(isset($_GET['user_id']) && !empty($_GET['user_id'])){
          $usr_id = $_GET['user_id'];

          if($usr_id != 0) {
          	if ( in_array( 'administrator', (array) $current_user->roles ) ) {
          		   $restaurantID = $wpdb->prepare("SELECT * FROM $wpdb->posts WHERE post_author='$usr_id' AND post_type='wp_restaurant' AND post_title !='Auto Draft' AND post_author!='0' ORDER BY ID DESC LIMIT 1",'');
			$rID= $wpdb->get_col($restaurantID);

			if (isset($rID[0])) {
				$post = get_post($rID[0]);
				$wp_contact_person_name = get_post_meta($rID[0],'wp_contact_person_name',true);
				$wp_restaurant_email = get_post_meta($rID[0],'wp_restaurant_email',true);
				$wp_restaurant_phone = get_post_meta($rID[0],'wp_restaurant_phone',true);
				$wp_restaurant_address = get_post_meta($rID[0],'wp_restaurant_address',true);
				$wp_restaurant_type = get_post_meta($rID[0],'wp_restaurant_type',true);
				$wp_restaurant_category = get_post_meta($rID[0],'wp_restaurant_category',true);
				$wp_restaurant_logo = get_post_meta($rID[0],'wp_restaurant_logo',true);
				$wp_restaurant_gallery = get_post_meta($rID[0],'wp_restaurant_gallery',true);
				$wp_restaurant_list_img = get_post_meta($rID[0],'wp_restaurant_list_img',true);
				$wp_restaurant_website = get_post_meta($rID[0],'wp_restaurant_website',true);
				$wp_restaurant_tagline = get_post_meta($rID[0],'wp_restaurant_tagline',true);
				$wp_restaurant_facebook = get_post_meta($rID[0],'wp_restaurant_facebook',true);
				$wp_restaurant_instagram = get_post_meta($rID[0],'wp_restaurant_instagram',true);
				$wp_restaurant_opening_days = get_post_meta($rID[0],'wp_restaurant_opening_days',true);
				$wp_restaurant_delivery_days = get_post_meta($rID[0],'wp_restaurant_delivery_days',true);
				$wp_restaurant_pickup_days = get_post_meta($rID[0],'wp_restaurant_pickup_days',true);
				$wp_restaurant_closing_date = get_post_meta($rID[0],'wp_restaurant_closing_date',true);


				$wp_restaurant_minimum_order_amount = get_post_meta($rID[0],'wp_restaurant_minimum_order_amount',true);
				$wp_restaurant_lead_time = get_post_meta($rID[0],'wp_restaurant_lead_time',true);
				$wp_restaurant_delivery_fee = get_post_meta($rID[0],'wp_restaurant_delivery_fee',true);
                $wp_restaurant_delivery = get_post_meta($rID[0],'wp_restaurant_delivery',true);
				$pickup_option = get_post_meta($rID[0],'pickup_option',true);
				$wp_restaurant_delivery_area = get_post_meta($rID[0],'wp_restaurant_delivery_pincodes',true);
				$wp_restaurant_delivery_everywhere = get_post_meta($rID[0],'wp_restaurant_delivery_everywhere',true);
                $terms_conditions = get_post_meta($rID[0],'terms_conditions',true);

				$bank_name = get_post_meta($rID[0],'bank_name',true);
				$bank_code = get_post_meta($rID[0],'bank_code',true);
				$account_name = get_post_meta($rID[0],'account_name',true);
				$account_number = get_post_meta($rID[0],'account_number',true);
				$swift_code = get_post_meta($rID[0],'swift_code',true);
				$branch_name = get_post_meta($rID[0],'branch_name',true);
				$branch_code = get_post_meta($rID[0],'branch_code',true);
				$preferred_mode = get_post_meta($rID[0],'preferred_mode',true);
				$html1 .='<input type="hidden" name="postID" class="" value="'.$rID[0].'">';
			}
		}
		 else{

           echo "You are not Eligible for view this Directly";

		 }

		  }



		}else{
         $usrID = $current_user->ID;
         if($usrID != 0) {
			$restaurantID = $wpdb->prepare("SELECT * FROM $wpdb->posts WHERE post_author='$usrID' AND post_type='wp_restaurant' AND post_title !='Auto Draft' AND post_author!='0' ORDER BY ID DESC LIMIT 1",'');
			$rID= $wpdb->get_col($restaurantID);

			if (isset($rID[0])) {
				$post = get_post($rID[0]);
				$wp_contact_person_name = get_post_meta($rID[0],'wp_contact_person_name',true);
				$wp_restaurant_email = get_post_meta($rID[0],'wp_restaurant_email',true);
				$wp_restaurant_phone = get_post_meta($rID[0],'wp_restaurant_phone',true);
				$wp_restaurant_address = get_post_meta($rID[0],'wp_restaurant_address',true);
				$wp_restaurant_type = get_post_meta($rID[0],'wp_restaurant_type',true);
				$wp_restaurant_category = get_post_meta($rID[0],'wp_restaurant_category',true);
				$wp_restaurant_logo = get_post_meta($rID[0],'wp_restaurant_logo',true);
				$wp_restaurant_gallery = get_post_meta($rID[0],'wp_restaurant_gallery',true);
				$wp_restaurant_list_img = get_post_meta($rID[0],'wp_restaurant_list_img',true);
				$wp_restaurant_website = get_post_meta($rID[0],'wp_restaurant_website',true);
				$wp_restaurant_tagline = get_post_meta($rID[0],'wp_restaurant_tagline',true);
				$wp_restaurant_facebook = get_post_meta($rID[0],'wp_restaurant_facebook',true);
				$wp_restaurant_instagram = get_post_meta($rID[0],'wp_restaurant_instagram',true);
				$wp_restaurant_opening_days = get_post_meta($rID[0],'wp_restaurant_opening_days',true);
				$wp_restaurant_delivery_days = get_post_meta($rID[0],'wp_restaurant_delivery_days',true);
				$wp_restaurant_pickup_days = get_post_meta($rID[0],'wp_restaurant_pickup_days',true);
				$wp_restaurant_closing_date = get_post_meta($rID[0],'wp_restaurant_closing_date',true);


				$wp_restaurant_minimum_order_amount = get_post_meta($rID[0],'wp_restaurant_minimum_order_amount',true);
				$wp_restaurant_lead_time = get_post_meta($rID[0],'wp_restaurant_lead_time',true);
				$wp_restaurant_delivery_fee = get_post_meta($rID[0],'wp_restaurant_delivery_fee',true);
                $wp_restaurant_delivery = get_post_meta($rID[0],'wp_restaurant_delivery',true);
				$pickup_option = get_post_meta($rID[0],'pickup_option',true);
				$wp_restaurant_delivery_area = get_post_meta($rID[0],'wp_restaurant_delivery_pincodes',true);
				$wp_restaurant_delivery_everywhere = get_post_meta($rID[0],'wp_restaurant_delivery_everywhere',true);
                $terms_conditions = get_post_meta($rID[0],'terms_conditions',true);

				$bank_name = get_post_meta($rID[0],'bank_name',true);
				$bank_code = get_post_meta($rID[0],'bank_code',true);
				$account_name = get_post_meta($rID[0],'account_name',true);
				$account_number = get_post_meta($rID[0],'account_number',true);
				$swift_code = get_post_meta($rID[0],'swift_code',true);
				$branch_name = get_post_meta($rID[0],'branch_name',true);
				$branch_code = get_post_meta($rID[0],'branch_code',true);
				$preferred_mode = get_post_meta($rID[0],'preferred_mode',true);
				$html1 .='<input type="hidden" name="postID" class="" value="'.$rID[0].'">';
			}
		}

		}





		if (!isset($rID[0]) || empty($rID[0])) {
			$post = new stdClass();
			$post->post_title = '';
			$post->post_content = '';
			$wp_contact_person_name = '';
			$wp_restaurant_email = '';
			$wp_restaurant_phone = '';
			$wp_restaurant_address = '';
			$wp_restaurant_type = '';
			$wp_restaurant_category = '';
			$wp_restaurant_logo = '';
			$wp_restaurant_gallery = '';
			$wp_restaurant_list_img = '';
			$wp_restaurant_website = '';
			$wp_restaurant_tagline = '';
			$wp_restaurant_facebook = '';
			$wp_restaurant_instagram = '';
			// $wp_restaurant_twitter = '';
			$wp_restaurant_opening_days = '';
			$wp_restaurant_delivery_days = '';
			$wp_restaurant_pickup_days = '';
			$wp_restaurant_closing_date = '';
			// $wp_restaurant_delivery_days = '';
			// $wp_restaurant_delivery_hours = '';
			// $wp_restaurant_price_range = '';
			// $wp_restaurant_video = '';

			$wp_restaurant_minimum_order_amount = '';
			$wp_restaurant_lead_time = '';
			// $wp_restaurant_delivery_fee = '';
            $wp_restaurant_delivery = [];
			$pickup_option = '';
			$wp_restaurant_delivery_area = '';
			$wp_restaurant_delivery_everywhere = '';
            $terms_conditions = '1';

			$bank_name = '';
			$bank_code = '';
			$account_name = '';
			$account_number = '';
			$swift_code = '';
			$branch_name = '';
			$branch_code = '';
			$preferred_mode = '';
		}
		$html1.='
			<fieldset>
				<label>Restaurant name</label>
				<div class="field ">
					<input type="text" name="title" class="" value="'.$post->post_title.'" placeholder="Enter the Restaurant name">
				</div>
			</fieldset>
			<fieldset>
				<label>Contact Person name</label>
				<div class="field ">
					<input type="text" name="wp_contact_person_name" class="" placeholder="Enter The Contact Person Name" value="'.$wp_contact_person_name.'">
				</div>
			</fieldset>
			<fieldset>
				<label>Restaurant email</label>
				<div class="field ">
					<input type="email" name="wp_restaurant_email" id="restaurant_name"  class="required-field" placeholder="Enter the Restaurant Email" value="'.($wp_restaurant_email ? $wp_restaurant_email : $current_user->user_email).'">
				</div>
			</fieldset>
			<fieldset>
					<label>Phone</label>
					<div class="field ">
						<input type="text" name="wp_restaurant_phone" id="wp_restaurant_phone" value="'.$wp_restaurant_phone.'">
					</div>
				</fieldset>
			<fieldset>
				<label>Address</label>
				<div class="field ">
                        <span class="maplocatiopn-icon">
                        	<i class="fa fa-map-marker button-0" style="font-size: 30px;"></i>
                        	<input type="text"  name="wp_restaurant_address" class="form-control input-lg input-0 " id="locationTextField" placeholder="Click &#8220;Detect location&#8221;" value="'.$wp_restaurant_address.'" >
                        </span>
				</div>
			</fieldset>
		';
		$html1.='
			<fieldset>
				<label>Restaurant type</label>
				<div class="field ">
					<select name="wp_restaurant_type[]" id="wp_restaurant_type" data-style="btn-default" class="selectpicker form-control" multiple data-max-options="2">';
						for ($i=0;$i<sizeof($dataRestaurantType);$i++) {
							$select="";
							if(is_array($wp_restaurant_type)) {
								for($j=0;$j<sizeof($wp_restaurant_type);$j++) {
								 	if($wp_restaurant_type[$j]==$dataRestaurantType[$i]->term_id) {
								 		$select= "Selected";
								 	}
								}
							}
							$html1 .='<option '.$select.'  value="'.$dataRestaurantType[$i]->term_id.'">'.$dataRestaurantType[$i]->name.'</option>';
						}
					$html1.='</select>
				</div>
			</fieldset>
		';
		$html1.='
			<fieldset>
				<label>Restaurant category</label>
				<div class="field ">
					<select name="wp_restaurant_category[]" id="wp_restaurant_category" data-style="btn-default" class="selectpicker form-control" multiple data-max-options="2">';
						for ($j=0;$j<sizeof($datadataRestaurantCategory);$j++) {
							$select="";
							if(is_array($wp_restaurant_category)) {
								for($k=0;$k<sizeof($wp_restaurant_category);$k++) {
								 	if($wp_restaurant_category[$k]==$datadataRestaurantCategory[$j]->term_id) {
								 		$select= "Selected";
								 	}
								}
							}
							$html1 .='<option '.$select.' value="'.$datadataRestaurantCategory[$j]->term_id.'">'.$datadataRestaurantCategory[$j]->name.'</option>';
						}
					$html1 .='</select>';
				$html1.='</div>
			</fieldset>
		';
	    if (isset($wp_restaurant_logo) && !empty($wp_restaurant_logo)) {
	        $restaurantLogo = wp_get_attachment_thumb_url($wp_restaurant_logo);
	    } else {
	        $restaurantLogo = WYZ_PLUGIN_ROOT_URL . 'assets/images/default-logo.png';
	    }
		$html1.='
			<fieldset>
				<label>Logo <small class="p_size">( Prefered size: <strong>150w X 150h</strong> )</small></label>
				<div class="field ">
					<div id="restaurant-logo-preview">
						<img class="myGallery" width="78" height="78" src="'. $restaurantLogo .'" />
					</div>
					<input type="file" name="wp_restaurant_logo" value="463" id="wp_restaurant_logo" />
				</div>
			</fieldset>
			<fieldset>
				<label>Cover Image <small class="p_size">(Prefered size: <strong>1200w X 800h</strong> )</small></label>
				<div class="field ">
					<div id="restaurant-cover-preview">';
					$restaurantCoverImg = '';
					if(is_array($wp_restaurant_gallery)) {
						for($i=0;$i<sizeof($wp_restaurant_gallery);$i++) {
							$restaurantCoverImg ='<img class="myGallery" width="78" height="78" src="'.wp_get_attachment_url($wp_restaurant_gallery[$i]).'">';
						}
					}

					if (empty($restaurantCoverImg)) {
						$restaurantCoverImg = '<img class="myGallery" width="78" height="78" src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/default-restaurant-cover.png">';
					}
					$html1.= $restaurantCoverImg;

					$html1.='
					</div>
					<input type="file" name="wp_restaurant_gallery[]" id="wp_restaurant_gallery" />
				</div>
			</fieldset>
			<fieldset>
				<label>Detail Image (On Listing Page) <small class="p_size">(Prefered size: <strong>500w X 300h</strong> )</small></label>
				<div class="field ">
					<div id="restaurant-list-preview">';
					if (isset($wp_restaurant_list_img) && !empty($wp_restaurant_list_img)) {
				        $restaurantListImg = wp_get_attachment_thumb_url($wp_restaurant_list_img);
				    } else {
				        $restaurantListImg = WYZ_PLUGIN_ROOT_URL .'assets/images/default-restaurant-list-img.png';
				    }

					$html1.='
						<img class="myGallery" width="78" height="78" src="'. $restaurantListImg .'" />
					</div>
					<input type="file" name="wp_restaurant_list_img" id="wp_restaurant_list_img" />
				</div>
			</fieldset>
			';
			$html1.='
				<fieldset>
					<label>Story</label>
					<div class="field ">
						<textarea name="description" class="editor1" >'.$post->post_content.'</textarea>
					</div>
					<!--<script>CKEDITOR.replace( "description" );</script> -->
				</fieldset>
			';

			$html1.='
				<!-- <fieldset>
					<label>Website</label>
					<div class="field ">
						<input type="text" name="wp_restaurant_website" id="wp_restaurant_website" value="'.$wp_restaurant_website.'">
					</div>
				</fieldset> -->
			';
			$html1 .= '
				<!-- <fieldset>
					<label>Facebook username</label>
					<div class="field ">
						<input type="url" name="wp_restaurant_facebook" id="wp_restaurant_facebook" value="'.$wp_restaurant_facebook.'">
					</div>
				</fieldset> -->
				<!-- <fieldset>
					<label>Instagram username</label>
					<div class="field ">
						<input type="url" name="wp_restaurant_instagram" id="wp_restaurant_instagram" value="'.$wp_restaurant_instagram.'">
					</div>
				</fieldset> -->
			';
			$html1 .= '
				<fieldset>
					<label>Tagline</label>
					<div class="field ">
						<input type="text" name="wp_restaurant_tagline" id="wp_restaurant_tagline" value="'.$wp_restaurant_tagline.'">
					</div>
				</fieldset>
			';
			$weekDays = array(
				'monday' => 'Monday',
				'tuesday' => 'Tuesday',
				'wednesday' => 'Wednesday',
				'thursday' => 'Thursday',
				'friday' => 'Friday',
				'saturday' => 'Saturday',
				'sunday' => 'Sunday',
				'public' => 'Public holidays',
			);
			$hourDropDownOptions = array(
				'0' => '12 AM',
				'1' => '1 AM',
				'2' => '2 AM',
				'3' => '3 AM',
				'4' => '4 AM',
				'5' => '5 AM',
				'6' => '6 AM',
				'7' => '7 AM',
				'8' => '8 AM',
				'9' => '9 AM',
				'10' => '10 AM',
				'11' => '11 AM',
				'12' => '12 PM',
				'13' => '1 PM',
				'14' => '2 PM',
				'15' => '3 PM',
				'16' => '4 PM',
				'17' => '5 PM',
				'18' => '6 PM',
				'19' => '7 PM',
				'20' => '8 PM',
				'21' => '9 PM',
				'22' => '10 PM',
				'23' => '11 PM',
			);
				$html1.='
					<div class="timings">
						<ul class="nav nav-tabs" role="tablist">
				          <li role="tab" class="active" data-tab="tab-1"><a href="javascript:void(0);" aria-controls="home" role="tab" data-toggle="tab">Opening Hours</a>
				          </li>
				          <li role="tab" data-tab="tab-2" ><a href="javascript:void(0);" aria-controls="profile" role="tab" data-toggle="tab">Delivery Hours</a>
				          </li>
				          <li role="tab" data-tab="tab-3"><a href="javascript:void(0);" aria-controls="messages" role="tab" data-toggle="tab">PICK-UP HOURS</a>
				          </li>
				          <li role="tab" data-tab="tab-4"><a href="javascript:void(0);" aria-controls="settings" role="tab" data-toggle="tab">HOLIDAYS</a>
				          </li>
				        </ul>
				        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab-1">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="custom-select-box-contain submit-timings-tabs">
					<fieldset>
						<div class="field ">
							<table class="table-responsive">
								<tr>
						';
						foreach ($weekDays as $dayKey => $dayText) {
							$html1 .= '
								<td>
									<span>'. $dayText .'</span>
									<div class="time-container">
										<a href="javascript:void(0)" class="add-time" data-name="wp_restaurant_opening_days['. $dayKey .']"  id="opening_days_'. $dayKey .'">Add</a>
									';
									if (!empty($wp_restaurant_opening_days && isset($wp_restaurant_opening_days[$dayKey]))) {
								foreach ($wp_restaurant_opening_days[$dayKey]['from'] as $key => $openFrom) {
											$html1 .= '
												<div class="time-wrapper"> From:
												    <select name="wp_restaurant_opening_days['. $dayKey .'][from][]" class="from-hours">
											    ';
											    	foreach ($hourDropDownOptions as $hourKey => $hourText) {
											    		$selected = '';
											    		if ($openFrom == $hourKey) {
											    			$selected = 'selected';
											    		}

											    		$html1 .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
											    	}
											    $html1 .= '
												    </select>
												    <br> To:
												    <select name="wp_restaurant_opening_days['. $dayKey .'][to][]" class="to-hours">
											    ';
											    	foreach ($hourDropDownOptions as $hourKey => $hourText) {
											    		$selected = '';
											    		if ($wp_restaurant_opening_days[$dayKey]['to'][$key] == $hourKey) {
											    			$selected = 'selected';
											    		}

											    		$html1 .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
											    	}
											    $html1 .= '
												    </select> <a href="javascript:void(0)" class="delete-time">Delete</a>
												    <hr>
											    </div>
											';
										}
									}
									$html1 .= '
									</div>
								</td>
							';
						}

						$html1 .= '
								</tr>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="tab-content">
          <div role="tabpanel" class="tab-pane" id="tab-2">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="custom-select-box-contain submit-timings-tabs">
					<fieldset>
						<div class="field ">
							<table class="table-responsive">
								<tr>
						';
						foreach ($weekDays as $dayKey => $dayText) {
							$html1 .= '
								<td>
									<span>'. $dayText .'</span>
									<div class="time-container">
										<a href="javascript:void(0)" class="add-time" data-name="wp_restaurant_delivery_days['. $dayKey .']"  id="delivery_days_'. $dayKey .'">Add</a>
									';
									if (!empty($wp_restaurant_delivery_days) && isset($wp_restaurant_delivery_days[$dayKey])) {
										foreach ($wp_restaurant_delivery_days[$dayKey]['from'] as $key => $deliveryFrom) {
											$html1 .= '
												<div class="time-wrapper"> <span>From</span>:
												    <select name="wp_restaurant_delivery_days['. $dayKey .'][from][]" class="from-hours">
											    ';
											    	foreach ($hourDropDownOptions as $hourKey => $hourText) {
											    		$selected = '';
											    		if ($deliveryFrom == $hourKey) {
											    			$selected = 'selected';
											    		}

											    		$html1 .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
											    	}
											    $html1 .= '
												    </select>
												    <br> To:
												    <select name="wp_restaurant_delivery_days['. $dayKey .'][to][]" class="to-hours">
											    ';
											    	foreach ($hourDropDownOptions as $hourKey => $hourText) {
											    		$selected = '';
											    		if ($wp_restaurant_delivery_days[$dayKey]['to'][$key] == $hourKey) {
											    			$selected = 'selected';
											    		}

											    		$html1 .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
											    	}
											    $html1 .= '
												    </select> <a href="javascript:void(0)" class="delete-time">Delete</a>
												    <hr>
											    </div>
											';
										}
									}
									$html1 .= '
									</div>
								</td>
							';
						}

						$html1 .= '
								</tr>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="tab-content">
          <div role="tabpanel" class="tab-pane" id="tab-3">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="custom-select-box-contain submit-timings-tabs">
					<fieldset>
						<div class="field ">
							<table class="table-responsive">
								<tr>
						';
						foreach ($weekDays as $dayKey => $dayText) {
							$html1 .= '
								<td>
									<span>'. $dayText .'</span>
									<div class="time-container">
										<a href="javascript:void(0)" class="add-time" data-name="wp_restaurant_pickup_days['. $dayKey .']"  id="pickup_days_'. $dayKey .'">Add</a>
									';
									if (!empty($wp_restaurant_pickup_days) && isset($wp_restaurant_pickup_days[$dayKey])) {
										foreach ($wp_restaurant_pickup_days[$dayKey]['from'] as $key => $pickupFrom) {
											$html1 .= '
												<div class="time-wrapper"> <span>From</span>:
												    <select name="wp_restaurant_pickup_days['. $dayKey .'][from][]" class="from-hours">
											    ';
											    	foreach ($hourDropDownOptions as $hourKey => $hourText) {
											    		$selected = '';
											    		if ($pickupFrom == $hourKey) {
											    			$selected = 'selected';
											    		}

											    		$html1 .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
											    	}
											    $html1 .= '
												    </select>
												    <br> To:
												    <select name="wp_restaurant_pickup_days['. $dayKey .'][to][]" class="to-hours">
											    ';
											    	foreach ($hourDropDownOptions as $hourKey => $hourText) {
											    		$selected = '';
											    		if ($wp_restaurant_pickup_days[$dayKey]['to'][$key] == $hourKey) {
											    			$selected = 'selected';
											    		}

											    		$html1 .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
											    	}
											    $html1 .= '
												    </select> <a href="javascript:void(0)" class="delete-time">Delete</a>
												    <hr>
											    </div>
											';
										}
									}
									$html1 .= '
									</div>
								</td>
							';
						}

						$html1 .= '
								</tr>
							</table>
						</div>
					</fieldset>
			';
			$html1 .= '
				<script type="text/javascript">
					var hourDropDownOptions = \'\
						<option value="0">12 AM</option>\
						<option value="1">1 AM</option>\
						<option value="2">2 AM</option>\
						<option value="3">3 AM</option>\
						<option value="4">4 AM</option>\
						<option value="5">5 AM</option>\
						<option value="6">6 AM</option>\
						<option value="7">7 AM</option>\
						<option value="8">8 AM</option>\
						<option value="9">9 AM</option>\
						<option value="10">10 AM</option>\
						<option value="11">11 AM</option>\
						<option value="12">12 PM</option>\
						<option value="13">1 PM</option>\
						<option value="14">2 PM</option>\
						<option value="15">3 PM</option>\
						<option value="16">4 PM</option>\
						<option value="17">5 PM</option>\
						<option value="18">6 PM</option>\
						<option value="19">7 PM</option>\
						<option value="20">8 PM</option>\
						<option value="21">9 PM</option>\
						<option value="22">10 PM</option>\
						<option value="23">11 PM</option>\
					\';
					jQuery(document).ready(function($){
						$(".add-time").on("click", function(){
							console.log("add-time : ", this);
							console.log("$(this).siblings().length: ", $(this).siblings().length);
							console.log("$(this).siblings(): ", $(this).siblings());
							if($(this).siblings().length < 2) {
								// let lineDivider = ($(this).siblings().length == 0) ? "<hr />" : "";
								let lineDivider = "";
								// console.log("$(this).siblings().length : ", $(this).siblings().length);
								$(this).parent().append(\'\
									<div class="time-wrapper">\
										\'+lineDivider+\'\
										<span>From:</span> <select name="\'+$(this).attr("data-name")+\'[from][]" class="from-hours">\'+hourDropDownOptions+\'</select><br />\
										To: <select name="\'+$(this).attr("data-name")+\'[to][]" class="to-hours">\'+hourDropDownOptions+\'</select>\
										<a href="javascript:void(0)" class="delete-time">Delete</a>\
										<hr />\
									</div>\
								\');
							} else {
								console.log("You can add upto two time intervals.");
							}
						});
						$(".time-container").on("click", ".delete-time", function(){
							// console.log("delete-time : ", this);
							$(this).parent().remove();
						});
						'. ((empty($wp_restaurant_opening_days)) ? '
							$(".add-time").click();
							$(".from-hours").val(8);
							$(".to-hours").val(20);
						' : '') .'
					});
				</script>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="tab-content">
          <div role="tabpanel" class="tab-pane" id="tab-4">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="custom-select-box-contain submit-timings-tabs">
				<fieldset>
					<div class="field ">
						<input type="hidden" id="wp_restaurant_closing_date_hide"   placeholder="Select the closing dates"  value="'.$wp_restaurant_closing_date.'">
						<input type="text" id="wp_restaurant_closing_date" name="wp_restaurant_closing_date"  placeholder="Select the closing dates" class="" autocomplete="true" "'.$wp_restaurant_closing_date.'">
					</div>
					<script type="text/javascript">
						jQuery(document).ready(function($){
                            $("#wp_restaurant_closing_date").multiDatesPicker();
                        });
					</script>
				</fieldset>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
';

			//$list = new Restaurant_Listing;
			//$html1 .= $list->loadMapJS();

			$deliveryContent .= '
				<fieldset>
					<label>Minimum order amount <small class="sgd-text">( in <b>SGD</b> )</small></label>
					<div class="field ">
						<input type="text" id="wp_restaurant_minimum_order_amount" class=""  name="wp_restaurant_minimum_order_amount" value="'.$wp_restaurant_minimum_order_amount.'" />
					</div>
				</fieldset>
			';
			$deliveryContent.='
				<fieldset>
					<label>Lead time</label>
					<div class="field ">
						<select name="wp_restaurant_lead_time" id="wp_restaurant_lead_time" class="" required>';
						for($i=1;$i<=10;$i++) {
							$val = $i*12;
							if($wp_restaurant_lead_time == $val) {
								$select = "Selected";
							}else{
								$select = "";
							}
						$deliveryContent .= '<option value="'.$val.'" '.$select.'>'.$val.'hrs</option>';
						}
					$deliveryContent.='</select>
					</div>
				</fieldset>
			';
			$deliveryContent .= '
				<fieldset class="delivery-fee-section">
					<label>Delivery Fee <small class="sgd-text">( in <b>SGD</b> )</small></label>
					<div class="field row first-delivery-fee">
                        <div class="col-md-5">
                            <sub>Price Greater Than</sub>
                            <input type="text" class="delivery-conditional-price"  name="wp_restaurant_delivery[conditional_price][]" value="0" readonly />
                        </div>
                        <div class="col-md-5">
                            <sub>Then Applicable Delivery Fee</sub>
                            <input type="text" class="delivery-fee"  name="wp_restaurant_delivery[applicable_fee][]" value="'. (isset($wp_restaurant_delivery['applicable_fee']) ? $wp_restaurant_delivery['applicable_fee'][0] : '' ) .'" />
                        </div>
                        <div class="col-md-2 delivery-action-button">
                            <button type="button" class="add-delivery-condition" tabindex="-1">+</button>
                        </div>
					</div>

                    <div class="rest-delivery-fees">';
                    
                    if (isset($wp_restaurant_delivery['conditional_price'])) {
                        foreach ($wp_restaurant_delivery['conditional_price'] as $key => $deliveryCondition) {
                            if (!$key) {
                                continue;
                            }

                            $deliveryContent .= '
                                <div class="field row first-delivery-fee">
                                    <div class="col-md-5">
                                        <sub>Price Greater Than</sub>
                                        <input type="text" class="delivery-conditional-price"  name="wp_restaurant_delivery[conditional_price][]" value="'. $deliveryCondition .'" />
                                    </div>
                                    <div class="col-md-5">
                                        <sub>Then Applicable Delivery Fee</sub>
                                        <input type="text" class="delivery-fee"  name="wp_restaurant_delivery[applicable_fee][]" value="'. $wp_restaurant_delivery['applicable_fee'][$key] .'" />
                                    </div>
                                    <div class="col-md-2 delivery-action-button">
                                        <button type="button" class="remove-delivery-condition" tabindex="-1">-</button>
                                        <button type="button" class="add-delivery-condition" tabindex="-1">+</button>
                                    </div>
                                </div>
                            ';
                        }
                    }

            $deliveryContent .= '</div>

                    <script type="text/javascript">
                        jQuery(document.body).ready(function($){
                            $(".delivery-fee-section").on("click", ".add-delivery-condition", function(){
                                $(".rest-delivery-fees").append(`
                                    <div class="field row first-delivery-fee">
                                        <div class="col-md-5">
                                            <sub>Price Greater Than</sub>
                                            <input type="text" class="delivery-conditional-price"  name="wp_restaurant_delivery[conditional_price][]" value="" />
                                        </div>
                                        <div class="col-md-5">
                                            <sub>Then Applicable Delivery Fee</sub>
                                            <input type="text" class="delivery-fee"  name="wp_restaurant_delivery[applicable_fee][]" value="" />
                                        </div>
                                        <div class="col-md-2 delivery-action-button">
                                            <button type="button" class="remove-delivery-condition" tabindex="-1">-</button>
                                            <button type="button" class="add-delivery-condition" tabindex="-1">+</button>
                                        </div>
                                    </div>
                                `);
                            });

                            $(".delivery-fee-section").on("click", ".remove-delivery-condition", function(){
                                $(this).parent().parent().remove();
                            });
                        });
                    </script>
				</fieldset>
			';
			$deliveryContent .='
				<fieldset>
			        <label>Pickup option available</label>
			        <div class="field ">
			            <label><input type="radio" name="pickup_option" id="pickup_option_yes" value="1" '. ($pickup_option ? 'checked' : '') .'> Yes</label>
			            <label><input type="radio" name="pickup_option" id="pickup_option_no" value="" '. ($pickup_option ? '' : 'checked') .'> No</label>
			        </div>
			    </fieldset>
			';

			$everywhereChecked = '';
			$hideMapElementStyle = '';
			if ($wp_restaurant_delivery_everywhere) {
				$everywhereChecked = 'checked';
				$hideMapElementStyle = 'style="display: none;"';
			}

            $termsConditionsChecked = 'checked';
            if ($terms_conditions) {
                $termsConditionsChecked = 'checked';
            }

			$deliveryContent .='
				<style>#wp_restaurant_delivery_everywhere{width:22px;height:22px;vertical-align:middle;}</style>
				<fieldset>
					<label>Areas delivered (Pincodes)</label>
					<div class="field ">
						<label><input type="checkbox" id="wp_restaurant_delivery_everywhere" value="1" class=""  name="wp_restaurant_delivery_everywhere" '. $everywhereChecked .' /> Deliver anywhere in Singapore</label>
						<input type="text" id="wp_restaurant_delivery_area" value="'.$wp_restaurant_delivery_area.'" class=""  name="wp_restaurant_delivery_pincodes" '. $hideMapElementStyle .' />
						<input type="button"  id="modal-open" class="openMap" data-toggle="modal" data-target="#myModal" value="MAP" '. $hideMapElementStyle .' />
					</div>
					<script type="text/javascript">
						jQuery(document.body).ready(function($){
							$("#wp_restaurant_delivery_everywhere").on("click", function(){
								if($(this).prop("checked")) {
									// $("#wp_restaurant_delivery_area").prop("disabled", true);
									$("#wp_restaurant_delivery_area").hide();
									$("#modal-open").hide();
								} else {
									// $("#wp_restaurant_delivery_area").prop("disabled", false);
									$("#wp_restaurant_delivery_area").show();
									$("#modal-open").show();
								}
							});
						});
					</script>
			';
			$deliveryContent .=$this->load_map_modal();
			$deliveryContent .='<hr /><button type="button" id="add-service" class="btn-model btn btn-info btn-lg add-service" data-toggle="modal" data-target="#service-modal" style="display: inline-block;">Add New Service</button>';

			$restaurantServices = [];
			if (isset($rID[0])) {
				$serviceQuery = "
					SELECT id, type, quantityRequired, question, '' as options
					FROM {$wpdb->prefix}wyz_restaurant_services 
					WHERE restaurant_id = {$rID[0]}
				";
				$restaurantServices = $wpdb->get_results($serviceQuery);
				if ($restaurantServices) {
					foreach ($restaurantServices as $key => $service) {
						$restaurantServices[$key]->question = htmlspecialchars_decode($service->question, ENT_QUOTES);
						$optionQuery = "
							SELECT id, name, price, tax, total
							FROM {$wpdb->prefix}wyz_restaurant_service_options 
							WHERE service_id = {$service->id}
						";
						$restaurantServices[$key]->options = $wpdb->get_results($optionQuery);
					}
				}
			}

			$deliveryContent .='
				<div class="restaurant-services-container">
					<input type="hidden" name="restaurant_services" id="restaurant_services" />
					<input type="hidden" name="deletedServices" id="deletedServices" />
					<input type="hidden" name="deletedOptions" id="deletedOptions" />
				</div>
			';

			$serviceScript = <<<ServiceScript
				if (restaurantServices.length > 0) {
			        for (key in restaurantServices) {
			            let service = restaurantServices[key];
			            let question = service['question'];
			            let typeText = parseInt(service['type']) ? 'Required' : 'Optional';
			            let serviceData = '';

			            serviceData = '\
			                <div class="restaurant-service" data-id="'+ key +'">\
			                    <div class="headerBox">\
			                        <div class="row">\
			                            <div class="col-md-11 col-sm-11">\
			                                <h5 class="titleHeader">\
			                                    <span class="service-question-text">'+ question +'</span>\
			                                </h5>\
			                                <p class="card-text">\
			                                    <span class="service-type-text">'+ typeText +'</span>\
			                                </p>\
			                            </div>\
			                            <div class="col-md-1 col-sm-1">\
			                                <div class="service-edit-wrapper">\
			                                    <a href="#edit" class="service-edit" onclick="editService(this, '+ key +'); return false;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>\
			                                    <a href="#copy" class="service-copy" onclick="copyService('+ key +'); return false;" ><i class="fa fa-copy"></i></a>\
			                                    <a href="#delete" class="service-delete" onclick="deleteService(this, '+ key +'); return false;"><i class="fa fa-trash-o"></i></a>\
			                                </div>\
			                            </div>\
			                        </div>\
			                    </div>\
			                </div>\
			            ';

			            $('.restaurant-services-container').append(serviceData);
			        }
			    }
ServiceScript;

			$deliveryContent .='
				<div class="modal-container">
			  		<div class="modal fade" id="service-modal" role="dialog">
				    	<div class="modal-dialog">
                            <div class="modal-content">
                                 <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  <h4 class="modal-title">Add New Service</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="service-types-container">
                                        <label>Service Type</label>
                                        <div class="service-types-wrapper">
                                        	<label><input type="radio" name="service[type]" class="service-type" value="0" checked> Optional</label>
                                        	<label><input type="radio" name="service[type]" class="service-type" value="1"> Required</label>
                                        </div>
                                    </div>
                                    <div class="service-quantity-container">
                                        <label>Quantity Required <input type="checkbox" class="service-quantity" value="1"></label>
                                    </div>
                                    <div class="service-question-container">
                                        <label>Question Text</label>
                                        <input type="text" class="service-question" placeholder="Please enter question..">
                                    </div>
                                    <div class="row service-options-container">
                                        <div class="service-option-data">
                                            <input type="hidden" class="option-id" value="">
                                            <div class="col-md-5"><input type="text" class="option-name" placeholder="Name"></div>
                                            <div class="col-md-2"><input type="text" class="option-price" placeholder="Base Price: eg. 20" value="0"></div>
                                            <div class="col-md-2"><input type="text" class="option-tax" placeholder="Tax(%): eg. 7" value="7"></div>
                                            <div class="col-md-2"><input type="text" class="option-total" placeholder="Total: eg. 25" value="0"></div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="modal-footer">
                                    <button type="button" class="btn dish-popup-button" data-dismiss="modal">Cancel</button>
                                    <button type="button" onclick="addService()" class="btn service-add-btn">Add</button>
                                    <button type="button" onclick="updateService(this)" class="btn service-save-btn" data-service-id="" style="display: none;">Save</button>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
				<script type="text/javascript">
					// .replace(/\"/g,\'&quot;\')
					var restaurantServices = '. json_encode($restaurantServices) .';
					document.getElementById("restaurant_services").value = JSON.stringify(restaurantServices);
					var restaurantDeletedServices = [];
					var restaurantDeletedOptions = [];
					var restaurantDeletedTempOptions = [];

					'. $serviceScript .'
				</script>
			';
			$deliveryContent .='</fieldset>';

			$html2  .= wyz_chef_dish::wp_form_shortcode();

			$htmlmaster.='
				<div class="tab submit_sidebar">
                    <div class="icon-tab-logo">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/wyzchef3.png" />
                    </div>
                    <div class="icon-tab '. ((!isset($_GET['active-tab']) || $_GET['active-tab'] == 'My Profile') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/my-profile-tab.svg" />
                        <input type="button" class="tablinks '. ((!isset($_GET['active-tab']) || $_GET['active-tab'] == 'My Profile') ? 'active' : '') .'" value="My Profile" onclick="openCity(event,this.value)">
                    </div>
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Delivery') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/delivery-tab.svg" />
                        <input type="button" class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Delivery') ? 'active' : '') .'" value="Delivery" onclick="openCity(event,this.value)">
                    </div>
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Payment') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/Payment-tab.svg" />
                        <input type="button" class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Payment') ? 'active' : '') .'" value="Payment" onclick="openCity(event,this.value)">
                    </div>
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Menu') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/menu-tab.svg" />
                        <input type="button"  class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Menu') ? 'active' : '') .'" value="Menu" onclick="openCity(event,this.value)">
                    </div>
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Orders') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/orders-tab.svg" />
                        <input type="button"  class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Orders') ? 'active' : '') .'" value="Orders" onclick="openCity(event,this.value)">
                    </div>
                    
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Review') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/reviews-tab.svg" />
                        <input type="button"  class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Review') ? 'active' : '') .'" value="Review" onclick="openCity(event,this.value)">
                    </div>
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Packages') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/Packages-tab.svg" />
                        <input type="button"  class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Packages') ? 'active' : '') .'" value="Packages" onclick="openCity(event,this.value)">
                    </div>
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Assistance') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/support-tab.svg" />
                        <input type="button"  class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Assistance') ? 'active' : '') .'" value="Assistance" onclick="openCity(event,this.value)">
                    </div>
                </div>
			';

			$profileContent = $html1;

			$paymentContent .= '
				<fieldset>
			        <label>Bank</label>
			        <div class="field ">
			          <input type="text" id="bank_name" name="bank_name" placeholder="Enter the bank name" class="" value="'. $bank_name .'">
			        </div>
			        <div class="field ">
			          <input type="text" name="bank_code" class="" placeholder="Enter the bank code" value="'. $bank_code .'">
			        </div>
			      </fieldset>
			      <fieldset>
			        <label>Account</label>
			        <div class="field ">
			          <input type="text" name="account_name" id="account_name" class="" placeholder="Enter the account name" value="'. $account_name .'">
			        </div>
			        <div class="field ">
			          <input type="text" id="account_number" name="account_number" placeholder="Number" class="" autocomplete="true" value="'. $account_number .'" >
			        </div>
			      </fieldset>
			      <fieldset>
			        <label>Swift Code</label>
			        <div class="field  w-100">
			            <input type="text" class="" name="swift_code" id="swift_code" placeholder="Enter the swift code" value="'. $swift_code .'">
			        </div>
			      </fieldset>
			      <fieldset>
			        <label>Branch</label>
			        <div class="field ">
			           <input type="text" class="" name="branch_name" id="branch_name" placeholder="Enter the branch name" value="'. $branch_name .'">
			        </div>
			        <div class="field ">
			         <input type="text" class="" name="branch_code" id="branch_code" placeholder="Enter the branch code" value="'. $branch_code .'">
			        </div>
			      </fieldset>
			      <fieldset>
			        <label>Preferred Mode of Payment</label>
			        <div class="field  radio-optn">
			        <input type="radio" name="preferred_mode" id="preferred_mode_cheque" value="cheque" '. ($preferred_mode == 'cheque' ? 'checked' : '') .'> Cheque
			          <br>
			          <input type="radio" name="preferred_mode" id="preferred_mode_wire" value="wire" '. ($preferred_mode == 'wire' ? 'checked' : '') .'> Wire Transfer
			          <br>
			        </div>
		      	</fieldset>
			';
			$htmlmaster.='
					<div id="My Profile" class="tabcontent '. ((!isset($_GET['active-tab']) || $_GET['active-tab'] == 'My Profile') ? 'active' : '') .'">
						<div class="dashboard-container">
						<h3 class="content-heading">MY PROFILE</h3>
						'.$profileContent.'
						</div>
					</div>
					<div id="Delivery" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Delivery') ? 'active' : '') .'">
						<div class="dashboard-container">
							<h3 class="content-heading">DELIVERY</h3>
							'.$deliveryContent.'
						</div>
					</div>
					<!-- Payment Tab starts here -->
					  <div id="Payment" style="min-height: 586px;" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Payment') ? 'active' : '') .'">
					    <div class="dashboard-container">
					      	<h3 class="content-heading">PAYMENT</h3>
					      	'. $paymentContent .'
					    </div>
					  </div>
				<!--my payment tab -->
					<div id="Menu" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Menu') ? 'active' : '') .'">
                        <div class="dashboard-container">
                            <h3 class="content-heading">MENU</h3>
                           '.$html2.'

                        </div>
					</div>


  <div id="Orders" style="min-height: 586px;" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Orders') ? 'active' : '') .'">
    <div class="dashboard-container">
      <h3 class="content-heading">ORDER</h3>
      <div class="first-section">
        <div class="bg-img">
          <img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/resto_bg.png">
        </div>
        <div class="page_description">
            <p>Orders are available at the dashboard. You can submit this page to check the orders.</p>
        </div>
      </div>
    </div>
  </div>
<!--my order tab -->
				<div id="Review" style="min-height: 586px;" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Review') ? 'active' : '') .'">
    <div class="dashboard-container">
      <h3 class="content-heading">REVIEW</h3>
      <div class="first-section">
        <div class="bg-img">
          <img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/resto_bg.png">
        </div>
        <div class="page_description">
         <p>Reviews are available at the dashboard. You can submit this page to check the reviews.</p>
        </div>
      </div>
    </div>
  </div>
<!--my review tab -->
				 <div id="Packages" style="min-height: 586px;" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Packages') ? 'active' : '') .'">
    <div class="dashboard-container">
      <h3 class="content-heading">PACKAGES</h3>
      <div class="first-section">
        <div class="bg-img">
          <img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/resto_bg.png">
        </div>
        <div class="page_description">
          <p>Packages are available at the dashboard. You can submit this page to check the packages.</p>
        </div>
      </div>
    </div>
  </div>
<!--my packages tab -->
				<div id="Assistance" style="min-height: 586px;" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Assistance') ? 'active' : '') .'">
    <div class="dashboard-container">
      <h3 class="content-heading">ASSISTANCE</h3>
      <div class="first-section">
        <div class="bg-img">
          <img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/resto_bg.png">
        </div>
        <div class="page_description">
          <p>For any assistance, you can contact us at below details</p>
          <p> Email: support@wyzchef.com</p>
          <p> Phone: +65 88913599</p>
        </div>
      </div>
    </div>
  </div>
<!--my assistance tab -->
<div class="dashboard-container tabcontent">
	<div id="errormsg"></div>
    <div class="dashboard-submit">
        <span class="terms-conditions-wrapper" style="position: absolute; visibility: hidden;"><input type="checkbox" name="terms_conditions" value="1" '. $termsConditionsChecked .' /> <a href="javascript:void(0)" data-toggle="modal" data-target="#termsAndCondtions">Terms &amp; Conditions </a></span>
    	<input type="submit" class="modify-button" id="dashboard_submit" value="SUBMIT" />
    </div>
    <!-- Dashboard submit button -->
    <div class="copyrights-sec">
      <p><span><img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/copyr-logo.png" alt/></span><span> for restaurants 2019 &copy;</span>
      </p>
    </div>
    <!-- copyrights-sec -->
  </div>

';
$htmlmaster.='</form>'. $this->submit_modal();
//wp_enqueue_script('customjs', WYZ_PLUGIN_ROOT_URL .'assets/js/custom.js');
echo  $htmlmaster;
