<?php
$orderTable    = $wpdb->prefix . 'wyz_restaurant_order';
$deliveryTable = $wpdb->prefix . 'wyz_restaurant_order_delivery_details';
$orderid       = $_GET["order_id"];

$results       = $wpdb->get_results( "SELECT ord.id, ord.user_id, ord.payment_status, ord.total, ord.status, delivery.street, delivery.floor, delivery.company, delivery.postal_code, delivery.city, 
  delivery.delivery_date, delivery.delivery_phone, delivery.people, delivery.email FROM $orderTable AS ord LEFT JOIN $deliveryTable AS delivery ON ord.`id` = delivery.`order_id` WHERE ord.`id` = $orderid");

if(get_current_user_id() != $results[0]->user_id) {
  wp_safe_redirect( $redirect_to );
  exit();
}
if($_GET['tab'] == 'confirmation' && !empty($_GET['order_id'] )){ ?>
  <script type="text/javascript">
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
      history.go(1);
    };

  </script>
  <script type="text/javascript">
    // window.onbeforeunload = function () {return false;}
  </script>

<div class="inner-content">
  <div class="alert alert-success">your order has been placed successfully! We will mail you the invoice with the payment details shortly.</div>
  <div class="step-heading">
    <h4>Order Confirmation</h4>
    <p>Your order will be delivered in the following address.</p>
  </div>
  <input type="hidden" id="confirmFlag" value = "1" />
  <address>
    <strong>Address</strong><br>
    <?php 
    echo $results[0]->company."<br>";
    echo $results[0]->street."<br>";
    echo ($results[0]->floor?$results[0]->floor.",":"");
    echo ($results[0]->city?$results[0]->city.",":"");
    echo ($results[0]->postal_code?$results[0]->postal_code."":"");
    ?>
    <br>
    <abbr title="Phone">P:</abbr> <?php echo get_user_meta($results[0]->user_id, "User_phone", true ); ?>
  </address>            
</div>
<div class="button-container">
  <a href="<?php echo home_url('corporate-dashboard'); ?>" class="wyz-button modify-button">Go to dashboard</a>
</div>
<?php } ?>