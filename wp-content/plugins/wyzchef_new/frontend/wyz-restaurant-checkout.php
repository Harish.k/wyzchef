<?php 
get_header(); 

$userLoggedin 	= 0;
$userDetails 	= [];
$userFirstName 	= "";
$userLastName 	= "";
$userEmail 		= "";
$userPhone 		= "";
$tab 			= "";
$redirectTab 	= 0;
$redirect_to 	= get_permalink( get_page_by_title( 'Checkout' ) ); 
if (get_current_user_id() > 0) {
	$userLoggedin 	=1;
	$userDetails 	= wp_get_current_user();

	$userFirstName 	= get_user_meta($userDetails->data->ID, "first_name", true );
//$userLastName 	= $userDetails->data->last_name;
	$userLastName 	= get_user_meta($userDetails->data->ID, "last_name", true );
	$userEmail 		= $userDetails->data->user_email;
	$userPhone 		= get_user_meta($userDetails->data->ID, "User_phone", true );

	$userstreet 	= get_user_meta($userDetails->data->ID, "company_address", true );
	$userfloor 		= get_user_meta($userDetails->data->ID, "floor_building", true );
	$unitnumber 	= get_user_meta($userDetails->data->ID, "unit_number", true );
	$postal 		= get_user_meta($userDetails->data->ID, "company_postal", true );
	$CustId			= get_user_meta( $userDetails->data->ID, "stripe_cust_id", true );

}

if (isset($_GET['email']) && $_GET['email'] == "registered") {
	$emailId = $_GET['id']; 
}

if(isset($_GET['tab'])) {
	$tab = $_GET['tab'];
	if(!$userLoggedin) {
		wp_safe_redirect( $redirect_to );
		exit();
	}
//$redirectTab = 1;
}

?>
<?php
// wp_enqueue_style('datetimepicker-css', WYZ_PLUGIN_ROOT_URL. 'assets/css/bootstrap-datetimepicker.css');
// wp_enqueue_script('datetime-moment-js', WYZ_PLUGIN_ROOT_URL. 'assets/js/moment.min.js');
// wp_enqueue_script('datetimepicker-js', WYZ_PLUGIN_ROOT_URL. 'assets/js/bootstrap-datetimepicker.js');
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<input type="hidden" id="redirectTab" value = "<?php echo $redirectTab; ?>" />
<div class="container">
	<div class="content-wrapper checkout-controller">
		<div class="row">
			<div class="col-lg-9 col-md-9">
				<div class="breadcrumbs">
					<ul class="checkout-bread row clearfix">
						<li class="col-sm-4"><a class="<?php echo $tab == ''? 'active':'disabled'; ?>">Customer Information</a></li>
						<li class="col-sm-4"><a class="<?php echo $tab == 'delivery'? 'active':'disabled'; ?> ">Delivery Address</a></li>
						<li class="col-sm-4"><a class="<?php echo $tab == 'confirmation'? 'active':'disabled'; ?> ">Confirmation</a></li>
					</ul>
				</div>
				<?php if($tab == "") { 
					include( plugin_dir_path( __FILE__ ) . 'wyz-restaurant-customer-info.php');
				} else if($tab == "delivery") {
					include( plugin_dir_path( __FILE__ ) . 'wyz-restaurant-delivery-info.php');
				} else if($tab == "confirmation") {
					include( plugin_dir_path( __FILE__ ) . 'wyz-restaurant-confirmation.php');
				} ?>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="row">
					<div class="restaurane-dish-checkout col-md-12">
						<div class="your-match">
							<h3 style="margin-top:0px;">Your Order </h3>
							<div class="order-summary">
								<div class="order-table">Cart information is loading..</div>
								<div class="coupon-code-container" style="display: none;">
                                    <?php if($tab == "") { ?>
									<strong>Coupon: </strong>
                                    <input type="text" name="coupon" class="coupon-code" style="width: 50% !important;" value="<?php echo ($_SESSION['coupon_applied'] ? $_SESSION['coupon_data']->code . '" disabled="disabled' : ''); ?>" />
                                    <button class="button apply-coupon" <?php echo ($_SESSION['coupon_applied'] ? 'style="display: none;"' : ''); ?> >Apply</button>
                                    <button class="button remove-coupon" <?php echo ($_SESSION['coupon_applied'] ? '' : 'style="display: none;"'); ?> ><i class="fa fa-trash-o"></i></button>
                                    <?php } ?>
								</div>
							</div>

							<input type="hidden" id="checkoutFlag" value = "1" />
							<input type="hidden" id="permaLinkCart" value = "<?php echo $redirect_to; ?>" />
							<div id="cart"></div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
<script type="text/javascript">
	/*
     * Manage cart functional flow
     */
    var multiCartData = {}, cartData = {}, cartItems = {}, cartServiceItems = {}, restaurantId = null, deliveryObj, tab = '<?php echo $tab; ?>';

    if ('confirmation' == tab) {
        multiCartData = JSON.parse(localStorage.getItem('wyzchef_cart_data'));
    	restaurantId = localStorage.getItem('wyzchef_cart_checkout_id');
    	if (restaurantId in multiCartData) {
    		delete multiCartData[restaurantId];
    		localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));
    		localStorage.removeItem('wyzchef_cart_checkout_id');
    	} 
    }

    if (localStorage.length && ('wyzchef_cart_data' in localStorage)) {
    	multiCartData = JSON.parse(localStorage.getItem('wyzchef_cart_data'));
    	restaurantId = localStorage.getItem('wyzchef_cart_checkout_id');
    	if (restaurantId in multiCartData) {
    		cartData = multiCartData[restaurantId];

    		deliveryObj = cartData['deliveryObj'];
		    if ('cartItems' in cartData) {
		    	cartItems = cartData['cartItems'];
		    	let cartGST = 0;
		    	let cartTotal = 0;
		    	let deliveryGST = 0;
	            let deliveryfee = 0;
		    	let cartHTML = '';

		    	if (Object.entries(cartItems).length > 0) {
	                if (cartData['cartServiceItems']) {
	                    cartServiceItems = cartData['cartServiceItems'];
	                }

			    	cartHTML = '\
					<div class="checkout_table_new"><table cellpadding="10" cellspacing="1" class="show_table"><tbody>\ ';
			    	for(key in cartItems) {
			    		let item = cartItems[key];
			    		let dishData = item['dish'];
			    		let modifiersText = item['modifiersText'];
			    		let totalBasePrice = parseFloat(item['totalBasePrice']);
			    		let totalGstPrice = parseFloat(item['totalGstPrice']);
			    		cartGST += (totalGstPrice - totalBasePrice)
			    		cartTotal += totalGstPrice;

						cartHTML += '\
							<tr class="dish-count-price-row">\
				                <td class="item-number">\
					                <div class="number chckout-number">\
										<input type="number" value="'+ item['quantity'] +'" disabled>\
					                </div>\
					            </td>\
				              	<td align="right" style="width:65px">$<span class="dish-amount'+ key +'">'+ totalGstPrice.toFixed(2) +'</span></td>\
							</tr>\
			    			<tr>\
					            <td class="dish-name-td">\
					            	<strong data-key="'+ key +'">'+ dishData['name'] +'</strong>\
					            	<span class="modifiers-info">'+ modifiersText.join(", ") +'</span>\
				            	</td>\
				            </tr>\
			            ';
			    	}

					if ('conditional_price' in deliveryObj) {
	                    for(let i = (deliveryObj['conditional_price'].length - 1); i >= 0; i--) {
	                        if (cartTotal > deliveryObj['conditional_price'][i]) {
	                            deliveryfee = deliveryObj['applicable_fee'][i];
	                            deliveryfee = parseFloat(deliveryfee ? deliveryfee : 0);
	                            deliveryGST = 0.07*deliveryfee;
	                            deliveryfee = 1.07*deliveryfee;
	                            break;
	                        }
	                    }
	                }
	                cartGST += deliveryGST;

					if (Object.entries(cartServiceItems).length > 0) {
						// cartHTML += '\
							// <table cellpadding="10" cellspacing="1" class="show_table"><tbody>\
			    		// ';
				    	for(key in cartServiceItems) {
				    		let service = cartServiceItems[key];
				    		let question = service['question'];
				    		let optionNames = service['serviceOptionNames'];
				    		let quantity = service['quantity'];
				    		let totalBasePrice = parseFloat(service['totalBasePrice']);
				    		let totalGstPrice = parseFloat(service['totalGstPrice']);
				    		cartGST += (totalGstPrice - totalBasePrice)
				    		cartTotal += totalGstPrice;

							cartHTML += '\
							<tr class="dish-count-price-row">\
				                <td class="item-number">\
					                <div class="number chckout-number">\
					                    <input type="number" value="'+ quantity +'" disabled>\
					                </div>\
					            </td>\
				              	<td align="right" style="width:65px">$'+ totalGstPrice.toFixed(2) +'</td>\
							</tr>\
				    			<tr>\
						            <td class="dish-name-td">\
						            	<strong data-key="'+ key +'">'+ question +'</strong>\
						            	<span class="modifiers-info">'+ optionNames.join(", ") +'</span>\
					            	</td>\
					            </tr>\
				            ';
				    	}
				    	// cartHTML += '</tbody></table>';
					}
	                cartHTML += '</tbody></table></div>';

			    	cartHTML += '<table class="cart-total-details"><tbody>';
			    	cartHTML += '\
			    		<tr><td><span>Delivery Fees:</span><i>$'+ deliveryfee.toFixed(2) +'</i></td></tr>\
			            <tr><td><span>GST (Included in price):</span><i>$'+ cartGST.toFixed(2) +'</i></td></tr>\
			            <tr><td><span><strong>Total: </strong></span><i>$<span class="total-amount">'+ (cartTotal + deliveryfee).toFixed(2) +'</span></i></td></tr>\
			    	';
			    	cartHTML += '</tbody></table>';

			    	if (jQuery('#cartCheckoutItems').length) {
			    		jQuery('#cartCheckoutItems').val(JSON.stringify({
			    			restaurantId: restaurantId,
			    			cartData: cartData,
			    		}));
			    	}

			    	jQuery('.coupon-code-container').show();
			    } else {
			    	cartHTML += '<p>You haven\'t added anything to your cart yet! <br/>Start adding your favourite office food!</p>';
			    }

		    	jQuery('.order-table').html(cartHTML);
		    }

		    if (
		    	jQuery('input[name="people"]').length
	        	&& ('headcount' in cartData)
	    	) {
	            jQuery('input[name="people"]').val(cartData['headcount']);
	        }

		    if (
		    	jQuery('input[name="delivery_date"]').length
	        	&& ('deliveryDate' in cartData)
	    	) {
	            jQuery('input[name="delivery_date"]').val(cartData['deliveryDate']);
	        }
    	} else {
			jQuery('.order-table').html('<p>You haven\'t added anything to your cart yet! <br/>Start adding your favourite office food!</p>');
	    	jQuery('.cart-action-checkout').hide();
		}
	} else {
		jQuery('.order-table').html('<p>You haven\'t added anything to your cart yet! <br/>Start adding your favourite office food!</p>');
    	jQuery('.cart-action-checkout').hide();
	}

    jQuery(".apply-coupon").click(function(e) {
        var couponCode = jQuery(".coupon-code").val();
        if (couponCode == '') {
            alert("Coupon code must have valid code");

            return false;
        }

        jQuery.ajax({
            url: "<?php echo admin_url('admin-ajax.php');?>",
            type: "POST",
            dataType: "json",
            data: { 
                action: 'wyzchef_apply_coupon',
                code: couponCode,
                cartItems: JSON.stringify(cartItems),
            },
            success: function(response) {
                if (response.success) {
                    let amounts = response.dishesAmounts;
                    for(key in amounts) {
                        jQuery('.dish-amount'+ key).text(amounts[key].toFixed(2));
                    }

                    let dishOldTotal = parseFloat(response.oldTotal);

                    let deliveryfee = 0;
                    if ('conditional_price' in deliveryObj) {
						for(let i = (deliveryObj['conditional_price'].length - 1); i >= 0; i--) {
							if (dishOldTotal > deliveryObj['conditional_price'][i]) {
								deliveryfee = parseFloat(deliveryObj['applicable_fee'][i]);
                                deliveryfee = 1.07*deliveryfee;
								break;
							}
						}
					}

					let servicePrice = 0;
					for (let key in cartServiceItems) {
						servicePrice += parseFloat(cartServiceItems[key]['totalGstPrice']);
					}

					let dishTotal = parseFloat(response.total);

                    jQuery('.total-amount').text((dishTotal + deliveryfee + servicePrice).toFixed(2));
                    
                    jQuery('.coupon-code').prop('disabled', true);
                    jQuery('.apply-coupon').hide();
                    jQuery('.remove-coupon').show();

                    cartData['cartCoupon'] = {
                        newTotalPrice: dishTotal,
                        oldTotalPrice: dishOldTotal,
                        status: true,
                    };
                    cartItems = response.cartItems;
                    cartData['cartItems'] = cartItems;
        			// localStorage.setItem(restaurantId, JSON.stringify(cartData));
        			multiCartData[restaurantId] = cartData;
        			localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));
                }
                alert(response.msg);
            },
            error: function(error) {
                console.log('error', error);
            }
        });
    });

    jQuery(".remove-coupon").click(function(e) {
        jQuery.ajax({
            url: "<?php echo admin_url('admin-ajax.php');?>",
            type: "POST",
            dataType: "json",
            data: { 
                action: 'wyzchef_remove_coupon',
            },
            success: function(response) {
                if (response.success) {
                    let amounts = response.dishesAmounts;
                    for(key in amounts) {
                        jQuery('.dish-amount'+ key).text((amounts[key]).toFixed(2));
                    }

                    let dishTotal = parseFloat(response.total);

                    let deliveryfee = 0;
                    if ('conditional_price' in deliveryObj) {
						for(let i = (deliveryObj['conditional_price'].length - 1); i >= 0; i--) {
							if (dishTotal > deliveryObj['conditional_price'][i]) {
								deliveryfee = parseFloat(deliveryObj['applicable_fee'][i]);
                                deliveryfee = 1.07*deliveryfee;
								break;
							}
						}
					}

					let servicePrice = 0;
					for (let key in cartServiceItems) {
						servicePrice += parseFloat(cartServiceItems[key]['totalGstPrice']);
					}

                    jQuery('.total-amount').text((dishTotal + deliveryfee + servicePrice).toFixed(2));

                    jQuery('.coupon-code').val('');
                    jQuery('.remove-coupon').hide();
                    jQuery('.apply-coupon').show();
                    jQuery('.coupon-code').prop('disabled', false);

                    cartData['cartCoupon'] = {
                        newTotalPrice: dishTotal,
                        oldTotalPrice: null,
                        status: false,
                    };
                    cartItems = response.cartItems;
                    cartData['cartItems'] = cartItems;
        			// localStorage.setItem(restaurantId, JSON.stringify(cartData));
        			multiCartData[restaurantId] = cartData;
        			localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));
                }
                alert(response.msg);
            },
            error: function(error) {
                console.log('error', error);
            }
        });
    });
</script>