<div class="inner-content">
	               <div class="step-heading">
	                  <h4>Customer Information</h4>
	                  <p>Please provide your contact information<?php if (!$userLoggedin) { ?>, or login if you already have an account<?php } ?></p>
	               </div>
	               <div class="row">
	                  <div class="<?php if (!$userLoggedin) echo 'col-md-6'; ?> checkout-line">
	                     <h5 class="section-heading">Your Contact Information</h5>
	                     <form class="checkout-form p-r-20" id="new_user" action="<?php echo admin_url( 'admin-post.php' ); ?>?action=checkout_register" accept-charset="UTF-8" method="post">
	                        <div class="form-group row">
	                           <div class="col-sm-6">
	                           		<label class="form-control-label">First Name <span class="label-required">*</span></label>
	                           		<input class="form-control" required="required" type="text" name="first_name" id="user_first_name" value = "<?php echo $userFirstName; ?>"/>
	                           	</div>
	                           <div class="col-sm-6">
	                           		<label class="form-control-label">Last Name <span class="label-required">*</span></label>
	                           		<input class="form-control" required="required" type="text" name="last_name" id="user_last_name" value = "<?php echo $userLastName; ?>"/>
	                           	</div>
	                        </div>
	                        <div class="form-group row">
	                           <div class="col-sm-12">
	                           		<label class="form-control-label">Email <span class="label-required">*</span></label>
	                          <input class="form-control" placeholder="example@email.com" required="required" type="email" name="email" id="user_email" value = "<?php echo $userEmail; ?>"/>
	                           		<?php if (isset($_GET['email']) && $_GET['email'] == "registered") { ?>
			                        	<div class="alert alert-danger">Email Id is already registered in the system. Please Login.</div>
			                        <?php } ?>
	                           	</div>
	                        </div>
	                        <div class="form-group row">
	                           <div class="col-sm-12">
	                           		<label class="form-control-label">Phone <span class="label-required">*</span></label>
	                           		<input class="form-control" required="required" type="text" name="mobile" id="user_mobile" value = "<?php echo $userPhone;?>"/>
	                           	</div>
	                        </div>
	                        <?php if (!$userLoggedin) { ?>
		                        <div class="form-group row">
		                           <div class="col-sm-12">
		                           		<label class="form-control-label">Password <span class="label-required">*</span></label>
		                           		<input class="form-control" type="password" name="password" id="user_password" />
		                           	</div>
		                        </div>
	                        <?php } ?>
	                        <div class="form-group row">
	                           <div class="col-sm-12">
	                           		<input type="hidden" name="redirectLink" value = "<?php echo $redirect_to; ?>" />
	                           		<button class="btn btn-primary btn-normal" id="checkoutContinue" type="submit">Continue</button>
	                           	</div>
	                        </div>

	                        <div class="form-group row">
	                           <div class="col-sm-12">
	                           		<a href="<?php echo  wp_get_referer(); ?>">Edit Cart</a>
	                           </div>
	                          
	                        </div>
	                     </form>
	                  </div>
	                  <?php if (!$userLoggedin) { ?>
	                  <div class="col-md-6">
	                     <h5 class="section-heading p-l-20">Already have an account?</h5>
	                     <form class="checkout-form p-l-20" id="new_session" name="checkoutLogin" action="<?php echo admin_url( 'admin-post.php' ); ?>?action=checkout_login" accept-charset="UTF-8" method="post">
	                        <div class="form-group row">
	                           <div class="col-sm-12"><label class="form-control-label">Email</label><input class="form-control" placeholder="example@email.com" type="email" name="email" id="session_email" value = "<?php echo $emailId; ?>"/></div>
	                        </div>
	                        <div class="form-group row">
	                           <div class="col-sm-12"><label class="form-control-label">Password</label><input class="form-control" type="password" name="password" id="session_password" /></div>
	                        </div>
	                        <?php if (isset($_GET['login']) && $_GET['login'] == "failed") { ?>
	                        	<div class="alert alert-danger">Invalid Credentials</div>
	                        <?php } ?>
	                        <div class="form-group row">
	                        	<input type="hidden" name="action" value="checkout_login"/>
	                        	<input type="hidden" name="redirectLink" value = "<?php echo $redirect_to; ?>" />
	                           <div class="col-sm-6"><button class="btn btn-primary btn-normal" type="submit">Sign in</button></div>
	                        </div>
	                     </form>
	                  </div>
	                  <?php } ?>
	               </div>
	            </div>