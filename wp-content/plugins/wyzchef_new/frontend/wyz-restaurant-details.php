<?php 
get_header(); 

$postId = $post->ID;
$restaurantName = $post->post_title;
$postMeta = get_post_meta($postId);

$dishListingLimit = 6;
$dishDetails = $wpdb->get_results('select * from '.$wpdb->prefix.'wyz_restaurant_dishes where restaurant_id ='.$postId.' ORDER BY id ASC LIMIT '. $dishListingLimit);
$menuDetails = $wpdb->get_results('SELECT distinct(menu_id) FROM  '. $wpdb->prefix .'wyz_restaurant_dishes WHERE restaurant_id = '.$postId .' ORDER BY menu_id
	');

$restaurantTypeString = null;

$taxonomies = get_object_taxonomies( (object) array( 'post_type' => 'wp_restaurant' ) );
$customPostTypeDetails = array();

if (count($taxonomies) > 0) {
	foreach ($taxonomies as $key => $value) {
		$terms = get_terms($value);

		if (count($terms) > 0) {
			foreach ($terms as $keyTerm => $valueTerm) {
				$customPostTypeDetails[$value][] = array(
					'term_id' => $valueTerm->term_id,
					'name' => $valueTerm->name,
					'slug' => $valueTerm->slug
				);
			}
		}
	}
} 

$restaurantType = array();
$restaurantCategory = array();
$restaurantMenu = array();

$termDetails = array();
$restaurantTermRelationQuery = $wpdb->get_results('select * from '.$wpdb->prefix.'term_relationships where object_id ='.$postId);

if (count($restaurantTermRelationQuery) > 0) {

	foreach ($restaurantTermRelationQuery as $key => $value) {
		$termDetails[] = $value->term_taxonomy_id;
	}
}

$restaurantDishDetails = $dishDetails;

$dishCategory = null;

$featuredImagePostId = $postMeta['_thumbnail_id'][0];

$galleryArguments = array(
	'post_parent' => $postId,
	'post_type' => 'attachment',
);

//$galleryImages = get_children($galleryArguments);
$galleryImages = get_post_meta($postId, 'wp_restaurant_gallery', true);

// for category 
$wp_restaurant_category = get_post_meta($postId, 'wp_restaurant_category',true);

// for Restaurant Type
$wp_restaurant_type = get_post_meta($postId, 'wp_restaurant_type',true);

// For Min Order Amount
$wp_restaurant_minimum_order_amount = get_post_meta($postId, 'wp_restaurant_minimum_order_amount',true);
$wp_restaurant_minimum_order_amount = $wp_restaurant_minimum_order_amount ? $wp_restaurant_minimum_order_amount : 1;

// delivery fee
$wp_restaurant_delivery_fee = get_post_meta($postId, 'wp_restaurant_delivery_fee',true);

// $applicableDeliveryFee = $wp_restaurant_delivery_fee;
$deliveryToolTip = '';
$wp_restaurant_delivery = get_post_meta($postId,'wp_restaurant_delivery',true);
if ($wp_restaurant_delivery && isset($wp_restaurant_delivery['conditional_price'])) {
	foreach ($wp_restaurant_delivery['conditional_price'] as $key => $deliveryCondition) {
		$deliveryfee = $wp_restaurant_delivery['applicable_fee'][$key];
		if ($key) {
			$deliveryToolTip .= "For order above <span>\${$deliveryCondition}</span>, delivery fee is <span>\${$deliveryfee}</span> <br>";
		} else {
			$deliveryToolTip .= "Standard delivery fee is <span>\${$deliveryfee}</span> <br>";
		}
	}
} else {
	$wp_restaurant_delivery = array(
        'conditional_price' => [0],
        'applicable_fee' => [($wp_restaurant_delivery_fee ? $wp_restaurant_delivery_fee : 0)],
    );
}
$applicableDeliveryFee = $wp_restaurant_delivery['applicable_fee'][0];

// Restaurant Services
$restaurantServicesArray = [];
if (isset($postId)) {
	$serviceQuery = "
		SELECT id, type, quantityRequired, question, '' as options
		FROM {$wpdb->prefix}wyz_restaurant_services 
		WHERE restaurant_id = {$postId}
	";
	$restaurantServices = $wpdb->get_results($serviceQuery);
	if ($restaurantServices) {
		foreach ($restaurantServices as $key => $service) {
			$restaurantServicesArray[$service->id]['id'] = $service->id;
			$restaurantServicesArray[$service->id]['type'] = $service->type;
			$restaurantServicesArray[$service->id]['quantityRequired'] = $service->quantityRequired;
			$restaurantServicesArray[$service->id]['question'] = $service->question;
			$optionQuery = "
				SELECT id, name, price, tax, total
				FROM {$wpdb->prefix}wyz_restaurant_service_options 
				WHERE service_id = {$service->id}
			";
			$restaurantServicesArray[$service->id]['options'] = $wpdb->get_results($optionQuery);
		}
	}
}

// lead time
$wp_restaurant_lead_time = get_post_meta($postId, 'wp_restaurant_lead_time',true);


$logoImageDetails = $galleryImages[$featuredImagePostId];
unset($galleryImages[$featuredImagePostId]);


$logoImageUrl = wp_get_attachment_image_src(get_post_thumbnail_id($postId));

$bannerImageUrl = null;

foreach ($galleryImages as $key => $value) {
	$bannerImageUrl = $value->guid;
}

wp_enqueue_style('comment-rating-styles', WYZ_PLUGIN_ROOT_URL. 'assets/css/rating.css');
?>

<style type = "text/css">
	.modal.in{
		opacity:1;
	}

	#download_cart{
		display: block;
		opacity: 1;
	}

  	@media print {
  		.details-left-sec {
		    margin: 0 !important;
		    height: 0 !important;
		}

		.modal.in .modal-dialog{
			margin-top: 10px !important;
		}
		
        .back-button, .whatsappme {
            display: none;
		}
		
		.your-match, .restaurane-dish-checkout{
			height: 0 !important;
			border: none !important;
		}

		 #main-header, .container-fluid, #main-footer, .Cart-Messages,.cart_icon_heading, .your-match .container-fluid, .restaurant-details-page .detail-row .col-lg-9, .bill-div, .details-left-sec.banner-sec,
		  .modal-header.invoice_header, .modal-footer {
			 display: none !important;
			 height: 0 !important;
     	}

     	.restaurane-dish-checkout {
     		position: static !important;
     	}

     	.restaurant-details-page .restaurane-dish-checkout .order-summary, 
     	.restaurant-details-page .restaurane-dish-checkout .order-table {
     		display:none;
     	}
  	}
</style>

<?php 
$queryString = array();
$deliveryDate = '';
if (isset($_GET['delivery_date']) && !empty($_GET['delivery_date'])) {
    $queryString[] = "delivery_date={$_GET[delivery_date]}";
    $deliveryDate = $_GET['delivery_date'];
}

$headcount = '';
if (isset($_GET['headcount']) && !empty($_GET['headcount'])) {
    $queryString[] = "headcount={$_GET[headcount]}";
    $headcount = $_GET['headcount'];
}

if (count($queryString)) {
    $queryString = '?' . implode('&', $queryString);
} else {
    $queryString = '';
}
?>

<div class="container restaurant-details-page">
				<span class="check_your_cart">
					View your cart
				</span>
<div class="row detail-row">
	<div class="details-left-sec banner-sec"  style="background: url('<?php 
			if($galleryImages[0]){ echo wp_get_attachment_url($galleryImages[0]); } 
			else { echo WYZ_PLUGIN_ROOT_URL. "assets/images/default-restaurant-cover.png";  }
			?>');margin-bottom:25px;min-height:280px;background-position: center 30%;background-size:cover;background-repeat: no-repeat;">
		<a class="back-button" href="<?php echo home_url('caterers').$queryString; ?>"><span class="fa fa-angle-left"></span>Back to the list</a>
		<div class="restaurant-header">
			<div class="bg_overlay"></div>
			<div class="restaurant-logo log-section-item">
				<?php if($logoImageUrl){ ?>
					<img src="<?php echo $logoImageUrl[0];?>" alt="<?php echo esc_html( get_the_title($postId) );?>">
				<?php } else { ?>
					<img src="<?php echo WYZ_PLUGIN_ROOT_URL;?>/images/Default-logo.png">
				<?php } ?>
			</div>
		</div>
		<div class="row banner-overlay-sec">
				<div class="col-md-7 col-sm-7 restaurant-name-section">
					<div class="row">
						<div class="col-md-12">
							<span class="restaurant-inner-name"><?php echo $restaurantName;?></span>

						</div>
						
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 restaurant-name-section resto-cat">
							<ul>
								<?php
								if (isset($wp_restaurant_type) && !empty($wp_restaurant_type)) {
									for ($i=0; $i < count($wp_restaurant_type); $i++) {
										$type = get_term($wp_restaurant_type[$i]);
										echo '<li>'. $type->name .'</li>';
									}

								}  ?>  
							</ul>
							
							<span><?php $reviews = new Wyz_Chef_Reviews();
							echo $reviews->wyz_comment_rating_get_average_ratings(get_the_ID())."&nbsp;";
							echo $reviews->wyz_comment_average_ratings(get_the_ID());
							do_action('wyz_restaurant_total_reviews', get_the_ID()); ?></span>
						</div>
					</div>
					<!-- <div class="row">          
						<div class="col-md-12 order-detail-review">
							<ul>
								<li>
									<span><img src="<?php echo WYZ_PLUGIN_ROOT_URL;?>assets/images/dollar.svg" /></span>
									<span>Min.order required:<b>$<?php echo $wp_restaurant_minimum_order_amount; ?></b></span>
								</li>
								<li>
									<span><img src="<?php echo WYZ_PLUGIN_ROOT_URL;?>assets/images/delivery.svg" /></span>
									<span>Delivery Fee:from $<?php echo $applicableDeliveryFee; ?></span>
									<a class="custom_info_tooltip" href="#"><i class="fa fa-info-circle"></i>
										<div class="tooltip_inner">
											<p>
												<?php echo $deliveryToolTip; ?>
											</p>
										</div>
									</a>
								</li>
								<li>
									<span><img src="<?php echo WYZ_PLUGIN_ROOT_URL;?>assets/images/time.svg" /></span>
									<span>Order Notice: <?php echo $wp_restaurant_lead_time;?>hrs</span>
								</li>
							</ul>
						</div>                    
					</div>               -->
				</div>
				<div class="col-md-5 col-sm-5 end-img">
					<?php
					if (isset($wp_restaurant_category) && !empty($wp_restaurant_category)) {
						for ($j=0; $j < count($wp_restaurant_category); $j++) {
							$category = get_term($wp_restaurant_category[$j]);
							$imageId = get_term_meta( $category->term_id, 'wyz-category-image-id', true );
							if( $imageId ) {
								$categoryImage = wp_get_attachment_thumb_url( $imageId, 'thumbnail' );
							} else {
								$categoryImage = WYZ_PLUGIN_ROOT_URL . 'assets/images/restaurant.svg';
							}
							echo '<div class="res-category-list">';
							echo '<img src="'. $categoryImage .'" alt="'.$category->name.'" />';
							echo '<span>'.$category->name.'</span>' ;
							echo '</div>';

						}
					}
					?>
				</div>
				<!--<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="row restaurant-rating-row-one">-->

			</div>
	</div>

<div class="col-lg-9 col-md-9 col-sm-12 new_detail_layout">
	<div class="categories_left_sec">
				
	</div>
<div class="details-left-sec">
	<div class="dish_search_data">
			<!-- <a href="#alertModal" id="alert_box">Check Alert</a> -->
			<div class="row restaurant-logo-section">
				<div class="col-md-5 log-section-item">
					<div class="row"></div>
				</div>
			</div>	

			<div class="row">
				<div class="col-md-12 col-sm-12 restaurant-name-section">
					<!-- <div class="row">
						<div class="col-md-12">
							<span class="restaurant-inner-name"><?php // echo $restaurantName;?></span>

							<span><?php /* $reviews = new Wyz_Chef_Reviews();
							echo $reviews->wyz_comment_rating_get_average_ratings(get_the_ID())."&nbsp;";
							echo $reviews->wyz_comment_average_ratings(get_the_ID());
							do_action('wyz_restaurant_total_reviews', get_the_ID()); */ ?></span>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 restaurant-name-section resto-cat">
							<ul>
								<?php /*
								if (isset($wp_restaurant_type) && !empty($wp_restaurant_type)) {
									for ($i=0; $i < count($wp_restaurant_type); $i++) {
										$type = get_term($wp_restaurant_type[$i]);
										echo '<li>'. $type->name .'</li>';
									}

								} */ ?>  
							</ul>
						</div>
					</div> -->
					<div class="row">          
						<div class="col-md-12 order-detail-review">
							<ul>
								<li>
									<span><img src="<?php echo WYZ_PLUGIN_ROOT_URL;?>assets/images/dollar.svg" /></span>
									<span class="order-info-list-icon">Min.order required:<b>$<?php echo $wp_restaurant_minimum_order_amount; ?></b></span>
								</li>
								<li>
									<span><img src="<?php echo WYZ_PLUGIN_ROOT_URL;?>assets/images/delivery.svg" /></span>
									<span class="order-info-list-icon">Delivery Fee:from <b>$<?php echo $applicableDeliveryFee; ?></b></span>
									<a class="custom_info_tooltip" href="#"><i class="fa fa-info-circle"></i>
										<div class="tooltip_inner">
											<p>
												<?php echo $deliveryToolTip; ?>
											</p>
										</div>
									</a>
								</li>
								<li>
									<span><img src="<?php echo WYZ_PLUGIN_ROOT_URL;?>assets/images/time.svg" /></span>
									<span class="order-info-list-icon">Order Notice: <b><?php echo $wp_restaurant_lead_time;?>hrs</b></span>
								</li>
							</ul>
						</div>                    
					</div>              
				</div>
				<!-- <div class="col-md-5 col-sm-5 end-img">
					<?php
					if (isset($wp_restaurant_category) && !empty($wp_restaurant_category)) {
						for ($j=0; $j < count($wp_restaurant_category); $j++) {
							$category = get_term($wp_restaurant_category[$j]);
							$imageId = get_term_meta( $category->term_id, 'wyz-category-image-id', true );
							if( $imageId ) {
								$categoryImage = wp_get_attachment_thumb_url( $imageId, 'thumbnail' );
							} else {
								$categoryImage = WYZ_PLUGIN_ROOT_URL . 'assets/images/restaurant.svg';
							}
							echo '<div class="res-category-list">';
							echo '<img src="'. $categoryImage .'" alt="'.$category->name.'" />';
							echo '<span>'.$category->name.'</span>' ;
							echo '</div>';

						}
					}
					?>
				</div> -->
				<!--<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="row restaurant-rating-row-one">-->

			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 header-filter-form detail-filter">
					<div class="dishes-search">
						<input type="text" id="dish-search" name="dish-search" placeholder="Search item" value=""/>
						<button id="dish-button"><span class="detail-search-icon fa fa-search"></span></button>
					</div>
				
					<?php 
						if ($postId) {
							global $wpdb;
							$sqlQuery = "
								SELECT DISTINCT tags FROM {$wpdb->prefix}wyz_restaurant_dishes 
								WHERE restaurant_id = {$postId} 
								ORDER BY id ASC
							";

							$dishTags = $wpdb->get_col($sqlQuery);
						}
						$dishTags = array_filter(array_unique(explode(',', implode(',', $dishTags))));
						
						$tags = array();
						if ($dishTags) {
							// tags filter dropdown
							$tags = get_terms([
								'taxonomy' => 'restaurant_tag', 
								'hide_empty' => false,
								'include' => $dishTags,
							]);

							echo '
								<span class="filter-tags inner-filter-button"><i id="filter_icon"></i>Filter</span>
								<span class="inner-reset-button">Clear filters</span>
								<div class="options-choices common-choices common-radio">
									<ul class="inline-display-list">
							';
							
							foreach( $tags as $dtag ){
								echo "<li><label><input name='tags[]' type='checkbox' class='dish-tag' value='{$dtag->term_id}'><span>{$dtag->name}</span></label></li>";
							}

							echo '<li></li>';

							echo '
									</ul>
								</div>
								<ul id="cat_list"></ul>
							';
						}
					?>
				</div>
			</div>
		<!-- <div class="row">	
		<div class="col-md-6 tracking-title">
		<ul>
		<li>Chinese</li>
		<li>Salad</li> 
		<li>Sandwiches</li> 
		</ul>
		</div>
		<div class="col-md-6 dishes-search">
		<div class="row">
		<input type="text" name="email" placeholder="Enter">
		<button>Receive Offers</button>		
		</div>		
		</div>	
		</div> -->
		<?php
		if (count($dishDetails) > 0) {
			$j = 0;
			{
				?>
				<div class="row search-section">
					<?php if ($j == 0) { ?>
						<div class="col-md-12">
							<div class="row">
								
								
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12 restaurant-name-section">
									<!-- restaurant menu section-->

									<ul class="menu-list-items">
										<h3>Categories</h3>
										<?php
										// $firstMenuId = null;
										foreach ($menuDetails as $key => $menu) {
											// if (!$firstMenuId) {
											//     $firstMenuId = $menu->menu_id;
											// }

											$menuId = $menu->menu_id;
											$menu = get_term($menuId);
											echo '<li><a href="#'.$menuId.'" class="btn dis-menu-list" data-id="'. $dishId .'" data-menu-id="'.$menuId.'">'. $menu->name .'</a></li>'

											;

										}
										?>  
									</ul>
								</div>
								
							</div>
						</div>
					<?php } else { ?>
						<div class="row">
							<div class="col-md-12"><span class="dishes-category-label"><?php echo $key;?></span></div>
						</div>
					<?php } } ?>
					<div class="col-md-12">
						<style>
							#dishes-listing-loader {
								background-color: #ffffff;
								height: 100%; 
								opacity: 0.8;
								position: absolute;
								text-align: center;
								width: 98%;
								z-index: 1;
								height: 75px;
								bottom: 20px;
							}

							#dishes-listing-loader .loader-wrapper {
								bottom: 0px;
								left: 0;
								position: absolute;
								right: 0;
								top:10%;
							}
						</style>
						<div id='dishes-listing-loader'>
							<div class='loader-wrapper'>
								<img src='<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/loading.gif' />
							</div>
						</div>
						<div class="col-md-12 dish_cat_head">
							<h4>All Available Dishes:</h4>
						</div>
						<div id="dish-listing" class="row dish-items">
							<?php
							$items = '';
							$j = 0;

							$dishArray = array();
							$dishModifierTable = "{$wpdb->prefix}wyz_restaurant_dish_modifiers";
							$modifierItemTable = "{$wpdb->prefix}wyz_restaurant_dish_modifier_items";
							foreach ($dishDetails as  $dish) {
								$dishId = $dish->id;
								// $i = $dishId;

								// $tags = explode(',', $dish->tags);
								// $dishTagClass = '';
								// foreach ($tags as $tag) {
								//     $dishTagClass .= "dishtag-{$tag} ";
								// }
								if($dishId > 0){ 
									$dishArray[$dishId]['id'] = $dish->id;
									$dishArray[$dishId]['name'] = htmlspecialchars_decode($dish->name, ENT_QUOTES);
									$dishArray[$dishId]['description'] = htmlspecialchars_decode($dish->description, ENT_QUOTES);
									$dishArray[$dishId]['price'] = $dish->price;
									$dishArray[$dishId]['tax'] = $dish->tax;

									$image = $dish->image;
									if(!empty($image)){
										$src = $image;
									}
									
									$dishArray[$dishId]['imageSrc'] = $src;
									$dishArray[$dishId]['min_quantity'] = $dish->min_quantity;
									$dishArray[$dishId]['min_guest_serve'] = $dish->min_guest_serve;
									$dishArray[$dishId]['max_guest_serve'] = $dish->max_guest_serve;
									$dishArray[$dishId]['having_modifiers'] = $dish->having_modifiers;

									if($dish->having_modifiers == '1'){
										$modifiers = $wpdb->get_results("SELECT `id`,`prompt_text`, `modifier_type`, `min`, `max` FROM $dishModifierTable WHERE `dish_id` = '{$dishId}'", ARRAY_A);
										foreach ($modifiers as $key1 => $modifier) {
											$dishArray[$dishId]['modifiers'][$modifier['id']]['modifier'] = $modifier;
											$modifierItems = $wpdb->get_results("SELECT * FROM {$modifierItemTable} WHERE `modifier_id` = {$modifier[id]}", ARRAY_A);
											// $dishArray[$dishId]['modifiers'][$modifier['id']]['modifire_items'] = $modifierItems;
											foreach ($modifierItems as $key2 => $modifierItem) {
												$dishArray[$dishId]['modifiers'][$modifier['id']]['modifire_items'][$modifierItem['id']] = $modifierItem;
											}
										}
									}
									?>
									
									<div class="col-md-12 col-sm-12 detail-add-popup">
										<div class="row dish-single-item" data-id="<?php echo $dishId; ?>" >
											<div class="col-md-8 col-sm-8 col-xs-8" >
												<h6 title=""><?php echo htmlspecialchars_decode($dish->name, ENT_QUOTES)?></h6>
												<p class="blurr-text"><?php 
													echo wp_trim_words( htmlspecialchars_decode($dish->description, ENT_QUOTES), 35, '...' );
													// echo wp_html_excerpt( htmlspecialchars_decode($dish->description, ENT_QUOTES), 150, '...' );
												?></p>
												<div class="dish-tag-sec">
													<?php
													$tags = $dish->tags;
													if(!empty($tags)){
														$tags_ids =  explode(",",$tags);
														foreach($tags_ids as $tags_id){
															$term = get_term( $tags_id );
															if(!empty($term)){ ?>

																<span class="dish-tags"><?php echo $term->name;?></span> 

														<?php }
												
														}
													}
												
													?>
												</div>
												
											</div>
											<div class="col-md-4 col-sm-4 col-xs-4">
												<div class="img_div">
													<img src="<?php echo $dish->image;?>">
												</div>
												<div class="dish-price-add-btn">
													<div class="price dish-box-price">
														<span class="dish-size"><?php echo "\${$dish->price}";?></span>
														<span class="dish-size with-gst">(<?php echo "\$". round(($dish->price * (1+intval($dish->tax)/100)), 2);?> w/ GST)</span>
													</div>
													<div class="dish-add-btn">
														<a href="javascript:void(0);">ADD</a>
													</div>	
												</div>
											</div>
											
										</div>
									</div>
									<?php
								}
							} 
							?>
						</div>
						<script type="text/javascript">
							var dishObj = <?php echo json_encode($dishArray) ?>;
							var deliveryObj = <?php echo json_encode($wp_restaurant_delivery) ?>;
							var serviceObj = <?php echo json_encode($restaurantServicesArray) ?>;
							var restaurantMinOrder = <?php echo $wp_restaurant_minimum_order_amount ?>;
							var restaurantLeadTime = <?php echo $wp_restaurant_lead_time ?>;
						</script>
						<input type="hidden" id="restaurant-id" value="<?php echo $postId; ?>">
						<!-- <input type="hidden" id="first-menu-id" value="<?php //echo $firstMenuId; ?>"> -->
						<input type="hidden" id="last-dish-id" value="<?php echo $dishId; ?>">
					</div> 
				</div> 
				<?php 
			$j++;
		} else { ?>
		<script type="text/javascript">
			var dishObj = {};
			var deliveryObj = {};
			var serviceObj = {};
			var restaurantMinOrder = {};
			var restaurantLeadTime = {};
		</script>
		<?php }
		?>
	</div>
<div class="row restaurant-about-section">
	<div class="col-md-12">
		<span class="restaurant-about">About <?php echo $restaurantName;?></span>			
	</div>
	<div class="col-md-12 restaurant-story">
		<div class="col-md-12 story-sec">
			
			<div class="restaurant-story-content">	
				<!--<h6>Story of the Partner</h6>-->
				<div>
					<?php
					$content =  get_post($postId);
					$content = $content->post_content;
					echo '<p>'.$content.'</p>';
					?>		
				</div>
			</div>
			<div class="restaurant-img">
				<?php if($logoImageUrl){ ?>
					<img src="<?php echo $logoImageUrl[0]; ?>">
				<?php } else{ ?>
					<img src="<?php echo WYZ_PLUGIN_ROOT_URL;?>/images/Default-logo.png;?>">
				<?php } ?>
			</div>
		</div>
	</div>
	<!-- map functionality removed as theres no use, neither using the locations nor the map section on the page -->
</div>

<!-- <div class="row receive-offer-section">
<div class="col-md-12">
<div class="row tracking-box">		
<div class="col-md-3 tracking-img">
<img src="" alt="">
</div>
<div class="col-md-9 tracking-content">
<h3>THIS RESTAURANT IS USING THE DELIVERY TRACKING SYSTEM.</h3>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
</div>
</div>
</div>
</div> -->
<!--<div class="row">	
<div class="col-md-6 tracking-title">Want to receive dine in offers from this restaurant?</div>
<div class="col-md-6 dishes-search">
<div class="row">
<input type="text" name="email" placeholder="Enter your Email" />
<button>Receive Offers</button>
</div>
</div>
</div>-->
<?php 
	$reviews = new Wyz_Chef_Reviews();
	$averageRatings = $reviews->wyz_comment_rating_get_average_ratings(get_the_ID());
	$style = 'style="display: none;"';
	if ($averageRatings) {
		$style = "";
	}
?>
<div class="row restaurant-review-section" <?php echo $style; ?> >
	<div class="col-md-12">
		<span class="restaurant-about">Reviews</span>
		<div class="review-ratings">
		<span><?php echo "{$averageRatings}&nbsp;";
		echo $reviews->wyz_comment_average_ratings(get_the_ID());
		do_action('wyz_restaurant_total_reviews', get_the_ID()); ?></span>
	</div>
	</div>
</div>

<!-- add form -->
<div class="row">
	<?php
	$reviews = new Wyz_Chef_Reviews();

	echo $reviews->wyz_restaurant_reviewslist(get_the_ID());
//echo wyz_post_total_reviews();
	do_action('wyz_chef_review_form_ui', get_the_ID());

	?>
</div>  
</div>       
</div>
<div class="col-lg-3 col-md-3 col-sm-12">
	<div class="row">
		<div class="col-md-12">
			<div class="restaurane-dish-checkout">
				<div class="your-match">
					<span class="fa fa-close"></span>
					<div class="cart_icon_heading">
						<span class="cart_top_icon"><img src="<?php echo WYZ_PLUGIN_ROOT_URL;?>assets/images/cart-cart-icon.svg"/></span>
						<h3>Your order with <?php echo $restaurantName;?></h3>
					</div>
					<div class="container-fluid filter-container">
						<div class="container filter-head new_layout">
							<form action="" method="get" class="header-filter-form" >
								<table>
									<tr>
										<td><span class="filter-tags input-group date" id="filter_date" ><i class="input-group-addon" id="filter_calender"></i><input type="text" class="datetimepicker" placeholder="Date" name="delivery_date" value="<?php echo $deliveryDate ?>" readonly></span></td>
										<td><span class="filter-tags input-group" id="head_counts" ><i class="input-group-addon" id="head_count"></i><input type="text" placeholder="Headcount" name="headcount" value="<?php echo $headcount ?>"></span></td>
									</tr>
								</table>
							</form>
						</div>
					</div>
					<div class="match-ratio text-center">
<!--<span>97%</span><p>MATCH WITH<br>YOUR PRIORITIES</p>
	<a href="" class="green-btn buttons">CHANGE PRIORITIES</a>-->
</div>
<div class="Cart-Messages">		 
	<div class="add-cart-msg" style="display: none; color:green;text-align:center;">Your item has been added to your cart.</div>
	<div class="remove-cart-msg" style="display: none; color:green;text-align:center;">Your item has been removed from your cart.</div>
	<div class="update-cart-msg" style="display: none; color:green;text-align:center;">Cart updated.</div>
	<div class="min-order-msg" style="display: none; color:red;text-align:center;">Your Order Does Not Meet The Minimum Amount.</div>
</div>
<div class="order-summary">
	<div class="default_cart_img">
		<img src="<?php echo WYZ_PLUGIN_ROOT_URL. "assets/images/new_cart.svg"; ?>">
		<p>Start Ordering</p>
		<span>Add item to your cart</span>
	</div>
	<div class="message"></div>
	<div class="order-table"></div>
	<div class="bill-div">
		<div class="cart-action-msg" style="color: #ef7844;"></div>
		<div class="bill-div download-quote-container">
			<a href="#checkout" class="orange-btn buttons cart-action-checkout" style="background-color: #ef7844; color: #FFFFFF !important; display: none;">CHECK OUT</a>
			<a href="javascript:void(0);" data-toggle="modal" data-target="#cart_download_new" class="orange-btn buttons cart-action-download" style="display: none;">DOWNLOAD QUOTE</a>
		</div>
	</div>	
</div>



</div>
</div>
</div>
</div>
</div>

    <div class="detail-add-popup">
        <div class="modal fade" id="dishModal" tabindex="-1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title cartModalLabel" id="cartModalLabel"></h5>
                        <button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <p class="blurr-text1 dish-description" style="white-space: pre;"></p>
								<ul>
									<li></li>
									<li></li>
								</ul>	
                            </div>
							<div class="col-md-5 col-sm-5 col-xs-12">
								<div class=" text-center dish_popup_img">
                                	<img src="<?php echo WYZ_PLUGIN_ROOT_URL. "assets/images/default-dish-image.png"; ?>" class="dish-image">
								</div>
								<div class="text-right popup_layout">
	
									<div class="number">
										<div class="dish_popup_price">
											<label>$<span class="dish-price"></span></label>
											<label>($<span class="dish-gst-price"></span> w/GST)</label>
										</div>		
										<span class="minus">-</span>
										<input type="text" name="quantity" class="quantity" data-min="1" value="1" />
										<span class="plus">+</span>
									</div>

									<div class="min-quantity-msg" style="color:red;">
										Minimum quantity required: <span class="min-quantity-number">1</span>
									</div>

									<div class='serve-people'>Serves <span class="min-guest-number">1</span>-<span class="max-guest-number">1</span> guests</div>

							</div>
                            </div>
                        </div> 
                        
                        <div id="modifier-parent-section"></div>
                    </div>
                    <div class="modal-footer">
                        <div class="min-quantity" style="display:none;color:red;">
                            Please Select Min <span class="min-quantity-number">1</span> Quantity Of this item
                        </div>
                        <div class="cart-added-msg"></div>
                        <div class="clearfix"></div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="add-to-cart" class="add-to-cart" data-id="" >Add</button>
                        <button type="button" id="update-cart" class="update-cart" data-key="" style="display: none;" >Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="detail-add-popup">
        <div class="modal fade" id="serviceModal" tabindex="-1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title cartModalLabel">Restaurant Services</h5>
                        <button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="service-parent-container"></div>
                    </div>
                    <div class="modal-footer">
                        <div class="service-added-msg"></div>
                        <div class="clearfix"></div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="service-checkout" class="service-checkout">Checkout</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php
$formData = array();
if (isset($_REQUEST['tags']) && !empty($_REQUEST['tags'])) {
    $tags = explode(',', $_REQUEST['tags']);
    foreach ($tags as $key => $tag) {
        $formData[] = 'dish_tag[]='. urlencode($tag);
    }
}

$formData[] = 'offset=0';
$formData[] = 'limit='. $dishListingLimit;

$formData = implode('&', $formData);
$restaurantTax = get_post_meta($postId, 'wp_restaurant_tax', true);
$restaurantTax = $restaurantTax ? $restaurantTax : 0;
?>

<script type="text/javascript">
	var restaurantTax = <?php echo $restaurantTax/100; ?>;
	var restaurantCalc = 1 + restaurantTax;

	jQuery(document).ready(function($){
	  	$('[data-toggle="tooltip"]').tooltip(); 

	  	let serviceData = '';
        
        let serviceClass = [
            'Optional',
            'Required',
        ];

        let serviceType = [
            'checkbox',
            'radio',
        ];

        let serviceHelpText = [
          
                    '<p class="req-text-p"><span class="optional"><span class="req_text">Optional</span><span class="req_text_num">(Choose as many as you like).</span></span></p>',
					'<p class="req-text-p"><span class="required"><span class="req_text">Required</span><span class="req_text_num">(Choose 1).</span></span></p>',

				];

        for(serviceKey in serviceObj) {
            let service = serviceObj[serviceKey];
            serviceData += `
                <div class="service-section" data-service-id="${ service['id'] }">
                    <span class="choice-name h5">${ service['question'] }</span>
            `;

            if (parseInt(service['quantityRequired'])) {
            	serviceData += `
            		: <input type="number" name="service[${ service['id'] }][quantity]" class="service-quantity" data-service-id="${ service['id'] }" min="1" step="1" value="1" />
        		`;
            } else {
            	serviceData += `
            		<input type="hidden" name="service[${ service['id'] }][quantity]" class="service-quantity" data-service-id="${ service['id'] }" value="1" />
        		`;
            }

            serviceData += '<br>'+ serviceHelpText[service['type']];

            let options = service['options'];
            for(optionKey in options) {
                let option = options[optionKey];
                let price = parseFloat(option['price']);
                let tax = parseFloat(option['tax']);
                let priceWithTax = (price*(1+(tax/100))).toFixed(2);
                serviceData += `
						<div class="single-collection">
							<label>
								<input type="${serviceType[service['type']]}" class ="${serviceType[service['type']]}" name="option[${ service['id'] }][]" value="${ optionKey }"><span>${ option['name'] } + $${ priceWithTax }</span>
							</label>
						</div>
                `;
            }
            serviceData += '</div>';
        }

		$('#service-parent-container').html(serviceData);
	
	});

    /*
     * Manage cart functional flow
     */
    var multiCartData = {}, cartData = {}, cartItems = {}, restaurantId = <?php echo $postId; ?>, deliveryFlag = ('<?= $deliveryDate ?>' ? '<?= $deliveryDate ?>' : false), headcount = ('<?= $headcount ?>' ? '<?= $headcount ?>' : false);

    // if (localStorage.length && (restaurantId in localStorage)) {
    if (localStorage.length && ('wyzchef_cart_data' in localStorage)) {
    	// cartData = JSON.parse(localStorage.getItem(restaurantId));
    	multiCartData = JSON.parse(localStorage.getItem('wyzchef_cart_data'));
    	if (restaurantId in multiCartData) {
    		cartData = multiCartData[restaurantId];
    	}

	    if ('cartItems' in cartData) {
	    	cartItems = cartData['cartItems'];
	    	let cartGST = 0;
	    	let cartTotal = 0;
            let deliveryGST = 0;
	    	let deliveryfee = 0;
	    	let cartHTML = '';

	    	if (Object.entries(cartItems).length > 0) {
		    	cartHTML = '<div class="checkout_table_new"><table cellpadding="10" cellspacing="1" class="show_table"><tbody>';
		    	for(key in cartItems) {
		    		let item = cartItems[key];
		    		let dishData = item['dish'];
		    		let modifiersText = item['modifiersText'];
		    		let totalBasePrice = parseFloat(item['totalBasePrice']);
		    		let totalGstPrice = parseFloat(item['totalGstPrice']);
		    		cartGST += (totalGstPrice - totalBasePrice)
		    		cartTotal += totalGstPrice;

		    		cartHTML += '\
						<tr class="dish-count-price-row">\
			                <td class="item-number">\
				                <div class="number">\
				                    <span class="minus">-</span>\
				                    <input type="number" class="cart-action-update" value="'+ item['quantity'] +'" data-key="'+ key +'" data-min="'+ dishData['min_quantity'] +'">\
				                    <span class="plus">+</span>\
				                </div>\
				            </td>\
			              	<td align="right" style="width:65px">$'+ totalGstPrice.toFixed(2) +'</td>\
			                <td><a href="#" class="cart-action-remove" data-key="'+ key +'"><span class="fa fa-trash"></span></a></td>\
						</tr>\
						<tr>\
						<td colspan="2" class="dish-name-td">\
				            	<strong data-key="'+ key +'">'+ dishData['name'] +'</strong>\
				            	<span class="modifiers-info">'+ modifiersText.join(", ") +'</span>\
			            	</td>\
						</tr>\
		            ';
		    	}

				if ('conditional_price' in deliveryObj) {
					for(let i = (deliveryObj['conditional_price'].length - 1); i >= 0; i--) {
						if (cartTotal > deliveryObj['conditional_price'][i]) {
							deliveryfee = deliveryObj['applicable_fee'][i];
							deliveryfee = parseFloat(deliveryfee ? deliveryfee : 0);
		                    deliveryGST = restaurantTax*deliveryfee;
                            deliveryfee = restaurantCalc*deliveryfee;
							break;
						}
					}
				}
                cartGST += deliveryGST;
				cartHTML += '</tbody></table></div>';

		    	cartHTML += '<table class="cart-total-details"><tbody>';

		    	if (headcount) {
		    		let perPax = parseFloat((cartTotal - cartGST - deliveryGST)/headcount);
		    		cartHTML += '\
		    			<tr class="delivery_box_row total-price-row"><td colspan="5" align="right"><span>Per Pax:</span><i>$'+ perPax.toFixed(2) +'</i></td></tr>\
		    		';
		    	}

		    	cartHTML += '\
		    		<tr class="delivery_box_row total-price-row"><td colspan="5" align="right"><span>Delivery Fees:</span><i>$'+ deliveryfee.toFixed(2) +'</i></td></tr>\
		            <tr class="total-price-row"><td colspan="5" align="right"><span>GST (Included in price):</span><i>$'+ cartGST.toFixed(2) +'</i></td></tr>\
		            <tr class="total-price-row"><td colspan="5" align="right"><span><strong>Total:</strong></span><i>$'+ (cartTotal + deliveryfee).toFixed(2) +'</i></td></tr>\
		    	';
		    	cartHTML += '</tbody></table>';
				jQuery('.default_cart_img').hide();
		    	jQuery('.cart-action-checkout').show();
		    	jQuery('.cart-action-download').show();

                cartData['deliveryObj'] = deliveryObj;
                cartData['deliveryDate'] = $('input[name="delivery_date"]').val();
                // localStorage.setItem(restaurantId, JSON.stringify(cartData));
                multiCartData[restaurantId] = cartData;
                localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));
		    } else {
		    	// cartHTML += '<p>You haven\'t added anything to your cart yet! <br/>Start adding your favourite office food!</p>';
		    	jQuery('.cart-action-checkout').hide();
				jQuery('.cart-action-download').hide();
				jQuery('.default_cart_img').show();
		    }

	    	jQuery('.order-table').html(cartHTML);
	    }
	} else {
		// jQuery('.order-table').html('<p>You haven\'t added anything to your cart yet! <br/>Start adding your favourite office food!</p>');
		jQuery('.default_cart_img').show();
    	jQuery('.cart-action-checkout').hide();
    	jQuery('.cart-action-download').hide();
	}

	if (
    	!('deliveryDate' in cartData)
    	|| (cartData['deliveryDate'] != deliveryFlag)
	) {
        cartData['deliveryDate'] = deliveryFlag;
    }

	var cartServiceItems = {};
	$('.service-checkout').on('click', function(){
		let parent = $('#service-parent-container');
		let serviceSection = $('.service-section', parent);
		let msgs = [];

		for (var i = 0; i < serviceSection.length; i++) {
			let serviceId = serviceSection[i].getAttribute('data-service-id');
			let service = serviceObj[serviceId];
			let inputText = $('input[type=number], input[type=hidden]', serviceSection[i]);
			let inputChecked = $('input:checked', serviceSection[i]);
			if (inputChecked.length) {
				let quantity = parseInt(inputText.val());

				let optionBasePrice = 0;
				let optionGstPrice = 0;
				let serviceOptions = [];
				let serviceOptionNames = [];
				let totalBasePrice = 0;
				let totalGstPrice = 0;

				if (inputChecked.length > 1) {
					for (var j = 0; j < inputChecked.length; j++) {
						let optionkey = inputChecked[j].value;
						let option = service["options"][optionkey];
						optionBasePrice += parseFloat(option['price']);
						optionGstPrice += parseFloat(option['total']);
						serviceOptions.push(option);
						serviceOptionNames.push(option['name']);
					}
				} else {
					let optionkey = inputChecked.val();
					let option = service["options"][optionkey];
					optionBasePrice += parseFloat(option['price']);
					optionGstPrice += parseFloat(option['total']);
					serviceOptions.push(option);
					serviceOptionNames.push(option['name']);
				}

				totalBasePrice += optionBasePrice*quantity;
				totalGstPrice += optionGstPrice*quantity;
				
				cartServiceItems[serviceId] = {
					'serviceId': serviceId,
					'question': serviceObj[serviceId]["question"],
					'serviceOptions': serviceOptions,
					'serviceOptionNames': serviceOptionNames,
					'quantity': quantity,
					'totalBasePrice': totalBasePrice,
					'totalGstPrice': totalGstPrice,
				};
			} else if (parseInt(service['type'])) {
				msgs.push(service['question'] + ' is required');
			}
		}

		if (msgs.length) {
			$('.service-added-msg').html(msgs.join('<br />'));
			setTimeout(function(){
                jQuery('.service-added-msg').html('');
            }, 2000);

			return false;
		}

		cartData['cartServiceItems'] = cartServiceItems;
        if (!('deliveryObj' in cartData)) {
            cartData['deliveryObj'] = deliveryObj;
        }

        if (
        	!('deliveryDate' in cartData)
        	|| (cartData['deliveryDate'] != $('input[name="delivery_date"]').val())
    	) {
            cartData['deliveryDate'] = $('input[name="delivery_date"]').val();
        }
        // localStorage.setItem(restaurantId, JSON.stringify(cartData));
        multiCartData[restaurantId] = cartData;
        localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));

		window.location.href='<?php echo site_url('checkout');?>';
	});

	jQuery('.cart-action-checkout').on('click', function(){
		let cartTotal = 0;
        let deliveryGST = 0;
        let deliveryfee = 0;
        let msgs = [];
		for(key in cartItems) {
			cartTotal += parseFloat(cartItems[key]['totalGstPrice']);
		}

        if ('conditional_price' in deliveryObj) {
            for(let i = (deliveryObj['conditional_price'].length - 1); i >= 0; i--) {
                if (cartTotal > deliveryObj['conditional_price'][i]) {
                    deliveryfee = deliveryObj['applicable_fee'][i];
                    deliveryfee = parseFloat(deliveryfee ? deliveryfee : 0);
                    deliveryGST = restaurantTax*deliveryfee;
                    deliveryfee = restaurantCalc*deliveryfee;
                    break;
                }
            }
        }
        cartTotal += deliveryfee;

		if(cartTotal < restaurantMinOrder) {
			msgs.push('Your cart is less than restaurant minimum order amount '+ restaurantMinOrder +' to checkout.');
		}

		if (!('deliveryDate' in cartData) || !(cartData["deliveryDate"]) || !deliveryFlag) {
			msgs.push('Please select valid delivery date for availability to proceed checkout.');
		}

		if (msgs.length) {
			// $('.cart-action-msg').html(msgs.join('<br />'));
			$('#alertModal .modal-body p').html(msgs.join('<br />'));
			$('#alertModal').modal();
			// setTimeout(function(){
                // $('#alertModal .modal-body p').html('');
                // $('#alertModal').modal('hide');
            // }, 3000);

			return false;
		}

		localStorage.setItem('wyzchef_cart_checkout_id', JSON.stringify(restaurantId));

		if (Object.entries(serviceObj).length) {
            $('#serviceModal').modal();
        } else {
            window.location.href='<?php echo site_url('checkout');?>';
        }

		return false;
	});

	jQuery('.download-order-info').on('click', function(){
		window.print();

		return false;
	});

    jQuery(document).on("click", ".add-to-cart", function(e){
    	let parentDiv = jQuery(this).parent().parent();
        let modifierSections = jQuery('.modifier-section', parentDiv);

        let msgArray = [];
        for (let i = 0; i < modifierSections.length; i++) {
        	let modifierSection = jQuery(modifierSections[i]);
        	if (modifierSection.hasClass('single_required')) {
        		if(jQuery('input:checked', modifierSection).length < 1) {
        			let propmtText = jQuery('span.choice-name', modifierSection).text();
        			msgArray.push('You must select anyone option for "'+ propmtText +'"');
        		}
        	} else if (modifierSection.hasClass('custom_check')) {
        		let minChecked = modifierSection.attr('data-min');
        		let maxChecked = modifierSection.attr('data-max');
        		let checkedOption = jQuery('input:checked', modifierSection).length;
        		if(
        			(checkedOption < minChecked) 
        			|| (
        				(minChecked < maxChecked)
        				&& (checkedOption > maxChecked)
    				)
				) {
        			let propmtText = jQuery('span.choice-name', modifierSection).text();
        			if ((minChecked < maxChecked)) {
        				msgArray.push('You must select option(s) between '+ minChecked +' to '+ maxChecked +' for "'+ propmtText +'"');
        			} else {
        				msgArray.push('You must select minimum '+ minChecked +' option(s) for "'+ propmtText +'"');
        			}
        		}
        	}
        }

        if (msgArray.length) {
        	// alert(msgArray.join('\n'));
        	jQuery('.cart-added-msg').html('<p style="color: #FF0000;">'+ msgArray.join('<br />') +'</p>');
        	setTimeout(function(){
			  	jQuery('.cart-added-msg').html('');
			}, 2000);

        	return false;
        }

        jQuery(this).prop('disabled', true);
        let totalBasePrice = 0;
        let totalGstPrice = 0;
        let dishId = jQuery(this).attr("data-id");
        let dishData = Object.assign({}, dishObj[dishId]);
        let dishQuantity = parseInt(jQuery('.quantity').val());
        let checkedInputs = jQuery('input:checked', modifierSections);
        let cartKey = ''+ dishId;
        let itemsObj = {};
        let dishPrice = parseFloat(dishData['price'])*dishQuantity;
        let dishTax = parseFloat(dishData['tax']);
        let modifiersText = [];
        totalBasePrice += dishPrice;
        totalGstPrice += dishPrice + (dishPrice*dishTax/100);
        for (let i = 0; i < checkedInputs.length; i++) {
        	let checkedInput = jQuery(checkedInputs[i]);
        	let modifierId = checkedInput.attr('data-modifier-id');
        	let itemId = checkedInput.val();
        	let itemText = checkedInput.next().text();
        	modifiersText.push(itemText);
        	itemsObj[itemId] = {
        		modifierId: modifierId,
        		itemId: itemId,
        		itemText: itemText
        	};
        	cartKey += itemId;
        	let modifierPrice = parseFloat(dishData['modifiers'][modifierId]['modifire_items'][itemId]['price'])*dishQuantity;
        	let modifierTax = parseFloat(dishData['modifiers'][modifierId]['modifire_items'][itemId]['tax']);
        	totalBasePrice += modifierPrice;
        	totalGstPrice += modifierPrice + (modifierPrice*modifierTax/100);
        }

        if (cartKey in cartItems) {
        	cartItems[cartKey].quantity = cartItems[cartKey].quantity + dishQuantity;
        	cartItems[cartKey].totalBasePrice += totalBasePrice;
        	cartItems[cartKey].totalGstPrice += totalGstPrice;
        } else {
        	dishData['imageSrc'] = '';
        	dishData['description'] = '';

        	cartItems[cartKey] = {
    			dish: dishData,
				modifiers: itemsObj,
				modifiersText: modifiersText,
				quantity: dishQuantity,
				totalBasePrice: totalBasePrice,
				totalGstPrice: totalGstPrice
        	};
        }

        // localStorage.setItem('cartItems', JSON.stringify(cartItems));
        cartData['cartItems'] = cartItems;
        if (!('deliveryObj' in cartData)) {
        	cartData['deliveryObj'] = deliveryObj;
        }

        if (
        	!('deliveryDate' in cartData)
        	|| (cartData['deliveryDate'] != $('input[name="delivery_date"]').val())
    	) {
            cartData['deliveryDate'] = $('input[name="delivery_date"]').val();
        }
        // localStorage.setItem(restaurantId, JSON.stringify(cartData));
        multiCartData[restaurantId] = cartData;
        localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));

        let cartGST = 0;
    	let cartTotal = 0;
    	let deliveryGST = 0;
        let deliveryfee = 0;
    	let cartHTML = '<div class="checkout_table_new"><table cellpadding="10" cellspacing="1" class="show_table"><tbody>';
    	for(key in cartItems) {
    		let item = cartItems[key];
    		let dishData = item['dish'];
	    	let modifiersText = item['modifiersText'];
    		let totalBasePrice = parseFloat(item['totalBasePrice']);
    		let totalGstPrice = parseFloat(item['totalGstPrice']);
    		cartGST += (totalGstPrice - totalBasePrice)
    		cartTotal += totalGstPrice;

    		cartHTML += '\
				<tr class="dish-count-price-row">\
					<td class="item-number">\
						<div class="number">\
							<span class="minus">-</span>\
							<input type="number" class="cart-action-update" value="'+ item['quantity'] +'" data-key="'+ key +'" data-min="'+ dishData['min_quantity'] +'">\
							<span class="plus">+</span>\
						</div>\
					</td>\
					<td align="right" style="width:65px">$'+ totalGstPrice.toFixed(2) +'</td>\
					<td><a href="#" class="cart-action-remove" data-key="'+ key +'"><span class="fa fa-trash"></span></a></td>\
				</tr>\
				<tr>\
				<td colspan="2" class="dish-name-td">\
						<strong data-key="'+ key +'">'+ dishData['name'] +'</strong>\
						<span class="modifiers-info">'+ modifiersText.join(", ") +'</span>\
					</td>\
				</tr>\
            ';
    	}

		if ('conditional_price' in deliveryObj) {
            for(let i = (deliveryObj['conditional_price'].length - 1); i >= 0; i--) {
                if (cartTotal > deliveryObj['conditional_price'][i]) {
                    deliveryfee = deliveryObj['applicable_fee'][i];
                    deliveryfee = parseFloat(deliveryfee ? deliveryfee : 0);
                    deliveryGST = restaurantTax*deliveryfee;
                    deliveryfee = restaurantCalc*deliveryfee;
                    break;
                }
            }
        }
        cartGST += deliveryGST;

		cartHTML += '</tbody></table></div>';

		cartHTML += '<table class="cart-total-details"><tbody>';

		if (headcount) {
    		let perPax = parseFloat((cartTotal - cartGST - deliveryGST)/headcount);
    		cartHTML += '\
    			<tr class="delivery_box_row total-price-row"><td colspan="5" align="right"><span>Per Pax:</span><i>$'+ perPax.toFixed(2) +'</i></td></tr>\
    		';
    	}

    	cartHTML += '\
					<tr class="delivery_box_row total-price-row"><td colspan="5" align="right"><span>Delivery Fees:</span><i>$'+ deliveryfee.toFixed(2) +'</i></td></tr>\
		            <tr class="total-price-row"><td colspan="5" align="right"><span>GST (Included in price):</span><i>$'+ cartGST.toFixed(2) +'</i></td></tr>\
		            <tr class="total-price-row"><td colspan="5" align="right"><span><strong>Total:</strong></span><i>$'+ (cartTotal + deliveryfee).toFixed(2) +'</i></td></tr>\
		    	';
		cartHTML += '</tbody></table>';
		jQuery('.default_cart_img').hide();
    	jQuery('.order-table').html(cartHTML);

    	jQuery('.cart-action-checkout').show();
    	jQuery('.cart-action-download').show();

        jQuery('.cart-added-msg').html('<p style="color: #1EBCB2;"><strong>"'+ dishData['name'] +'"</strong> added to cart.</p>');

        setTimeout(function(){
		  	jQuery('#dishModal').modal('hide');
		  	jQuery('.cart-added-msg').html('');
		  	jQuery('.add-to-cart').prop('disabled', false);
		}, 1000);

        if (Object.entries(cartData['cartItems']).length < 2) {
        	let msgs = [];
	    	if (!('deliveryDate' in cartData) || !(cartData["deliveryDate"]) || !deliveryFlag) {
				msgs.push('Select your event date and time to make sure that this vendor is available:');
			}

			if (msgs.length) {
				$('#alertModal .modal-body p').html(msgs.join('<br />'));
				$('#alertModal').modal();
				// setTimeout(function(){
	                // $('#alertModal .modal-body p').html('');
	                // $('#alertModal').modal('hide');
	            // }, 3000);

				// return false;
			}
        }

        return false;
    });

	// jQuery(document).on("click", ".minordercheck", function(e){
	// 	jQuery('.min-order-msg').show();
	// 	jQuery('.min-order-msg').delay(2000).fadeOut('slow');
        
	// 	return false;
	// });

	jQuery('.order-table').on('click', '.cart-action-remove', function(){
    	let cartHTML = '';
		let cartKey = jQuery(this).attr('data-key');
		
		delete cartItems[cartKey];
		// localStorage.setItem('cartItems', JSON.stringify(cartItems));
		cartData['cartItems'] = cartItems;
		if (Object.entries(cartItems).length > 0) {
			let cartGST = 0;
	    	let cartTotal = 0;
	    	let deliveryGST = 0;
            let deliveryfee = 0;
    		
	        
            // localStorage.setItem(restaurantId, JSON.stringify(cartData));
            multiCartData[restaurantId] = cartData;
            localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));

	    	cartHTML = '<div class="checkout_table_new"><table cellpadding="10" cellspacing="1" class="show_table"><tbody>';
	    	for(key in cartItems) {
	    		let item = cartItems[key];
	    		let dishData = item['dish'];
	    		let modifiersText = item['modifiersText'];
	    		let totalBasePrice = parseFloat(item['totalBasePrice']);
	    		let totalGstPrice = parseFloat(item['totalGstPrice']);
	    		cartGST += (totalGstPrice - totalBasePrice)
	    		cartTotal += totalGstPrice;

	    		cartHTML += '\
						<tr class="dish-count-price-row">\
			                <td class="item-number">\
				                <div class="number">\
				                    <span class="minus">-</span>\
				                    <input type="number" class="cart-action-update" value="'+ item['quantity'] +'" data-key="'+ key +'" data-min="'+ dishData['min_quantity'] +'">\
				                    <span class="plus">+</span>\
				                </div>\
				            </td>\
			              	<td align="right" style="width:65px">$'+ totalGstPrice.toFixed(2) +'</td>\
			                <td><a href="#" class="cart-action-remove" data-key="'+ key +'"><span class="fa fa-trash"></span></a></td>\
						</tr>\
						<tr>\
							<td colspan="2" class="dish-name-td">\
				            	<strong data-key="'+ key +'">'+ dishData['name'] +'</strong>\
				            	<span class="modifiers-info">'+ modifiersText.join(", ") +'</span>\
			            	</td>\
						</tr>\
	            ';
	    	}

			if ('conditional_price' in deliveryObj) {
                for(let i = (deliveryObj['conditional_price'].length - 1); i >= 0; i--) {
                    if (cartTotal > deliveryObj['conditional_price'][i]) {
                        deliveryfee = deliveryObj['applicable_fee'][i];
                        deliveryfee = parseFloat(deliveryfee ? deliveryfee : 0);
                        deliveryGST = restaurantTax*deliveryfee;
                        deliveryfee = restaurantCalc*deliveryfee;
                        break;
                    }
                }
            }
            cartGST += deliveryGST;
			cartHTML += '</tbody></table></div>';

		    cartHTML += '<table class="cart-total-details"><tbody>';

		    if (headcount) {
	    		let perPax = parseFloat((cartTotal - cartGST - deliveryGST)/headcount);
	    		cartHTML += '\
	    			<tr class="delivery_box_row total-price-row"><td colspan="5" align="right"><span>Per Pax:</span><i>$'+ perPax.toFixed(2) +'</i></td></tr>\
	    		';
	    	}

	    	cartHTML += '\
					<tr class="delivery_box_row total-price-row"><td colspan="5" align="right"><span>Delivery Fees:</span><i>$'+ deliveryfee.toFixed(2) +'</i></td></tr>\
		            <tr class="total-price-row"><td colspan="5" align="right"><span>GST (Included in price):</span><i>$'+ cartGST.toFixed(2) +'</i></td></tr>\
		            <tr class="total-price-row"><td colspan="5" align="right"><span><strong>Total:</strong></span><i>$'+ (cartTotal + deliveryfee).toFixed(2) +'</i></td></tr>\
		    	';
	    	cartHTML += '</tbody></table>';
	    } else {
	    	localStorage.clear();
	    	// cartHTML += '<p>Your cart is empty now. <br/>Start adding your favourite office foods!</p>';
			jQuery('.default_cart_img').show();
	    	jQuery('.cart-action-checkout').hide();
	    	jQuery('.cart-action-download').hide();
	    }

    	jQuery('.order-table').html(cartHTML);

    	jQuery('.message').html('<strong style="color: #ff0000; font-size: 12px;">Item removed from cart.</strong>');

    	setTimeout(function(){
		  	jQuery('.message').html('');
		}, 2000);

		return false;
	});

    var lockCalling = false, offset = 0, limit = <?php echo $dishListingLimit; ?>;
    function getDishesList(data) {
        jQuery.post(aj_ajax_demo.ajax_url, data, function(response) {
            if(response.dish && Object.keys(response.dish).length) {
				let dishHTML = '';
                for(key in response.dish){
                    let dish = response.dish[key];
                    dishObj[key] = dish;
                    dishHTML += `
						<div class="col-md-12 col-sm-12 detail-add-popup">
							<div class="row dish-single-item" data-id="${ dish.id }">
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                    <h6>${ dish.name }</h6>
                                    <p class="blurr-text">${ (dish.description.length > 200 ? dish.description.substring(0, 200) + '...' : dish.description) }</p>
                                    <div class="dish-tag-sec">
                                `;

                                for (let i = 0; i < dish['tags'].length; i++) {
                                    dishHTML += `<span class="dish-tags">${dish['tags'][i]}</span>`;
                                }

                                dishHTML += `
                                    </div>
                                </div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<div class="img_div">
										<img src="${ dish.imageSrc }">
									</div>
									<div class="price dish-box-price">
										<span class="dish-size">$${ dish.price }</span>
										<span class="dish-size with-gst">(w/GST $${ (dish.price * (1+dish.tax/100)).toFixed(2) })</span>
									</div>
									<div class="dish-add-btn">
										<a href="javascript:void(0);">ADD</a>
									</div>
								</div>
                            </div>
                        </div>
                    `;
                    jQuery('#last-dish-id').val(dish.id);
                }
                if (parseInt(response.lastDishID)) {
                    jQuery("#dish-listing").append(dishHTML);
                } else {
                    jQuery("#dish-listing").html(dishHTML);
                }
            } else if (!parseInt(response.lastDishID)) {
                jQuery("#dish-listing").html('<div class="col-md-6 col-sm-6"><h4>No Result Found..</h4></div>');
            }

            if(response.dish && Object.keys(response.dish).length) {
                lockCalling = false;
            } else {
                lockCalling = true;
            }

            jQuery("#dishes-listing-loader").hide();
        }, "json");
    }

    function getRestaurantAvailability(data) {
    	jQuery('.cart-action-msg').html('');
        jQuery.post(aj_ajax_demo.ajax_url, data, function(response) {
        	if(!response['restaurant']) {
            	deliveryFlag = false;
                jQuery('.cart-action-msg').html('<h4>Restaurant is not available for selected date.</h4>');
            } else {
            	deliveryFlag = true;
            	jQuery('.cart-action-msg').html('<h4>Restaurant is available.</h4>');

            	cartData['deliveryDate'] = data['delivery_date'];
	            multiCartData[restaurantId] = cartData;
	            localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));
            }
            // setTimeout(function(){
            //     jQuery('.cart-action-msg').html('');
            // }, 2000);
        }, "json");
    }

    jQuery(document.body).ready(function($) {
    	$('.header-filter-form').on("change", 'input[name="headcount"]', function(e){
    		$('input[name="headcount"]').val(this.value);
    		headcount = this.value;
    		cartData['headcount'] = headcount;
    	});

    	$('.header-filter-form').on("dp.change change", 'input[name="delivery_date"]', function(e){
    		$('input[name="delivery_date"]').val(this.value);
    		getRestaurantAvailability({
		        "action": "getRestaurantAvailability",
		        "id": restaurantId, 
		        "delivery_date": $(this).val(),
		    });
    	});
		
        $('.inner-reset-button').on('click', function(){
            let parentContainer = $(this).parent().parent();
            $('input[type="checkbox"]', parentContainer).prop('checked', false);
			$('#dish-button').trigger('click');
			$(".detail-filter #cat_list li").hide();	
			jQuery(".options-choices .inline-display-list li label span").removeClass( 'added' );


		});
		jQuery(document).on('click', '#cat_list li span.close', function(){
			$('#dish-button').trigger('click');

		});
		
    	jQuery("#dishes-listing-loader").hide();

        $('#dish-listing').on('click', '.dish-single-item', function(){
        	let dishId = $(this).attr('data-id');
            let dishData = dishObj[dishId];


            $('#dishModal .modal-title').text(dishData['name']);
            $('#dishModal .dish-image').attr('src', dishData['imageSrc']);
            $('#dishModal .dish-description').text(dishData['description']);

            let minQuantity = dishData['min_quantity'] > 1 ? dishData['min_quantity'] : 1;
            $('#dishModal .quantity').val(minQuantity);
            $('#dishModal .quantity').attr('data-min', minQuantity);
            // if (minQuantity > 1) {
                $('#dishModal .min-quantity-number').text(minQuantity);
            // }

            let dishPrice = parseFloat(dishData['price']);
            let dishTax = parseFloat(dishData['tax']);
            let dishGstPrice = (dishPrice + (dishPrice*dishTax/100)).toFixed(2);
			$('#dishModal .dish-price').text(dishPrice);
			$('#dishModal .dish-gst-price').text(dishGstPrice);
			

            if (
                (dishData['min_guest_serve'] > 0) 
                && (dishData['max_guest_serve'] >= dishData['min_guest_serve'])
            ) {
                $('.serve-people').show();
                $('#dishModal .min-guest-number').text(dishData['min_guest_serve']);
                $('#dishModal .max-guest-number').text(dishData['max_guest_serve']);
            } else {
                $('.serve-people').hide();
            }

            let modifierData = '';
            if (dishData['having_modifiers']) {
                let modifierClass = [
                    '',
                    'single_required',
                    'single_optional',
                    'multi_optional',
                    'custom_check',
                ];

                let modifierType = [
                    '',
                    'radio',
                    'radio',
                    'checkbox',
                    'checkbox',
                ];

                let modifierHelpText = [
                    '',
                    '<p class="req-text-p"><span class="required"><span class="req_text">Required</span><span class="req_text_num">(Choose 1).</span></span></p>',
                    '<p class="req-text-p"><span class="optional"><span class="req_text">Optional</span><span class="req_text_num">(Choose one as you like).</span></span></p>',
                    '<p class="req-text-p"><span class="optional"><span class="req_text">Optional</span><span class="req_text_num">(Choose as many as you like).</span></span></p>',
                    'custom_check',
                ];

                for(modifierKey in dishData['modifiers']) {
                    let modifier = dishData['modifiers'][modifierKey]['modifier'];
                    modifierData += `
                        <div class="modifier-section ${ modifierClass[modifier['modifier_type']] }" data-dish-id="${ dishId }" data-modifier-id="${ modifier['id'] }" data-min="${ modifier['min'] }" data-max="${ modifier['max'] }">
                            <div class="req-optional-text"><span class="choice-name h5">${ modifier['prompt_text'] }</span>
                    `;

                    if (modifier['modifier_type'] < 4) {
                        modifierData += modifierHelpText[modifier['modifier_type']];
                    } else {
                        modifierData += `
                            <p class="req-text-p"><span class="custom_check"><span class="req_text_num">(You have to select Minimum ${ modifier['min'] } ${ modifier['max'] > modifier['min'] ? 'and maximum '+ modifier['max'] : '' })</span></span></p>
                        `;
                    }
					modifierData += ` </div>`;

					modifierData += `<div class="row">`;

                    let modifierItems = dishData['modifiers'][modifierKey]['modifire_items'];
                    for(modifierItemKey in modifierItems) {
                        let modifierItem = modifierItems[modifierItemKey];
                        let price = parseFloat(modifierItem['price']);
                        let tax = parseFloat(modifierItem['tax']);
                        // let priceWithTax = (price+(price*tax/100)).toFixed(2);
                        price = (price).toFixed(2);
                        if (price == 0) {
                        	price = '';
                        } else {
                        	price = ' + $'+ price;
                        }

                        modifierData += `
								<div class="single-collection">
									<label>
										<input type="${modifierType[modifier['modifier_type']]}" class ="${modifierType[modifier['modifier_type']]} ${ modifierClass[modifier['modifier_type']] }" name="dishItem[${ dishId }][${ modifier['id'] }][]" value="${ modifierItem['id'] }" data-dish-id="${ dishId }" data-modifier-id="${ modifier['id'] }"><span>${ modifierItem['text'] }${ price }</span>
									</label>
								</div>	
                        `;
                    }
                    modifierData += '</div></div>';
                }
            }

            $('#modifier-parent-section').html(modifierData);

            $('.add-to-cart').attr('data-id', dishId);

            $('#dishModal').modal();
        });


        $('#dish-search').on('keyup', function(e){
            if (13 == e.keyCode) {
                $('#dish-button').trigger('click');
            }
        });

        $(document.body).on("click", ".dish-tag, .dis-menu-list, #dish-button", function(e){
        	$("#dishes-listing-loader").show();
			$('#dishes-listing-loader').addClass('sort_loader');
            let dishSearch = '', menuId = '';
            if ($('.dis-menu-list.active').length) {
                menuId = $('.dis-menu-list.active').attr("data-menu-id");
            }

            if ($("#dish-search").val()) {
                dishSearch = $("#dish-search").val();
            }

            let checkBoxes = $('input.dish-tag:checked').serialize();
            if (checkBoxes) {
                checkBoxes += "&";
            }

            let selectedFormData = checkBoxes;
            selectedFormData += "menuID="+ menuId +"&dishSearch="+ dishSearch;
            selectedFormData += "&restro_id="+ $('#restaurant-id').val() +"&last_dish_id=0&limit="+ limit;

            getDishesList({
                "action": "load_more_dishes",
                "formData": selectedFormData,
            });
        });

        $(window).scroll(function() {
            let finalDifference = ($('#main-header').height() + $('.restaurant-details-page').height() - $('.restaurant-about-section').height() - $('.restaurant-review-section').height() - $(window).height() - 400);

            if(
                $('#last-dish-id').length 
                && !lockCalling 
                && (
                    $(window).scrollTop() >= finalDifference
                )
            ) {
            	$("#dishes-listing-loader").show();
                lockCalling = true;

                let dishSearch = '', menuId = '';
                if ($('.dis-menu-list.active').length) {
                    menuId = $('.dis-menu-list.active').attr("data-menu-id");
                }

                if ($("#dish-search").val()) {
                    dishSearch = $("#dish-search").val();
                }

                let checkBoxes = $('input.dish-tag:checked').serialize();
                if (checkBoxes) {
                    checkBoxes += "&";
                }

                let selectedFormData = checkBoxes;
                selectedFormData += "menuID="+ menuId +"&dishSearch="+ dishSearch;
                selectedFormData += "&restro_id="+ $('#restaurant-id').val() +"&last_dish_id="+ $('#last-dish-id').val() +"&limit="+ limit;

                getDishesList({
                    "action": "load_more_dishes",
                    "formData": selectedFormData,
                });
            }
        });
        
	  	$('#dishModal').on('click', '.minus', function () {
	    	let parentElement = $(this).parent();
	    	let quantityInput = $('input', parentElement);
	    	let quantity = parseInt(quantityInput.val());
	    	let minQuantity = quantityInput.attr('data-min');
	    	minQuantity = minQuantity > 1 ? minQuantity : 1;
	    	if (minQuantity < quantity) {
	        	quantityInput.val(quantity - 1);
	    	}

	    	return false;
	  	});

	  	$('#dishModal').on('click', '.plus', function () {
	    	let parentElement = $(this).parent();
	    	let quantityInput = $('input', parentElement);
	    	let quantity = parseInt(quantityInput.val());
	    	quantityInput.val(quantity + 1);

	    	return false;
	  	});
        
	  	$('.order-table').on('click', '.minus', function () {
	  		let parentElement = $(this).parent();
	    	let quantityInput = $('input', parentElement);
	    	let quantity = parseInt(quantityInput.val());
	    	let minQuantity = quantityInput.attr('data-min');
	    	minQuantity = minQuantity > 1 ? minQuantity : 1;
	    	if (minQuantity < quantity) {
	    		quantity--;
	        	quantityInput.val(quantity);

	        	let cartKey = quantityInput.attr('data-key');
	        	let oldQuantity = parseInt(cartItems[cartKey]["quantity"]);
	        	let totalBasePrice = parseFloat(cartItems[cartKey]["totalBasePrice"])*quantity/oldQuantity;
	        	let totalGstPrice = parseFloat(cartItems[cartKey]["totalGstPrice"])*quantity/oldQuantity;

				cartItems[cartKey]["quantity"] = quantity;
				cartItems[cartKey]["totalBasePrice"] = totalBasePrice;
				cartItems[cartKey]["totalGstPrice"] = totalGstPrice;
				// localStorage.setItem('cartItems', JSON.stringify(cartItems));
				cartData['cartItems'] = cartItems;
                // localStorage.setItem(restaurantId, JSON.stringify(cartData));
                multiCartData[restaurantId] = cartData;
                localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));

				let cartGST = 0;
		    	let cartTotal = 0;
		    	let deliveryGST = 0;
                let deliveryfee = 0;
		    	let cartHTML = '';

		    	if (Object.entries(cartItems).length > 0) {
			    	cartHTML = '<div class="checkout_table_new"><table cellpadding="10" cellspacing="1" class="show_table"><tbody>';
			    	for(key in cartItems) {
			    		let item = cartItems[key];
			    		let dishData = item['dish'];
	    				let modifiersText = item['modifiersText'];
			    		let totalBasePrice = parseFloat(item['totalBasePrice']);
			    		let totalGstPrice = parseFloat(item['totalGstPrice']);
			    		cartGST += (totalGstPrice - totalBasePrice)
			    		cartTotal += totalGstPrice;

			    		cartHTML += '\
						<tr class="dish-count-price-row">\
			                <td class="item-number">\
				                <div class="number">\
				                    <span class="minus">-</span>\
				                    <input type="number" class="cart-action-update" value="'+ item['quantity'] +'" data-key="'+ key +'" data-min="'+ dishData['min_quantity'] +'">\
				                    <span class="plus">+</span>\
				                </div>\
				            </td>\
			              	<td align="right" style="width:65px">$'+ totalGstPrice.toFixed(2) +'</td>\
			                <td><a href="#" class="cart-action-remove" data-key="'+ key +'"><span class="fa fa-trash"></span></a></td>\
						</tr>\
						<tr>\
						<td colspan="2" class="dish-name-td">\
				            	<strong data-key="'+ key +'">'+ dishData['name'] +'</strong>\
				            	<span class="modifiers-info">'+ modifiersText.join(", ") +'</span>\
			            	</td>\
						</tr>\
			            ';
			    	}

					if ('conditional_price' in deliveryObj) {
                        for(let i = (deliveryObj['conditional_price'].length - 1); i >= 0; i--) {
                            if (cartTotal > deliveryObj['conditional_price'][i]) {
                                deliveryfee = deliveryObj['applicable_fee'][i];
                                deliveryfee = parseFloat(deliveryfee ? deliveryfee : 0);
                                deliveryGST = restaurantTax*deliveryfee;
                            	deliveryfee = restaurantCalc*deliveryfee;
                                break;
                            }
                        }
                    }
                    cartGST += deliveryGST;
					cartHTML += '</tbody></table></div>';

		    	    cartHTML += '<table class="cart-total-details"><tbody>';

		    	    if (headcount) {
		    		let perPax = parseFloat((cartTotal - cartGST - deliveryGST)/headcount);
			    		cartHTML += '\
			    			<tr class="delivery_box_row total-price-row"><td colspan="5" align="right"><span>Per Pax:</span><i>$'+ perPax.toFixed(2) +'</i></td></tr>\
			    		';
			    	}

			    	cartHTML += '\
						<tr class="delivery_box_row total-price-row"><td colspan="5" align="right"><span>Delivery Fees:</span><i>$'+ deliveryfee.toFixed(2) +'</i></td></tr>\
			            <tr class="total-price-row"><td colspan="5" align="right"><span>GST (Included in price):</span><i>$'+ cartGST.toFixed(2) +'</i></td></tr>\
			            <tr class="total-price-row"><td colspan="5" align="right"><span><strong>Total:</strong></span><i>$'+ (cartTotal + deliveryfee).toFixed(2) +'</i></td></tr>\
			    	';

			    	cartHTML += '</tbody></table>';
			    } else {
			    	// cartHTML += '<p>Your cart is empty now. <br/>Start adding your favourite office foods!</p>';
			    }

		    	$('.order-table').html(cartHTML);
	    	}

	    	return false;
	  	});

	  	$('.order-table').on('click', '.plus', function () {
	  		let parentElement = $(this).parent();
	    	let quantityInput = $('input', parentElement);
	    	let quantity = parseInt(quantityInput.val()) + 1;
	    	quantityInput.val(quantity);

	    	let cartKey = quantityInput.attr('data-key');
        	let oldQuantity = parseInt(cartItems[cartKey]["quantity"]);
        	let totalBasePrice = parseFloat(cartItems[cartKey]["totalBasePrice"])*quantity/oldQuantity;
        	let totalGstPrice = parseFloat(cartItems[cartKey]["totalGstPrice"])*quantity/oldQuantity;

			cartItems[cartKey]["quantity"] = quantity;
			cartItems[cartKey]["totalBasePrice"] = totalBasePrice;
			cartItems[cartKey]["totalGstPrice"] = totalGstPrice;

			// localStorage.setItem('cartItems', JSON.stringify(cartItems));
			cartData['cartItems'] = cartItems;
            // localStorage.setItem(restaurantId, JSON.stringify(cartData));
            multiCartData[restaurantId] = cartData;
            localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));

			let cartGST = 0;
	    	let cartTotal = 0;
	    	let deliveryGST = 0;
            let deliveryfee = 0;
	    	let cartHTML = '';

	    	if (Object.entries(cartItems).length > 0) {
		    	cartHTML = '<div class="checkout_table_new"><table cellpadding="10" cellspacing="1" class="show_table"><tbody>';
		    	for(key in cartItems) {
		    		let item = cartItems[key];
		    		let dishData = item['dish'];
		    		let modifiersText = item['modifiersText'];
		    		let totalBasePrice = parseFloat(item['totalBasePrice']);
		    		let totalGstPrice = parseFloat(item['totalGstPrice']);
		    		cartGST += (totalGstPrice - totalBasePrice)
		    		cartTotal += totalGstPrice;

		    		cartHTML += '\
						<tr class="dish-count-price-row">\
			                <td class="item-number">\
				                <div class="number">\
				                    <span class="minus">-</span>\
				                    <input type="number" class="cart-action-update" value="'+ item['quantity'] +'" data-key="'+ key +'" data-min="'+ dishData['min_quantity'] +'">\
				                    <span class="plus">+</span>\
				                </div>\
				            </td>\
			              	<td align="right" style="width:65px">$'+ totalGstPrice.toFixed(2) +'</td>\
			                <td><a href="#" class="cart-action-remove" data-key="'+ key +'"><span class="fa fa-trash"></span></a></td>\
						</tr>\
						<tr>\
							<td colspan="2" class="dish-name-td">\
				            	<strong data-key="'+ key +'">'+ dishData['name'] +'</strong>\
				            	<span class="modifiers-info">'+ modifiersText.join(", ") +'</span>\
			            	</td>\
						</tr>\
		            ';
		    	}

				if ('conditional_price' in deliveryObj) {
                    for(let i = (deliveryObj['conditional_price'].length - 1); i >= 0; i--) {
                        if (cartTotal > deliveryObj['conditional_price'][i]) {
                            deliveryfee = deliveryObj['applicable_fee'][i];
                            deliveryfee = parseFloat(deliveryfee ? deliveryfee : 0);
                            deliveryGST = restaurantTax*deliveryfee;
                            deliveryfee = restaurantCalc*deliveryfee;
                            break;
                        }
                    }
                }
                cartGST += deliveryGST;
				cartHTML += '</tbody></table></div>';

		    	cartHTML += '<table class="cart-total-details"><tbody>';

		    	if (headcount) {
		    		let perPax = parseFloat((cartTotal - cartGST - deliveryGST)/headcount);
		    		cartHTML += '\
		    			<tr class="delivery_box_row total-price-row"><td colspan="5" align="right"><span>Per Pax:</span><i>$'+ perPax.toFixed(2) +'</i></td></tr>\
		    		';
		    	}

		    	cartHTML += '\
					<tr class="delivery_box_row total-price-row"><td colspan="5" align="right"><span>Delivery Fees:</span><i>$'+ deliveryfee.toFixed(2) +'</i></td></tr>\
		            <tr class="total-price-row"><td colspan="5" align="right"><span>GST (Included in price):</span><i>$'+ cartGST.toFixed(2) +'</i></td></tr>\
		            <tr class="total-price-row"><td colspan="5" align="right"><span><strong>Total:</strong></span><i>$'+ (cartTotal + deliveryfee).toFixed(2) +'</i></td></tr>\
		    	';
		    	cartHTML += '</tbody></table>';
		    } else {
		    	// cartHTML += '<p>Your cart is empty now. <br/>Start adding your favourite office foods!</p>';
		    }

	    	$('.order-table').html(cartHTML);

	    	return false;
	  	});

	  	$('#dishModal').on('change', '.quantity', function () {
	  		let quantity = 1;
	  		let minQuantity = parseInt($(this).attr('data-min'));
	  		if ($(this).val()) {
	  			quantity = parseInt($(this).val());
	  			quantity = (quantity < minQuantity) ? minQuantity : quantity;
	  		} else {
	  			quantity = minQuantity > 1 ? minQuantity : 1;
	  		}

	  		$(this).val(quantity);
	  	});

	  	$('.order-table').on('change', '.cart-action-update', function () {
	  		let quantity = 1;
	  		let minQuantity = parseInt($(this).attr('data-min'));
	  		if ($(this).val()) {
	  			quantity = parseInt($(this).val());
	  			quantity = (quantity < minQuantity) ? minQuantity : quantity;
	  		} else {
	  			quantity = minQuantity > 1 ? minQuantity : 1;
	  		}

	    	let cartKey = $(this).attr('data-key');
			let oldQuantity = parseInt(cartItems[cartKey]["quantity"]);
        	let totalBasePrice = parseFloat(cartItems[cartKey]["totalBasePrice"])*quantity/oldQuantity;
        	let totalGstPrice = parseFloat(cartItems[cartKey]["totalGstPrice"])*quantity/oldQuantity;

			cartItems[cartKey]["quantity"] = quantity;
			cartItems[cartKey]["totalBasePrice"] = totalBasePrice;
			cartItems[cartKey]["totalGstPrice"] = totalGstPrice;

			// localStorage.setItem('cartItems', JSON.stringify(cartItems));
			cartData['cartItems'] = cartItems;
            // localStorage.setItem(restaurantId, JSON.stringify(cartData));
            multiCartData[restaurantId] = cartData;
            localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));

			let cartGST = 0;
	    	let cartTotal = 0;
	    	let deliveryGST = 0;
            let deliveryfee = 0;
	    	let cartHTML = '';

	    	if (Object.entries(cartItems).length > 0) {
		    	cartHTML = '<div class="checkout_table_new"><table cellpadding="10" cellspacing="1" class="show_table"><tbody>';
		    	for(key in cartItems) {
		    		let item = cartItems[key];
		    		let dishData = item['dish'];
		    		let modifiersText = item['modifiersText'];
		    		let totalBasePrice = parseFloat(item['totalBasePrice']);
		    		let totalGstPrice = parseFloat(item['totalGstPrice']);
		    		cartGST += (totalGstPrice - totalBasePrice)
		    		cartTotal += totalGstPrice;

		    		cartHTML += '\
						<tr class="dish-count-price-row">\
			                <td class="item-number">\
				                <div class="number">\
				                    <span class="minus">-</span>\
				                    <input type="number" class="cart-action-update" value="'+ item['quantity'] +'" data-key="'+ key +'" data-min="'+ dishData['min_quantity'] +'">\
				                    <span class="plus">+</span>\
				                </div>\
				            </td>\
			              	<td align="right" style="width:65px">$'+ totalGstPrice.toFixed(2) +'</td>\
			                <td><a href="#" class="cart-action-remove" data-key="'+ key +'"><span class="fa fa-trash"></span></a></td>\
						</tr>\
						<tr>\
							<td colspan="2" class="dish-name-td">\
				            	<strong data-key="'+ key +'">'+ dishData['name'] +'</strong>\
				            	<span class="modifiers-info">'+ modifiersText.join(", ") +'</span>\
			            	</td>\
						</tr>\
		            ';
		    	}

				if ('conditional_price' in deliveryObj) {
                    for(let i = (deliveryObj['conditional_price'].length - 1); i >= 0; i--) {
                        if (cartTotal > deliveryObj['conditional_price'][i]) {
                            deliveryfee = deliveryObj['applicable_fee'][i];
                            deliveryfee = parseFloat(deliveryfee ? deliveryfee : 0);
                            deliveryGST = restaurantTax*deliveryfee;
                            deliveryfee = restaurantCalc*deliveryfee;
                            break;
                        }
                    }
                }
                cartGST += deliveryGST;
				cartHTML += '</tbody></table></div>';

		    	cartHTML += '<table class="cart-total-details"><tbody>';

		    	if (headcount) {
		    		let perPax = parseFloat((cartTotal - cartGST - deliveryGST)/headcount);
		    		cartHTML += '\
		    			<tr class="delivery_box_row total-price-row"><td colspan="5" align="right"><span>Per Pax:</span><i>$'+ perPax.toFixed(2) +'</i></td></tr>\
		    		';
		    	}

		    	cartHTML += '\
					<tr class="delivery_box_row total-price-row"><td colspan="5" align="right"><span>Delivery Fees:</span><i>$'+ deliveryfee.toFixed(2) +'</i></td></tr>\
		            <tr class="total-price-row"><td colspan="5" align="right"><span>GST (Included in price):</span><i>$'+ cartGST.toFixed(2) +'</i></td></tr>\
		            <tr class="total-price-row"><td colspan="5" align="right"><span><strong>Total:</strong></span><i>$'+ (cartTotal + deliveryfee).toFixed(2) +'</i></td></tr>\
		    	';
		    	cartHTML += '</tbody></table>';
		    } else {
		    	// cartHTML += '<p>Your cart is empty now. <br/>Start adding your favourite office foods!</p>';
		    }
			
	    	$('.order-table').html(cartHTML);

	    	return false;
	  	});


        $('.order-table').on('click', 'td.dish-name-td strong', function(){
            let cartKey = $(this).attr('data-key');
            let dishId = parseInt(cartItems[cartKey]['dish']['id']);
			let dishData = dishObj[dishId];
			let cartItemModifiers = cartItems[cartKey]["modifiers"];
			let cartItemQuantity = parseInt(cartItems[cartKey]['quantity']);

            $('#dishModal .modal-title').text(dishData['name']);
            $('#dishModal .dish-image').attr('src', dishData['imageSrc']);
            $('#dishModal .dish-description').text(dishData['description']);

            let minQuantity = dishData['min_quantity'] > 1 ? dishData['min_quantity'] : 1;
            $('#dishModal .quantity').val(cartItemQuantity);
            $('#dishModal .quantity').attr('data-min', minQuantity);
            $('#dishModal .min-quantity-number').text(minQuantity);

            let dishPrice = parseFloat(dishData['price']);
            let dishTax = parseFloat(dishData['tax']);
            let dishGstPrice = (dishPrice + (dishPrice*dishTax/100)).toFixed(2);
			$('#dishModal .dish-price').text(dishPrice);
			$('#dishModal .dish-gst-price').text(dishGstPrice);

            if (
                (dishData['min_guest_serve'] > 0) 
                && (dishData['max_guest_serve'] >= dishData['min_guest_serve'])
            ) {
                $('.serve-people').show();
                $('#dishModal .min-guest-number').text(dishData['min_guest_serve']);
                $('#dishModal .max-guest-number').text(dishData['max_guest_serve']);
            } else {
                $('.serve-people').hide();
            }

            let modifierData = '';
            if (dishData['having_modifiers']) {
            	let checked = '';
                let modifierClass = [
                    '',
                    'single_required',
                    'single_optional',
                    'multi_optional',
                    'custom_check',
                ];

                let modifierType = [
                    '',
                    'radio',
                    'radio',
                    'checkbox',
                    'checkbox',
                ];

                let modifierHelpText = [
					'',
                    '<p class="req-text-p"><span class="required"><span class="req_text">Required</span><span class="req_text_num">(Choose 1).</span></span></p>',
                    '<p class="req-text-p"><span class="optional"><span class="req_text">Optional</span><span class="req_text_num">(Choose one as you like).</span></span></p>',
                    '<p class="req-text-p"><span class="optional"><span class="req_text">Optional</span><span class="req_text_num">(Choose as many as you like).</span></span></p>',
                    'custom_check',
                ];

                for(modifierKey in dishData['modifiers']) {
                    let modifier = dishData['modifiers'][modifierKey]['modifier'];
                    modifierData += `
                        <div class="modifier-section ${ modifierClass[modifier['modifier_type']] }" data-dish-id="${ dishId }" data-modifier-id="${ modifier['id'] }" data-min="${ modifier['min'] }" data-max="${ modifier['max'] }">
                            <span class="choice-name h5">${ modifier['prompt_text'] }<br></span>
                    `;

                    if (modifier['modifier_type'] < 4) {
                        modifierData += modifierHelpText[modifier['modifier_type']];
                    } else {
                        modifierData += `
                            <p><span class="custom_check">You have to select Minimum ${ modifier['min'] } ${ modifier['max'] > modifier['min'] ? 'and maximum '+ modifier['max'] : '' }
                            </span></p>
                        `;
                    }

                    let modifierItems = dishData['modifiers'][modifierKey]['modifire_items'];
                    for(modifierItemKey in modifierItems) {
                        let modifierItem = modifierItems[modifierItemKey];
                        let price = parseFloat(modifierItem['price']);
                        let tax = parseFloat(modifierItem['tax']);
                        // let priceWithTax = (price+(price*tax/100)).toFixed(2);
                        price = (price).toFixed(2);
                        if (price == 0) {
                        	price = '';
                        } else {
                        	price = ' + $'+ price;
                        }

                        checked = (modifierItem['id'] in cartItemModifiers) ? 'checked' : '';
                        modifierData += `
								<div class="single-collection">
									<label>
										<input type="${modifierType[modifier['modifier_type']]}" class ="${modifierType[modifier['modifier_type']]} ${ modifierClass[modifier['modifier_type']] }" name="dishItem[${ dishId }][${ modifier['id'] }][]" value="${ modifierItem['id'] }" data-dish-id="${ dishId }" data-modifier-id="${ modifier['id'] }" ${ checked }><span>${ modifierItem['text'] }${ price }</span>
									</label>
								</div>
                        `;
                    }
                    modifierData += '</div>';
                }
            }

            $('#modifier-parent-section').html(modifierData);

            $('.add-to-cart').hide();
            $('.update-cart').attr('data-id', dishId);
            $('.update-cart').attr('data-key', cartKey);
            $('.update-cart').show();

            $('#dishModal').modal();
        });
		
		$('.update-cart').on('click', function(){
			let cartKey = $(this).attr('data-key');
			delete cartItems[cartKey];
			cartData['cartItems'] = cartItems; 
            // localStorage.setItem(restaurantId, JSON.stringify(cartData));
            multiCartData[restaurantId] = cartData;
            localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));

			$('#dishModal').modal('hide');
			$(this).hide();
			$('.add-to-cart').show();
			$('.add-to-cart').trigger('click');

			return false;
        });

        /*$('input[name="delivery_date"]').on("dp.change", function(e){
			cartData['deliveryDate'] = $(this).val();
            // localStorage.setItem(restaurantId, JSON.stringify(cartData));
            multiCartData[restaurantId] = cartData;
            localStorage.setItem('wyzchef_cart_data', JSON.stringify(multiCartData));
		});*/

		$('.cart-action-download').click(function(){
			let formattedDate = 'N/A';
			let formattedTime = 'N/A';
			if (cartData["deliveryDate"]) {
				let dateObj = new Date(cartData["deliveryDate"]);
				formattedDate = dateObj.toString().split(' ')[0] +', ';
				formattedDate += dateObj.toLocaleString('default', { month: 'short' }) +' '+ dateObj.getDate() +', ';
				formattedDate += dateObj.getFullYear();

				formattedTime = dateObj.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
			}
			
			$('.quote-vendor-name').text($('.restaurant-inner-name').text());
			$('.quote-delivery-date-time').text(formattedDate +' at '+formattedTime);
			$('.quote-delivery-date').text(formattedDate);
			$('.quote-delivery-time').text(formattedTime);
			$('.quote-headcount').text($('input[name="headcount"]').val());

			let cartGST = 0;
	    	let cartTotal = 0;
            let deliveryGST = 0;
	    	let deliveryfee = 0;
	    	let cartHTML = '';

			if (Object.entries(cartItems).length > 0) {
		    	cartHTML = '<tr> <th>Qty</th> <th colspan="3">Item</th> <th>Price</th> <th>w/o GST</th> </tr> <tr>';
		    	for(key in cartItems) {
		    		let item = cartItems[key];
		    		let dishData = item['dish'];
		    		let modifiersText = item['modifiersText'];
		    		let totalBasePrice = parseFloat(item['totalBasePrice']);
		    		let totalGstPrice = parseFloat(item['totalGstPrice']);
		    		cartGST += (totalGstPrice - totalBasePrice)
		    		cartTotal += totalGstPrice;

		    		cartHTML += '\
						<tr>\
							<td>'+ item['quantity'] +'</td>\
							<td colspan="3">'+ dishData['name'] +'<br><sup>'+ modifiersText.join(", ") +'</sup></td>\
							<td>SGD</td>\
							<td>'+ totalGstPrice.toFixed(2) +'</td>\
						</tr>\
		            ';
		    	}

				if ('conditional_price' in deliveryObj) {
					for(let i = (deliveryObj['conditional_price'].length - 1); i >= 0; i--) {
						if (cartTotal > deliveryObj['conditional_price'][i]) {
							deliveryfee = deliveryObj['applicable_fee'][i];
							deliveryfee = parseFloat(deliveryfee ? deliveryfee : 0);
		                    deliveryGST = restaurantTax*deliveryfee;
                            deliveryfee = restaurantCalc*deliveryfee;
							break;
						}
					}
				}
                cartGST += deliveryGST;

		    	cartHTML += '\
		    		<tr>\
		    			<td></td>\
			    		<td colspan="3">Sub Total</td>\
						<td>SGD</td>\
						<td>'+ cartTotal.toFixed(2) +'</td>\
					</tr>\
		    		<tr>\
		    			<td></td>\
			    		<td colspan="3">Delivery Fee</td>\
						<td>SGD</td>\
						<td>'+ deliveryfee.toFixed(2) +'</td>\
					</tr>\
		    		<tr>\
		    			<td></td>\
			    		<td colspan="3">Surcharge</td>\
						<td>SGD</td>\
						<td>0</td>\
					</tr>\
		    		<tr>\
		    			<td></td>\
			    		<td colspan="3">GST</td>\
						<td>SGD</td>\
						<td>'+ cartGST.toFixed(2) +'</td>\
					</tr>\
		    		<tr>\
		    			<td></td>\
			    		<td colspan="3">Tip</td>\
						<td>SGD</td>\
						<td>0</td>\
					</tr>\
		    		<tr>\
		    			<td></td>\
			    		<td colspan="3">Total</td>\
						<td>SGD</td>\
						<td>'+ (cartTotal + deliveryfee).toFixed(2) +'</td>\
					</tr>\
		    	';
		    }

		    $('.quote-cart-items').html(cartHTML);

			 $('#cart_download_new').modal();
		});
    });

	jQuery(window).on('beforeunload', function() {
		$('body').hide();
		$(window).scrollTop(0);
	});

    jQuery(document.body).on("click", ".download-order-info", function(){
        window.print();

        return false;
	});
</script>






<div class="alert-popup">
	<div class="modal fade" id="alertModal" tabindex="-1">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title cartModalLabel" id="cartModalLabel"></h5>
					<button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<p></p>

						<!-- Alert inner date and headcount section -->
						<div class="container-fluid filter-container">
							<div class="container filter-head new_layout full-filter-bar">
								<form action="" method="get" class="header-filter-form">
									<table>
										<tbody><tr>
											<td><span class="filter-tags input-group date" id="filter_date"><i class="input-group-addon" id="filter_calender"></i><input type="text" class="datetimepicker fixed-top-cal" placeholder="Date" name="delivery_date" value="" readonly=""></span></td>
											<td><span class="filter-tags input-group" id="head_counts"><i class="input-group-addon" id="head_count"></i><input type="text" placeholder="Headcount" name="headcount" value=""></span></td>
										</tr>
									</tbody></table>
								</form>
							</div>
						</div>
					</div> 
				</div>
				<div class="modal-footer">
					<div class="clearfix"></div>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Cart Invoice popup for PDF -->
<div class="cart-download-popup invoice-popup">
    <div class="modal fade" id="cart_download_new">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header invoice_header">
                    <button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="download_cart">
				<div id="wpPUSResult">
					<div class="resultWrapper">
					<div class="header_wrapper">
						<table class="header_table">
						<tbody>
							<tr>
								<td class="dateTableData">
									<span><img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/wyzchef.png"></span><br />
								</td>
								<td style=text-align:left >
									<span>HELP: +65 8891 3599</span>
								</td>
								<td>
								<span>EMAIL: order@wyzchef.com</span>
								</td>
							</tr>
						</tbody>
						</table>
					</div>
					<div class="order-detail-header">
						<span># Order Quotation For delivery on </span> <span class="quote-delivery-date-time"></span>
					</div>	
					<div class="row three_Year_table">
						<table class="header_table">
							<tbody>
								<tr>
									<td><h4>Order Details</h4></td>
								</tr>
								<tr>
									<th>Vendor's Name</th>
									<th>Date</th>
								</tr>
								<tr>
									<td class="quote-vendor-name"></td>
									<td class="quote-delivery-date"></td>
								</tr>
								<tr>
									<th>Headcount</th>
									<th>Time</th>
								</tr>
								<tr>
									<td class="quote-headcount"></td>
									<td class="quote-delivery-time"></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="three_Year_table" style= "border-color:black; margin-top:100px;">
					<table class="header_table price_table">
						<tbody class="quote-cart-items"></tbody>
						</table>
					</div>
					</div>
				</div>
                </div>
                <div id="editor"></div>
                <div class="modal-footer">
                    <div class="clearfix"></div>
                    <button type="button" class="btn btn-secondary pdf_download download-order-info">Print Order</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>