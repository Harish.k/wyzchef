<?php
if (!defined('ABSPATH')) {
	exit('No direct script access allowed');
}
if (!class_exists('Wyz_Chef_Reviews')) {
	class Wyz_Chef_Reviews {
		public static $post_type_name = 'wyz_reviews';
		public static $posts_per_page = 10;
		public $name = "test";
		public function __construct() {
			add_action('init', array($this, 'wyz_admin_init_callback'), 10);
		}	
		public function wyz_admin_init_callback() {
			add_action('wyz_chef_review_form_ui', array($this, 'wyz_review_form_ui_callback'), 100, 1);
			add_action('add_meta_boxes_comment', array(&$this, 'reviews_add_meta_boxes_callback'));
			add_action('wyz_restaurant_total_reviews', array($this, 'wyz_restaurant_total_reviews_callback'), 100, 2);
			add_action( 'wp_enqueue_scripts', array($this, 'comment_rating_styles'), 100, 2 );
			// add_action('wp_ajax_nopriv_wyz_post_user_review', array($this,'wyz_post_user_review'));
			// add_action( 'wp_ajax_wyz_post_user_review', array($this,'wyz_post_user_review'));
		}

		public function comment_rating_styles() {
			wp_register_style( 'comment-rating-styles', WYZ_PLUGIN_ROOT_URL. 'assets/css/rating.css' );
			wp_enqueue_style( 'comment-rating-styles' );
		}


		public function reviews_add_meta_boxes_callback()
		{

			add_meta_box('wyz_comment_rating_box', __('Rating', 'mytextdomain'), array(&$this, 'show_rating'), 'comment', 'normal');

		}

		public function show_rating($comment){
			$current = get_comment_meta( $comment->comment_ID, 'wyz_rating', true );
			?>
			<select name="rating" id="rating">
				<?php
				for ( $rating = 1; $rating <= 5; $rating ++ ) {
printf( '<option value="%1$s"%2$s>%1$s</option>', $rating, selected( $current, $rating, false ) ); // WPCS: XSS ok.
}
?>
</select>
<?php
}



// public function wyz_post_user_review(){
//              global $wpdb;
//              $time = current_time('mysql');
//              $json_message = array();
//              $data = array(
// 		'comment_post_ID' 		=> $_POST['comment_post_ID'],
// 		'comment_author' 		=> $_POST['comment_author'],
// 		'comment_author_email' 	=> $_POST['review_author_email'],
// 		'comment_author_url' 	=> '',
// 		'comment_content' 		=> $_POST['comment_content'],
// 		'comment_type' 			=> '',
// 		'comment_parent' 		=> 0,
// 		'user_id' 				=> $_POST['comment_user_id'],
// 		'comment_author_IP' 	=> '',
// 		'comment_agent' 		=> $_SERVER['HTTP_USER_AGENT'],
// 		'comment_date' 			=> $time,
// 		'comment_approved' 		=> 0,
// 	); 

//           $results = $wpdb->get_row("SELECT * FROM $wpdb->comments WHERE `comment_post_ID` = ".$_POST['comment_post_ID']." AND `user_id` = ".$_POST['comment_user_id']."");
//          // echo "<pre>";
//           if( count($results) > 0 ){
//            $json_message['Response'] = 'error';
//            $json_message['class'] = 'alert alert-danger';
//         $json_message['Msg'] = 'You have already submit the review.';
//           }else{
//            $comment_id = wp_insert_comment($data);
//            update_comment_meta( $comment_id, 'wyz_rating', $_POST['rating'] );
//            update_comment_meta( $comment_id, 'wyz_title', $_POST['review_title'] );
//            $json_message['Response'] = 'success';
//            $json_message['class'] = 'alert alert-success';
//         $json_message['Msg'] = 'Your comment has been submitted.';
//           }

//           update_post_meta($_POST['comment_post_ID'], 'wyz_restaurant_avg_rating', $this->wyz_comment_average_ratings($_POST['comment_post_ID']));

//        wp_send_json($json_message, true);
//        exit;	 

// }


public function getRealIpAddr()
{
if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
{
	$ip=$_SERVER['HTTP_CLIENT_IP'];
}
elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
{
	$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
}
else
{
	$ip=$_SERVER['REMOTE_ADDR'];
}
return $ip;
}

public function wyz_restaurant_total_reviews_callback($post_id, $comment = false){
	global $wpdb;

	$comments = get_comments( array( 'post_id' => $post_id, 'status' => 'approve' ) ); 

	$total_comment = count($comments);
	if( $comment ==  true )
		return $total_comment;

	if( $total_comment  > 1){
		echo sprintf( __( '(%s  Reviews)', 'entrada' ), $total_comment );
	}
	elseif( $total_comment  > 0) {
		echo sprintf( __( '%s  Review', 'entrada' ), $total_comment );	
	}
}

//Get the average rating stars of a restaurant.
public	function wyz_comment_rating_get_average_ratings( $id ) {
	$comments = get_approved_comments( $id );


	if ( $comments ) {
		$i = 0;
		$total = 0;
		$return = '';
		foreach( $comments as $comment ){
			$rate = get_comment_meta( $comment->comment_ID, 'wyz_rating', true );
			if( isset( $rate ) && '' !== $rate ) {
				$i++;
				$total += $rate;
			}
		}
		if ( 0 === $i ) {
			return false;
		} else {
			$ratings =  round( $total / $i, 1 );
			for ($i=1; $i <=$ratings ; $i++) { 
				$return .= "<span class='fa fa-star'></span>";
			}
			$rounded_rating = round( $ratings );

			if ( abs( $rounded_rating - $ratings ) > 0.2 ) {
				$return .= '<span class="fa fa-star-half-empty" data-rating="' . $i . '"></span>';
			} else {
				$ratings = floor( $ratings );
			}

			$off_rating = 5 - $ratings;
			for ( $i = 1; $i <= $off_rating; $i++ ) {
				$a		 = $i + $ratings;
				$return .= "<span class='fa fa-star-o'></span>";;
			}

			return $return;

		}
	} else {
		return false;
	}
}

//Get the average rating stars of a restaurant.
public	function wyz_comment_average_ratings( $id ) {
	$comments = get_approved_comments( $id );


	if ( $comments ) {
		$i = 0;
		$total = 0;
		$return = '';
		foreach( $comments as $comment ){
			$rate = get_comment_meta( $comment->comment_ID, 'wyz_rating', true );
			if( isset( $rate ) && '' !== $rate ) {
				$i++;
				$total += $rate;
			}
		}
		if ( 0 === $i ) {
			return false;
		} else {
			return round( $total / $i, 1 );

		}
	} else {
		return false;
	}
}	

public function wyz_restaurant_reviewslist($post_id, $comment = false){

	global $wpdb;

	$comments = get_comments( array( 'post_id' => $post_id, 'status' => 'approve' ) ); 


	$total_comment = count($comments);
	if( $comment ==  true )
		return $total_comment;

	if( $total_comment  > 0){ ?>



		<?php
		foreach ($comments as $comment) {

			?>

			<div class="col-md-12">
				<div class="row dish-single-item review-box-design">
					<div class="col-xs-9">
						<div class="">
							<h4><?php echo $comment->comment_date; ?></h4>
							<?php 
							$return = '';
							$ratings =  get_comment_meta( $comment->comment_ID, 'wyz_rating', true );
							$off_rating = 5 - $ratings;
							for ($i=1; $i <=$ratings ; $i++) { 
								$return	 .= "<span class='fa fa-star'></span>";
							}
							for ( $i = 1; $i <= $off_rating; $i++ ) {
								$a	= $i + $ratings;
								$return .= "<span class='fa fa-star-o'></span>";
							}

							echo $return;
							?>
							<h6><?php echo $comment->comment_author; ?></h6>
							<p><?php echo $comment->comment_content; ?></p>

						</div>
						

					</div>
					<div class="col-xs-3 text-center">
						<div class="img_div">
							<?php
								$profile_pic = get_user_meta($comment->user_id,"profile_pic_url", true);
								if($profile_pic){ ?>

									<img src="<?php echo $profile_pic;?>" alt="">


								<?php }else{

									echo get_avatar( $comment->comment_author_email, '68', '', get_the_author_meta( 'display_name' ) );

								}


								?>
						</div>
						
					</div>
				</div>
			</div>


		<?php    } ?>


	<?php	}

}


public function wyz_review_form_ui_callback($post_id = '') {

	if(is_user_logged_in() ){

// add form for logged in user

		$user_id = 0;
		$user_email = '';
		$user_full_name = '';
		$current_user = wp_get_current_user();
		if (0 < $current_user->ID) {
			$user_id = $current_user->ID;
			$user_full_name = $current_user->user_firstname . ' ' . $current_user->user_lastname;
			$user_email = $current_user->user_email;
		}
		?>



<?php  }

else{




}

}
}
new Wyz_Chef_Reviews();		
}
?>
