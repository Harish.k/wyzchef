<?php
$dir = plugin_dir_url( WYZ_RESTAURANT_PLUGIN_FILE );
wp_enqueue_style( 'front-css',$dir. 'assets/fonts/font-awesome.min.css');
wp_enqueue_script( 'front-script',$dir. 'assets/js/menu.js');

global $wpdb;
    $current_user = wp_get_current_user();
        
        $htmlmaster.= '
            <style type = "text/css">
                .header-image{display:none;}
                .order-notification{position:absolute;width:25px;height:25px;border-radius:50%;color:#fff;background-color:#ef7844;right:25px;text-align:center;}
                .tabcontent {display: none;}
                .tabcontent.active {display: block;}

                @media print {
                    .modal-header, .whatsappme {
                        display: none;
                    }
                    
                    #main-header, .container-fluid, #main-footer, .side-menu-button, .submit_sidebar, .pending-orders h3, .Confirmed-orders h3, .Past-orders h3, .dish-single-item, .btn-secondary, .fa-exclamation-triangle, .bg-img, .dashboard-submit, .copyrights-sec, #errormsg {
                        display: none !important;
                    }

                    .page_description .pending-orders, .page_description .Confirmed-orders, .page_description .Past-orders {
                        border: none;
                    }
                }
            </style>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARteWOCnYMfmc4hP7xyZpLVulT7fG_Cn4&libraries=places,drawing"></script>
            <form id="submissionForm" action="'. get_the_permalink() .'" method="POST" class="restaurant-listings-form" onsubmit="return validationForm()" enctype="multipart/form-data" novalidate>
            <div class="side-menu-button">
                <span class="one"></span>
                <span class="two"></span>
                <span class="three"></span>
            </div>
        ';

        $checkAdmin = false;
        if ( in_array( 'administrator', (array) $current_user->roles ) ) {
            $checkAdmin = true;
        }

        if ($checkAdmin && isset($_GET['user_id']) && !empty($_GET['user_id'])) {
            $usrID = intval($_GET['user_id']);
        } else {
            $usrID = $current_user->ID;
        }
        
        if($usrID != 0) {
            if ($checkAdmin && isset($_GET['post_id']) && !empty($_GET['post_id'])) {
                $rID[0] = intval($_GET['post_id']);
            } else {
                $restaurantID = $wpdb->prepare("SELECT * FROM $wpdb->posts WHERE post_author='$usrID' AND post_type='wp_restaurant' AND post_title !='Auto Draft' AND post_author!='0' ORDER BY ID DESC LIMIT 1",'');
                $rID = $wpdb->get_col($restaurantID);
            }

            if (isset($rID[0])) {
                $post = get_post($rID[0]);

                $wp_contact_person_name = get_post_meta($rID[0],'wp_contact_person_name',true);
                $wp_restaurant_email = get_post_meta($rID[0],'wp_restaurant_email',true);
                $wp_restaurant_phone = get_post_meta($rID[0],'wp_restaurant_phone',true);
                $wp_restaurant_address = get_post_meta($rID[0],'wp_restaurant_address',true);
                $wp_restaurant_type = get_post_meta($rID[0],'wp_restaurant_type',true);
                $wp_restaurant_category = get_post_meta($rID[0],'wp_restaurant_category',true);
                $wp_restaurant_logo = get_post_meta($rID[0],'wp_restaurant_logo',true);
                $wp_restaurant_gallery = get_post_meta($rID[0],'wp_restaurant_gallery',true);
                $wp_restaurant_website = get_post_meta($rID[0],'wp_restaurant_website',true);
                $wp_restaurant_tagline = get_post_meta($rID[0],'wp_restaurant_tagline',true);
                $wp_restaurant_facebook = get_post_meta($rID[0],'wp_restaurant_facebook',true);
                $wp_restaurant_instagram = get_post_meta($rID[0],'wp_restaurant_instagram',true);
                // $wp_restaurant_twitter = get_post_meta($rID[0],'wp_restaurant_twitter',true);
                $wp_restaurant_opening_days = get_post_meta($rID[0],'wp_restaurant_opening_days',true);
                $wp_restaurant_delivery_days = get_post_meta($rID[0],'wp_restaurant_delivery_days',true);
                $wp_restaurant_pickup_days = get_post_meta($rID[0],'wp_restaurant_pickup_days',true);
                $wp_restaurant_closing_date = get_post_meta($rID[0],'wp_restaurant_closing_date',true);
                
                $wp_restaurant_minimum_order_amount = get_post_meta($rID[0],'wp_restaurant_minimum_order_amount',true);
                $wp_restaurant_lead_time = get_post_meta($rID[0],'wp_restaurant_lead_time',true);
                $wp_restaurant_delivery_fee = get_post_meta($rID[0],'wp_restaurant_delivery_fee',true);
                $pickup_option = get_post_meta($rID[0],'pickup_option',true);
                $wp_restaurant_delivery_area = get_post_meta($rID[0],'wp_restaurant_delivery_pincodes',true);
                $wp_restaurant_delivery_everywhere = get_post_meta($rID[0],'wp_restaurant_delivery_everywhere',true);
                
                $bank_name = get_post_meta($rID[0],'bank_name',true);
                $bank_code = get_post_meta($rID[0],'bank_code',true);
                $account_name = get_post_meta($rID[0],'account_name',true);
                $account_number = get_post_meta($rID[0],'account_number',true);
                $swift_code = get_post_meta($rID[0],'swift_code',true);
                $branch_name = get_post_meta($rID[0],'branch_name',true);
                $branch_code = get_post_meta($rID[0],'branch_code',true);
                $preferred_mode = get_post_meta($rID[0],'preferred_mode',true);

                $html1 .='<input type="hidden" name="postID" class="" value="'.$rID[0].'">';
            }
        }

        if (!isset($rID[0]) || empty($rID[0])) {
            $post = new stdClass();
            $post->post_title = '';
            $post->post_content = '';

            $wp_contact_person_name = '';
            $wp_restaurant_email = '';
            $wp_restaurant_phone = '';
            $wp_restaurant_address = '';
            $wp_restaurant_type = '';
            $wp_restaurant_category = '';
            $wp_restaurant_logo = '';
            $wp_restaurant_gallery = '';
            $wp_restaurant_website = '';
            $wp_restaurant_tagline = '';
            $wp_restaurant_facebook = '';
            $wp_restaurant_instagram = '';
            // $wp_restaurant_twitter = '';
            $wp_restaurant_opening_days = '';
            $wp_restaurant_delivery_days = '';
            $wp_restaurant_pickup_days = '';
            $wp_restaurant_closing_date = '';
            
            $wp_restaurant_minimum_order_amount = '';
            $wp_restaurant_lead_time = '';
            $wp_restaurant_delivery_fee = '';
            $pickup_option = '';
            $wp_restaurant_delivery_area = '';
            $wp_restaurant_delivery_everywhere = '';
            
            $bank_name = '';
            $bank_code = '';
            $account_name = '';
            $account_number = '';
            $swift_code = '';
            $branch_name = '';
            $branch_code = '';
            $preferred_mode = '';
        }
            
            if ($wp_restaurant_delivery_everywhere) {
                $wp_restaurant_delivery_area = 'Delivering all across Singapore';
            }

            $deliveryContent .= '
                <div class="dashboard-li-details">
                    <div class="bank-details">
                        <p>Minimum order amount <small class="sgd-text">( in <b>SGD</b> )</small></p>
                        <ul>
                            <li>'.$wp_restaurant_minimum_order_amount.'</li>
                        </ul>
                    </div>
                    <div class="account-details">
                        <p>Lead time</p>
                        <ul>
                            <li>'. $wp_restaurant_lead_time .'</li>
                        </ul>
                    </div>
                    <div class="swift-code">
                        <p>Delivery Fee <small class="sgd-text">( in <b>SGD</b> )</small></p>
                        <ul>
                            <li>'.$wp_restaurant_delivery_fee.'</li>
                        </ul>
                    </div>
                    <div class="preferred-mode">
                        <p>Pickup option available</p>
                        <ul>
                            <li>'. ($pickup_option ? 'Yes' : 'No') .'</li>
                        </ul>
                    </div>
                    <div class="preferred-mode">
                        <p>Areas delivered (Pincodes)</p>
                        <ul>
                            <li>'.$wp_restaurant_delivery_area.'</li>
                        </ul>
                    </div>
                </div>
            ';

            if (isset($rID[0])) {
                $serviceQuery = "
                    SELECT id, type, quantityRequired, question, '' as options
                    FROM {$wpdb->prefix}wyz_restaurant_services 
                    WHERE restaurant_id = {$rID[0]}
                ";
                $restaurantServices = $wpdb->get_results($serviceQuery, ARRAY_A);
                if ($restaurantServices) {
                    $deliveryContent .='<hr><div class="restaurant-services-container">';
                    foreach ($restaurantServices as $service) {
                        $deliveryContent .='
                            <div class="restaurant-service">
                                <div class="headerBox">
                                    <div class="row">
                                        <div class="col-md-11 col-sm-11">
                                            <h5 class="titleHeader">
                                                <span class="service-question-text">'. $service['question'] .'</span>
                                            </h5>
                                            <p class="card-text">
                                                <span class="service-type-text">'. ($service['type'] ? 'Required' : 'Optional') .'</span>
                                            </p>
                                        </div>
                                        <div class="col-md-1 col-sm-1"></div>
                                    </div>
                                </div>
                            </div>
                        ';
                    }
                    $deliveryContent .= '</div>';
                }
            }
            
            $html2  .= wyz_chef_dish::wp_dashboard_form_shortcode();
    
            $htmlmaster.='
                <div class="tab submit_sidebar">
                    <div class="icon-tab-logo">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/wyzchef3.png" />
                    </div>
                    <div class="icon-tab '. ((!isset($_GET['active-tab']) || $_GET['active-tab'] == 'My Profile') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/my-profile-tab.svg" />
                        <input type="button" class="tablinks '. ((!isset($_GET['active-tab']) || $_GET['active-tab'] == 'My Profile') ? 'active' : '') .'" value="My Profile" onclick="openCity(event,this.value)">
                    </div>
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Delivery') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/delivery-tab.svg" />
                        <input type="button" class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Delivery') ? 'active' : '') .'" value="Delivery" onclick="openCity(event,this.value)">
                    </div>
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Payment') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/Payment-tab.svg" />
                        <input type="button" class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Payment') ? 'active' : '') .'" value="Payment" onclick="openCity(event,this.value)">
                    </div>
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Menu') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/menu-tab.svg" />
                        <input type="button"  class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Menu') ? 'active' : '') .'" value="Menu" onclick="openCity(event,this.value)">
                    </div>
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Orders') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/orders-tab.svg" />
                        <input type="button"  class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Orders') ? 'active' : '') .'" value="Orders" onclick="openCity(event,this.value)">

                   <span class="order-notification">'.$this->pending_order_notification($rID[0]).'</span>
                    </div>
                    
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Review') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/reviews-tab.svg" />
                        <input type="button"  class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Review') ? 'active' : '') .'" value="Review" onclick="openCity(event,this.value)">
                    </div>
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Packages') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/Packages-tab.svg" />
                        <input type="button"  class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Packages') ? 'active' : '') .'" value="Packages" onclick="openCity(event,this.value)">
                    </div>
                    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Assistance') ? 'active' : '') .'">
                        <img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/support-tab.svg" />
                        <input type="button"  class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Assistance') ? 'active' : '') .'" value="Assistance" onclick="openCity(event,this.value)">
                    </div>
                </div>
            ';

            $showSubmit = true;
            if(isset($_REQUEST['form']) && $_REQUEST['form']==true) {
                $profileContent = $html1;
            } else {
                if($rID[0] > 0) {
                    $Rd = new restaurant_dashborad;
                    $detail = $Rd->Restaurantdetail($rID[0]);
                    $detail .= '<input type="hidden" name="postID" class="" value="'.$rID[0].'">';
                    $profileContent = $detail;
                    $showSubmit = false;
                } else {
                    $profileContent = $html1;
                }
            }


                
            $paymentContent .= '
                <div class="dashboard-li-details">
                    <div class="bank-details">
                        <p>Bank Details</p>
                        <ul>
                            <li>'. $bank_name .'</li>
                            <li>'. $bank_code .'</li>
                        </ul>
                    </div>
                    <div class="account-details">
                        <p>Account Details</p>
                        <ul>
                            <li>'. $account_name .'</li>
                            <li>'. $account_number .'</li>
                        </ul>
                    <div class="swift-code">
                        <p>Swift Code</p>
                        <ul>
                            <li>'. $swift_code .'</li>
                        </ul>
                    </div>
                    </div>
                    <div class="branch-details">
                        <p>Branch Details</p>
                        <ul>
                            <li>'. $branch_name .'</li>
                            <li>'. $branch_code .'</li>
                        </ul>
                    </div>
                    <div class="preferred-mode">
                        <p>Preferred Mode of Payment</p>
                        <ul>
                            <li>'. ($preferred_mode == 'cheque' ? 'Cheque' : 'Wire Transfer') .'</li>
                        </ul>
                    </div>
                </div>
            ';

            $htmlmaster.='
                    <div id="My Profile" class="tabcontent '. ((!isset($_GET['active-tab']) || $_GET['active-tab'] == 'My Profile') ? 'active' : '') .'">
                        <div class="dashboard-container">
                        <h3 class="content-heading">MY PROFILE</h3>
                        '.$profileContent.'
                        </div>
                    </div>

                    <div id="Delivery" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Delivery') ? 'active' : '') .'">
                        <div class="dashboard-container">
                            <h3 class="content-heading">DELIVERY</h3>
                            '.$deliveryContent.'
                        </div>
                    </div>

                    <!-- Payment Tab starts here -->
                      <div id="Payment" style="min-height: 586px;" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Payment') ? 'active' : '') .'">
                        <div class="dashboard-container">
                            <h3 class="content-heading">PAYMENT</h3> 
                            '. $paymentContent .'
                        </div>
                      </div>
                <!--my payment tab -->

                    <div id="Menu" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Menu') ? 'active' : '') .'">
                        <div class="dashboard-container dashboard-menu-show">
                        <h3 class="content-heading">MENU</h3>
                            '.$html2.'
                        </div>
                    </div>

                <!--my menu tab -->

  <div id="Orders" style="min-height: 586px;" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Orders') ? 'active' : '') .'">
    <div class="dashboard-container">';
    $htmlmaster .= '<h3 class="content-heading">ORDER</h3>';

     $htmlmaster .= '<div class="first-section">
        <div class="bg-img">
          <img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/resto_bg.png">
        </div>
        <div class="page_description">
            <script type="text/javascript">
             jQuery(document).on("click", ".approve", function(e){
             
             var id = jQuery(this).attr("data-id");
             var status = jQuery(this).attr("data-status");
             var restro_id = jQuery(this).attr("data-restro");
             var user_id = jQuery(this).attr("data-user");
             jQuery.ajax({
                type: "POST",
                url: "'.admin_url("admin-ajax.php").'",
                data: { 
                    action: "wyz_approve_order",
                    restro_id:restro_id,
                    user_id:user_id,
                    status:status, 
                    id:id
                   
                },
                success: function(result) {
                       jQuery(".approved-"+id).hide();
                       jQuery(".rejected-"+id).hide();
                       jQuery(".order-status-"+id).html(result);
                     },
                error: function(result) {
                        alert("error");
                    }
                });
             });

             jQuery(document).on("click", ".reject", function(e){
             
             var id = jQuery(this).attr("data-id");
             var status = jQuery(this).attr("data-status");
             var restro_id = jQuery(this).attr("data-restro");
             var user_id = jQuery(this).attr("data-user");
             jQuery.ajax({
                type: "POST",
                url: "'.admin_url("admin-ajax.php").'",
                data: { 
                    action: "wyz_decline_order",
                    restro_id:restro_id,
                    user_id:user_id,
                    status:status, 
                    id:id
                   
                },
                success: function(result) {
                       jQuery(".approved-"+id).hide();
                       jQuery(".rejected-"+id).hide();
                       jQuery(".order-status-"+id).append(result);
                     },
                error: function(result) {
                        alert("Some error occured. Please refresh the page");
                    }
                });
             });

            jQuery(document.body).on("click", ".download-order-info", function(){
                window.print();

                return false;
            });
        </script>
        ';
         if (isset($rID[0]) & !empty($rID[0])) { 
          $orderTable    = $wpdb->prefix . 'wyz_restaurant_order';
          $deliveryTable = $wpdb->prefix . 'wyz_restaurant_order_delivery_details';
         
          //pending orders
          $order_details_pending = $wpdb->get_results("SELECT ord.id, ord.user_id, ord.total, ord.status, ord.created_at, ord.delivery_fees, ord.gst, delivery.delivery_date FROM $orderTable AS ord INNER JOIN $deliveryTable AS delivery ON ord.`id` = delivery.`order_id` WHERE delivery.delivery_date > CURRENT_DATE() AND ord.restaurant_id = '".$rID[0]."' AND ord.status = '0' ORDER BY ord.id DESC", ARRAY_A);

          //Confirmed Orders
          $order_details_confirmed = $wpdb->get_results("SELECT ord.id, ord.user_id, ord.total, ord.status, ord.created_at, ord.delivery_fees, ord.gst, delivery.delivery_date FROM $orderTable AS ord INNER JOIN $deliveryTable AS delivery ON ord.`id` = delivery.`order_id` WHERE ord.restaurant_id = '".$rID[0]."' AND ord.status = '1' ORDER BY ord.id DESC", ARRAY_A);

          //Past Orders
          $order_past_details = $wpdb->get_results("SELECT ord.id, ord.user_id, ord.total, ord.status, ord.created_at, ord.delivery_fees, ord.gst, delivery.delivery_date FROM $orderTable AS ord LEFT JOIN $deliveryTable AS delivery ON ord.`id` = delivery.`order_id` WHERE delivery.delivery_date < CURRENT_DATE() AND ord.restaurant_id = '".$rID[0]."' ORDER BY ord.id DESC", ARRAY_A);


    
          if(count($order_details_pending) > 0){
            $htmlmaster .= '<div class="row pending-orders"><h3>Pending Orders <span class="order-notification-second">'.$this->pending_order_notification($rID[0]).'</span></h3> 
                <span class="fa fa-exclamation-triangle">Action Required</span>
                <div class="col-md-12">
                <div class="row">';
            foreach ($order_details_pending as $order) {
                $user_info = get_userdata($order['user_id']);
                $htmlmaster .=  '<div class="col-md-4 col-sm-6 col-xs-12">
                <div class="dish-single-item">';
                $htmlmaster .=  "<span>Order Id: #{$order[id]}</span><br>";
                $htmlmaster .= "<span>Total Amount: $ {$order[total]}</span><br>";
                 $htmlmaster .= "<span>No. Of Pax:".$this->delivery_pax($order['id'])."</span><br>";
                $htmlmaster .= $user_info->first_name .  " " . $user_info->last_name . "<br>"; 
                $htmlmaster .= 'Delivery Address: '.$this->delivery_address($order['id'])."<br>";
                $htmlmaster .= $this->delivery_date($order['id'])."<br>";
                $htmlmaster .= 'Created At: '.date('F d, Y h:i A', strtotime($order['created_at']))."<br>";
            // $htmlmaster .= 'Created At: '.$order['created_at']."<br>";
                $htmlmaster .=  '<div class="order-status-'.$order['id'].'">';
            // $htmlmaster .= $order['status'];
                if($order['status'] == 1){
                   // $htmlmaster .=   '<button type="button" data-id="'.$order['id'].'" class="btn btn-primary approved">Accepted</button>';
                  $htmlmaster .=   '<span data-id="'.$order['id'].'" class="approved">Status: Confirmed</span>';

                } elseif($order['status'] == 2){
                    $htmlmaster .=   '<button type="button" data-id="'.$order['id'].'" class="btn btn-primary reject">Declined</button>';
                } else{
                    $htmlmaster .=   '<button type="button" data-restro = "'.$rID[0].'" data-user = "'.$order['user_id'].'" data-status="2" data-id="'.$order['id'].'" class="btn btn-primary reject rejected-'.$order['id'].'">Decline</button>';

                    $htmlmaster .=   '<button type="button" data-restro = "'.$rID[0].'" data-user = "'.$order['user_id'].'" data-status="1" data-id="'.$order['id'].'" class="btn btn-primary approve approved-'.$order['id'].'">Accept</button>';
               }
                $htmlmaster .=  '</div>';
                $htmlmaster .=  '<div class="order-btn">';
                $htmlmaster .=  '<style>.order-btn .modal.in{opacity:1;}</style>';
            //$htmlmaster .=   '<b class="btn1 btn-primary1" data-toggle="modal" data-target="#order-det-'.$order['id'].'"  >Order Detail</button>';


                $htmlmaster .=   '<a href="javascript:void(0)" class="orders-popup-order" data-toggle="modal" data-target="#order-det-'.$order['id'].'">Order Detail</a>';
                

                $htmlmaster .=   '</div>';
                // $htmlmaster .= '</div>';

                $htmlmaster .= '</div></div>';


                // Order Invoice popup for PDF //
                $htmlmaster.='<div class="invoice-popup">
                <div class="modal fade" id="order-det-'.$order['id'].'" tabindex="-1">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header invoice_header">
                        <button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body" id="content">
                               
                          <div id="wpPUSResult">
                            <div class="resultWrapper">
                              <div class="header_wrapper">
                                <table class="header_table">
                                  <tbody>
                                    <tr>
                                      <td class="dateTableData">
                                        <span><img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/wyzchef.png"></span><br />
                                      </td>
                                      <td style=text-align:left >
                                        <span>HELP: +65 8891 3599</span>
                                      </td>
                                      <td>
                                      <span>EMAIL: order@wyzchef.com</span>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                              '. $this->dish_details($order) .'
                            </div>
                          </div>
                     </div>
                     <div id="editor"></div>
                      <div class="modal-footer">
                        <div class="clearfix"></div>
                        <button type="button" class="btn btn-secondary pdf_download download-order-info">Print Order</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
                </div>';
           }

          $htmlmaster .= '</div></div></div>'; 


          }
        // $htmlmaster .= '</div>';   
         if(count($order_details_confirmed) > 0){

        $htmlmaster .= '<div class="row Confirmed-orders">
            <h3>Confirmed Orders</h3>
            ';
            $htmlmaster .= '<div class="col-md-12">
               <div class="row">';
          //$htmlmaster .= '<h2>Pending Orders</h2>';
          foreach ($order_details_confirmed as $order) {
              
                 $user_info = get_userdata($order['user_id']);
                $htmlmaster .=  '<div class="col-md-4 col-sm-6 col-xs-12">
                <div class="dish-single-item">';
                $htmlmaster .=  "<span>Order Id: #{$order[id]}</span><br>";
                $htmlmaster .= "<span>Total Amount: $ {$order[total]}</span><br>";
                $htmlmaster .= $user_info->first_name .  " " . $user_info->last_name . "<br>"; 
                $htmlmaster .= 'Delivery Address: '.$this->delivery_address($order['id'])."<br>";
                $htmlmaster .= $this->delivery_date($order['id'])."<br>";
                $htmlmaster .= 'Created At: '.date('F d, Y h:i A', strtotime($order['created_at']))."<br>";
            // $htmlmaster .= 'Created At: '.$order['created_at']."<br>";
                $htmlmaster .=  '<div class="order-status-'.$order['id'].'">';
            // $htmlmaster .= $order['status'];
                if($order['status'] == 1){
                   // $htmlmaster .=   '<button type="button" data-id="'.$order['id'].'" class="btn btn-primary approved">Accepted</button>';
                  $htmlmaster .=   '<span data-id="'.$order['id'].'" class="approved">Status: Confirmed</span>';

                } elseif($order['status'] == 2){
                    $htmlmaster .=   '<button type="button" data-id="'.$order['id'].'" class="btn btn-primary reject">Declined</button>';
                } else{
                    $htmlmaster .=   '<button type="button" data-status="2" data-id="'.$order['id'].'" class="btn btn-primary reject rejected-'.$order['id'].'">Decline</button>';

                    $htmlmaster .=   '<button type="button" data-status="1" data-id="'.$order['id'].'" class="btn btn-primary approve approved-'.$order['id'].'">Accept</button>';
                }
                $htmlmaster .=  '</div>';
                $htmlmaster .=  '<div class="order-btn">';
                $htmlmaster .=  '<style>.order-btn .modal.in{opacity:1;}</style>';
            //$htmlmaster .=   '<b class="btn1 btn-primary1" data-toggle="modal" data-target="#order-det-'.$order['id'].'"  >Order Detail</button>';


                $htmlmaster .=   '<a href="javascript:void(0)" class="orders-popup-order" data-toggle="modal" data-target="#order-det-'.$order['id'].'">Order Detail</a>';
                
                $htmlmaster .=   '</div>';
                // $htmlmaster .= '</div>';

                $htmlmaster .= '</div></div>';

                // Order Invoice popup for PDF //
                $htmlmaster.='<div class="invoice-popup">
                <div class="modal fade" id="order-det-'.$order['id'].'" tabindex="-1">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header invoice_header">
                        <button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body" id="content">
                               
                          <div id="wpPUSResult">
                            <div class="resultWrapper">
                              <div class="header_wrapper">
                                <table class="header_table">
                                  <tbody>
                                    <tr>
                                      <td class="dateTableData">
                                        <span><img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/wyzchef.png"></span><br />
                                      </td>
                                      <td style=text-align:left >
                                        <span>HELP: +65 8891 3599</span>
                                      </td>
                                      <td>
                                      <span>EMAIL: order@wyzchef.com</span>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                              '. $this->dish_details($order) .'
                            </div>
                          </div>
                     </div>
                     <div id="editor"></div>
                      <div class="modal-footer">
                        <div class="clearfix"></div>
                        <button type="button" class="btn btn-secondary pdf_download download-order-info">Print Order</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
                </div>';
           }
           $htmlmaster .= '</div></div></div>'; 
        } 
        // $htmlmaster .= '</div>'; 

         if(count($order_past_details) > 0){

            $htmlmaster .= '<div class="row Past-orders"><h3>Past Orders</h3>
               <div class="col-md-12">
               <div class="row">';
          //$htmlmaster .= '<h2>Pending Orders</h2>';
          foreach ($order_past_details as $order) {
              
                 $user_info = get_userdata($order['user_id']);
                $htmlmaster .=  '<div class="col-md-4 col-sm-6 col-xs-12">
                <div class="dish-single-item">';
                $htmlmaster .=  "<span>Order Id: #{$order[id]}</span><br>";
                $htmlmaster .= "<span>Total Amount: $ {$order[total]}</span><br>";
                $htmlmaster .= $user_info->first_name .  " " . $user_info->last_name . "<br>"; 
                $htmlmaster .= 'Delivery Address: '.$this->delivery_address($order['id'])."<br>";
                $htmlmaster .= $this->delivery_date($order['id'])."<br>";
                $htmlmaster .= 'Created At: '.date('F d, Y h:i A', strtotime($order['created_at']))."<br>";
            // $htmlmaster .= 'Created At: '.$order['created_at']."<br>";
                $htmlmaster .=  '<div class="order-status-'.$order['id'].'">';
            // $htmlmaster .= $order['status'];
                if($order['status'] == 0){
                   // $htmlmaster .=   '<button type="button" data-id="'.$order['id'].'" class="btn btn-primary approved">Accepted</button>';
                  $htmlmaster .=   '<span data-id="'.$order['id'].'" class="approved">Status: Pending</span>';

                }
                if($order['status'] == 1){
                   // $htmlmaster .=   '<button type="button" data-id="'.$order['id'].'" class="btn btn-primary approved">Accepted</button>';
                  $htmlmaster .=   '<span data-id="'.$order['id'].'" class="approved">Status: Confirmed</span>';

                } elseif($order['status'] == 2){
     
                    $htmlmaster .=   '<span data-id="'.$order['id'].'" class="approved">Status: Rejected</span>';
                   
                } 
                $htmlmaster .=  '</div>';
                $htmlmaster .=  '<div class="order-btn">';
                $htmlmaster .=  '<style>.order-btn .modal.in{opacity:1;}</style>';
            //$htmlmaster .=   '<b class="btn1 btn-primary1" data-toggle="modal" data-target="#order-det-'.$order['id'].'"  >Order Detail</button>';


                $htmlmaster .=   '<a href="javascript:void(0)" class="orders-popup-order" data-toggle="modal" data-target="#order-det-'.$order['id'].'">Order Detail</a>';
                
                $htmlmaster .=   '</div>';
                // $htmlmaster .= '</div>';

                $htmlmaster .= '</div></div>';

                // Order Invoice popup for PDF //
                $htmlmaster.='<div class="invoice-popup">
                <div class="modal fade" id="order-det-'.$order['id'].'" tabindex="-1">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header invoice_header">
                        <button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body" id="content">
                               
                          <div id="wpPUSResult">
                            <div class="resultWrapper">
                              <div class="header_wrapper">
                                <table class="header_table">
                                  <tbody>
                                    <tr>
                                      <td class="dateTableData">
                                        <span><img src="'.WYZ_PLUGIN_ROOT_URL . 'assets/images/wyzchef.png"></span><br />
                                      </td>
                                      <td style=text-align:left >
                                        <span>HELP: +65 8891 3599</span>
                                      </td>
                                      <td>
                                      <span>EMAIL: order@wyzchef.com</span>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                              '. $this->dish_details($order) .'
                            </div>
                          </div>
                     </div>
                     <div id="editor"></div>
                      <div class="modal-footer">
                        <div class="clearfix"></div>
                        <button type="button" class="btn btn-secondary pdf_download download-order-info">Print Order</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
                </div>';
           }
       $htmlmaster .= '</div></div></div>';  
          }
        }
    $htmlmaster.='
        </div>
      </div>
    </div>
  </div>
<!--my order tab -->
                <div id="Review" style="min-height: 586px;" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Review') ? 'active' : '') .'">
    <div class="dashboard-container">
      <h3 class="content-heading">REVIEW</h3> 
      <div class="first-section">
        <div class="bg-img">
          <img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/resto_bg.png">
        </div>
        <div class="page_description">';
            if (isset($rID[0]) & !empty($rID[0])) { 
                $comments = get_comments( array( 'post_id' => $rID[0], 'status' => 'approve' ) ); 
                //echo "<pre>";
               // print_r($comments);
            $total_comment = count($comments);
                if( $total_comment  > 0){
           
                         foreach ($comments as $comment) {
                         
                            $htmlmaster .= '<div class="col-md-6 col-sm-6 col-xs-12">';
                      
                            $htmlmaster .= '<div class="row dish-single-item review-box-design">';
                        
                            $htmlmaster .=  '<div class="col-md-4 col-xs-12">';
                      
                                  
                     
                                      $profile_pic = get_user_meta($comment->user_id,"profile_pic_url", true);
                                      if($profile_pic){
                                           $htmlmaster .=  '<div class="review-img"><img src="'.$profile_pic.'" alt=""></div>';

                                      }else{
                                         $htmlmaster .=  '<div class="review-img">';
                                         $htmlmaster .=  get_avatar( $comment->comment_author_email, '68', '', get_the_author_meta( 'display_name' ) );
                                          $htmlmaster .= '</div>';

                                      }

                               
                               
                    
                                $htmlmaster .=  '</div>';
                                $htmlmaster .=  '<div class="col-md-8 col-xs-12">';
                                    $htmlmaster .='<h6>'.get_comment_meta( $comment->comment_ID, 'wyz_title', true ).'</h6>';
                                    $htmlmaster .=   '<div class="rating-star">';
                                
                                            //$return = '';
                                            $ratings =  get_comment_meta( $comment->comment_ID, 'wyz_rating', true );
                                            $off_rating = 5 - $ratings;
                                            //echo $off_rating;die(); 
                                            for ($i=1; $i <=$ratings ; $i++) { 
                                                $htmlmaster  .= "<span class='fa fa-star'></span>";
                                            }
                                            for ( $i = 1; $i <= $off_rating; $i++ ) {
                                                $a    = $i + $ratings;
                                                $htmlmaster .= "<span class='fa fa-star-o'></span>";
                                            }
                                    
                                            
                                        
                                        $htmlmaster .=  '</div>';
                                    $htmlmaster .='<p>'.$comment->comment_content.'</p>';
                                    $htmlmaster .='<h4>Posted on: '.date('F d, Y',strtotime($comment->comment_date)).'</h4>';

                                $htmlmaster .=  '</div>';
                            
                        $htmlmaster .=  '</div>';
                            
                        $htmlmaster .= '</div>';
                        } 
                      
                    
                    }else{
    
                $htmlmaster .= "You don't Have any reviews Yet";
                    }
            }
          $htmlmaster.='</div>
      </div>
    </div>
  </div>
<!--my review tab -->
                 <div id="Packages" style="min-height: 586px;" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Packages') ? 'active' : '') .'">
    <div class="dashboard-container">
      <h3 class="content-heading">PACKAGES</h3> 
      <div class="first-section">
        <div class="bg-img">
          <img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/resto_bg.png">
        </div>
        <div class="page_description">
          <p>We also provide various services to help our partner restaurants, caterers and food providers</p>
          <p><strong>Digital Marketing Services</strong></p>
          <p><strong>Operations and delivery optimization</strong> *Coming Soon</p>
          <p><strong>Financial analysis</strong> *Coming Soon</p>
          <p>Contact us at +65 88913599 or send us an email at contact@wyzchef.com for more information</p>
        </div>
      </div>
    </div>
  </div>
<!--my packages tab -->
                <div id="Assistance" style="min-height: 586px;" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Assistance') ? 'active' : '') .'">
    <div class="dashboard-container">
      <h3 class="content-heading">ASSISTANCE</h3> 
      <div class="first-section">
        <div class="bg-img">
          <img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/resto_bg.png">
        </div>
        <div class="page_description">
          <p>For any assistance, you can contact us at below details</p>
          <p> Email: support@wyzchef.com</p>
          <p> Phone: +65 88913599</p>
        </div>
      </div>
    </div>
  </div>
<!--my assistance tab -->
<div class="dashboard-container tabcontent">
    <div id="errormsg"></div>
    <div class="dashboard-submit">
        <a href="javascript:void(0)" data-toggle="modal" data-target="#termsAndCondtions">Terms &amp; Conditions </a>
       <!-- <span class="terms-conditions-wrapper"><input type="checkbox" name="terms_conditions" value="1" /> <a href="javascript:void(0)" data-toggle="modal" data-target="#termsAndCondtions">Terms &amp; Conditions </a></span> -->
        <!-- <a href="'. site_url('submit-restaurant') .'" class="modify-button">Edit</a> -->
    </div>
    <!-- Dashboard submit button -->
    <div class="copyrights-sec">
      <p><span><img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/copyr-logo.png" alt/></span><span>for restaurants 2019 &copy;</span>
      </p>
    </div>
    <!-- copyrights-sec -->
    
  </div>
';

$htmlmaster.='</form>'. $this->submit_modal();
//wp_enqueue_script('customjs', WYZ_PLUGIN_ROOT_URL .'assets/js/custom.js');

//echo $htmlmaster;

$htmlmaster.="
<script src='https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js'></script>

<script>
  var a4 = [595.28, 841.89]; // for a4 size paper width and height  
  var pdf = new jsPDF({
    unit: 'px',  
    format: 'a4' 
  });
 
  jQuery('#cmd').click(function () {
     pdf.addHTML(jQuery('#content')[0], function () {
        pdf.save('order-invoice.pdf');
        pdf.internal.scaleFactor = 30;     
      }); 
  });
</script>";

echo $htmlmaster;
