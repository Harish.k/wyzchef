<link rel="stylesheet" href="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/css/bootstrap-datetimepicker.css" type="text/css" />

<div class="inner-content">
  <?php

  $cartItems = $_SESSION["cart_item"];
  $order_date = '';
  foreach ($cartItems as $key => $cartItem) {
    $restro_id = $_SESSION["cart_item"][$key]['restro_id'];
    $order_date = $_SESSION["cart_item"][$key]["delivery_date"];

  }
  if($order_date == ''){

    $delivery_date = '';


  } else{
    $delivery_date = date('Y-m-d H:i:s',strtotime($order_date));

  } 

  ?>
  <?php if (isset($_GET['error'])) { ?>
    <div class="alert alert-danger">Some error occured, Please try again!</div>
  <?php } ?>
  <div class="step-heading">
    <h4>Delivery Details</h4>
    <p>Please provide your Delivery Address.</p>
  </div>
  <form class="checkout-form" id="new_address" action="<?php echo admin_url( 'admin-post.php' ); ?>" accept-charset="UTF-8" method="post">
    <input type="hidden" name="action" value="checkout_delivery"/>
    <div class="form-group row">
      <div class="col-sm-6">
        <label class="form-control-label">Company name</label>
        <input class="form-control" type="text" name="company_name" id="address_company_name" value="<?php echo get_user_meta($userDetails->ID, 'company_name', true); ?>" />
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label class="form-control-label">Postal code</label>
        <input class="form-control" type="text" name="postcode" id="address_postcode" value="<?php echo $postal; ?>" />
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label class="form-control-label">Address <span class="label-required">*</span></label>
        <input required="required" autofocus="autofocus" class="form-control" placeholder="Ex. 12 Hong Kong St." type="text" name="street_address" id="address_street_address" value = "<?php echo $userstreet; ?>" />
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label class="form-control-label">City</label>
        <input class="form-control" type="text" name="city" id="address_city" value= "Singapore" placeholder="Singapore" />
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label class="form-control-label">Unit number (Eg. #12-318)</label><br>
        <div class="unit-one-div">
          <span class="hash-unitnumber">#</span><input class="form-control" placeholder="12" type="text" name="unit_address[]" id="floor_building" value = "<?php echo $userfloor; ?>" /><span class="dash-unitnumber"style="float:left">–</span></div>
          <div class="unit-two-div">
            <input class="form-control" placeholder="318" type="text" name="unit_address[]" id="unit_number" value = "<?php echo $unitnumber; ?>" />
          </div>

        </div>
      </div>



      <!-- no. of People Required-->
      <div class="form-group row">
        <div class="col-sm-6">
          <label class="form-control-label">No. Of People<span class="label-required">*</span></label>
          <input class="form-control" required="required" type="number" name="people" min="1"  />
        </div>
      </div>

      <!-- end of text -->
      <!-- phone-->
      <div class="form-group row" style="display: none;">
        <div class="col-sm-6">

          <input class="form-control" required="required" type="hidden" name="mobile" id="user_mobile" value = "<?php echo $userPhone; ?>"/>
        </div>
      </div> 

      <!-- email-->
      <div class="form-group row">
        <div class="col-sm-6">

          <input class="form-control" required="required" type="hidden" name="email" id="user_email" value = "<?php echo $_GET['email'] ?>"/>
        </div>
      </div> 

      <!-- delivery date option-->
      <div class="form-group row">
        <div class="col-sm-6">
          <label class="form-control-label">Delivery Date<span class="label-required">*</span></label>
          <span class="filter-tags input-group date" ><i class="input-group-addon" id="filter_calender"></i>
            <input type="text" class="" name="delivery_date" readonly></span>
          </div>


        </div>
        <div class="form-group row">
          <div class="col-sm-12">
            <label class="form-control-label">Delivery instructions/ Allergies</label>
            <textarea class="form-control" rows="3" placeholder="Message Here" name="additional_instructions" id="address_additional_instructions">
            </textarea>
          </div>
        </div>
        <div class="form-group row">
          <input type="hidden" name="redirectLink" value = "<?php echo $redirect_to; ?>" />
          <input type="hidden" name="cart" id = "cartCheckoutItems" value = "" />
          <div class="col-sm-12 text-xs-center text-md-right">
            <button class="btn btn-primary btn-normal delivery-check" type="submit">Place order</button></div>
          </div>
          <div class="form-group row">
            <a href="<?php echo site_url();?>/checkout">Back</a>
          </div>   

        </form>
      </div>


      <!-- <script type="text/javascript" src="<?php // echo WYZ_PLUGIN_ROOT_URL; ?>assets/js/moment.min.js"></script>
      <script type="text/javascript" src="<?php // echo WYZ_PLUGIN_ROOT_URL; ?>assets/js/bootstrap-datetimepicker.js"></script> -->

      <script>
        document.body.classList.add('list-page');
        /*jQuery(function () {
          jQuery("#filter_date").datetimepicker({
            sideBySide: true,
          });

          jQuery("#filter_date").data("DateTimePicker").date(moment(new Date('<?php echo $delivery_date;?>') ).format('MM/DD/YYYY HH:mm'));
        });*/
      </script>

