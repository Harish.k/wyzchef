<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

global $wpdb;
$pluginUrl = WYZ_PLUGIN_ROOT_URL;
wp_enqueue_style( 'font-awesome-css', $pluginUrl .'assets/fonts/font-awesome.min.css');
wp_enqueue_style('select2', $pluginUrl .'assets/css/select-2min.css');

if (isset($_GET['user_id']) && !empty($_GET['user_id'])) {
    $usrID = intval($_GET['user_id']);
} else {
    $usrID = $current_user->ID;
}

if (isset($_GET['post_id']) && !empty($_GET['post_id'])) {
    $restaurantId = intval($_GET['post_id']);
    $restaurant = get_post($restaurantId);
} else {
    $query = $wpdb->prepare("SELECT * FROM {$wpdb->posts} WHERE post_author = %d AND post_type = 'wp_restaurant' AND post_title != 'Auto Draft' ORDER BY ID DESC LIMIT 1", $usrID);
    $restaurant = $wpdb->get_row($query);

    if ($restaurant && ('wp_restaurant' != $restaurant->post_type)) {
        echo "<h4>You are not Eligible to view this page.</h4>";

        return;
    } elseif ($restaurant) {
        $restaurantId = $restaurant->ID;
    } else {
        $restaurant = new stdClass();
        $restaurant->post_title = $restaurant->post_content = '';
        $restaurantId = 0;
    }
}

$wp_contact_person_name = get_post_meta($restaurantId, 'wp_contact_person_name', true);
$wp_restaurant_email = get_post_meta($restaurantId, 'wp_restaurant_email', true);
$wp_restaurant_phone = get_post_meta($restaurantId, 'wp_restaurant_phone', true);
$wp_restaurant_address = get_post_meta($restaurantId, 'wp_restaurant_address', true);
$wp_restaurant_type = get_post_meta($restaurantId, 'wp_restaurant_type', true);
$wp_restaurant_category = get_post_meta($restaurantId, 'wp_restaurant_category', true);
$wp_restaurant_occasion = get_post_meta($restaurantId, 'wp_restaurant_occasion', true);
$wp_restaurant_service = get_post_meta($restaurantId, 'wp_restaurant_service', true);
$wp_restaurant_logo = get_post_meta($restaurantId, 'wp_restaurant_logo', true);
$wp_restaurant_gallery = get_post_meta($restaurantId, 'wp_restaurant_gallery', true);
$wp_restaurant_list_img = get_post_meta($restaurantId, 'wp_restaurant_list_img', true);
$wp_restaurant_website = get_post_meta($restaurantId, 'wp_restaurant_website', true);
$wp_restaurant_tagline = get_post_meta($restaurantId, 'wp_restaurant_tagline', true);
$wp_restaurant_facebook = get_post_meta($restaurantId, 'wp_restaurant_facebook', true);
$wp_restaurant_instagram = get_post_meta($restaurantId, 'wp_restaurant_instagram', true);
$wp_restaurant_opening_days = get_post_meta($restaurantId, 'wp_restaurant_opening_days', true);
$wp_restaurant_delivery_days = get_post_meta($restaurantId, 'wp_restaurant_delivery_days', true);
$wp_restaurant_pickup_days = get_post_meta($restaurantId, 'wp_restaurant_pickup_days', true);
$wp_restaurant_closing_date = get_post_meta($restaurantId, 'wp_restaurant_closing_date', true);

$wp_restaurant_minimum_order_amount = get_post_meta($restaurantId, 'wp_restaurant_minimum_order_amount', true);
$wp_restaurant_lead_time = get_post_meta($restaurantId, 'wp_restaurant_lead_time', true);
$wp_restaurant_tax = get_post_meta($restaurantId, 'wp_restaurant_tax', true);
$wp_restaurant_delivery_fee = get_post_meta($restaurantId, 'wp_restaurant_delivery_fee', true);
$wp_restaurant_delivery = get_post_meta($restaurantId, 'wp_restaurant_delivery', true);
$pickup_option = get_post_meta($restaurantId, 'pickup_option', true);
$wp_restaurant_delivery_everywhere = get_post_meta($restaurantId, 'wp_restaurant_delivery_everywhere', true);
$wp_restaurant_delivery_area = get_post_meta($restaurantId, 'wp_restaurant_delivery_pincodes', true);

$bank_name = get_post_meta($restaurantId, 'bank_name', true);
$bank_code = get_post_meta($restaurantId, 'bank_code', true);
$account_name = get_post_meta($restaurantId, 'account_name', true);
$account_number = get_post_meta($restaurantId, 'account_number', true);
$swift_code = get_post_meta($restaurantId, 'swift_code', true);
$branch_name = get_post_meta($restaurantId, 'branch_name', true);
$branch_code = get_post_meta($restaurantId, 'branch_code', true);
$preferred_mode = get_post_meta($restaurantId, 'preferred_mode', true);
$terms_conditions = get_post_meta($restaurantId, 'terms_conditions', true);

$sideMenuHTML = '
    <div class="side-menu-button">
        <span class="one"></span>
        <span class="two"></span>
        <span class="three"></span>
    </div>

    <div class="tab submit_sidebar">
        <div class="icon-tab-logo">
            <img src="'. $pluginUrl .'assets/images/wyzchef3.png">
        </div>
        <div class="icon-tab '. ((!isset($_GET['active-tab']) || $_GET['active-tab'] == 'My Profile') ? 'active' : '') .'">
            <img src="'. $pluginUrl .'assets/images/my-profile-tab.svg">
            <input type="button" class="tablinks '. ((!isset($_GET['active-tab']) || $_GET['active-tab'] == 'My Profile') ? 'active' : '') .'" value="My Profile" onclick="openCity(event,this.value)">
        </div>
        <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Delivery') ? 'active' : '') .'">
            <img src="'. $pluginUrl .'assets/images/delivery-tab.svg">
            <input type="button" class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Delivery') ? 'active' : '') .'" value="Delivery" onclick="openCity(event,this.value)">
        </div>
        <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Payment') ? 'active' : '') .'">
            <img src="'. $pluginUrl .'assets/images/Payment-tab.svg">
            <input type="button" class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Payment') ? 'active' : '') .'" value="Payment" onclick="openCity(event,this.value)">
        </div>
        <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Menu') ? 'active' : '') .'">
            <img src="'. $pluginUrl .'assets/images/menu-tab.svg">
            <input type="button" class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Menu') ? 'active' : '') .'" value="Menu" onclick="openCity(event,this.value)">
        </div>
        <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Orders') ? 'active' : '') .'">
            <img src="'. $pluginUrl .'assets/images/orders-tab.svg">
            <input type="button" class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Orders') ? 'active' : '') .'" value="Orders" onclick="openCity(event,this.value)">
        </div>

        <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Review') ? 'active' : '') .'">
            <img src="'. $pluginUrl .'assets/images/reviews-tab.svg">
            <input type="button" class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Review') ? 'active' : '') .'" value="Review" onclick="openCity(event,this.value)">
        </div>
        <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Packages') ? 'active' : '') .'">
            <img src="'. $pluginUrl .'assets/images/Packages-tab.svg">
            <input type="button" class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Packages') ? 'active' : '') .'" value="Packages" onclick="openCity(event,this.value)">
        </div>
        <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Assistance') ? 'active' : '') .'">
            <img src="'. $pluginUrl .'assets/images/support-tab.svg">
            <input type="button" class="tablinks '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Assistance') ? 'active' : '') .'" value="Assistance" onclick="openCity(event,this.value)">
        </div>
    </div>
';

$restaurantTypes = get_terms( array(
    'taxonomy' => 'restaurant_type',
    'hide_empty' => false,
) );

$restaurantTypesHTML = '';
foreach ($restaurantTypes as $key => $rType) {
    $select="";
    if(is_array($wp_restaurant_type)) {
        for($j=0;$j<sizeof($wp_restaurant_type);$j++) {
            if($wp_restaurant_type[$j]==$rType->term_id) {
                $select= "Selected";
            }
        }
    }
    $restaurantTypesHTML .= "<option {$select} value='{$rType->term_id}'>{$rType->name}</option>";
}

$restaurantCategories = get_terms( array(
    'taxonomy' => 'restaurant_category',
    'hide_empty' => false,
) );

$restaurantCategoriesHTML = '';
foreach ($restaurantCategories as $key => $rCategory) {
    $select="";
    if(is_array($wp_restaurant_category)) {
        for($k=0;$k<sizeof($wp_restaurant_category);$k++) {
            if($wp_restaurant_category[$k]==$rCategory->term_id) {
                $select= "Selected";
            }
        }
    }
    $restaurantCategoriesHTML .= "<option {$select} value='{$rCategory->term_id}'>{$rCategory->name}</option>";
}

$restaurantOccasions = get_terms( array(
    'taxonomy' => 'restaurant_occasion',
    'hide_empty' => false,
) );

$restaurantOccasionsHTML = '';
foreach ($restaurantOccasions as $key => $rOccasion) {
    $select="";
    if(is_array($wp_restaurant_occasion)) {
        for($k=0;$k<sizeof($wp_restaurant_occasion);$k++) {
            if($wp_restaurant_occasion[$k]==$rOccasion->term_id) {
                $select= "Selected";
            }
        }
    }
    $restaurantOccasionsHTML .= "<option {$select} value='{$rOccasion->term_id}'>{$rOccasion->name}</option>";
}

$restaurantServices = get_terms( array(
    'taxonomy' => 'restaurant_service',
    'hide_empty' => false,
) );

$restaurantServicesHTML = '';
foreach ($restaurantServices as $key => $rService) {
    $select="";
    if(is_array($wp_restaurant_service)) {
        for($k=0;$k<sizeof($wp_restaurant_service);$k++) {
            if($wp_restaurant_service[$k]==$rService->term_id) {
                $select= "Selected";
            }
        }
    }
    $restaurantServicesHTML .= "<option {$select} value='{$rService->term_id}'>{$rService->name}</option>";
}

if (isset($wp_restaurant_logo) && !empty($wp_restaurant_logo)) {
    $restaurantLogo = wp_get_attachment_thumb_url($wp_restaurant_logo);
} else {
    $restaurantLogo = WYZ_PLUGIN_ROOT_URL . 'assets/images/default-logo.png';
}

$restaurantCoverImgUrl = '';
if(is_array($wp_restaurant_gallery)) {
    for($i=0;$i<sizeof($wp_restaurant_gallery);$i++) {
        $restaurantCoverImgUrl = wp_get_attachment_url($wp_restaurant_gallery[$i]);
    }
}

if (empty($restaurantCoverImgUrl)) {
    $restaurantCoverImgUrl = "{$pluginUrl}assets/images/default-restaurant-cover.png";
}

if (isset($wp_restaurant_list_img) && !empty($wp_restaurant_list_img)) {
    $restaurantListImg = wp_get_attachment_thumb_url($wp_restaurant_list_img);
} else {
    $restaurantListImg = WYZ_PLUGIN_ROOT_URL .'assets/images/default-restaurant-list-img.png';
}

$weekDays = array(
    'monday' => 'Monday',
    'tuesday' => 'Tuesday',
    'wednesday' => 'Wednesday',
    'thursday' => 'Thursday',
    'friday' => 'Friday',
    'saturday' => 'Saturday',
    'sunday' => 'Sunday',
    'public' => 'Public holidays',
);

$hourDropDownOptions = array(
    '0' => '12 AM',
    '1' => '1 AM',
    '2' => '2 AM',
    '3' => '3 AM',
    '4' => '4 AM',
    '5' => '5 AM',
    '6' => '6 AM',
    '7' => '7 AM',
    '8' => '8 AM',
    '9' => '9 AM',
    '10' => '10 AM',
    '11' => '11 AM',
    '12' => '12 PM',
    '13' => '1 PM',
    '14' => '2 PM',
    '15' => '3 PM',
    '16' => '4 PM',
    '17' => '5 PM',
    '18' => '6 PM',
    '19' => '7 PM',
    '20' => '8 PM',
    '21' => '9 PM',
    '22' => '10 PM',
    '23' => '11 PM',
);

$openingHoursHTML = '';
foreach ($weekDays as $dayKey => $dayText) {
    $openingHoursHTML .= '
        <td>
            <span>'. $dayText .'</span>
            <div class="time-container">
                <a href="javascript:void(0)" class="add-time" data-name="wp_restaurant_opening_days['. $dayKey .']"  id="opening_days_'. $dayKey .'">Add</a>
            ';
            if (!empty($wp_restaurant_opening_days && isset($wp_restaurant_opening_days[$dayKey]))) {
        foreach ($wp_restaurant_opening_days[$dayKey]['from'] as $key => $openFrom) {
                    $openingHoursHTML .= '
                        <div class="time-wrapper"> From:
                            <select name="wp_restaurant_opening_days['. $dayKey .'][from][]" class="from-hours">
                        ';
                            foreach ($hourDropDownOptions as $hourKey => $hourText) {
                                $selected = '';
                                if ($openFrom == $hourKey) {
                                    $selected = 'selected';
                                }

                                $openingHoursHTML .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
                            }
                        $openingHoursHTML .= '
                            </select>
                            <br> To:
                            <select name="wp_restaurant_opening_days['. $dayKey .'][to][]" class="to-hours">
                        ';
                            foreach ($hourDropDownOptions as $hourKey => $hourText) {
                                $selected = '';
                                if ($wp_restaurant_opening_days[$dayKey]['to'][$key] == $hourKey) {
                                    $selected = 'selected';
                                }

                                $openingHoursHTML .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
                            }
                        $openingHoursHTML .= '
                            </select> <a href="javascript:void(0)" class="delete-time">Delete</a>
                            <hr>
                        </div>
                    ';
                }
            }
            $openingHoursHTML .= '
            </div>
        </td>
    ';
}

$deliveryHoursHTML = '';
foreach ($weekDays as $dayKey => $dayText) {
    $deliveryHoursHTML .= '
        <td>
            <span>'. $dayText .'</span>
            <div class="time-container">
                <a href="javascript:void(0)" class="add-time" data-name="wp_restaurant_delivery_days['. $dayKey .']"  id="delivery_days_'. $dayKey .'">Add</a>
            ';
            if (!empty($wp_restaurant_delivery_days) && isset($wp_restaurant_delivery_days[$dayKey])) {
                foreach ($wp_restaurant_delivery_days[$dayKey]['from'] as $key => $deliveryFrom) {
                    $deliveryHoursHTML .= '
                        <div class="time-wrapper"> <span>From</span>:
                            <select name="wp_restaurant_delivery_days['. $dayKey .'][from][]" class="from-hours">
                        ';
                            foreach ($hourDropDownOptions as $hourKey => $hourText) {
                                $selected = '';
                                if ($deliveryFrom == $hourKey) {
                                    $selected = 'selected';
                                }

                                $deliveryHoursHTML .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
                            }
                        $deliveryHoursHTML .= '
                            </select>
                            <br> To:
                            <select name="wp_restaurant_delivery_days['. $dayKey .'][to][]" class="to-hours">
                        ';
                            foreach ($hourDropDownOptions as $hourKey => $hourText) {
                                $selected = '';
                                if ($wp_restaurant_delivery_days[$dayKey]['to'][$key] == $hourKey) {
                                    $selected = 'selected';
                                }

                                $deliveryHoursHTML .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
                            }
                        $deliveryHoursHTML .= '
                            </select> <a href="javascript:void(0)" class="delete-time">Delete</a>
                            <hr>
                        </div>
                    ';
                }
            }
            $deliveryHoursHTML .= '
            </div>
        </td>
    ';
}

$pickupHoursHTML = '';
foreach ($weekDays as $dayKey => $dayText) {
    $pickupHoursHTML .= '
        <td>
            <span>'. $dayText .'</span>
            <div class="time-container">
                <a href="javascript:void(0)" class="add-time" data-name="wp_restaurant_pickup_days['. $dayKey .']"  id="pickup_days_'. $dayKey .'">Add</a>
            ';
            if (!empty($wp_restaurant_pickup_days) && isset($wp_restaurant_pickup_days[$dayKey])) {
                foreach ($wp_restaurant_pickup_days[$dayKey]['from'] as $key => $pickupFrom) {
                    $pickupHoursHTML .= '
                        <div class="time-wrapper"> <span>From</span>:
                            <select name="wp_restaurant_pickup_days['. $dayKey .'][from][]" class="from-hours">
                        ';
                            foreach ($hourDropDownOptions as $hourKey => $hourText) {
                                $selected = '';
                                if ($pickupFrom == $hourKey) {
                                    $selected = 'selected';
                                }

                                $pickupHoursHTML .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
                            }
                        $pickupHoursHTML .= '
                            </select>
                            <br> To:
                            <select name="wp_restaurant_pickup_days['. $dayKey .'][to][]" class="to-hours">
                        ';
                            foreach ($hourDropDownOptions as $hourKey => $hourText) {
                                $selected = '';
                                if ($wp_restaurant_pickup_days[$dayKey]['to'][$key] == $hourKey) {
                                    $selected = 'selected';
                                }

                                $pickupHoursHTML .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
                            }
                        $pickupHoursHTML .= '
                            </select> <a href="javascript:void(0)" class="delete-time">Delete</a>
                            <hr>
                        </div>
                    ';
                }
            }
            $pickupHoursHTML .= '
            </div>
        </td>
    ';
}

$myProfileHTML = '
    <h3 class="content-heading">MY PROFILE</h3>

    <input type="hidden" name="restaurantId" value="'. $restaurantId .'">
    <fieldset>
        <label>Restaurant name</label>
        <div class="field ">
            <input type="text" name="title" placeholder="Enter the Restaurant name" value="'. $restaurant->post_title .'">
        </div>
    </fieldset>
    <fieldset>
        <label>Contact Person name</label>
        <div class="field ">
            <input type="text" name="wp_contact_person_name" placeholder="Enter The Contact Person Name" value="'. $wp_contact_person_name .'">
        </div>
    </fieldset>
    <fieldset>
        <label>Restaurant email</label>
        <div class="field ">
            <input type="email" name="wp_restaurant_email" placeholder="Enter the Restaurant Email" value="'. ($wp_restaurant_email ? $wp_restaurant_email : $current_user->user_email) .'">
        </div>
    </fieldset>
    <fieldset>
        <label>Phone</label>
        <div class="field ">
            <input type="text" name="wp_restaurant_phone" id="wp_restaurant_phone" value="'. $wp_restaurant_phone .'">
        </div>
    </fieldset>
    <fieldset>
        <label>Address</label>
        <div class="field ">
            <span class="maplocatiopn-icon">
                <i class="fa fa-map-marker button-0" style="font-size: 30px;"></i>
                <input type="text" name="wp_restaurant_address" class="form-control input-lg input-0 " id="locationTextField" placeholder="Click &#8220;Detect location&#8221;" value="'. $wp_restaurant_address .'">
            </span>
        </div>
    </fieldset>

    <fieldset>
        <label>Restaurant type</label>
        <div class="field ">
            <select name="wp_restaurant_type[]" id="wp_restaurant_type" class="selectpicker form-control" multiple>
                '. $restaurantTypesHTML .'
            </select>
        </div>
    </fieldset>

    <fieldset>
        <label>Restaurant category</label>
        <div class="field ">
            <select name="wp_restaurant_category[]" id="wp_restaurant_category" class="selectpicker form-control" multiple>
                '. $restaurantCategoriesHTML .'
            </select>
        </div>
    </fieldset>

    <fieldset>
        <label>Restaurant Occasion</label>
        <div class="field ">
            <select name="wp_restaurant_occasion[]" id="wp_restaurant_occasion" class="selectpicker form-control" multiple>
                '. $restaurantOccasionsHTML .'
            </select>
        </div>
    </fieldset>

    <fieldset>
        <label>Restaurant Service</label>
        <div class="field ">
            <select name="wp_restaurant_service[]" id="wp_restaurant_service" class="selectpicker form-control" multiple>
                '. $restaurantServicesHTML .'
            </select>
        </div>
    </fieldset>

    <fieldset>
        <label>Logo <small class="p_size">( Prefered size: <strong>150w X 150h</strong> )</small></label>
        <div class="field ">
            <div id="restaurant-logo-preview">
                <img class="myGallery" width="78" height="78" src="'. $restaurantLogo .'">
            </div>
            <input type="file" name="wp_restaurant_logo" value="463" id="wp_restaurant_logo">
        </div>
    </fieldset>

    <fieldset>
        <label>Cover Image <small class="p_size">(Prefered size: <strong>1200w X 800h</strong> )</small></label>
        <div class="field ">
            <div id="restaurant-cover-preview"><img class="myGallery" width="78" height="78" src="'. $restaurantCoverImgUrl .'">
            </div>
            <input type="file" name="wp_restaurant_gallery[]" id="wp_restaurant_gallery">
        </div>
    </fieldset>

    <fieldset>
        <label>Detail Image (On Listing Page) <small class="p_size">(Prefered size: <strong>500w X 300h</strong> )</small></label>
        <div class="field ">
            <div id="restaurant-list-preview">
                <img class="myGallery" width="78" height="78" src="'. $restaurantListImg .'">
            </div>
            <input type="file" name="wp_restaurant_list_img" id="wp_restaurant_list_img">
        </div>
    </fieldset>

    <fieldset>
        <label>Story</label>
        <div class="field ">
            <textarea name="description" class="editor1">'. $restaurant->post_content .'</textarea>
        </div>
    </fieldset>

    <fieldset>
        <label>Tagline</label>
        <div class="field ">
            <input type="text" name="wp_restaurant_tagline" id="wp_restaurant_tagline" value="'. $wp_restaurant_tagline .'">
        </div>
    </fieldset>

    <div class="timings">
        <ul class="nav nav-tabs" role="tablist">
            <li role="tab" class="active" data-tab="tab-1"><a href="javascript:void(0);" role="tab" data-toggle="tab">Opening Hours</a>
            </li>
            <li role="tab" data-tab="tab-2"><a href="javascript:void(0);" role="tab" data-toggle="tab">Delivery Hours</a>
            </li>
            <li role="tab" data-tab="tab-3"><a href="javascript:void(0);" role="tab" data-toggle="tab">PICK-UP HOURS</a>
            </li>
            <li role="tab" data-tab="tab-4"><a href="javascript:void(0);" role="tab" data-toggle="tab">HOLIDAYS</a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="tab-1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="custom-select-box-contain submit-timings-tabs">
                                <fieldset>
                                    <div class="field ">
                                        <table class="table-responsive">
                                            <tbody>
                                                <tr>'. $openingHoursHTML .'</tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane" id="tab-2">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="custom-select-box-contain submit-timings-tabs">
                                <fieldset>
                                    <div class="field ">
                                        <table class="table-responsive">
                                            <tbody>
                                                <tr>'. $deliveryHoursHTML .'</tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane" id="tab-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="custom-select-box-contain submit-timings-tabs">
                                <fieldset>
                                    <div class="field ">
                                        <table class="table-responsive">
                                            <tbody>
                                                <tr>'. $pickupHoursHTML .'</tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane" id="tab-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="custom-select-box-contain submit-timings-tabs">
                                <fieldset>
                                    <div class="field ">
                                        <input type="text" id="wp_restaurant_closing_date" name="wp_restaurant_closing_date" placeholder="Select the closing dates" class="hasDatepicker" value="'. $wp_restaurant_closing_date .'">
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
';

$leadTimeHTML = '';
for($i=1;$i<=10;$i++) {
    $val = $i*12;
    if($wp_restaurant_lead_time == $val) {
        $select = "Selected";
    }else{
        $select = "";
    }
    $leadTimeHTML .= '<option value="'.$val.'" '.$select.'>'.$val.'hrs</option>';
}

$restDeliveryFeesHTML = '';
if (isset($wp_restaurant_delivery['conditional_price'])) {
    foreach ($wp_restaurant_delivery['conditional_price'] as $key => $deliveryCondition) {
        if (!$key) {
            continue;
        }

        $restDeliveryFeesHTML .= '
            <div class="field row first-delivery-fee">
                <div class="col-md-5">
                    <sub>Price Greater Than</sub>
                    <input type="text" class="delivery-conditional-price"  name="wp_restaurant_delivery[conditional_price][]" value="'. $deliveryCondition .'" />
                </div>
                <div class="col-md-5">
                    <sub>Then Applicable Delivery Fee</sub>
                    <input type="text" class="delivery-fee"  name="wp_restaurant_delivery[applicable_fee][]" value="'. $wp_restaurant_delivery['applicable_fee'][$key] .'" />
                </div>
                <div class="col-md-2 delivery-action-button">
                    <button type="button" class="remove-delivery-condition" tabindex="-1">-</button>
                    <button type="button" class="add-delivery-condition" tabindex="-1">+</button>
                </div>
            </div>
        ';
    }
}

$everywhereChecked = '';
$hideMapElementStyle = '';
if ($wp_restaurant_delivery_everywhere) {
    $everywhereChecked = 'checked';
    $hideMapElementStyle = 'style="display: none;"';
}

$termsConditionsChecked = 'checked';
if ($terms_conditions) {
    $termsConditionsChecked = 'checked';
}

$restaurantServices = [];
$serviceQuery = "
    SELECT id, type, quantityRequired, question, '' as options
    FROM {$wpdb->prefix}wyz_restaurant_services 
    WHERE restaurant_id = {$restaurantId}
";
$restaurantServices = $wpdb->get_results($serviceQuery);
if ($restaurantServices) {
    foreach ($restaurantServices as $key => $service) {
        $restaurantServices[$key]->question = htmlspecialchars_decode($service->question, ENT_QUOTES);
        $optionQuery = "
            SELECT id, name, price, tax, total
            FROM {$wpdb->prefix}wyz_restaurant_service_options 
            WHERE service_id = {$service->id}
        ";
        $restaurantServices[$key]->options = $wpdb->get_results($optionQuery);
    }
}
$restaurantServicesJSON = json_encode($restaurantServices);

$deliveryHTML = '
    <h3 class="content-heading">DELIVERY</h3>

    <fieldset>
        <label>Minimum order amount <small class="sgd-text">( in <b>SGD</b> )</small></label>
        <div class="field ">
            <input type="text" id="wp_restaurant_minimum_order_amount" name="wp_restaurant_minimum_order_amount" value="'. $wp_restaurant_minimum_order_amount .'">
        </div>
    </fieldset>

    <fieldset>
        <div class="field row">
            <div class="field col-md-6">
                <label>Lead time</label>
                <select name="wp_restaurant_lead_time" id="wp_restaurant_lead_time">
                    '. $leadTimeHTML .'
                </select>
            </div>
            <div class="field col-md-6">
                <label>Restaurant Tax</label>
                <input type="text" id="wp_restaurant_tax" name="wp_restaurant_tax" value="'. $wp_restaurant_tax .'">
            </div>
        </div>
    </fieldset>

    <fieldset class="delivery-fee-section">
        <label>Delivery Fee <small class="sgd-text">( in <b>SGD</b> )</small></label>
        <div class="field row first-delivery-fee">
            <div class="col-md-5">
                <sub>Price Greater Than</sub>
                <input type="text" class="delivery-conditional-price" name="wp_restaurant_delivery[conditional_price][]" value="0" readonly="">
            </div>
            <div class="col-md-5">
                <sub>Then Applicable Delivery Fee</sub>
                <input type="text" class="delivery-fee" name="wp_restaurant_delivery[applicable_fee][]" value="'. (isset($wp_restaurant_delivery['applicable_fee']) ? $wp_restaurant_delivery['applicable_fee'][0] : '' ) .'">
            </div>
            <div class="col-md-2 delivery-action-button">
                <button type="button" class="add-delivery-condition" tabindex="-1">+</button>
            </div>
        </div>

        <div class="rest-delivery-fees">
            '. $restDeliveryFeesHTML .'
        </div>
    </fieldset>

    <fieldset>
        <label>Pickup option available</label>
        <div class="field ">
            <label><input type="radio" name="pickup_option" id="pickup_option_yes" value="1" '. ($pickup_option ? 'checked' : '') .'> Yes</label>
            <label><input type="radio" name="pickup_option" id="pickup_option_no" value="" '. ($pickup_option ? '' : 'checked') .'> No</label>
        </div>
    </fieldset>

    <fieldset>
        <label>Areas delivered (Pincodes)</label>
        <div class="field ">
            <label>
                <input type="checkbox" id="wp_restaurant_delivery_everywhere" value="1" name="wp_restaurant_delivery_everywhere" '. $everywhereChecked .'> Deliver anywhere in Singapore
            </label>
            <input type="text" id="wp_restaurant_delivery_area" value="'. $wp_restaurant_delivery_area .'" name="wp_restaurant_delivery_pincodes" '. $hideMapElementStyle .'>
            <input type="button" id="modal-open" class="openMap" data-toggle="modal" data-target="#myModal" value="MAP" '. $hideMapElementStyle .'>
        </div>

        '. $this->loadOptimizeMapModal() .'
        <hr />
        <button type="button" id="add-service" class="btn-model btn btn-info btn-lg add-service" data-toggle="modal" data-target="#service-modal" style="display: inline-block;">Add New Service</button>
        <div class="restaurant-services-container">
            <input type="hidden" name="restaurant_services" id="restaurant_services" />
            <input type="hidden" name="deletedServices" id="deletedServices" />
            <input type="hidden" name="deletedOptions" id="deletedOptions" />
        </div>
        <hr />

        <div class="modal-container">
            <div class="modal fade" id="service-modal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Add New Service</h4>
                        </div>
                        <div class="modal-body">
                            <div class="service-types-container">
                                <label>Service Type</label>
                                <div class="service-types-wrapper">
                                    <label>
                                        <input type="radio" name="service[type]" class="service-type" value="0" checked=""> Optional</label>
                                    <label>
                                        <input type="radio" name="service[type]" class="service-type" value="1"> Required</label>
                                </div>
                            </div>
                            <div class="service-quantity-container">
                                <label>Quantity Required
                                    <input type="checkbox" class="service-quantity" value="1">
                                </label>
                            </div>
                            <div class="service-question-container">
                                <label>Question Text</label>
                                <input type="text" class="service-question" placeholder="Please enter question..">
                            </div>
                            <div class="row service-options-container">
                                <div class="service-option-data">
                                    <input type="hidden" class="option-id" value="">
                                    <div class="col-md-5">
                                        <input type="text" class="option-name" placeholder="Name">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="option-price" placeholder="Base Price: eg. 20" value="0">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="option-tax" placeholder="Tax(%): eg. 7" value="7">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="option-total" placeholder="Total: eg. 25" value="0">
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn dish-popup-button" data-dismiss="modal">Cancel</button>
                            <button type="button" onclick="addService()" class="btn service-add-btn">Add</button>
                            <button type="button" onclick="updateService(this)" class="btn service-save-btn" data-service-id="" style="display: none;">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
';

$paymentHTML = '
    <h3 class="content-heading">PAYMENT</h3>

    <fieldset>
        <label>Bank</label>
        <div class="field ">
            <input type="text" id="bank_name" name="bank_name" placeholder="Enter the bank name" value="'. $bank_name .'">
        </div>
        <div class="field ">
            <input type="text" name="bank_code" placeholder="Enter the bank code" value="'. $bank_code .'">
        </div>
    </fieldset>
    <fieldset>
        <label>Account</label>
        <div class="field ">
            <input type="text" name="account_name" id="account_name" placeholder="Enter the account name" value="'. $account_name .'">
        </div>
        <div class="field ">
            <input type="text" id="account_number" name="account_number" placeholder="Number" autocomplete="true" value="'. $account_number .'">
        </div>
    </fieldset>
    <fieldset>
        <label>Swift Code</label>
        <div class="field  w-100">
            <input type="text" name="swift_code" id="swift_code" placeholder="Enter the swift code" value="'. $swift_code .'">
        </div>
    </fieldset>
    <fieldset>
        <label>Branch</label>
        <div class="field ">
            <input type="text" name="branch_name" id="branch_name" placeholder="Enter the branch name" value="'. $branch_name .'">
        </div>
        <div class="field ">
            <input type="text" name="branch_code" id="branch_code" placeholder="Enter the branch code" value="'. $branch_code .'">
        </div>
    </fieldset>
    <fieldset>
        <label>Preferred Mode of Payment</label>
        <div class="field  radio-optn">
            <input type="radio" name="preferred_mode" id="preferred_mode_cheque" value="cheque" '. ($preferred_mode == 'cheque' ? 'checked' : '') .'> Cheque
            <br>
            <input type="radio" name="preferred_mode" id="preferred_mode_wire" value="wire" '. ($preferred_mode == 'wire' ? 'checked' : '') .'> Wire Transfer
            <br>
        </div>
    </fieldset>
';

// $menuHTML = wyz_chef_dish::wp_form_shortcode();
$menuHTML = wyz_chef_dish::wp_menu_optimize();

$ordersHTML = '
    <h3 class="content-heading">ORDER</h3>

    <div class="first-section">
        <div class="bg-img">
            <img src="'. $pluginUrl .'assets/images/resto_bg.png">
        </div>
        <div class="page_description">
            <p>Orders are available at the dashboard. You can submit this page to check the orders.</p>
        </div>
    </div>
';

$reviewHTML = '
    <h3 class="content-heading">REVIEW</h3>

    <div class="first-section">
        <div class="bg-img">
            <img src="'. $pluginUrl .'assets/images/resto_bg.png">
        </div>
        <div class="page_description">
            <p>Reviews are available at the dashboard. You can submit this page to check the reviews.</p>
        </div>
    </div>
';

$packagesHTML = '
    <h3 class="content-heading">PACKAGES</h3>

    <div class="first-section">
        <div class="bg-img">
            <img src="'. $pluginUrl .'assets/images/resto_bg.png">
        </div>
        <div class="page_description">
            <p>Packages are available at the dashboard. You can submit this page to check the packages.</p>
        </div>
    </div>
';

$assistanceHTML = '
    <h3 class="content-heading">ASSISTANCE</h3>

    <div class="first-section">
        <div class="bg-img">
            <img src="'. $pluginUrl .'assets/images/resto_bg.png">
        </div>
        <div class="page_description">
            <p>For any assistance, you can contact us at below details</p>
            <p> Email: support@wyzchef.com</p>
            <p> Phone: +65 88913599</p>
        </div>
    </div>
';

$profileTab = ((!isset($_GET['active-tab']) || $_GET['active-tab'] == 'My Profile') ? 'active' : '');
$deliveryTab = ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Delivery') ? 'active' : '');
$paymentTab = ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Payment') ? 'active' : '');
$menuTab = ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Menu') ? 'active' : '');
$ordersTab = ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Orders') ? 'active' : '');
$reviewTab = ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Review') ? 'active' : '');
$packagesTab = ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Packages') ? 'active' : '');
$assistanceTab = ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Assistance') ? 'active' : '');

$formPostUrl = esc_url( admin_url( 'admin-post.php' ) );
$submitNonce = wp_create_nonce( 'wyzchef_submit_user_meta_form_nonce' );
$htmlmaster = <<<RESTAURANT_SUBMIT_PAGE
    <style>
        #wp_restaurant_delivery_everywhere {
            width: 22px;
            height: 22px;
            vertical-align: middle;
        }

        .tabcontent {
            display: none;
        }

        .tabcontent.active {
            display: block;
        }
    </style>
    <form id="submissionForm" action="{$formPostUrl}" method="POST" class="restaurant-form" enctype="multipart/form-data" novalidate>
        <input type="hidden" name="action" value="saveRestaurantData">
        <input type="hidden" name="wyzchef_submit_user_meta_nonce" value="{$submitNonce}" />
        {$sideMenuHTML}

        <div id="My Profile" class="tabcontent {$profileTab}">
            <div class="dashboard-container">{$myProfileHTML}</div>
        </div>
        <div id="Delivery" class="tabcontent {$deliveryTab}">
            <div class="dashboard-container">{$deliveryHTML}</div>
        </div>
        <!-- Payment Tab starts here -->
        <div id="Payment" style="min-height: 586px;" class="tabcontent {$paymentTab}">
            <div class="dashboard-container">{$paymentHTML}</div>
        </div>
        <!--my payment tab -->
        <div id="Menu" class="tabcontent {$menuTab}">
            <div class="dashboard-container">{$menuHTML}</div>
            <input type="hidden" name="menu_change" >
        </div>

        <div id="Orders" style="min-height: 586px;" class="tabcontent {$ordersTab}">
            <div class="dashboard-container">{$ordersHTML}</div>
        </div>
        <!--my order tab -->
        <div id="Review" style="min-height: 586px;" class="tabcontent {$reviewTab}">
            <div class="dashboard-container">{$reviewHTML}</div>
        </div>
        <!--my review tab -->
        <div id="Packages" style="min-height: 586px;" class="tabcontent {$packagesTab}">
            <div class="dashboard-container">{$packagesHTML}</div>
        </div>
        <!--my packages tab -->
        <div id="Assistance" style="min-height: 586px;" class="tabcontent {$assistanceTab}">
            <div class="dashboard-container">{$assistanceHTML}</div>
        </div>
        <!--my assistance tab -->
        <div class="dashboard-container tabcontent">
            <div id="errormsg"></div>
            <div class="dashboard-submit">
                <span class="terms-conditions-wrapper" style="position: absolute; visibility: hidden;"><input type="checkbox" name="terms_conditions" value="1" checked=""> <a href="javascript:void(0)" data-toggle="modal" data-target="#termsAndCondtions">Terms &amp; Conditions </a></span>
                <input type="submit" class="modify-button" id="dashboard_submit" value="SUBMIT">
            </div>
            <!-- Dashboard submit button -->
            <div class="copyrights-sec">
                <p><span><img src="{$pluginUrl}assets/images/copyr-logo.png" alt=""></span><span> for restaurants 2019 ©</span>
                </p>
            </div>
            <!-- copyrights-sec -->
        </div>
    </form>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARteWOCnYMfmc4hP7xyZpLVulT7fG_Cn4&libraries=places,drawing"></script>
    <script type="text/javascript" src="{$pluginUrl}assets/js/select-2min.js"></script>

    <script type="text/javascript">
        var WYZ_PLUGIN_ROOT_URL = '{$pluginUrl}';
        var drawingManager;
        var selectedShape;
        var colors = ["#1E90FF", "#FF1493", "#32CD32", "#FF8C00", "#4B0082"];
        var selectedColor;
        var colorButtons = {};

        function clearSelection() {
            if (selectedShape) {
                if (typeof selectedShape.setEditable == "function") {
                    selectedShape.setEditable(false);
                }
                selectedShape = null;
            }
            curseldiv.innerHTML = "<b>cursel</b>:";
        }

        function updateCurSelText(shape) {
            posstr = "" + selectedShape.position;
            if (typeof selectedShape.position == "object") {
                posstr = selectedShape.position.toUrlValue();
            }
            pathstr = "" + selectedShape.getPath;
            if (typeof selectedShape.getPath == "function") {
                pathstr = "[ ";
                for (var i = 0; i < selectedShape.getPath().getLength(); i++) {
                    // .toUrlValue(5) limits number of decimals, default is 6 but can do more
                    pathstr += selectedShape.getPath().getAt(i).toUrlValue() + " , ";
                }
                pathstr += "]";
            }
            bndstr = "" + selectedShape.getBounds;
            cntstr = "" + selectedShape.getBounds;
            if (typeof selectedShape.getBounds == "function") {
                var tmpbounds = selectedShape.getBounds();
                cntstr = "" + tmpbounds.getCenter().toUrlValue();
                bndstr = "[NE: " + tmpbounds.getNorthEast().toUrlValue() + " SW: " + tmpbounds.getSouthWest().toUrlValue() + "]";
            }
            cntrstr = "" + selectedShape.getCenter;

            if (typeof selectedShape.getCenter == "function") {
                cntrstr = "" + selectedShape.getCenter().toUrlValue();
            }
            radstr = "" + selectedShape.getRadius;

            if (typeof selectedShape.getRadius == "function") {
                radstr = "" + selectedShape.getRadius();
            }
            curseldiv.innerHTML = "<b>cursel</b>: " + selectedShape.type + " " + selectedShape + "; <i>pos</i>: " + posstr + " ; <i>path</i>: " + pathstr + " ; <i>bounds</i>: " + bndstr + " ; <i>Cb</i>: " + cntstr + " ; <i>radius</i>: " + radstr + " ; <i>Cr</i>: " + cntrstr;
            document.getElementById("latlng").value = pathstr;
        }

        function setSelection(shape, isNotMarker) {
            clearSelection();
            selectedShape = shape;
            if (isNotMarker)
                shape.setEditable(true);
            selectColor(shape.get("fillColor") || shape.get("strokeColor"));
            updateCurSelText(shape);
        }

        function deleteSelectedShape() {
            if (selectedShape) {
                selectedShape.setMap(null);
            }
        }

        function selectColor(color) {
            selectedColor = color;

            for (var i = 0; i < colors.length; ++i) {
                var currColor = colors[i];
                colorButtons[currColor].style.border = currColor == color ? "2px solid #789" : "2px solid #fff";
            }
            // Retrieves the current options from the drawing manager and replaces the
            // stroke or fill color as appropriate.
            var polylineOptions = drawingManager.get("polylineOptions");
            polylineOptions.strokeColor = color;
            drawingManager.set("polylineOptions", polylineOptions);
            var rectangleOptions = drawingManager.get("rectangleOptions");
            rectangleOptions.fillColor = color;
            drawingManager.set("rectangleOptions", rectangleOptions);
            var circleOptions = drawingManager.get("circleOptions");
            circleOptions.fillColor = color;
            drawingManager.set("circleOptions", circleOptions);
            var polygonOptions = drawingManager.get("polygonOptions");
            polygonOptions.fillColor = color;
            drawingManager.set("polygonOptions", polygonOptions);
        }

        function setSelectedShapeColor(color) {
            if (selectedShape) {
                if (selectedShape.type == google.maps.drawing.OverlayType.POLYLINE) {
                    selectedShape.set("strokeColor", color);
                } else {
                    selectedShape.set("fillColor", color);
                }
            }
        }

        function makeColorButton(color) {
            var button = document.createElement("span");
            button.className = "color-button";
            button.style.backgroundColor = color;
            google.maps.event.addDomListener(button, "click", function() {
                selectColor(color);
                setSelectedShapeColor(color);
            });
            return button;
        }

        function buildColorPalette() {
            var colorPalette = document.getElementById("color-palette");

            for (var i = 0; i < colors.length; ++i) {
                var currColor = colors[i];
                var colorButton = makeColorButton(currColor);
                colorPalette.appendChild(colorButton);
                colorButtons[currColor] = colorButton;
            }
            selectColor(colors[0]);
        }
        /////////////////////////////////////
        var map; //= new google.maps.Map(document.getElementById("map"), {
        // these must have global refs too!:
        var placeMarkers = [];
        var input;
        var searchBox;
        var curposdiv;
        var curseldiv;

        function deletePlacesSearchResults() {
            for (var i = 0, marker; marker = placeMarkers[i]; i++) {
                marker.setMap(null);
            }
            placeMarkers = [];
            input.value = ""; // clear the box too
        }
        /////////////////////////////////////
        function initialize() {
            map = new google.maps.Map(document.getElementById("map"), { //var
                zoom: 6, //10,
                center: new google.maps.LatLng(1.302570, 103.834690),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: false,
                zoomControl: true
            });
            curposdiv = document.getElementById("curpos");
            curseldiv = document.getElementById("cursel");
            var polyOptions = {
                strokeWeight: 0,
                fillOpacity: 0.45,
                editable: true
            };
            // Creates a drawing manager attached to the map that allows the user to draw
            // markers, lines, and shapes.
            drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                markerOptions: {
                    draggable: true,
                    editable: true,
                },
                polylineOptions: {
                    editable: true
                },
                rectangleOptions: polyOptions,
                circleOptions: polyOptions,
                polygonOptions: polyOptions,
                map: map
            });
            google.maps.event.addListener(drawingManager, "overlaycomplete", function(e) {
                //~ if (e.type != google.maps.drawing.OverlayType.MARKER) {
                var isNotMarker = (e.type != google.maps.drawing.OverlayType.MARKER);
                // Switch back to non-drawing mode after drawing a shape.
                drawingManager.setDrawingMode(null);
                // Add an event listener that selects the newly-drawn shape when the user
                // mouses down on it.
                var newShape = e.overlay;
                newShape.type = e.type;
                google.maps.event.addListener(newShape, "click", function() {
                    setSelection(newShape, isNotMarker);
                });
                google.maps.event.addListener(newShape, "drag", function() {
                    updateCurSelText(newShape);
                });
                google.maps.event.addListener(newShape, "dragend", function() {
                    updateCurSelText(newShape);
                });
                setSelection(newShape, isNotMarker);
                //~ }// end if
            });
            // Clear the current selection when the drawing mode is changed, or when the
            // map is clicked.
            google.maps.event.addListener(drawingManager, "drawingmode_changed", clearSelection);
            google.maps.event.addListener(map, "click", clearSelection);
            google.maps.event.addDomListener(document.getElementById("delete-button"), "click", deleteSelectedShape);
            buildColorPalette();

            input = /** @type {HTMLInputElement} */ ( //var
                document.getElementById("pac-input"));

            var DelPlcButDiv = document.createElement("div");
            //~ DelPlcButDiv.style.color = "rgb(25,25,25)"; // no effect?
            DelPlcButDiv.style.backgroundColor = "#fff";
            DelPlcButDiv.style.cursor = "pointer";
            DelPlcButDiv.innerHTML = "DEL";
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(DelPlcButDiv);
            google.maps.event.addDomListener(DelPlcButDiv, "click", deletePlacesSearchResults);

            searchBox = new google.maps.places.SearchBox( //var
                /** @type {HTMLInputElement} */
                (input)
            );
            // Listen for the event fired when the user selects an item from the
            // pick list. Retrieve the matching places for that item.
            google.maps.event.addListener(searchBox, "places_changed", function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                for (var i = 0, marker; marker = placeMarkers[i]; i++) {
                    marker.setMap(null);
                }
                // For each place, get the icon, place name, and location.
                placeMarkers = [];
                var bounds = new google.maps.LatLngBounds();
                for (var i = 0, place; place = places[i]; i++) {
                    var image = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };
                    // Create a marker for each place.
                    var marker = new google.maps.Marker({
                        map: map,
                        icon: image,
                        title: place.name,
                        position: place.geometry.location
                    });
                    placeMarkers.push(marker);
                    bounds.extend(place.geometry.location);
                }
                map.fitBounds(bounds);
            });
            // Bias the SearchBox results towards places that are within the bounds of the
            // current map"s viewport.
            google.maps.event.addListener(map, "bounds_changed", function() {
                var bounds = map.getBounds();
                searchBox.setBounds(bounds);
                curposdiv.innerHTML = "<b>curpos</b> Z: " + map.getZoom() + " C: " + map.getCenter().toUrlValue();
            }); //////////////////////
            var geocoder = new google.maps.Geocoder;
            var infowindow = new google.maps.InfoWindow;
            document.getElementById("submit").addEventListener("click", function() {
                geocodeLatLng(geocoder, map, infowindow);
            });
        }
        google.maps.event.addDomListener(window, "load", initialize);

        function geocodeLatLng(geocoder, map, infowindow) {
            var input = document.getElementById("latlng").value;
            var latlngStr1 = input.split(" , ");
            /*console.log(latlngStr1);*/
            var latlngStr = "";
            var zipstr = "";
            for (var i = 0; i < latlngStr1.length - 1; i++) {
                latlngStrs = latlngStr1[i].replace("[ ", "");
                latlngStr = latlngStrs.split(",", 2);
                console.log(latlngStr);
                var latlng = {
                    lat: parseFloat(latlngStr[0]),
                    lng: parseFloat(latlngStr[1])
                };
                geocoder.geocode({
                    "location": latlng
                }, function(results, status) {
                    if (status === "OK") {
                        if (results[0]) {
                            map.setZoom(11);
                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map
                            });
                            console.log(results[0].address_components);
                            zip = results[0].address_components[results[0].address_components.length - 1];
                            if (isNaN(zip.long_name)) {
                                zip = results[0].address_components[0];
                            }
                            zipstr = zip.long_name + "," + zipstr;
                            $("#wp_restaurant_delivery_area").val(zipstr);
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        } else {
                            window.alert("No results found");
                        }
                    } else {
                        window.alert("Geocoder failed due to: " + status);
                    }
                });
            }
        }

        var hourDropDownOptions = '\
            <option value="0">12 AM</option>\
            <option value="1">1 AM</option>\
            <option value="2">2 AM</option>\
            <option value="3">3 AM</option>\
            <option value="4">4 AM</option>\
            <option value="5">5 AM</option>\
            <option value="6">6 AM</option>\
            <option value="7">7 AM</option>\
            <option value="8">8 AM</option>\
            <option value="9">9 AM</option>\
            <option value="10">10 AM</option>\
            <option value="11">11 AM</option>\
            <option value="12">12 PM</option>\
            <option value="13">1 PM</option>\
            <option value="14">2 PM</option>\
            <option value="15">3 PM</option>\
            <option value="16">4 PM</option>\
            <option value="17">5 PM</option>\
            <option value="18">6 PM</option>\
            <option value="19">7 PM</option>\
            <option value="20">8 PM</option>\
            <option value="21">9 PM</option>\
            <option value="22">10 PM</option>\
            <option value="23">11 PM</option>\
        ';

        var restaurantServices = {$restaurantServicesJSON};
        document.getElementById("restaurant_services").value = JSON.stringify(restaurantServices);
        var restaurantDeletedServices = [];
        var restaurantDeletedOptions = [];
        var restaurantDeletedTempOptions = [];

        if (restaurantServices.length > 0) {
            for (key in restaurantServices) {
                let service = restaurantServices[key];
                let question = service["question"];
                let typeText = parseInt(service["type"]) ? "Required" : "Optional";
                let serviceData = '';

                serviceData = '\
                    <div class="restaurant-service" data-id="'+ key +'">\
                        <div class="headerBox">\
                            <div class="row">\
                                <div class="col-md-11 col-sm-11">\
                                    <h5 class="titleHeader">\
                                        <span class="service-question-text">'+ question +'</span>\
                                    </h5>\
                                    <p class="card-text">\
                                        <span class="service-type-text">'+ typeText +'</span>\
                                    </p>\
                                </div>\
                                <div class="col-md-1 col-sm-1">\
                                    <div class="service-edit-wrapper">\
                                        <a href="#edit" class="service-edit" onclick="editService(this, '+ key +'); return false;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>\
                                        <a href="#copy" class="service-copy" onclick="copyService('+ key +'); return false;" ><i class="fa fa-copy"></i></a>\
                                        <a href="#delete" class="service-delete" onclick="deleteService(this, '+ key +'); return false;"><i class="fa fa-trash-o"></i></a>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                ';

                $(".restaurant-services-container").append(serviceData);
            }
        }

        jQuery(document.body).ready(function($) {
            $('.selectpicker').select2();

            $(".add-time").on("click", function() {
                console.log("add-time : ", this);
                console.log("$(this).siblings().length: ", $(this).siblings().length);
                console.log("$(this).siblings(): ", $(this).siblings());
                if ($(this).siblings().length < 2) {
                    // let lineDivider = ($(this).siblings().length == 0) ? "<hr />" : "";
                    // console.log("$(this).siblings().length : ", $(this).siblings().length);
                    $(this).parent().append('\
                        <div class="time-wrapper">\
                            <span>From:</span> <select name="'+ $(this).attr("data-name") +'[from][]" class="from-hours">'+ hourDropDownOptions +'</select><br />\
                            To: <select name="'+ $(this).attr("data-name") +'[to][]" class="to-hours">'+ hourDropDownOptions +'</select>\
                            <a href="javascript:void(0)" class="delete-time">Delete</a>\
                            <hr />\
                        </div>\
                    ');
                } else {
                    console.log("You can add upto two time intervals.");
                }
            });
            $(".time-container").on("click", ".delete-time", function() {
                // console.log("delete-time : ", this);
                $(this).parent().remove();
            });

            $("#wp_restaurant_closing_date").multiDatesPicker();

            $(".delivery-fee-section").on("click", ".add-delivery-condition", function() {
                $(".rest-delivery-fees").append(`
                        <div class="field row first-delivery-fee">
                            <div class="col-md-5">
                                <sub>Price Greater Than</sub>
                                <input type="text" class="delivery-conditional-price"  name="wp_restaurant_delivery[conditional_price][]" value="" />
                            </div>
                            <div class="col-md-5">
                                <sub>Then Applicable Delivery Fee</sub>
                                <input type="text" class="delivery-fee"  name="wp_restaurant_delivery[applicable_fee][]" value="" />
                            </div>
                            <div class="col-md-2 delivery-action-button">
                                <button type="button" class="remove-delivery-condition" tabindex="-1">-</button>
                                <button type="button" class="add-delivery-condition" tabindex="-1">+</button>
                            </div>
                        </div>
                    `);
            });

            $(".delivery-fee-section").on("click", ".remove-delivery-condition", function() {
                $(this).parent().parent().remove();
            });

            $("#wp_restaurant_delivery_everywhere").on("click", function() {
                if ($(this).prop("checked")) {
                    // $("#wp_restaurant_delivery_area").prop("disabled", true);
                    $("#wp_restaurant_delivery_area").hide();
                    $("#modal-open").hide();
                } else {
                    // $("#wp_restaurant_delivery_area").prop("disabled", false);
                    $("#wp_restaurant_delivery_area").show();
                    $("#modal-open").show();
                }
            });

            $(".add-dish").show();
        });
    </script>
RESTAURANT_SUBMIT_PAGE;

echo $htmlmaster;