<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <style>
        .order-email-template{max-width: 980px;width:100%;box-shadow: 0 2px 13px 0px #ccc;margin-top:25px;}
        .mailer-head, .mailer-body, .mailer-footer{padding:10px;}
        .mailer-body h3{font-weight:400;font-size:19px;color:grey;}
        .mailer-body h3 span{font-weight:600;}
        .mailer-body ul li{position:relative;padding:10px;padding-left:50px;list-style: none;}
        .mailer-body ul li img{position: absolute;width:25px;height:25px;left:10px;top:9px;}
        .mailer-footer a{color:#0ab9b1; display: inline-block;border:2px solid #0ab9b1;padding:5px 20px;border-radius:35px;transition: 0.3s all ease-in-out;}
        .mailer-footer a:hover{text-decoration: none;background-color:#0ab9b1;color:#ffffff;}
    </style>
</head>
<body>
    <div class="container">
        <div class="row order-email-template">
            <div class="col-md-12">
                <div class="mailer-head">
                    <a href="<?php echo site_url(); ?>"><img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/wyzchef3.png" alt="WYZchef" id="logo"></a>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="mailer-body">
                    <h3>Thank You! You have successfully placed a <span>new order(ref:#<?php echo $orderId; ?>) with</span><span><?php echo $restaurant_name;?></span></h3>
                    <ul>
                        <li><span class="calender-icon"><img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/calender-icon.png" alt="WYZchef" id="logo"></span><?php echo date('l', strtotime($delivery_date));?> 
                        <span><?php echo date('jS F Y', strtotime($delivery_date));?> at <?php echo date('H:i A', $delivery_date);?> </span></li>
                        <li><span class="map-marker-icon"><img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/maps-and-flags.png" alt="WYZchef" id="logo"></span><span><?php echo $_POST['company_name'].", ".$_POST['street_address'].", ".$unit_address.", ".$_POST['postcode'].", ".$_POST['city']; ?></span></li>
                        <li><span class="dish-icon"><img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/prefrences-icon.png" alt="WYZchef" id="logo"></span><span><?php echo $pax." PAX";?></span></li>
                        <li><span class="invoice-icon"><img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/invoice-icon.png" alt="WYZchef" id="logo"></span><span><?php echo $total;?> SGD</span></li>
                    </ul>
                    <h3><?php echo $restaurant_name;?> is yet to confirm this order.</strong> We will keep you informed when they do.</h3>
                    <h3>To ensure the smooth delivery on the day of your event,<strong>Please make sure that You have provided the necessary information for the delivery </strong>driver(floor number, room number, company name, contact person on site, special instructions, etc.)</h3>

                    <h3>Thank you for choosing wisely!</h3>
                </div>
            </div>
            <div class="col-md-12">
                <div class="mailer-footer">
                    <a href="<?php echo site_url('corporate-dashboard/?active-tab=Orders');?>">Review details</a>
                    <p>The WYZchef Team.</p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>