jQuery(document).ready(function($){
  var addButton = $('#addBanner');
  //var deleteButton = document.getElementById('fjern-bilde');
  var img = document.getElementById('image-tag');
  var hidden = document.getElementById('img-hidden-field');
  var imageUploader = wp.media({
    title: 'Velg bilde',
    button: {
      text: 'Velg bilde(r)'
    },
    multiple: 'add'
  });

  addButton.click(  function() {
    if (imageUploader) {
      imageUploader.open();
    }
  });

  imageUploader.on('open',function() {
    var selection = imageUploader.state().get('selection');
    ids = jQuery('#img-hidden-field-selected').val().split(',');
      ids.forEach(function(id) {
    attachment = wp.media.attachment(id);
    attachment.fetch();
    selection.add( attachment ? [ attachment ] : [] );

  });
  });

  imageUploader.on( 'close', function() {
    //var attachment1 = imageUploader.state().get('selection').first().toJSON();
    var attachment = imageUploader.state().get('selection');
    var ids = attachment.map( function (attachment) {
        return attachment.id;
    });
    hidden.setAttribute( 'value', ids.join(',') );
      
  });

  imageUploader.on( 'select', function() {
    var content ='';
    $(".myimgs").remove();
    $(".selected").each(function(){
      $(this).find("img").each(function(){
        var a =$(this).attr("src");
        console.log(a);
        content = "<img width='76' height='76' class='myimgs' src="+a+"> &nbsp;&nbsp;"
         
        $("#imgageAdd").append(content);
      })
    })
    //var attachment = imageUploader.state().get('selection').first().toJSON();
    var attachment = imageUploader.state().get('selection');
    var ids = attachment.map( function (attachment) {
        return attachment.id;
    });
    hidden.setAttribute( 'value', ids.join(',') );
    
  });
});


 
function removeImage(id){
  $("#"+id).remove();
  var hidden = document.getElementById('img-hidden-field');
  var str = ''
    $(".myGallery").each(function(){
      var a = $(this).attr("data-value");
      console.log(a);
      str = ","+a+str
      hidden.setAttribute( 'value', str );
    });
}
