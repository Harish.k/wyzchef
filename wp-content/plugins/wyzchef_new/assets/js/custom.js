jQuery(document).ready(function(){

    jQuery(function () {
        jQuery(".datetimepicker").datetimepicker({ 
            weekStart: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1,
            format:'yyyy-mm-dd  HH:ii P',
            });
        });

          jQuery(document).on("click", ".restaurant-name-section ul.menu-list-items li", function(event) {
            jQuery("html, body").animate({
            scrollTop: jQuery('html').offset().top
            }, 500);
            });
         

            // Hiding Date and Headcount fields on alert popup//

            jQuery('.bill-div .cart-action-checkout').click(function(){

                const $span = jQuery('.alert-popup .filter-container .filter-tags input').parent().parent().parent('tr');
              
                if (jQuery('.alert-popup .filter-container .filter-tags input').val().length > 0) {
                  $span.addClass('filled-fields');
                } else {
                  $span.removeClass('filled-fields');
                }


            });
              

});

// Added by SK for query param
function addQueryParam(queryString = '') {
    if (history.pushState) {
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + queryString;
        window.history.pushState({path:newurl},'',newurl);
    }
}

$ = jQuery.noConflict();

/*jQuery(document).ready(function($){
  jQuery('input.timepicker').timepicker({});
});*/

jQuery("#addType").click(function(){

	jQuery("#tabletype").toggle();
})

jQuery("#addCategory").click(function(){

	jQuery("#tablecategory").toggle();
});

  jQuery(document).ready(function($){
    var str=jQuery('#selected_restaurant').val();
        jQuery('.slectthecheckbox').click(function(){

            if(jQuery(this).is(":checked")){

             str= jQuery(this).val()+','+str;
              str= str.replace(',,',',');;
             console.log(str);
            }
            else if(jQuery(this).is(":not(:checked)")){
                str= str.replace(jQuery(this).val(),'');;
                str= str.replace(',,',',');;
                console.log(str);
            }
             jQuery('#selected_restaurant').val(str);
        });
  });

jQuery(window).load(function() {

  jQuery(".select2-container--default").attr("style","width:100%");

  var datess = jQuery("#wp_restaurant_closing_date_hide").val();
  //alert(datess);
  jQuery("#wp_restaurant_closing_date").val(datess);

});

function openCity(evt,city) {
    var i, tabcontent, tablinks;
    cityName =city;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

function validationForm(){
    var err=0;
    var chebox1 = '';
    $("#errormsg").text("");
    $(".required-field").each(function(){
    var val =$(this).val();
    var name = String($(this).attr('name'));
    var label = replaceAll(name,"_", " ");
    label = label.replace("wp ","");
    if(val=='' && label!='undefined'){
      $("#errormsg").append("Please Enter "+label+"<br>");
      document.body.scrollTop = 200;
      document.documentElement.scrollTop = 200;
      document.getElementById('errormsg').focus();
      $("#errormsg").click();
      ++err;
    }
  })
  if(err>0){
    err
    return false;
  }else if($("#term-condtion-checkbox1").prop("checked") == 0 && $("#term-condtion-checkbox2").prop("checked") == 0){
    // alert("Please check terms & condiotion");
    $("#Submitbtn").attr("data-target","");
    return false;
  }else{
    $("#Submitbtn").attr("data-target","");
     return true;
  }
}
function escapeRegExp(string){
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}
function replaceAll(str, term, replacement) {
    return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
}

function mergeCart(cartItems) {
  var item      = {};
  var allArray  = [];
  for (i = 0; i < cartItems.length; i++) {
    var item              = {};
    item['id']            = cartItems[i].id;
    item['title']         = cartItems[i].title;
    item['size']          = cartItems[i].size;
    item['quantity']      = cartItems[i].quantity;
    item['restaurantId']  = cartItems[i].restaurantId;

    if(i == 0) {
      allArray.push( item );

    }
    else {
      var index = allArray.findIndex(x => (x.id === cartItems[i].id && x.size === cartItems[i].size));
      if(index >= 0) {
        var quantity              = allArray[index].quantity;
        allArray[index].quantity  = allArray[index].quantity + cartItems[i].quantity;
      }
      else {
        allArray.push( item );
      }
    }

  }
  return allArray;
}

function displayCart() {
  /*
  var cartItems       = JSON.parse(localStorage.getItem("cartItems"));
  var restaurantCart  = JSON.parse(localStorage.getItem("restaurantCart"));
  //console.log(restaurantCart);
  if(jQuery("#cartCheckoutItems").length){
      jQuery("#cartCheckoutItems").val(localStorage.getItem("cartItems"));
  }
  var checkoutLink    = jQuery("#permaLinkCart").val();
  var checkoutFlag    = jQuery("#checkoutFlag").val();
  var html            = "";
  var total           = 0;
  var restaurantAmout = [];
  var checkoutDisable = 0;
  var checkoutClass   = "checkout-enable";
  var redirectTab     = jQuery("#redirectTab").val();
  if(cartItems != null) {

    for (i = 0; i < cartItems.length; i++) {
      var sizeArray     = cartItems[i].size;
      sizeArray         = sizeArray.split("-");
      var price         = parseFloat(sizeArray[1]) * cartItems[i].quantity;
      var itemTitle     = cartItems[i].title;
      itemTitle         = itemTitle.substring(0, 10)+"...";
      var restaurantId  = cartItems[i].restaurantId;

      if(redirectTab == 1) {
        var qtyHtml = "<span>"+cartItems[i].quantity+"</span>";
      }
      else {
        var qtyHtml = "<span class='minus' data-id='"+i+"'>-</span><span class='quantity'>"+cartItems[i].quantity+"</span><span class='plus' data-id='"+i+"' >+</span>";
      }

      html          = html + "<div class='cart-row'><span>("+sizeArray[0]+")</span><span> "+itemTitle+" x </span>"+qtyHtml+"<span class='cartPrice'> $"+price+"</span></div>";
      html          = html + "<input type='hidden' id='qty"+i+"' value='"+cartItems[i].quantity+"' />";
      total         = total + price;

      if(typeof restaurantAmout[restaurantId] === 'undefined')
        restaurantAmout[restaurantId] = 0;

      restaurantAmout[restaurantId] = restaurantAmout[restaurantId] + price;
    }

  }

  restaurantAmout.forEach(function(value, index){
    if(restaurantAmout[index] < restaurantCart[index]) {
        checkoutDisable = 1;
    }
  });

  if(cartItems == null || cartItems.length <= 0 || checkoutDisable == 1) {
    if(redirectTab == 1)
      window.location.replace(checkoutLink);
  }

  if(checkoutDisable == 1) {
      checkoutLink  = "javascript:(void(0));";
      checkoutClass = "checkout-disable";
      jQuery( "#checkoutContinue" ).addClass("checkout-disable");
      jQuery( "#checkoutContinue" ).prop('type', 'button');
  }
  else {
    jQuery( "#checkoutContinue" ).removeClass("checkout-disable");
    jQuery( "#checkoutContinue" ).prop('type', 'submit');
  }

  if(total > 0) {
    html = html + "<div class='cart-Total'> Total $"+ total + "</div><div class='clearfix'></div>";
    
    if(checkoutFlag != "1")
        html = html + '<a href="'+checkoutLink+'"><button class="btn btn-primary checkout '+checkoutClass+'">Checkout</button></a><div class="clearfix"></div>';
  }

  jQuery("#cart").html(html);

  */
}

jQuery(document).ready(function($){
  
  displayCart();
  if(jQuery("#confirmFlag").val() == 1) {
    localStorage.removeItem("cartItems");
    localStorage.removeItem("restaurantCart");
  }
  jQuery('.updateCart').click(function(){
    var cartItems = JSON.parse(localStorage.getItem("cartItems"));
    if(cartItems == null) {
      cartItems   = [];
    }
    var cartItem              = {};
    var id                    = jQuery(this).attr("data-id");
    var restaurantId          = jQuery("#restaurantId").val();
    var title                 = jQuery("#itemDetails"+id).val();
    var size                  = jQuery("#size"+id).val();
    var quantity              = jQuery("#quantity"+id).val();
    cartItem['id']            = id;
    cartItem['title']         = title;
    cartItem['size']          = size;
    cartItem['quantity']      = (quantity == ""?0:parseInt(quantity));
    cartItem['restaurantId']  = restaurantId;
    cartItems.push( cartItem );
    cartItems                 = mergeCart(cartItems);
    var cart                  = JSON.stringify(cartItems);
    localStorage.setItem("cartItems", cart);

    var restaurantCart = JSON.parse(localStorage.getItem("restaurantCart"));
    if(restaurantCart == null) {
      restaurantCart  = [];
    }
    var minOrderAmout             = jQuery("#minOrderAmout").val();
    restaurantCart[restaurantId]  = parseInt(minOrderAmout);
    var rCart                     = JSON.stringify(restaurantCart);
    localStorage.setItem("restaurantCart", rCart);

    displayCart();
  });


  // jQuery(document).on("click", ".plus, .minus", function(){
  //     var cartItems = JSON.parse(localStorage.getItem("cartItems"));
  //     var id        = jQuery(this).attr("data-id");
  //     var className = jQuery(this).attr("class");
  //     var quantity  = jQuery("#qty"+id).val();
  //     var newCart   = [];

  //     if(className == "plus")
  //       cartItems[id].quantity = parseInt(quantity) + 1;
  //     else
  //       cartItems[id].quantity = parseInt(quantity) - 1;

  //     for (i = 0; i < cartItems.length; i++) {
  //         if(cartItems[i].quantity > 0) {
  //             newCart.push( cartItems[i] );
  //         }
  //     }

  //     var cart = JSON.stringify(newCart);
  //     localStorage.setItem("cartItems", cart);
  //     displayCart();
  // });
});

jQuery(document).ready(function($){
 jQuery(".next").click(function(){
    var $toHighlight = jQuery('.active').next().length > 0 ? jQuery('.active').next() : jQuery('#addCategoryForDish li').first();
      jQuery('.active').removeClass('active');
        $toHighlight.addClass('active');
    });

    jQuery(".prev").click(function(){
      var $toHighlight = jQuery('.active').prev().length > 0 ? jQuery('.active').prev() : jQuery('#addCategoryForDish li').last();
        jQuery('.active').removeClass('active');
        $toHighlight.addClass('active');
});

    jQuery(".filter-head .fa-shopping-cart").click(function(){
         jQuery(".restaurane-dish-checkout").toggleClass("open-cart");

    });

    jQuery(".details-left-sec").click(function(){
         jQuery(".restaurane-dish-checkout").removeClass("open-cart");

    });
    jQuery(".logo_container").click(function(){
         jQuery(".restaurane-dish-checkout").removeClass("open-cart");

    });

     jQuery(".maplocatiopn-icon.filter-tags").click(function(){
         jQuery(".maplocatiopn-icon.filter-tags input").toggleClass("mobile-datepicker");

    });

    jQuery(".check_your_cart").click(function(){
        jQuery(".restaurane-dish-checkout").toggleClass("open-cart");

   });

   jQuery(".restaurane-dish-checkout .your-match span.fa-close").click(function(){
    jQuery(".restaurane-dish-checkout").toggleClass("open-cart");

    }); 


    jQuery('.options-choices.common-choices.common-radio h4').click(function(){

        // jQuery('.options-choices.common-choices.common-radio h4').siblings('ul').stop(true).slideUp();
        jQuery(this).siblings('ul').stop(true).slideToggle();
        jQuery(this).siblings('span.more').hide();
        jQuery(this).siblings('.options-choices.common-choices.common-radio ul').removeClass('more-list');
        
                if( jQuery(this).hasClass( 'angle-up' ) ){
                        jQuery('.options-choices.common-choices.common-radio h4').siblings('.list-page .more').hide(10);
                        jQuery(this).siblings('.list-page .more').show(10);
                        jQuery(this).removeClass( 'angle-up' )
                     
                }else{

                    jQuery(this).addClass( 'angle-up' )
                    jQuery(this).siblings('.list-page .more').hide();
                    jQuery('.options-choices.common-choices.common-radio h4').siblings('.list-page .less').hide();
                }
            

        });


    jQuery('.list-page .more').click(function(){
        jQuery(this).siblings('.options-choices.common-choices.common-radio ul').addClass('more-list');
        jQuery(this).hide();
        jQuery(this).siblings('.less').show();

    });
    jQuery('.list-page .less').click(function(){
        jQuery(this).siblings('.options-choices.common-choices.common-radio ul').removeClass('more-list');
        jQuery('.list-page .less').hide();
        jQuery(this).siblings('.more').show();


    });

    jQuery('.list-page .mobile-filters').click(function(){

        jQuery('.list-page .filter-box').addClass('show_mob_filters');
        jQuery('.list-page .col-lg-10.col-md-9.col-sm-9').addClass('list_page_filter_sec');
        jQuery('.list-page .close_mob_filters.fa.fa-close').show();

    });


    jQuery('.list-page .close_mob_filters.fa.fa-close').click(function(){
        jQuery('.list-page .filter-box').removeClass('show_mob_filters');
        jQuery('.list-page .col-lg-10.col-md-9.col-sm-9').removeClass('list_page_filter_sec');
        jQuery(this).hide();

    });


    jQuery('.list-page .inner-reset-button').click(function(){
        jQuery(this).hide();
    });

    jQuery('.list-page .options-choices.common-choices.common-radio li').click(function(){
        jQuery('.list-page .inner-reset-button').show();
    });


});



// $('.custom-right').click(function(e) {
//     e.preventDefault();
//     $('#MyAccountsTab').animate({
//         'margin-left' : '-=40px'
//     });
// });






//SCRIPT WRITTEN BY HARISH KATHAYAT//

//sticky header
jQuery(window).scroll(function(){
  if (jQuery(window).scrollTop() >= 30) {
    jQuery('#submissionForm .tab').addClass('fixed-top');
    jQuery('.side-menu-button').addClass('side-menu-fixed');
    jQuery('body.wyzchef-admin-console').addClass('fixed-menu-filter');
    jQuery('.list-page .datetimepicker').addClass('fixed');
    // jQuery('.container.filter-head').addClass('full-filter-bar');
    jQuery('.restaurane-dish-checkout').addClass('fixed-top-cart');
    jQuery('.filter-box').addClass('fixed-filter-box');
    jQuery('.list-head-filter-bar').addClass('fixed-filter-bar');
       

        if ($(window).width() < 1200){
            jQuery('.list-head-filter-bar').addClass('fixed-filter-bar-mobile');
            jQuery('.list-page .difrentListing').addClass('mobile-list-container'); 
    
        }
       
       
  }
  else {
    jQuery('#submissionForm .tab').removeClass('fixed-top');
    jQuery('.side-menu-button').removeClass('side-menu-fixed');
    jQuery('body.wyzchef-admin-console').removeClass('fixed-menu-filter');
    // jQuery('.container.filter-head').removeClass('full-filter-bar');
    jQuery('.restaurane-dish-checkout').removeClass('fixed-top-cart');
    jQuery('.list-head-filter-bar').removeClass('fixed-filter-bar-mobile');    
    jQuery('.list-page .difrentListing').removeClass('mobile-list-container');    
    jQuery('.filter-box').removeClass('fixed-filter-box'); 
    jQuery('.list-page .datetimepicker').removeClass('fixed');
    jQuery('.list-head-filter-bar').removeClass('fixed-filter-bar');  
   

  }

  if (jQuery(window).scrollTop() >= 350) {
    jQuery('.restaurant-name-section ul.menu-list-items').addClass('fixed-side-menu');
    jQuery('.restaurane-dish-checkout').addClass('fixed-cart'); 
    jQuery('.datetimepicker').addClass('fixed-top-cal');

  }
  else {
    jQuery('.restaurant-name-section ul.menu-list-items').removeClass('fixed-side-menu');
    jQuery('.restaurane-dish-checkout').removeClass('fixed-cart');    
    jQuery('.datetimepicker').removeClass('fixed-top-cal');

  }

  
});


// $(window).on('scroll', function () {
//     var scrollTop = $(window).scrollTop();
//     if (scrollTop > 50) {
//         $('.filter-box').stop().animate({height: "30px"},200);
//     }
//     else {
//          $('.filter-box').stop().animate({height: "50px"},200);   
//     }
// });


// jQuery(window).scroll(function(){
//     if (jQuery(window).scrollTop() >= 100) {
//         var windowH = jQuery(window).height();
//         var wrapperH = jQuery('.filter-box').height();
//         if(windowH < wrapperH) {   
//             // jQuery('.filter-box').css({'display': 'none'});                         
//             jQuery(windowH).css('height',wrapperH +'px');
//         }                                                                               
        
//         console.log(wrapperH);
//     }
// });


// $(function(){
//     $('.filter-box .row').each(function() {
//       var off = $(this).offset().top
//       $(this).data('orig-offset', off);
//     });
// $(window).scroll(function(){
//     var scrollTop = $(window).scrollTop();

//      $('.filter-box .row').each(function(){
//       var off = $(this).data('orig-offset');
      
       
//       if (scrollTop >= off) {
//         var translate =  (off - scrollTop) / $(window).height() * 100;
//         console.log(translate);
//         $(this).css({transform: 'translateY(' + translate +'%)'});
//       }
//      });
//   });
// });

// jQuery(window).scroll(function(){
//     if (jQuery(window).scrollTop() >= 100) {
//         var windowH = jQuery(window).height();
//         var wrapperH = jQuery('.filter-box').height();
//         if(windowH < wrapperH) {   
//             // jQuery('.filter-box').css({'display': 'none'});                         
//             jQuery(windowH).css('height',wrapperH +'px');
//         }                                                                               
        
//         console.log(wrapperH);
//     }
// });





jQuery(window).scroll(function(){
    if (jQuery(window).scrollTop() >= 120) {
        if ($(window).width() < 980){
            jQuery('.list-page .difrentListing').toggleClass('mobile-list-container');    
        }
        
    }
});

//My profile gallery image preview
function myFunction(imgs) {
  var expandImg = document.getElementById("expandedImg");
  var imgText = document.getElementById("imgtext");
  expandImg.src = imgs.src;
  imgText.innerHTML = imgs.alt;
  expandImg.parentElement.style.display = "block";
}

//To show map on delivery tab

jQuery(document).ready(function($){

  jQuery(".map-btn").click(function(){
  jQuery(this).siblings("#map-content").slideToggle("slow");
});

  jQuery(".map-close").click(function(){
  jQuery("#map-content").slideToggle("slow");
});

// Hours tabbing on My profile tab

  jQuery('ul.nav-tabs li').click(function(){
    var tab_id = jQuery(this).attr('data-tab');

    jQuery('ul.nav-tabs li').removeClass('active');
   jQuery('.tab-content >.tab-pane').removeClass('active');

   jQuery(this).addClass('active');
    jQuery("#"+tab_id).addClass('active');
  });

 jQuery('.filter-tags.filter-button').click(function(e){

  jQuery(".filter-box").slideToggle();
  jQuery(".restauranList").toggleClass("more-margin");
  jQuery(".restaurant-details-page").toggleClass("more-margin");
  
 });

 

 jQuery('#reward-profile').click(function(){
  jQuery('.admin-console-menu .profile-tab').addClass('active');
  jQuery('.admin-console-menu .profile-tab').addClass('profile-active');
  jQuery('.admin-console-menu .profile-tab').children().addClass('active');
  jQuery('#Profile').css('display', 'block');
  jQuery('#Profile').offsetTop
  jQuery('.admin-console-menu .rewards-tab').removeClass('active');
  jQuery('.admin-console-menu .rewards-tab').children().removeClass('active');
  jQuery('#Rewards').css('display', 'none');
  $("html, body").animate({ scrollTop: 0 }, "slow");

 });

 jQuery('#reward-preference').click(function(){
  jQuery('.admin-console-menu .preferences-tab').addClass('active');
  jQuery('.admin-console-menu .preferences-tab').addClass('profile-active');
  jQuery('.admin-console-menu .preferences-tab').children().addClass('active');
  jQuery('#Preferences').css('display', 'block');
  jQuery('#Preferences').offsetTop
  jQuery('.admin-console-menu .rewards-tab').removeClass('active');
  jQuery('.admin-console-menu .rewards-tab').children().removeClass('active');
  jQuery('#Rewards').css('display', 'none');
  $("html, body").animate({ scrollTop: 0 }, "slow");
 });


 jQuery('#rewards-review').click(function(){
  jQuery('.admin-console-menu .orders-tab').addClass('active');
  jQuery('.admin-console-menu .orders-tab').addClass('profile-active');
  jQuery('.admin-console-menu .orders-tab').children().addClass('active');
  jQuery('#Orders').css('display', 'block');
  jQuery('.admin-console-menu .rewards-tab').removeClass('active');
  jQuery('.admin-console-menu .rewards-tab').children().removeClass('active');
  jQuery('#Rewards').css('display', 'none');
  $("html, body").animate({ scrollTop: 0 }, "slow");


 });

 jQuery('.restaurant-name-section .menu-list-items li a').click(function(){
  
   jQuery('.restaurant-name-section .menu-list-items li a').removeClass('active');
   jQuery(this).addClass('active');
    
});


jQuery(window).scroll(function(){
    if (jQuery(window).scrollTop() >= 600) {
        jQuery('#dishes-listing-loader').removeClass('sort_loader');

    }
    
});


/*jQuery(".inline-display-list li label span").on("click", function(){
    let parentContainer = jQuery(this).parent().parent().parent();
    if( jQuery(this).hasClass("added") ){
        var text = jQuery(this).text();
        jQuery( "#cat_list li" ).each(function(){
            var ctext = jQuery( this).find(".flt-text").text();
            if( text == ctext ){

                jQuery(this).remove();
            }
        });

        jQuery(this).removeClass("added")
    }else{
        jQuery( this ).addClass( 'added' );
        //if(checkedCount){
        var li = document.createElement("li");
        var customSpan = $(this).text();
        var textSpan = document.createElement("SPAN");
        var flText = document.createTextNode(customSpan);
        textSpan.className = "flt-text";
        textSpan.appendChild(flText);
        var t = document.createTextNode(customSpan);
        var span = document.createElement("SPAN");
        var txt = document.createTextNode("\u00D7");
        span.className = "close";
        span.appendChild(txt);
        li.appendChild(span);
        li.appendChild(textSpan);
        document.getElementById("cat_list").appendChild(li);
        //}
    }
});*/

// For listing page filters
// jQuery(".list-page .options-choices .inline-display-list li label span").on("click", function(){
//     let parentContainer = jQuery(this).parent().parent().parent();
//     if( jQuery(this).hasClass("added") ){
//         var text = jQuery(this).text();
//         jQuery( "#list-page-filter-list li" ).each(function(){
//             var ctext = jQuery( this).find(".flt-text").text();
//             if( text == ctext ){

//                 jQuery(this).remove();
//             }
//         });

//         jQuery(this).removeClass("added")
//     }else{
//         jQuery(this).addClass('added');
//         var li = document.createElement("li");
//         var customSpan = $(this).text();
//         var textSpan = document.createElement("SPAN");
//         var flText = document.createTextNode(customSpan);
//         textSpan.className = "flt-text";
//         textSpan.appendChild(flText);
//         var t = document.createTextNode(customSpan);
//         var span = document.createElement("SPAN");
//         var txt = document.createTextNode("\u00D7");
//         span.className = "close";
//         span.appendChild(txt);
//         li.appendChild(span);
//         li.appendChild(textSpan);
//         document.getElementById("list-page-filter-list").appendChild(li);
//     }
// });


// jQuery('.super-category-list .common-radio ul li').click(function(){

//      //if(jQuery(this).children('input').is(':checked')){
//     if(jQuery('.super-category-list .common-radio ul li input').is(':checked')){
//         jQuery('.super-category-list > ul >li').addClass('selected-category');
//     }

//     else{
//         jQuery('.super-category-list > ul >li').removeClass('selected-category');
//     }

// });

jQuery('.super-category-list input[type="checkbox"]').click(function(){
    let parentContainer = jQuery(this).parent().parent().parent();
    let checkAll = jQuery(this).hasClass('checkAll');
    if (checkAll) {
        if (jQuery(this).prop('checked')) {
            jQuery('input[type="checkbox"]', parentContainer).prop('checked', true);
        } else {
            jQuery('input[type="checkbox"]', parentContainer).prop('checked', false);
        }
    } else {
        jQuery('input.checkAll', parentContainer).prop('checked', false);
    }

    let checkedCount = jQuery('input[type="checkbox"]:checked', parentContainer).length;
    if(checkedCount){
        parentContainer.parent().parent().addClass('selected-category');
    } else {
        parentContainer.parent().parent().removeClass('selected-category');
    }
});

jQuery('.super-filter .header-filter-form-reset').click(function(){
    $('.super-category-list ul li').removeClass('selected-category');
});

// jQuery('body').click(function(){
//     jQuery('.detail-filter .options-choices.common-choices.common-radio').slideUp();
// });

$(document).ready(function () {
    jQuery('.detail-filter .inner-filter-button').click(function(){
        jQuery('.detail-filter .options-choices.common-choices.common-radio').stop(true).slideToggle();
        return false;
    });
    $(document).click(function (e) {
        if (!$(e.target).closest('.detail-filter .inner-filter-button, .detail-filter .options-choices.common-choices.common-radio').length){
            jQuery('.detail-filter .options-choices.common-choices.common-radio').stop(true).slideUp();

        }
    });
});

// jQuery('.detail-filter .options-choices.common-choices.common-radio').click(function(){
//     // jQuery(this).show();
//     jQuery(this).slideDown();
//     return false;

// });


jQuery('.menu-list-items li a').click(function(){
    let catText= jQuery(this).text();
    jQuery('.dish_cat_head h4').text(catText)
});  




jQuery(".header-filter-form").on("click", ".filter-tags.date", function(){
        jQuery(".catering_warning_message p").hide();
});


jQuery('body').click(function(evt){    
  // if(evt.target.className == "without-scroll")
  //    return;
  //    alert('In scope');
  //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
  if(jQuery(evt.target).closest('.without-scroll').length)
     return;          
jQuery('.super-category-list').removeClass('without-scroll');
 //Do processing of click event here for every element except with id menu_content

});


/*
* Check & Uncheck all super categories checkbox.
*/
// jQuery(".super-category-list .checkAll").change(function () {
//   var id = jQuery(this).attr('rel');
//   jQuery("#checkAll"+id).parent().parent().parent().find("input:checkbox").prop('checked', jQuery(this).prop("checked"));
// });


// To redirect the dashboard menu to submit menu

  /*jQuery('.dash-menu .icon-tab').click(function(){
    var menu_list_id = jQuery(this).find("input").val();
    alert(menu_list_id);

     var sidebar_list = $(".submit_sidebar .icon-tab");
     sidebar_list.each(function(idx, li) {
    
});*/


/*jQuery('.restaurant-listings-form .tabcontent > a.edit-button').click(function(){
    var current_edit_link = jQuery(this).parent().attr('id');
    var menu_list_id = jQuery(this).parent().siblings('.tab').find(".icon-tab input").val();

     var sidebar_list = jQuery(".dash-menu .icon-tab");
     sidebar_list.each(function(i) {

     var a = sidebar_list.find("input").val();

     if(current_edit_link === a){
        alert("yes");
     }
     else{
      alert("no");
     }
     });

    
});*/

    jQuery('.side-menu-button').click(function(){
        jQuery("#submissionForm .tab").toggleClass("mobile-side-bar");
        jQuery(this).toggleClass("menu-show-icon");
        jQuery(".admin-console-menu").toggleClass("mobile-side-bar");
    });


    if ( jQuery(window).width() < 1200) {


            jQuery('.icon-tab').click(function(){
                jQuery("#submissionForm .tab").toggleClass("mobile-side-bar");
                jQuery(this).toggleClass("menu-show-icon");
                jQuery(".admin-console-menu").toggleClass("mobile-side-bar");
            });
       
    } else{
      
    }



    jQuery('.tab .icon-tab').click(function(){
        jQuery('.tab .icon-tab').removeClass("active");
        jQuery(this).addClass("active");
        // jQuery(".tab .icon-tab").children('.image-default').removeClass("active");
        // jQuery(this).children('.image-default').addClass("active");
        // jQuery(".tab .icon-tab").children('.image-hover').removeClass("active");
        // jQuery(this).children('.image-hover').addClass("active");
        jQuery('.tabcontent').removeClass('active');
        let tabContentId = jQuery('.tablinks', this).val();
        jQuery('#'+tabContentId).addClass('active');
        // jQuery('#'+tabContentId).show();

        jQuery("#submissionForm .tab").removeClass("mobile-side-bar");
        jQuery(".admin-console-menu").removeClass("mobile-side-bar");
        jQuery('.side-menu-button').removeClass("menu-show-icon");
    });


    jQuery('.admin-console-menu input.tablinks').on('click', function(){
        let queryString = '';

        url = new URL(window.location.href);
        var postId = url.searchParams.get("post_id");
        var userId = url.searchParams.get("user_id");
        
        if (postId) {
            queryString += 'post_id='+ postId;
        }
        
        if (userId) {
            if ('' == queryString) {
                queryString += 'user_id='+ userId;
            } else {
                queryString += '&user_id='+ userId;
            }
        }

        if ('' == queryString) {
            queryString += 'active-tab='+ $(this).val();
        } else {
            queryString += '&active-tab='+ $(this).val();
        }
        
        queryString = '?' + queryString;
        
        addQueryParam(queryString);

        if ($('#submissionForm').length) {
            $('#submissionForm').attr('action', window.location.href);
        }
    });
    

    jQuery('#wp_restaurant_logo').on('change', function() {
        let countFiles = jQuery(this)[0].files.length;
        let imgPath = jQuery(this)[0].value;
        let extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        let fileSize = this.files[0].size;
        
        if ((fileSize > 1024000)) {
            alert("Image size must less than 1MB");
        } else if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if (typeof(FileReader) != "undefined") {
                for (let i = 0; i < countFiles; i++) {
                    let reader = new FileReader();
                    reader.onload = function(e) {
                        jQuery('#restaurant-logo-preview').html('<img src="'+e.target.result +'" class="myGallery" width="78" height="78" />');  
                    }
            
                    reader.readAsDataURL(jQuery(this)[0].files[i]);
                }
            } else {
                console.log("This browser does not support FileReader.");
            }
        } else {
            console.log("Please select only images");
        }
    });

    jQuery('#wp_restaurant_gallery').on('change', function() {
        let countFiles = jQuery(this)[0].files.length;
        let imgPath = jQuery(this)[0].value;
        let extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        let fileSize = this.files[0].size;
        
        if ((fileSize > 2048000)) {
            alert("Image size must less than 2MB");
        } else if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if (typeof(FileReader) != "undefined") {
                for (let i = 0; i < countFiles; i++) {
                    let reader = new FileReader();
                    reader.onload = function(e) {
                        jQuery('#restaurant-cover-preview').html('<img src="'+e.target.result +'" class="myGallery" width="78" height="78" />');  
                    }
            
                    reader.readAsDataURL(jQuery(this)[0].files[i]);
                }
            } else {
                console.log("This browser does not support FileReader.");
            }
        } else {
            alert("Please select only images");
        }
    });

    jQuery('#wp_restaurant_list_img').on('change', function() {
        let countFiles = jQuery(this)[0].files.length;
        let imgPath = jQuery(this)[0].value;
        let extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        let fileSize = this.files[0].size;
        
        if ((fileSize > 1024000)) {
            alert("Image size must less than 1MB");
        } else if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if (typeof(FileReader) != "undefined") {
                for (let i = 0; i < countFiles; i++) {
                    let reader = new FileReader();
                    reader.onload = function(e) {
                        jQuery('#restaurant-list-preview').html('<img src="'+e.target.result +'" class="myGallery" width="78" height="78" />');  
                    }
            
                    reader.readAsDataURL(jQuery(this)[0].files[i]);
                }
            } else {
                console.log("This browser does not support FileReader.");
            }
        } else {
            alert("Please select only images");
        }
    });

    $('#submissionForm').on('submit', function(){
        if (!$('input[name="terms_conditions"]').prop('checked')) {
            $('#termsAndCondtions').modal();
            
            return false;
        }
        
        $("<input />").attr("type", "hidden")
            .attr("name", "menu_dishes")
            .attr("value", JSON.stringify(finalPostData))
            .appendTo(this);
    });

    $('.submit_sidebar input.tablinks').on('click', function(){
        let queryString = '';

        url = new URL(window.location.href);
        var postId = url.searchParams.get("post_id");
        var userId = url.searchParams.get("user_id");
        
        if (postId) {
            queryString += 'post_id='+ postId;
        }
        
        if (userId) {
            if ('' == queryString) {
                queryString += 'user_id='+ userId;
            } else {
                queryString += '&user_id='+ userId;
            }
        }

        if ('' == queryString) {
            queryString += 'active-tab='+ $(this).val();
        } else {
            queryString += '&active-tab='+ $(this).val();
        }
        
        queryString = '?' + queryString;
        
        addQueryParam(queryString);

        // if ($('#submissionForm').length) {
        //     $('#submissionForm').attr('action', window.location.href);
        // }
    });


    $('.service-options-container').on('change', 'input.option-name:last', function(){
        if ($(this).val().trim() != '') {
            $('.service-options-container').append('\
                <div class="option-data">\
                    <input type="hidden" class="option-id" value="" />\
                    <div class="col-md-5"><input type="text" class="option-name" placeholder="Name" /></div>\
                    <div class="col-md-2"><input type="text" class="option-price" placeholder="Price: eg. 20" value="0" /></div>\
                    <div class="col-md-2"><input type="text" class="option-tax" placeholder="Tax(%): eg. 7" value="7" /></div>\
                    <div class="col-md-2"><input type="text" class="option-total" placeholder="Total: eg. 25" value="0" /></div>\
                    <div class="col-md-1">\
                        <a tabindex="-1" href="#delete-option" class="delete-option" data-service-id="" data-option-id=""><i class="fa fa-trash-o"></i></a>\
                    </div>\
                </div>\
            ');
        }
    });

    $('.service-options-container').on('change', '.option-price', function(){
        let price = parseFloat($(this).val());
        let taxParent = $(this).parent().next();
        let totalParent = $(this).parent().next().next();
        let tax = parseFloat($('.option-tax', taxParent).val());
        tax = 1 + (tax/100);
        let totalElement = $('.option-total', totalParent);

        totalElement.val((price*tax).toFixed(2));
    });

    $('.service-options-container').on('change', '.option-total', function(){
        let total = parseFloat($(this).val());
        let taxParent = $(this).parent().prev();
        let priceParent = $(this).parent().prev().prev();
        let tax = parseFloat($('.option-tax', taxParent).val());
        tax = 1 + tax/100;
        let priceElement = $('.option-price', priceParent);

        priceElement.val((total/tax).toFixed(2));
    });

    $('.service-options-container').on('change', '.option-tax', function(){
        let tax = parseFloat($(this).val());
        tax = 1 + tax/100;
        let priceParent = $(this).parent().prev();
        let totalParent = $(this).parent().next();
        let priceElement = $('.option-price', priceParent);
        let totalElement = $('.option-total', totalParent);
        let price = parseFloat(priceElement.val());
        let total = parseFloat(totalElement.val());

        if (price) {
            totalElement.val((price*tax).toFixed(2));
        } else if (total) {
            priceElement.val((total/tax).toFixed(2));
        } else {
            totalElement.val(0);
            priceElement.val(0);
        }
    });

    $('.service-options-container').on('click', '.delete-option', function(){
        let optionId = $(this).attr('data-option-id');
        if (optionId) {
            restaurantDeletedTempOptions.push(optionId);
        }

        $(this).parent().parent().remove();

        return false;
    });

    $('#service-modal').on('hidden.bs.modal', function (e) {
        restaurantDeletedTempOptions = [];
        $('.service-type').first().prop('checked', true);
        $('.service-quantity').prop('checked', false);
        $('.service-question').val('');
        let firstOption = $('.service-options-container .option-data').first();
        firstOption.siblings().remove();
        $('.option-id').val('');
        $('.option-name').val('');
        $('.option-price').val(0);
        $('.option-tax').val(7);
        $('.option-total').val(0);

        $('.service-add-btn').show();
        $('.service-save-btn').attr('data-service-id', '');
        $('.service-save-btn').hide();
    });
});

jQuery(document).on('click', '#cat_list li span.close', function(){
    let txt = jQuery(this).next().text();
    var div = this.parentElement;
    div.remove();
    jQuery('.inline-display-list span').each(function(){
        let txt1 = jQuery(this).text();
        
        if(txt == txt1){
            jQuery( this ).removeClass('added');
            jQuery(this).siblings('.dish-tag').prop('checked', false);
            //$('#dish-button').trigger('click');
        }
    });
    
});


// For listing page filters

// jQuery(document).on('click', '#list-page-filter-list li span.close', function(){
//     let txt = jQuery(this).next().text();
//     var div = this.parentElement;
//     div.remove();
//     jQuery('.list-page .options-choices .inline-display-list span').each(function(){
//         let txt1 = jQuery(this).text();
        
//         if(txt == txt1){
//             jQuery( this ).removeClass('added');
//             jQuery(this).siblings('.dish-tag').prop('checked', false);
//         }
//     });
    
// });

/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/

function addTypeOfService() {
    // let id = Date.now();
    let type = (parseInt($('.type-of-service-type:checked').val()) ? 1 : 0);
    let typeText = (type ? 'Required' : 'Optional');
    // let typeText = $('.type-of-service-type:checked').parent().text();
    let quantityRequired = $('.type-of-service-quantity').prop('checked');
    let question = $('.type-of-service-question').val().trim();

    let optionIds = $('.type-of-service-options-container .type-of-option-id');
    let optionNames = $('.type-of-service-options-container .type-of-option-name');
    let optionsPrices = $('.type-of-service-options-container .type-of-option-price');
    let optionsTaxs = $('.type-of-service-options-container .type-of-option-tax');
    let optionsTotals = $('.type-of-service-options-container .type-of-option-total');
    let options = [];
    for(let i = 0; i < optionNames.length; i++) {
        let optionName = optionNames[i].value.trim();
        if (optionName) {
            options.push({
                id: '',
                name: optionName,
                price: parseFloat(optionsPrices[i].value),
                tax: parseFloat(optionsTaxs[i].value),
                total: parseFloat(optionsTotals[i].value)
            });
        }
    }

    restaurantTypeOfServices.push({
        id: '',
        type: type,
        quantityRequired: quantityRequired,
        question: question,
        options: options
    });

    let serviceId = restaurantTypeOfServices.length - 1;

    let serviceData = '';
    serviceData = '\
        <div class="restaurant-type-of-service" data-id="'+ serviceId +'">\
            <div class="headerBox">\
                <div class="row">\
                    <div class="col-md-11 col-sm-11">\
                        <h5 class="titleHeader">\
                            <span class="type-of-service-question-text">'+ question +'</span>\
                        </h5>\
                        <p class="card-text">\
                            <span class="type-of-service-type-text">'+ typeText +'</span>\
                        </p>\
                    </div>\
                    <div class="col-md-1 col-sm-1">\
                        <div class="type-of-service-edit-wrapper">\
                            <a href="#edit" class="type-of-service-edit" onclick="editTypeOfService(this, '+ serviceId +'); return false;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>\
                            <a href="#copy" class="type-of-service-copy" onclick="copyTypeOfService('+ serviceId +'); return false;" ><i class="fa fa-copy"></i></a>\
                            <a href="#delete" class="type-of-service-delete" onclick="deleteTypeOfService(this); return false;"><i class="fa fa-trash-o"></i></a>\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    ';

    $('.restaurant-type-of-services-container').append(serviceData);

    $("#type-of-service-modal").modal('hide');

    $('#restaurant_type_of_services').val(JSON.stringify(restaurantTypeOfServices));

    return false;
}

function editTypeOfService(elementObj, typeOfServiceId) {
    let typeOfService = restaurantTypeOfServices[typeOfServiceId];
    let type = parseInt(typeOfService['type']);
    let quantityRequired = parseInt(typeOfService['quantityRequired']);
    let question = typeOfService['question'];
    let options = typeOfService['options'];

    $('.type-of-service-type[value='+ type +']').prop('checked', true);
    $('.type-of-service-quantity').prop('checked', quantityRequired);
    $('.type-of-service-question').val(question);

    let optionsData = '';
    let actionIcon = '';
    for (key in options) {
        let option = options[key];
        if (actionIcon) {
            actionIcon = '<a tabindex="-1" href="#delete-type-of-option" class="delete-type-of-option" data-type-of-service-id="'+ typeOfServiceId +'" data-type-of-option-id="'+ option['id'] +'"><i class="fa fa-trash-o"></i></a>';
        }

        optionsData += '\
            <div class="type-of-option-data">\
                <input type="hidden" class="type-of-option-id" value="'+ option['id'] +'" />\
                <div class="col-md-5">\
                    <input type="text" class="type-of-option-name" placeholder="Name" value="'+ option['name'] +'">\
                </div>\
                <div class="col-md-2">\
                    <input type="text" class="type-of-option-price" placeholder="Price: eg. 20" value="'+ option['price'] +'">\
                </div>\
                <div class="col-md-2">\
                    <input type="text" class="type-of-option-tax" placeholder="Tax(%): eg. 7" value="'+ option['tax'] +'">\
                </div>\
                <div class="col-md-2">\
                    <input type="text" class="type-of-option-total" placeholder="Total: eg. 25" value="'+ option['total'] +'">\
                </div>\
                <div class="col-md-1">\
                    '+ actionIcon +'\
                </div>\
            </div>\
        ';

        actionIcon = 'allow icons after first loop';
    }

    optionsData += '\
        <div class="type-of-option-data">\
            <input type="hidden" class="type-of-option-id" value="" />\
            <div class="col-md-5">\
                <input type="text" class="type-of-option-name" placeholder="Name" value="">\
            </div>\
            <div class="col-md-2">\
                <input type="text" class="type-of-option-price" placeholder="Price: eg. 20" value="0">\
            </div>\
            <div class="col-md-2">\
                <input type="text" class="type-of-option-tax" placeholder="Tax(%): eg. 7" value="7">\
            </div>\
            <div class="col-md-2">\
                <input type="text" class="type-of-option-total" placeholder="Total: eg. 25" value="0">\
            </div>\
            <div class="col-md-1">\
                <a tabindex="-1" href="#delete-type-of-option" class="delete-option" data-type-of-service-id="" data-type-of-option-id=""><i class="fa fa-trash-o"></i></a>\
            </div>\
        </div>\
    ';

    $(".type-of-service-options-container").html(optionsData);

    $('.type-of-service-add-btn').hide();
    $('.type-of-service-save-btn').attr('data-type-of-service-id', typeOfServiceId);
    $('.type-of-service-save-btn').show();

    $("#type-of-service-modal").modal();

    return false;
}

function updateTypeOfService(btnObject) {
    let typeOfServiceId = $(btnObject).attr('data-type-of-service-id');
    let parentContainer = $('div[data-id="'+ typeOfServiceId +'"]')
    let type = (parseInt($('.type-of-service-type:checked').val()) ? 1 : 0);
    let typeText = (type ? 'Required' : 'Optional');
    // let typeText = $('.type-of-service-type:checked').parent().text();
    let quantityRequired = $('.type-of-service-quantity').prop('checked');
    let question = $('.type-of-service-question').val().trim();

    let optionIds = $('.type-of-service-options-container .type-of-option-id');
    let optionNames = $('.type-of-service-options-container .type-of-option-name');
    let optionsPrices = $('.type-of-service-options-container .type-of-option-price');
    let optionsTaxs = $('.type-of-service-options-container .type-of-option-tax');
    let optionsTotals = $('.type-of-service-options-container .type-of-option-total');
    let options = [];
    for(let i = 0; i < optionNames.length; i++) {
        let optionName = optionNames[i].value.trim();
        if (optionName) {
            options.push({
                id: optionIds[i].value,
                name: optionName,
                price: parseFloat(optionsPrices[i].value),
                tax: parseFloat(optionsTaxs[i].value),
                total: parseFloat(optionsTotals[i].value)
            });
        }
    }

    restaurantTypeOfServices[typeOfServiceId] = {
        id: restaurantTypeOfServices[typeOfServiceId]['id'],
        type: type,
        quantityRequired: quantityRequired,
        question: question,
        options: options
    };

    $('.type-of-service-question-text', parentContainer).text(question);
    $('.type-of-service-type-text', parentContainer).text(typeText);

    $("#type-of-service-modal").modal('hide');

    if (restaurantDeletedTempOptions.length) {
        for (var i = 0; i < restaurantDeletedTempOptions.length; i++) {
            restaurantDeletedOptions.push(restaurantDeletedTempOptions[i]);
        }
        
        $('#deletedTypeOfOptions').val(restaurantDeletedOptions.join());
    }

    $('#restaurant_type_of_services').val(JSON.stringify(restaurantTypeOfServices));

    return false;
}

function copyTypeOfService(typeOfServiceId) {
    let typeOfService = restaurantTypeOfServices[typeOfServiceId];
    // let id = Date.now();
    let type = (parseInt(typeOfService['type']) ? 1 : 0);
    let typeText = (type ? 'Required' : 'Optional');
    let quantityRequired = parseInt(typeOfService['quantityRequired']);
    let question = typeOfService['question'];

    let options = [];
    for(key in typeOfService['options']) {
        options.push({
            id: '',
            name: typeOfService['options'][key]['name'],
            price: parseFloat(typeOfService['options'][key]['price']),
            tax: parseFloat(typeOfService['options'][key]['tax']),
            total: parseFloat(typeOfService['options'][key]['total'])
        });
    }

    restaurantTypeOfServices.push({
        id: '',
        type: type,
        quantityRequired: quantityRequired,
        question: question,
        options: options
    });

    let newTypeOfServiceId = restaurantTypeOfServices.length - 1;

    let typeOfServiceData = '';
    typeOfServiceData = '\
        <div class="restaurant-type-of-service" data-id="'+ newTypeOfServiceId +'">\
            <div class="headerBox">\
                <div class="row">\
                    <div class="col-md-11 col-sm-11">\
                        <h5 class="titleHeader">\
                            <span class="type-of-service-question-text">'+ question +'</span>\
                        </h5>\
                        <p class="card-text">\
                            <span class="type-of-service-type-text">'+ typeText +'</span>\
                        </p>\
                    </div>\
                    <div class="col-md-1 col-sm-1">\
                        <div class="type-of-service-edit-wrapper">\
                            <a href="#edit" class="type-of-service-edit" onclick="editTypeOfService(this, '+ newTypeOfServiceId +'); return false;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>\
                            <a href="#copy" class="type-of-service-copy" onclick="copyTypeOfService('+ newTypeOfServiceId +'); return false;" ><i class="fa fa-copy"></i></a>\
                            <a href="#delete" class="type-of-service-delete" onclick="deleteTypeOfService(this); return false;"><i class="fa fa-trash-o"></i></a>\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    ';

    $('.type-of-restaurant-services-container').append(typeOfServiceData);

    $('#restaurant_type_of_services').val(JSON.stringify(restaurantTypeOfServices));

    return false;
}

function deleteTypeOfService(elementObj, typeOfServiceId = 0) {
    if (typeOfServiceId) {
        restaurantDeletedTypeOfServices.push(restaurantTypeOfServices[typeOfServiceId]['id']);
        $('#deletedTypeOfServices').val(restaurantDeletedTypeOfServices.join());

        for(key in restaurantTypeOfServices[typeOfServiceId]['options']) {
            let option = restaurantTypeOfServices[typeOfServiceId]['options'][key];
            if (option['id']) {
                restaurantDeletedOptions.push(option['id']);
            }
        }
        
        $('#deletedTypeOfOptions').val(restaurantDeletedOptions.join());
    }

    delete restaurantTypeOfServices[typeOfServiceId];

    $('#restaurant_type_of_services').val(JSON.stringify(restaurantTypeOfServices));

    $(elementObj).parents('.restaurant-type-of-service').remove();

    return false;
}

/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/
/* #######################################################################################*/


function addService() {
    // let id = Date.now();
    let type = (parseInt($('.service-type:checked').val()) ? 1 : 0);
    let typeText = (type ? 'Required' : 'Optional');
    // let typeText = $('.service-type:checked').parent().text();
    let quantityRequired = $('.service-quantity').prop('checked') ? 1 : 0;
    let question = $('.service-question').val().trim();

    let optionIds = $('.service-options-container .option-id');
    let optionNames = $('.service-options-container .option-name');
    let optionsPrices = $('.service-options-container .option-price');
    let optionsTaxs = $('.service-options-container .option-tax');
    let optionsTotals = $('.service-options-container .option-total');
    let options = [];
    for(let i = 0; i < optionNames.length; i++) {
        let optionName = optionNames[i].value.trim();
        if (optionName) {
            options.push({
                id: '',
                name: optionName,
                price: parseFloat(optionsPrices[i].value),
                tax: parseFloat(optionsTaxs[i].value),
                total: parseFloat(optionsTotals[i].value)
            });
        }
    }

    restaurantServices.push({
        id: '',
        type: type,
        quantityRequired: quantityRequired,
        question: question,
        options: options
    });

    let serviceId = restaurantServices.length - 1;

    let serviceData = '';
    serviceData = '\
        <div class="restaurant-service" data-id="'+ serviceId +'">\
            <div class="headerBox">\
                <div class="row">\
                    <div class="col-md-11 col-sm-11">\
                        <h5 class="titleHeader">\
                            <span class="service-question-text">'+ question +'</span>\
                        </h5>\
                        <p class="card-text">\
                            <span class="service-type-text">'+ typeText +'</span>\
                        </p>\
                    </div>\
                    <div class="col-md-1 col-sm-1">\
                        <div class="service-edit-wrapper">\
                            <a href="#edit" class="service-edit" onclick="editService(this, '+ serviceId +'); return false;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>\
                            <a href="#copy" class="service-copy" onclick="copyService('+ serviceId +'); return false;" ><i class="fa fa-copy"></i></a>\
                            <a href="#delete" class="service-delete" onclick="deleteService(this); return false;"><i class="fa fa-trash-o"></i></a>\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    ';

    $('.restaurant-services-container').append(serviceData);

    $("#service-modal").modal('hide');

    $('#restaurant_services').val(JSON.stringify(restaurantServices));

    return false;
}

function editService(elementObj, serviceId) {
    let service = restaurantServices[serviceId];
    let type = parseInt(service['type']);
    let quantityRequired = parseInt(service['quantityRequired']) ? 1 : 0;
    let question = decodeHTMLEntities(service['question']);
    let options = service['options'];

    $('.service-type[value='+ type +']').prop('checked', true);
    $('.service-quantity').prop('checked', quantityRequired);
    $('.service-question').val(question);

    let optionsData = '';
    let actionIcon = '';
    for (key in options) {
        let option = options[key];
        if (actionIcon) {
            actionIcon = '<a tabindex="-1" href="#delete-option" class="delete-option" data-service-id="'+ serviceId +'" data-option-id="'+ option['id'] +'"><i class="fa fa-trash-o"></i></a>';
        }

        optionsData += '\
            <div class="option-data">\
                <input type="hidden" class="option-id" value="'+ option['id'] +'" />\
                <div class="col-md-5">\
                    <input type="text" class="option-name" placeholder="Name" value="'+ decodeHTMLEntities(option['name']) +'">\
                </div>\
                <div class="col-md-2">\
                    <input type="text" class="option-price" placeholder="Price: eg. 20" value="'+ option['price'] +'">\
                </div>\
                <div class="col-md-2">\
                    <input type="text" class="option-tax" placeholder="Tax(%): eg. 7" value="'+ option['tax'] +'">\
                </div>\
                <div class="col-md-2">\
                    <input type="text" class="option-total" placeholder="Total: eg. 25" value="'+ option['total'] +'">\
                </div>\
                <div class="col-md-1">\
                    '+ actionIcon +'\
                </div>\
            </div>\
        ';

        actionIcon = 'allow icons after first loop';
    }

    optionsData += '\
        <div class="option-data">\
            <input type="hidden" class="option-id" value="" />\
            <div class="col-md-5">\
                <input type="text" class="option-name" placeholder="Name" value="">\
            </div>\
            <div class="col-md-2">\
                <input type="text" class="option-price" placeholder="Price: eg. 20" value="0">\
            </div>\
            <div class="col-md-2">\
                <input type="text" class="option-tax" placeholder="Tax(%): eg. 7" value="7">\
            </div>\
            <div class="col-md-2">\
                <input type="text" class="option-total" placeholder="Total: eg. 25" value="0">\
            </div>\
            <div class="col-md-1">\
                <a tabindex="-1" href="#delete-option" class="delete-option" data-service-id="" data-option-id=""><i class="fa fa-trash-o"></i></a>\
            </div>\
        </div>\
    ';

    $(".service-options-container").html(optionsData);

    $('.service-add-btn').hide();
    $('.service-save-btn').attr('data-service-id', serviceId);
    $('.service-save-btn').show();

    $("#service-modal").modal();

    return false;
}

function updateService(btnObject) {
    let serviceId = $(btnObject).attr('data-service-id');
    let parentContainer = $('div[data-id="'+ serviceId +'"]')
    let type = (parseInt($('.service-type:checked').val()) ? 1 : 0);
    let typeText = (type ? 'Required' : 'Optional');
    // let typeText = $('.service-type:checked').parent().text();
    let quantityRequired = $('.service-quantity').prop('checked') ? 1 : 0;
    let question = $('.service-question').val().trim();

    let optionIds = $('.service-options-container .option-id');
    let optionNames = $('.service-options-container .option-name');
    let optionsPrices = $('.service-options-container .option-price');
    let optionsTaxs = $('.service-options-container .option-tax');
    let optionsTotals = $('.service-options-container .option-total');
    let options = [];
    for(let i = 0; i < optionNames.length; i++) {
        let optionName = optionNames[i].value.trim();
        if (optionName) {
            options.push({
                id: optionIds[i].value,
                name: optionName,
                price: parseFloat(optionsPrices[i].value),
                tax: parseFloat(optionsTaxs[i].value),
                total: parseFloat(optionsTotals[i].value)
            });
        }
    }

    restaurantServices[serviceId] = {
        id: restaurantServices[serviceId]['id'],
        type: type,
        quantityRequired: quantityRequired,
        question: question,
        options: options
    };

    $('.service-question-text', parentContainer).text(question);
    $('.service-type-text', parentContainer).text(typeText);

    $("#service-modal").modal('hide');

    if (restaurantDeletedTempOptions.length) {
        for (var i = 0; i < restaurantDeletedTempOptions.length; i++) {
            restaurantDeletedOptions.push(restaurantDeletedTempOptions[i]);
        }
        
        $('#deletedOptions').val(restaurantDeletedOptions.join());
    }

    $('#restaurant_services').val(JSON.stringify(restaurantServices));

    return false;
}

function copyService(serviceId) {
    let service = restaurantServices[serviceId];
    // let id = Date.now();
    let type = (parseInt(service['type']) ? 1 : 0);
    let typeText = (type ? 'Required' : 'Optional');
    let quantityRequired = parseInt(service['quantityRequired']) ? 1 : 0;
    let question = service['question'];

    let options = [];
    for(key in service['options']) {
        options.push({
            id: '',
            name: service['options'][key]['name'],
            price: parseFloat(service['options'][key]['price']),
            tax: parseFloat(service['options'][key]['tax']),
            total: parseFloat(service['options'][key]['total'])
        });
    }

    restaurantServices.push({
        id: '',
        type: type,
        quantityRequired: quantityRequired,
        question: question,
        options: options
    });

    let newServiceId = restaurantServices.length - 1;

    let serviceData = '';
    serviceData = '\
        <div class="restaurant-service" data-id="'+ newServiceId +'">\
            <div class="headerBox">\
                <div class="row">\
                    <div class="col-md-11 col-sm-11">\
                        <h5 class="titleHeader">\
                            <span class="service-question-text">'+ question +'</span>\
                        </h5>\
                        <p class="card-text">\
                            <span class="service-type-text">'+ typeText +'</span>\
                        </p>\
                    </div>\
                    <div class="col-md-1 col-sm-1">\
                        <div class="service-edit-wrapper">\
                            <a href="#edit" class="service-edit" onclick="editService(this, '+ newServiceId +'); return false;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>\
                            <a href="#copy" class="service-copy" onclick="copyService('+ newServiceId +'); return false;" ><i class="fa fa-copy"></i></a>\
                            <a href="#delete" class="service-delete" onclick="deleteService(this); return false;"><i class="fa fa-trash-o"></i></a>\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    ';

    $('.restaurant-services-container').append(serviceData);

    $('#restaurant_services').val(JSON.stringify(restaurantServices));

    return false;
}

function deleteService(elementObj, serviceId = 0) {
    if (serviceId) {
        restaurantDeletedServices.push(restaurantServices[serviceId]['id']);
        $('#deletedServices').val(restaurantDeletedServices.join());

        for(key in restaurantServices[serviceId]['options']) {
            let option = restaurantServices[serviceId]['options'][key];
            if (option['id']) {
                restaurantDeletedOptions.push(option['id']);
            }
        }
        
        $('#deletedOptions').val(restaurantDeletedOptions.join());
    }

    delete restaurantServices[serviceId];

    $('#restaurant_services').val(JSON.stringify(restaurantServices));

    $(elementObj).parents('.restaurant-service').remove();

    return false;
}



$(document).ready(function(){

    if ($(window).width() < 680) {
        $('.list-page .filter-head.new_layout span.list-mob-filters').click(function(){
            $(this).parent().siblings().children('span.list-mob-filters').removeClass('active');
            if($('.list-page .filter-head.new_layout span.list-mob-filters').hasClass('active') == false){
                
                $(this).parent().siblings().children().children('.mob-filter-input').slideUp();
                $(this).children('.mob-filter-input').slideToggle();
                // $(this).parent().siblings().children().children('.mob-filter-input').removeClass('active');
                $(this).addClass('active');
            }
            //else{
               // $(this).children('.mob-filter-input').slideUp();
                //$(this).removeClass('active');
            //}
        });

        $('.list-header-submit-btn').click(function(){
            $('.mob-filter-input').slideUp();
        });


    }
});