<?php
/*
	Plugin Name: WYZ Restaurant
	Introduction-to-WordPress-Plugins---Location-Plugin
	Description: This plugin is used to manage Restaurant and its activities / functionalities like Restaurant, Menu, Dishes and Checkout dishes.
	Version: 1.0.1
	Author: Vaibhav
	URL: http://www.wyzchef.com
	Author URI: http://www.wyzchef.com
*/
if ( ! defined( 'ABSPATH' ) ) exit;
if ( !function_exists( 'add_action' ) ) {
	echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
	exit;
}
date_default_timezone_set("Asia/Singapore");

define('WYZ_RESTAURANT_PLUGIN_FILE', __FILE__);
define('WYZ_PLUGIN_ROOT_PATH', plugin_dir_path(__FILE__));
define('WYZ_PLUGIN_ROOT_URL', plugin_dir_url(__FILE__));

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(0);

global $wpdb;
$wpdb->query('SET SQL_MODE=""');
require( WYZ_PLUGIN_ROOT_PATH . 'admin/install.php' );
require( WYZ_PLUGIN_ROOT_PATH . 'admin/menu.php' );
require( WYZ_PLUGIN_ROOT_PATH . 'admin/uninstall.php' );
require_once('stripe-php/init.php');
/* Existing Implementation */
include(WYZ_PLUGIN_ROOT_PATH . 'wp_wyz_dish.php');
include(WYZ_PLUGIN_ROOT_PATH . 'wp_restaurant_listing.php');
// include(WYZ_PLUGIN_ROOT_PATH . 'wp_restaurant_listing_new.php');

// include(WYZ_PLUGIN_ROOT_PATH . 'wp_multi_image_uploader.php');
include(WYZ_PLUGIN_ROOT_PATH . 'class-wyz-restaurant-dashboard.php');
/*
* Reviews Manager
*/
include(WYZ_PLUGIN_ROOT_PATH . 'frontend/classes/class-reviews-manager.php');
 //$reviews = new Wyz_Chef_Reviews();

add_filter( 'body_class', function( $classes ) {
    return array_merge( $classes, array( 'wyzchef-restaurant-main' ) );
} );

static $callbackCouter = 0;
Class wyz_chef extends wyz_chef_dish {
	public function __construct() {
		// Add action to register menu tax
		add_action('init', array($this,'add_menu_taxonomy'));
		add_action( 'admin_menu', array( $this,'wyz_chef_orders' ) );
		
		// Image field actions
	    add_action( "restaurant_menu_add_form_fields", array( $this, 'addMenuImage' ), 10, 2);
	    add_action( "restaurant_menu_edit_form_fields", array( $this, 'editMenuImage' ), 10, 2);
	    add_action( "created_restaurant_menu", array( $this, 'saveMenuImage' ), 10, 2);
	    add_action( "edited_restaurant_menu", array( $this, 'saveMenuImage' ), 10, 2);
        // Add field column action/filter
        add_filter( "manage_edit-restaurant_menu_columns", array( $this, 'relatedMenuColumnHeader' ), 10);
        add_action( "manage_restaurant_menu_custom_column", array( $this, 'relatedMenuColumnContent' ), 10, 3);
	    // Load js for media scripts
	    add_action( 'admin_enqueue_scripts', array( $this, 'loadMenuMedia' ) );
	    add_action( 'admin_footer', array( $this, 'loadMenuJS' ) );
		// Add action to register menu tax
		add_action('init', array($this,'add_dish_tags'));
		
		// Image field actions
	    add_action( "restaurant_category_add_form_fields", array( $this, 'addCategoryImage' ), 10, 2);
	    add_action( "restaurant_category_edit_form_fields", array( $this, 'editCategoryImage' ), 10, 2);
	    add_action( "created_restaurant_category", array( $this, 'saveCategoryImage' ), 10, 2);
	    add_action( "edited_restaurant_category", array( $this, 'saveCategoryImage' ), 10, 2);

        // Add field column action/filter
        add_filter( "manage_edit-restaurant_category_columns", array( $this, 'relatedCategoryColumnHeader' ), 10);
        add_action( "manage_restaurant_category_custom_column", array( $this, 'relatedCategoryColumnContent' ), 10, 3);

	    // Load js for media scripts
	    add_action( 'admin_enqueue_scripts', array( $this, 'loadCategoryMedia' ) );
	    add_action( 'admin_footer', array( $this, 'loadCategoryJS' ) );

		add_action('init', array($this,'add_custom_taxonomies'));
		add_action('init', array($this,'add_resturant_taxonomies'));
		add_action('init', array($this,'register_location_shortcodes')); //shortcodes
		add_action('init', array($this,'register_restaurant_content_type'));
		add_action('init', array($this,'load_css_js'));
		//add_action('init', array($this,'admin_style'));
		add_action('init', array($this,'add_cart_page'));
		add_action('add_meta_boxes', array($this,'add_location_meta_boxes'));
		add_action('save_post', array($this,'save_location')); 
        //add bank information
        add_action('add_meta_boxes', array($this,'add_bank_inforamtion_meta_boxes'));
        add_action('save_post', array($this,'save_bank_information'), 10, 1);

       //Detail Image Meta box
        add_action('add_meta_boxes', array($this,'detail_image_meta_boxes'));
        //add_action('save_post', array($this,'save_detail_image'));

        //Cover Image Meta box
         add_action('add_meta_boxes', array($this,'cover_image_meta_boxes'));
        //  add_action('save_post', array($this,'save_cover_image'), 10, 1);
       
        //add retaurant type meta box information
        add_action('add_meta_boxes', array($this,'add_restaurant_types_meta_boxes'));
        add_action('save_post', array($this,'save_restaurant_types_information'), 10, 1);

        //add retaurant Categories meta box information
        add_action('add_meta_boxes', array($this,'add_restaurant_categories_meta_boxes'));
        add_action('save_post', array($this,'save_restaurant_categories_information'), 10, 1);

        //add retaurant Order Information
        add_action('add_meta_boxes', array($this,'add_restaurant_order_meta_boxes'));
        add_action('save_post', array($this,'save_restaurant_order_information'), 10, 1);

        //add retaurant author Information
        add_action('add_meta_boxes', array($this,'add_restaurant_user_meta_boxes'));
        add_action('save_post', array($this,'save_restaurant_user_information'), 10, 1);

        // for admin restaurant css
        add_action('admin_enqueue_scripts', array($this,'restaurant_style'));

		add_action( 'show_user_profile', array($this,'crf_show_extra_profile_fields'));	
		add_action( 'edit_user_profile', array($this,'crf_show_extra_profile_fields') );
		add_action( 'user_profile_update_errors', array($this,'crf_user_profile_update_errors'), 10, 3 );
		add_action( 'personal_options_update',  array($this,'crf_update_profile_fields') );
		add_action( 'edit_user_profile_update', array($this,'crf_update_profile_fields') );

		add_action( 'admin_post_save_corporate_profile', array($this, 'save_corporate_profile') );
		// add_action( 'admin_post_nopriv_save_corporate_profile', array($this, 'save_corporate_profile') );
		add_action( 'admin_post_save_corporate_company', array($this, 'save_corporate_company') );
		add_action( 'admin_post_send_corporate_refer', array($this, 'send_corporate_refer') );
		add_action( 'admin_post_send_corporate_suggestion', array($this, 'send_corporate_suggestion') );
		
        // Add coupons menu
        add_action( 'admin_menu', array( $this,'wyzchef_coupons_menu' ) );
        add_action( 'admin_menu', array( $this,'wyzchef_add_coupon_menu' ) );
        add_action( 'admin_post_wyzchef_save_coupon', array($this, 'wyzchef_save_coupon') );
        add_action( 'admin_post_wyzchef_delete_coupon', array($this, 'wyzchef_delete_coupon') );

        add_action( 'admin_post_saveRestaurantData', array($this, 'saveRestaurantData') );

        // Add Retaurant Order Commission MetaBox
        if ( is_admin() ) {
            add_action( 'load-post.php',     array( $this, 'initRestaurantMetaboxes' ) );
            add_action( 'load-post-new.php', array( $this, 'initRestaurantMetaboxes' ) );
        }
	}

	/**
     * Meta box initialization.
     */
    public function initRestaurantMetaboxes() {
        // Custom Data Field For Commission
        add_action( 'add_meta_boxes', array( $this, 'addRestaurantCommissionMetabox'  )        );
        add_action( 'save_post',      array( $this, 'saveRestaurantCommissionMetabox' ), 10, 2 );

        // Custom Data Field For Services
        add_action( 'add_meta_boxes', array( $this, 'addRestaurantServicesMetabox'  )        );
        add_action( 'save_post',      array( $this, 'saveRestaurantServicesMetabox' ), 10, 2 );

        // Custom Data Field For Services
        add_action( 'add_meta_boxes', array( $this, 'addRestaurantOccasionsMetabox'  )        );
        add_action( 'save_post',      array( $this, 'saveRestaurantOccasionsMetabox' ), 10, 2 );
    }

    /**
     * Adds the meta box.
     */
    public function addRestaurantCommissionMetabox() {
        add_meta_box(
            'wyz-commission-metabox',
            __( 'Restaurant Commission', 'wyzchef' ),
            array( $this, 'renderRestaurantCommissionMetabox' ),
            'wp_restaurant',
            'side',
            'default'
        );
 
    }
 
    /**
     * Renders the meta box.
     */
    public function renderRestaurantCommissionMetabox( $post ) {
        // Add nonce for security and authentication.
        wp_nonce_field( 'wyzchef_restaurant_commission', 'wyzchef_restaurant_commission_nonce' );

        $commission = get_post_meta($post->ID,'wp_restaurant_commission', true);
		?>
		<fieldset>
			<input type="number" step="0.01" name="wp_restaurant_commission" style="width:80%" value="<?php echo $commission;?>">
		</fieldset>
		<?php
    }
 
    /**
     * Handles saving the meta box.
     *
     * @param int     $post_id Post ID.
     * @param WP_Post $post    Post object.
     * @return null
     */
    public function saveRestaurantCommissionMetabox( $post_id ) {
    	// Check if our nonce is set.
        if ( ! isset( $_POST['wyzchef_restaurant_commission_nonce'] ) ) {
            return $post_id;
        }
 
        // Add nonce for security and authentication.
        $nonce   = $_POST['wyzchef_restaurant_commission_nonce'];
 
        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce, 'wyzchef_restaurant_commission' ) ) {
            return $post_id;
        }

        /*
         * If this is an autosave, our form has not been submitted,
         * so we don't want to do anything.
         */
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }
 
        // Check if not a revision.
        if ( wp_is_post_revision( $post_id ) ) {
            return $post_id;
        }
 
        // Check the user's permissions.
        if ( 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }

        // Sanitize the user input.
        $mydata = sanitize_text_field( $_POST['wp_restaurant_commission'] );
 
        // Update the meta field.
        update_post_meta( $post_id, 'wp_restaurant_commission', $mydata );
    }

    /**
     * Adds the service meta box.
     */
    public function addRestaurantServicesMetabox() {
        add_meta_box(
            'wyz-services-metabox',
            __( 'Restaurant Services', 'wyzchef' ),
            array( $this, 'renderRestaurantServicesMetabox' ),
            'wp_restaurant',
            'side',
            'default'
        );
    }
 
    /**
     * Renders the service meta box.
     */
    public function renderRestaurantServicesMetabox( $post ) {
        // Add nonce for security and authentication.
        wp_nonce_field( 'wyzchef_restaurant_services', 'wyzchef_restaurant_services_nonce' );

        $checkedServices = get_post_meta($post->ID, 'wp_restaurant_service', true);
        $restaurantServices = get_terms( array(
		    'taxonomy' => 'restaurant_service',
		    'hide_empty' => false,
		) );
        ?>

        <fieldset>
	        <div class="categorydiv">
	        	<ul><?php
		   		foreach( $restaurantServices as $service ){
		   			$checked = "";
			   		if ( !empty($checkedServices) && in_array( $service->term_id, $checkedServices ) ) {
			   			$checked = "checked='checked'";
			   		}

			   		echo "<li><label><input type='checkbox' name='wp_restaurant_service[]' value='{$service->term_id}' {$checked}  /> {$service->name}</label></li>";
			   	}
			    ?></ul>
			</div>
		</fieldset>

		<?php
    }
 
    /**
     * Handles saving the meta box.
     *
     * @param int     $post_id Post ID.
     * @param WP_Post $post    Post object.
     * @return null
     */
    public function saveRestaurantServicesMetabox( $post_id ) {
    	// Check if our nonce is set.
        if ( ! isset( $_POST['wyzchef_restaurant_services_nonce'] ) ) {
            return $post_id;
        }
 
        // Add nonce for security and authentication.
        $nonce   = $_POST['wyzchef_restaurant_services_nonce'];
 
        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce, 'wyzchef_restaurant_services' ) ) {
            return $post_id;
        }

        /*
         * If this is an autosave, our form has not been submitted,
         * so we don't want to do anything.
         */
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }
 
        // Check if not a revision.
        if ( wp_is_post_revision( $post_id ) ) {
            return $post_id;
        }
 
        // Check the user's permissions.
        if ( 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }

        // Get selected services
        $services = $_POST['wp_restaurant_service'];
 
        // Update the service data
        update_post_meta( $post_id, 'wp_restaurant_service', $services );
    }

    /**
     * Adds the occasion meta box.
     */
    public function addRestaurantOccasionsMetabox() {
        add_meta_box(
            'wyz-occasions-metabox',
            __( 'Restaurant Occasions', 'wyzchef' ),
            array( $this, 'renderRestaurantOccasionsMetabox' ),
            'wp_restaurant',
            'side',
            'default'
        );
    }
 
    /**
     * Renders the occasion meta box.
     */
    public function renderRestaurantOccasionsMetabox( $post ) {
        // Add nonce for security and authentication.
        wp_nonce_field( 'wyzchef_restaurant_occasions', 'wyzchef_restaurant_occasions_nonce' );

        $checkedOccasions = get_post_meta($post->ID, 'wp_restaurant_occasion', true);
        $restaurantOccasions = get_terms( array(
		    'taxonomy' => 'restaurant_occasion',
		    'hide_empty' => false,
		) );
        ?>

        <fieldset>
	        <div class="categorydiv">
	        	<ul><?php
		   		foreach( $restaurantOccasions as $occasion ){
		   			$checked = "";
			   		if ( !empty($checkedOccasions) && in_array( $occasion->term_id, $checkedOccasions ) ) {
			   			$checked = "checked='checked'";
			   		}

			   		echo "<li><label><input type='checkbox' name='wp_restaurant_occasion[]' value='{$occasion->term_id}' {$checked}  /> {$occasion->name}</label></li>";
			   	}
			    ?></ul>
			</div>
		</fieldset>

		<?php
    }
 
    /**
     * Handles saving the meta box.
     *
     * @param int     $post_id Post ID.
     * @param WP_Post $post    Post object.
     * @return null
     */
    public function saveRestaurantOccasionsMetabox( $post_id ) {
    	// Check if our nonce is set.
        if ( ! isset( $_POST['wyzchef_restaurant_occasions_nonce'] ) ) {
            return $post_id;
        }
 
        // Add nonce for security and authentication.
        $nonce   = $_POST['wyzchef_restaurant_occasions_nonce'];
 
        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce, 'wyzchef_restaurant_occasions' ) ) {
            return $post_id;
        }

        /*
         * If this is an autosave, our form has not been submitted,
         * so we don't want to do anything.
         */
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }
 
        // Check if not a revision.
        if ( wp_is_post_revision( $post_id ) ) {
            return $post_id;
        }
 
        // Check the user's permissions.
        if ( 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }

        // Get selected occasions
        $occasions = $_POST['wp_restaurant_occasion'];
 
        // Update the occasion data
        update_post_meta( $post_id, 'wp_restaurant_occasion', $services );
    }
	 
	//location shortcode
	public function register_location_shortcodes() {
		add_shortcode('wp_restaurant_dashboard', array($this,'dashboard_shortcode_output'));
		add_shortcode('wp_restaurant_details', array($this,'restaurantdetails_shortcode_output'));
		add_shortcode('wp_submit_restaurant', array($this,'restaurant_shortcode_output'));
		add_shortcode('wp_resetpassword', array($this,'resetPassword'));
		add_shortcode('wp_corporate_dashboard', array($this,'corporate_dashboard_shortcode_output'));
		
		$list = new Restaurant_Listing;
		add_shortcode("wp_restaurant_list", array($list, 'lsiting' ));
		// $list = new Restaurant_Listing_New;
		// add_shortcode("wp_restaurant_list_New", array($list, 'lsiting' ));
	}

	public function add_cart_page() {
		if (!get_page_by_title('Checkout', OBJECT, 'page')) {
			$page_id = wp_insert_post(
		        array(
		        'comment_status' => 'close',
		        'ping_status'    => 'close',
		        'post_author'    => 1,
		        'post_title'     => ucwords('Checkout'),
		        'post_name'      => strtolower(str_replace(' ', '-', trim('Checkout'))),
		        'post_status'    => 'publish',
		        'post_content'   => '',
		        'post_type'      => 'page',
		        'post_parent'    => ''
		        )
		    );
		}
	}
   
	
	
	public function load_css_js() {
        wp_enqueue_style('font-awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
        wp_enqueue_style('google-font-montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,600,800&display=swap');
        wp_enqueue_style('jQuery-ui-css', WYZ_PLUGIN_ROOT_URL .'assets/css/jquery-ui.css');
		wp_enqueue_style('bootstrap', WYZ_PLUGIN_ROOT_URL .'assets/css/bootstrap.min.css');
		wp_enqueue_style('timepicker-ui', WYZ_PLUGIN_ROOT_URL .'assets/css/timepicker-ui.css');
		wp_enqueue_style('addtocalendar', WYZ_PLUGIN_ROOT_URL .'assets/css/addtocalendar.css');
		wp_enqueue_style('datetimepicker-css', WYZ_PLUGIN_ROOT_URL . 'assets/css/bootstrap-datetimepicker.min.css');

        
        wp_enqueue_style('custom', WYZ_PLUGIN_ROOT_URL .'assets/css/custom.css', ['jQuery-ui-css', 'bootstrap']);
        wp_enqueue_style('restaurant-layout-css', WYZ_PLUGIN_ROOT_URL .'assets/css/restaurant-layout.css');
        // wp_enqueue_style('responsive', WYZ_PLUGIN_ROOT_URL .'assets/css/responsive.css', ['custom']);      

        wp_enqueue_script('jquery', WYZ_PLUGIN_ROOT_URL .'assets/js/jquery.js');
        wp_enqueue_script('jquery_ui', WYZ_PLUGIN_ROOT_URL .'assets/js/jquery-ui.js');
        wp_enqueue_script( 'bootstrap-js', WYZ_PLUGIN_ROOT_URL .'assets/js/bootstrap.min.js', array('jquery'), '3.4.0', true );
		wp_enqueue_script('timepicker', WYZ_PLUGIN_ROOT_URL .'assets/js/timepicker.js');
		wp_enqueue_script('datetimepicker-js',  WYZ_PLUGIN_ROOT_URL . 'assets/js/bootstrap-datetimepicker.js');


        // wp_enqueue_script('ajax_jquery', WYZ_PLUGIN_ROOT_URL .'assets/js/ajax-js.js');
        
        wp_enqueue_script('customjs', WYZ_PLUGIN_ROOT_URL .'assets/js/custom.js'); 
    }

	/*public function admin_style() {
        wp_enqueue_style('custom', WYZ_PLUGIN_ROOT_URL .'assets/css/custom.css');
	}*/

	public function restaurant_style() {
        wp_enqueue_style('restaurant', WYZ_PLUGIN_ROOT_URL .'assets/css/restaurant.css');
	}

	public function resetPassword() {
		global $wpdb;
		if(isset($_POST['user_pass']) && isset($_POST['user_login'])) {
			 require_once (ABSPATH . 'wp-includes/class-phpass.php'); 
			$password = $_POST['user_pass'];
			$user_id = $_POST['id'];
			$username = $_POST['user_login'];
			$user_id = wp_update_user( array( 'ID' => $user_id, 'user_pass' => $password,'user_nicename'=>$username ) );
			$query ='UPDATE `wp_users` SET `user_login` = "'.$username.'" WHERE `wp_users`.`ID` = '.$user_id;
			$wpdb->query($query);
		}
    
		$html= '<form action="" method="POST" class="restaurant-listings-form" enctype="multipart/form-data">
				<input type="text" name ="id">
				<fieldset>
					<label>User Name</label>
					<div class="field account-sign-in">
						<input type="text" name="user_login">			
					</div>
				</fieldset>
				<fieldset>
					<label>User Password</label>
					<div class="field account-sign-in">
						<input type="text" name="user_pass">			
					</div>
				</fieldset>
				<input type="submit">
			</from>
		';
		return $html;
	} 
           /*
 * Meta Box Removal
 */
 public function rudr_post_tags_meta_box_remove() {
	$id = 'taxonomy-restaurant_type'; // you can find it in a page source code (Ctrl+U)
	$post_type = 'wp_restaurant'; // remove only from post edit screen
	$position = 'side';
	remove_meta_box( $id, $post_type, $position );
}

public function pending_order_notification($restaurant_id) {
	global $wpdb;
	if (empty($restaurant_id) )   $restaurant_id = '-10'; 
	$orderTable = $wpdb->prefix . 'wyz_restaurant_order';
	$query = "SELECT Count(*) FROM $orderTable WHERE `status` = '0' AND `restaurant_id` = '".$restaurant_id."'";
	$OrderDetailsTable = $wpdb->get_results($query, ARRAY_A); 
	
	if($OrderDetailsTable[0]['Count(*)'] > 0){
		return $OrderDetailsTable[0]['Count(*)'];
	

	}else{ ?>
		<style>
			span.order-notification{
				display:none;

			}
		</style>
	<?php }
}

public function delivery_address($orderid) {
	global $wpdb;
	$deliveryTable = $wpdb->prefix . 'wyz_restaurant_order_delivery_details';
	$address = $wpdb->get_row("SELECT * FROM $deliveryTable WHERE order_id = $orderid",ARRAY_A);
	return $address['street']."<br>"."<span class='hash-unitnumber1'>#</span>".
	str_replace(",","-",$address['floor'])."".$address['city']."".$address['postal_code']."<br>";
}

public function delivery_date($orderid) {
	global $wpdb;
	$deliveryTable = $wpdb->prefix . 'wyz_restaurant_order_delivery_details';
	$address = $wpdb->get_row("SELECT * FROM $deliveryTable WHERE order_id = $orderid",ARRAY_A);
	return 'Delivery Date: '.date('F d, Y h:i A', strtotime($address['delivery_date']))."<br>";
}
public function get_delivery_email($orderid) {
	global $wpdb;
	$deliveryTable = $wpdb->prefix . 'wyz_restaurant_order_delivery_details';
	$delivery = $wpdb->get_row("SELECT * FROM $deliveryTable WHERE order_id = $orderid",ARRAY_A);
	return $delivery['email'];
}

public function delivery_instructions($orderid) {
	global $wpdb;
	$deliveryTable = $wpdb->prefix . 'wyz_restaurant_order_delivery_details';
	$instructions = $wpdb->get_row("SELECT `instructions` FROM $deliveryTable WHERE order_id = $orderid",ARRAY_A);
	return $instructions['instructions'];

}

public function delivery_pax($orderid) {
	global $wpdb;
	$deliveryTable = $wpdb->prefix . 'wyz_restaurant_order_delivery_details';
	$no_of_pax = $wpdb->get_row("SELECT `people` FROM $deliveryTable WHERE order_id = $orderid",ARRAY_A);
	return $no_of_pax['people'];
}

public function get_delivery_fees($order_id){
	global $wpdb;
	$ordertable = $wpdb->prefix . 'wyz_restaurant_order';
	$sql = "SELECT `delivery_fees` FROM $ordertable WHERE id = {$order_id}";
	$delivery_fees = $wpdb->get_row($sql, ARRAY_A);
	return $delivery_fees['delivery_fees'];
}

public function get_tax($order_id){
    global $wpdb;
    $ordertable = $wpdb->prefix . 'wyz_restaurant_order';
    $sql = "SELECT `gst` FROM $ordertable WHERE id = {$order_id}";
    $tax = $wpdb->get_row($sql, ARRAY_A);
    return $tax['gst'];
}

public function dish_details($order) {
	$htmlmaster = '';
    $unixTimeStamp = strtotime($order['delivery_date']);
    $date = date('D, M d', $unixTimeStamp);
    $time = date('h:i A', $unixTimeStamp);

    global $wpdb;
    $orderDeliveryDetailsTable = "{$wpdb->prefix}wyz_restaurant_order_delivery_details";
    $orderDetailsTable = "{$wpdb->prefix}wyz_restaurant_order_details";
    $dishesTable = "{$wpdb->prefix}wyz_restaurant_dishes";
    $postTable = "{$wpdb->prefix}posts";
    $sql = "
        SELECT d.id, d.name, odd.company, odd.floor, odd.street, odd.city, odd.postal_code, odd.people, od.quantity, od.line_total, od.modifier, p.post_title FROM {$orderDeliveryDetailsTable} AS odd 
        INNER JOIN {$orderDetailsTable} AS od 
        ON od.order_id = odd.order_id 
        INNER JOIN {$dishesTable} AS d 
        ON od.dish_id = d.id 
        INNER JOIN {$postTable} AS p 
        ON d.restaurant_id = p.ID 
        WHERE odd.order_id = {$order[id]}
    ";
    $orderDetails = $wpdb->get_results($sql, ARRAY_A);
    
    $orderServicesTable = "{$wpdb->prefix}wyz_restaurant_order_services";
    $servicesTable = "{$wpdb->prefix}wyz_restaurant_services";
    $sql = "
        SELECT s.id, s.question, os.quantity, os.total_price, os.options FROM {$orderServicesTable} AS os 
        INNER JOIN {$servicesTable} AS s
        ON s.id = os.service_id 
        WHERE order_id = {$order[id]}
    ";
    $services = $wpdb->get_results($sql, ARRAY_A);
    
    if(count($orderDetails) > 0){
        $htmlmaster .= '
            <div class="order-detail-header">
                <span>Order # '. $order['id'] .'</span> <span>For delivery on '. $date .' at '. $time .'</span>
            </div>
        ';
        $htmlmaster .= '
            <div class="row three_Year_table">
                <table class="header_table">
                    <tbody>
                      <tr>
                        <td><h4>Order Details</h4></td>
                      </tr>
                      <tr>
                        <th>Vendor\'s Name</th>
                        <th>Date</th>
                      </tr>
                      <tr>
                        <td>'. $orderDetails[0]['post_title'] .'</td>
                        <td>'. $date .'</td>
                      </tr>
                      <tr>
                        <th>Headcount</th>
                        <th>Time</th>
                      </tr>
                      <tr>
                        <td>'. $orderDetails[0]['people'] .'</td>
                        <td>'. $time .'</td>
                      </tr>
                      <tr>
                        <th>Company</th>
                      </tr>
                      <tr>
                        <td>'. $orderDetails[0]['company'] .'</td>
                      </tr>
                      <tr>
                        <th>Address</th>
                      </tr>
                      <tr>
                        <td>'. $orderDetails[0]['floor'] .' '. $orderDetails[0]['street'] .' '. $orderDetails[0]['city'] .' '. $orderDetails[0]['postal_code'] .'</td>
                      </tr>
                    </tbody>
                </table>
            </div>
            <div class="help_box">
                <p>Call +65 8891 3599 if you’re lost / late so we can help!</p>
            </div>
            <div class="three_Year_table" style= "border-color:black; margin-top:100px;">
                <table class="header_table price_table">
                    <tbody>
                      <tr>
                        <th>Qty</th>
                        <th colspan="3">Item</th>
                        <th>Price</th>
                        <th>w/GST</th>
                      </tr>
                    ';

                    foreach ($orderDetails as $orderDetail) {
                        $htmlmaster .=  '<tr>';
                        $htmlmaster .= '<td>'.$orderDetail["quantity"].'</td>';

                        $htmlmaster .= '<td  colspan="3">'.$orderDetail['name'];
                        $modifiers = unserialize($orderDetail['modifier']);
                        if ($modifiers) {
                            $modifierNames = array();
                            foreach ($modifiers as $key => $modifier) {
                                $modifierNames[] = $modifier->itemText;
                            }
                            if (!empty($modifierNames)) {
                                $htmlmaster .= '<br/><sup>('. implode(', ', $modifierNames) .')</sup>';
                            }
                        }
                        $htmlmaster .= '</td>';

                        $htmlmaster .= '<td>SGD</td>';
                        
                        $htmlmaster .= '<td>'.round($orderDetail["line_total"], 2).'</td>';
                        $htmlmaster .=  '</tr>';
                    
                        $item_total += $orderDetail["line_total"];
                    }

                    if(count($services) > 0){
                        foreach ($services as $service) {
                            $htmlmaster .=  '<tr>';
                            $htmlmaster .= '<td>'.$service["quantity"].'</td>';

                            $htmlmaster .= '<td  colspan="3">'.$service['question'];
                            $options = unserialize($service['options']);
                            if ($options) {
                                $optionNames = array();
                                foreach ($options as $key => $option) {
                                    $optionNames[] = $option->name;
                                }
                                if (!empty($optionNames)) {
                                $htmlmaster .= '<br/><sup>('. implode(', ', $optionNames) .')</sup>';
                                }
                            }
                            $htmlmaster .= '</td>';

                            $htmlmaster .= '<td>SGD</td>';
                            
                            $htmlmaster .= '<td>'.round($service["total_price"], 2).'</td>';
                            $htmlmaster .=  '</tr>';
                
                            $item_total += $service["total_price"];
                        }
                    }

                    $htmlmaster .= '
                      <tr>
                        <td></td>
                        <td colspan="3">Tableware<br>Include: Cups</td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3">Sub Total</td>
                        <td>SGD</td>
                        <td>'. round($item_total, 2) .'</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3">Delivery Fee</td>
                        <td>SGD</td>
                        <td>'. $order['delivery_fees'] .'</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3">Surcharge</td>
                        <td>SGD</td>
                        <td>0.00</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3">GST (included in price)</td>
                        <td>SGD</td>
                        <td>'. $order['gst'] .'</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3">Tip</td>
                        <td>SGD</td>
                        <td>0.00</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3">Total</td>
                        <td>SGD</td>
                        <td>'. $order['total'] .'</td>
                      </tr>
                    </tbody>
                </table>
            </div>
        ';

    } else {
        $htmlmaster .=  "<div class='no-orders'>
            <h1>No Orders Found For This Restaurant</h1>
            <img src='". WYZ_PLUGIN_ROOT_URL."assets/images/Review-catering.svg'>
            <a  href='". home_url('caterers') ."'>Log In</a>
        </div>";
    }

	return $htmlmaster;  
}
	//shortcode to display restaurant dashboard
	public function dashboard_shortcode_output($atts, $content = '', $tag) {
		$current_user = wp_get_current_user();
		if (!isset($current_user->user_login) || empty($current_user->user_login)) {
			return '
				<div class="login-required">
					<h1>You need to login or register to access this page...</h1>
					<img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/notloggedimg.svg">
					<div class="login-buttons">
						<a href="">Register</a>
						<a href="">Log In</a>
					</div>
				</div>
			';
		}
	
	 	include(WYZ_PLUGIN_ROOT_PATH . 'frontend/wyz-restaurant-dashboard.php');
	}

	/**
	 * Save Corporate User Data
	 * @var $_POST
	 * 
	 * @return null
	 */
	public function save_corporate_profile() {
		if(session_id() == '')  {
			session_start();
		}
		$msgArray = array(
			'success' => array(
				'status' => false,
				'msg' => '',
			), 
			'error' => array(
				'status' => false,
				'msg' => '',
			), 
		);
		$current_user = wp_get_current_user();
		
		$current_user->user_email = $_POST['userData']['email'];
		wp_update_user($current_user);
		unset($_POST['userData']['email']);

		foreach ($_POST['userData'] as $metaKey => $metaData) {
			update_user_meta($current_user->ID, $metaKey, $metaData);
		}

		if ( ! function_exists( 'wp_handle_upload' ) ) {
		    require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}

		$uploadedfile = $_FILES['profile_pic'];
		$upload_overrides = array( 'test_form' => false );
		$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
		if ( $movefile && ! isset( $movefile['error'] ) ) {
			update_user_meta($current_user->ID, 'profile_pic_url', $movefile['url']);
			update_user_meta($current_user->ID, 'profile_pic_path', $movefile['file']);
		} else {
		    /**
		     * Error generated by _wp_handle_upload()
		     * @see _wp_handle_upload() in wp-admin/includes/file.php
		     */
		    $msgArray['error']['msg'] = $movefile['error'];
		    $msgArray['error']['status'] = true;
		}

		$msgArray['success']['msg'] = 'User data saved';
		$msgArray['success']['status'] = true;

		$_SESSION["msgArray"] = $msgArray;

	    wp_safe_redirect( wp_get_referer() );
	}

	/**
	 * Save Corporate Company Data
	 * @var $_POST
	 * 
	 * @return null
	 */
	public function save_corporate_company() {
		if(session_id() == '')  {
			session_start();
		}
		$msgArray = array(
			'success' => array(
				'status' => false,
				'msg' => '',
			), 
			'error' => array(
				'status' => false,
				'msg' => '',
			), 
		);
		$current_user = wp_get_current_user();

		foreach ($_POST['userData'] as $metaKey => $metaData) {
			update_user_meta($current_user->ID, $metaKey, $metaData);
		}

		$msgArray['success']['msg'] = 'Company data saved';
		$msgArray['success']['status'] = true;

		$_SESSION["msgArray"] = $msgArray;

	    wp_safe_redirect( wp_get_referer() );
	}

	/**
	 * Send Corporate Refer Mail
	 * @var $_POST
	 * 
	 * @return null
	 */
	public function send_corporate_refer() {
		if(session_id() == '')  {
			session_start();
		}
		$msgArray = array(
			'success' => array(
				'status' => false,
				'msg' => '',
			), 
			'error' => array(
				'status' => false,
				'msg' => '',
			), 
		);
		$current_user = wp_get_current_user();

		$to[] = $_POST['colleague_email'];
		$to[] = $current_user->user_email;
		$subject = 'Refer Mail';
		$body = "Hey : {$_POST[colleague_name]} <br /><br />";
		$body .= $_POST['refer_message'];
		// $headers = array('Content-Type: text/html; charset=UTF-8');
		$headers = array(
	        'Content-Type: text/html; charset=UTF-8',
	        'From: WYZchef <contact@wyzchef.com>',
	    );
		 
		wp_mail( $to, $subject, $body, $headers );

		$msgArray['success']['msg'] = 'Refer mail sent';
		$msgArray['success']['status'] = true;

		$_SESSION["msgArray"] = $msgArray;

	    wp_safe_redirect( wp_get_referer() );
	}

	/**
	 * Send suggestions to admin on mail
	 * @var $_POST
	 * 
	 * @return null
	 */
	public function send_corporate_suggestion() {
		if(session_id() == '')  {
			session_start();
		}
		$msgArray = array(
			'success' => array(
				'status' => false,
				'msg' => '',
			), 
			'error' => array(
				'status' => false,
				'msg' => '',
			), 
		);
		$current_user = wp_get_current_user();

		$to[] = get_option( 'admin_email' );
		$to[] = $current_user->user_email;
		$subject = 'Coporate Suggestion Mail';
		$body = "Restaurant Name : {$_POST[restaurant_name]} <br />";
		$body .= "Restaurant Website : {$_POST[website]} <br />";
		$body .= $_POST['comment_msg'];
		// $headers = array('Content-Type: text/html; charset=UTF-8');
		$headers = array(
	        'Content-Type: text/html; charset=UTF-8',
	        'From: WYZchef <contact@wyzchef.com>',
	    );
		 
		wp_mail( $to, $subject, $body, $headers );

		$msgArray['success']['msg'] = 'Suggestion mail sent';
		$msgArray['success']['status'] = true;

		$_SESSION["msgArray"] = $msgArray;

	    wp_safe_redirect( wp_get_referer() );
	}

	//shortcode to display corporate dashboard
	public function corporate_dashboard_shortcode_output($atts, $content = '', $tag) {
        $current_user = wp_get_current_user();
		if (!isset($current_user->user_login) || empty($current_user->user_login)) {
			return '
				<div class="login-required">
					<h1>You need to login or register to access this page...</h1>
					<img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/notloggedimg.svg">
					<div class="login-buttons">
						<a  href="'. home_url('login?action=register') .'">Register</a>
						<a  href="'. home_url('login') .'">Log In</a>
					</div>
				</div>
			';
		}
	
	 	include(WYZ_PLUGIN_ROOT_PATH . 'corporate/wyz-corporate-dashboard.php');
	}
	//shortcode to display restaurant details
	// public function restaurantdetails_shortcode_output($atts, $content = '', $tag) {
	
	//  	include(WYZ_PLUGIN_ROOT_PATH . 'frontend/wyz-restaurant-details.php');
	// }

	// shortcode to display optimize restaurant submit page
	public function restaurant_shortcode_output($atts, $content = '', $tag) {
		$current_user = wp_get_current_user();
		if (
        	!isset($current_user->user_login) || 
        	!(
        		in_array('administrator', $current_user->roles) || 
        		in_array('editor', $current_user->roles)
        	)
        ) {
			return '
				<div class="login-required">
                    <h1>You don\'t have privilege to access this page. <br />Please contact administrator.</h1>
				</div>
			';
		}

	  	ob_start();
	  	include(WYZ_PLUGIN_ROOT_PATH . 'frontend/wyz-submit-restaurant-optimize.php');
	  	return ob_get_clean();
	}

	public function register_restaurant_content_type() {
		 //Labels for post type
		 $labels = array(
            'name'               => 'Restaurant',
            'singular_name'      => 'Restaurant',
            'menu_name'          => 'WYZ Restaurants',
            'name_admin_bar'     => 'Restaurant',
            'add_new'            => 'Add New', 
            'add_new_item'       => 'Add New Restaurant',
            'new_item'           => 'New Location', 
            'edit_item'          => 'Edit Restaurant',
            'view_item'          => 'View Restaurant',
            'all_items'          => 'All Restaurants',
            'search_items'       => 'Search Restaurants',
            'parent_item_colon'  => 'Parent Location:', 
            'not_found'          => 'No Restaurant found.', 
            'not_found_in_trash' => 'No Restaurant found in Trash.',
        );
        //arguments for post type
        $args = array(
            'labels'            => $labels,
            'public'            => true,
            'publicly_queryable'=> true,
            'show_ui'           => true,
            'show_in_nav'       => true,
            'query_var'         => true,
            'hierarchical'      => false,
            'has_archive'       => true,
            'menu_position'     => 20,
            'show_in_admin_bar' => true,
            'menu_icon'         => WYZ_PLUGIN_ROOT_URL . 'assets/images/restaurant.svg',
            'rewrite'			=> array(
            	'slug' 			=> 'restaurant', 
            	'with_front' 	=> 'true', 
            	'comments' 		=> true,
            ),
            'supports' 			=> array( 'title', 'editor', 'comments','thumbnail' ),
            'taxonomies' 		=> array( 'restaurant_type', 'restaurant_category', 'restaurant_service', 'restaurant_occasion' ),
        );
 		register_post_type('wp_restaurant', $args);
	}

	function add_resturant_taxonomies() {
	  	register_taxonomy(
		  	'restaurant_category',
		  	'restaurant_categories',
		  	array(
			    'hierarchical' => true,
			    // This array of options controls the labels displayed in the WordPress Admin UI
			    'labels' => array(
			      	'name' => _x( 'Restaurant Category', 'taxonomy general name' ),
			      	'singular_name' => _x( 'Restaurant Category', 'taxonomy singular name' ),
			      	'search_items' =>  __( 'Search Restaurant Category' ),
			      	'all_items' => __( 'All Restaurant Category' ),
			      	'parent_item' => __( 'Parent Restaurant Category' ),
			      	'parent_item_colon' => __( 'Parent Restaurant Category:' ),
			      	'edit_item' => __( 'Edit Restaurant Category' ),
			      	'update_item' => __( 'Update Restaurant Category' ),
			      	'add_new_item' => __( 'Add New Restaurant Category' ),
			      	'new_item_name' => __( 'New Restaurant type Name' ),
			      	'menu_name' => __( 'Restaurant Category' ),
			      	'show_admin_column' => false,
			      	'show_ui' => true,
			    ),
			    // Control the slugs used for this taxonomy
			    'rewrite' => array(
			      	'slug' => 'restaurant_category', // This controls the base slug that will display before each term
			      	'with_front' => true, // Don't display the category base before "/locations/"
			      	'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
		    	),
	  		)
	  	);

	  	// Add Services
	  	$labels = array(
			'name'                       => _x( 'Restaurant Services', 'Taxonomy General Name', 'wyzchef' ),
			'singular_name'              => _x( 'Restaurant Service', 'Taxonomy Singular Name', 'wyzchef' ),
			'menu_name'                  => __( 'Restaurant Services', 'wyzchef' ),
			'all_items'                  => __( 'All Restaurant Services', 'wyzchef' ),
			'new_item_name'              => __( 'New Restaurant Service', 'wyzchef' ),
			'add_new_item'               => __( 'Add New Service', 'wyzchef' ),
			'edit_item'                  => __( 'Edit Service', 'wyzchef' ),
			'update_item'                => __( 'Update Service', 'wyzchef' ),
			'view_item'                  => __( 'View Service', 'wyzchef' ),
			'add_or_remove_items'        => __( 'Add or remove services', 'wyzchef' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'wyzchef' ),
			'popular_items'              => __( 'Popular Services', 'wyzchef' ),
			'search_items'               => __( 'Search Services', 'wyzchef' ),
			'not_found'                  => __( 'Not Found', 'wyzchef' ),
			'no_terms'                   => __( 'No services', 'wyzchef' ),
			'items_list'                 => __( 'Services list', 'wyzchef' ),
			'items_list_navigation'      => __( 'Services list navigation', 'wyzchef' ),
		);
		
		$args = array(
			'labels'                     => $labels,
			'public'                     => true,
			'show_ui'                    => true,
			'show_in_nav_menus'          => true,
			'hierarchical'               => false,
			'show_admin_column'          => false,
		    'show_in_quick_edit'         => false,
		    'meta_box_cb'                => false,
		);
		register_taxonomy( 'restaurant_service', array( 'wp_restaurant' ), $args );

		// Add Occasions
		$labels = array(
			'name'                       => _x( 'Restaurant Occasions', 'Taxonomy General Name', 'wyzchef' ),
			'singular_name'              => _x( 'Restaurant Occasion', 'Taxonomy Singular Name', 'wyzchef' ),
			'menu_name'                  => __( 'Restaurant Occasions', 'wyzchef' ),
			'all_items'                  => __( 'All Restaurant Occasions', 'wyzchef' ),
			'new_item_name'              => __( 'New Restaurant Occasion', 'wyzchef' ),
			'add_new_item'               => __( 'Add New Occasion', 'wyzchef' ),
			'edit_item'                  => __( 'Edit Occasion', 'wyzchef' ),
			'update_item'                => __( 'Update Occasion', 'wyzchef' ),
			'view_item'                  => __( 'View Occasion', 'wyzchef' ),
			'add_or_remove_items'        => __( 'Add or remove occasions', 'wyzchef' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'wyzchef' ),
			'popular_items'              => __( 'Popular Occasions', 'wyzchef' ),
			'search_items'               => __( 'Search Occasions', 'wyzchef' ),
			'not_found'                  => __( 'Not Found', 'wyzchef' ),
			'no_terms'                   => __( 'No occasions', 'wyzchef' ),
			'items_list'                 => __( 'Occasions list', 'wyzchef' ),
			'items_list_navigation'      => __( 'Occasions list navigation', 'wyzchef' ),
		);
		
		$args = array(
			'labels'                     => $labels,
			'public'                     => true,
			'show_ui'                    => true,
			'show_in_nav_menus'          => true,
			'hierarchical'               => false,
			'show_admin_column'          => false,
		    'show_in_quick_edit'         => false,
		    'meta_box_cb'                => false,
		);
		register_taxonomy( 'restaurant_occasion', array( 'wp_restaurant' ), $args );
	}

	function add_custom_taxonomies() {
	   	register_taxonomy(
		   	'restaurant_type', 
		   	'restaurant_types', 
		   	array(
			    'hierarchical' => true,
			    // This array of options controls the labels displayed in the WordPress Admin UI
			    'labels' => array(
		      		'name' => _x( 'Restaurant Types', 'taxonomy general name' ),
		      		'singular_name' => _x( 'Restaurant Type', 'taxonomy singular name' ),
		      		'search_items' =>  __( 'Search Restaurant Types' ),
		      		'all_items' => __( 'All Restaurant Types' ),
		      		'parent_item' => __( 'Parent Restaurant Type' ),
		      		'parent_item_colon' => __( 'Parent Restaurant Type:' ),
		      		'edit_item' => __( 'Edit Restaurant Type' ),
		      		'update_item' => __( 'Update Restaurant Type' ),
		      		'add_new_item' => __( 'Add New Restaurant Type' ),
		      		'new_item_name' => __( 'New Restaurant Type Name' ),
		      		'menu_name' => __( 'Restaurant Types' ),
		      		'show_admin_column' => false,
		      		'show_ui' => true,
		    	),
			    // Control the slugs used for this taxonomy
			    'rewrite' => array(
		      		'slug' => 'restaurant_types', // This controls the base slug that will display before each term
		      		'with_front' => true, // Don't display the category base before "/locations/"
		      		'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
		    	),
	  		)
	   	);
	}
   


public function detail_image_meta_boxes(){
	add_meta_box(
        'wp_restaurant_detail_image_meta_boxes', //id
        'Detail Image', //name
        array($this,'restaurant_detail_image_meta_box_display'), //display function
        'wp_restaurant', //post type
        'normal', //location
        'default' //priority
       );
}

public function restaurant_detail_image_meta_box_display($post_id) {
	global $post;
	$image_src = '';
	$image_id = get_post_meta( $post->ID, 'wp_restaurant_list_img', true );
	$image_src = wp_get_attachment_url( $image_id );
	?>
	<div id="detail">          
		<img id="detail_image" src="<?php echo $image_src ?>" style="max-width:100%;" />
		<input type="hidden" name="wp_restaurant_list_img" id="upload_detail_image_id" value="<?php echo $image_id; ?>" />
		<p style="padding-top: 10px;">
			<a title="<?php esc_attr_e( 'Set detail image' ) ?>" href="#" id="set-detail-image"><?php _e( 'Set Detail image' ) ?></a> |
			<a title="<?php esc_attr_e( 'Remove detail image' ) ?>" href="#" id="remove-detail-image" style="<?php echo ( ! $image_id ? 'display:none;' : '' ); ?>"><?php _e( 'Remove detail image' ) ?></a>
		</p>
		<script type="text/javascript">
			jQuery(document).ready(function($) {

// save the send_to_editor handler function
window.send_to_editor_default = window.send_to_editor;

jQuery('#set-detail-image').click(function(){

// replace the default send_to_editor handler function with our own
window.send_to_editor = window.attach_image;
tb_show('', 'media-upload.php?post_id=<?php echo $post->ID ?>&amp;type=image&amp;TB_iframe=true');

return false;
});

jQuery('#remove-detail-image').click(function() {
	jQuery('#upload_detail_image_id').val('');
	jQuery('img').attr('src', '');
	jQuery(this).hide();

	return false;
});

// handler function which is invoked after the user selects an image from the gallery popup.
// this function displays the image and sets the id so it can be persisted to the post meta
window.attach_image = function(html) {

//alert(html);
// turn the returned image html into a hidden image element so we can easily pull the relevant attributes we need
jQuery('#detail').append('<div id="temp_image">' + html + '</div>');

var img = jQuery('#temp_image').find('img');


imgurl   = img.attr('src');
imgclass = img.attr('class');
imgid    = parseInt(imgclass.replace(/\D/g, ''), 10);

jQuery('#upload_detail_image_id').val(imgid);
jQuery('#remove-detail-image').show();

jQuery('img#detail_image').attr('src', imgurl);
try{tb_remove();}catch(e){};
jQuery('#temp_image').remove();

// restore the send_to_editor handler function
window.send_to_editor = window.send_to_editor_default;

}

});
</script>
</div>  
<?php }


public function cover_image_meta_boxes(){
	add_meta_box(
        'wp_restaurant_cover_image_meta_boxes', //id
        'Cover Image', //name
        array($this,'restaurant_cover_image_meta_box_display'), //display function
        'wp_restaurant', //post type
        'normal', //location
        'default' //priority
      );

}


public function restaurant_cover_image_meta_box_display($post_id) {
	global $post;
	$image_src = '';
	$image_id = get_post_meta( $post->ID, 'wp_restaurant_gallery', true);
	$image_src = wp_get_attachment_url( $image_id[0]);

	?>
	<div id="cover">
		<img id="cover_image" src="<?php echo $image_src ?>" style="max-width:100%;" />
		<input type="hidden" name="wp_restaurant_gallery" id="upload_cover_image_id" value="<?php echo $image_id[0]; ?>" />
		<p style="padding-top: 10px;">
			<a title="<?php esc_attr_e( 'Set Cover image' ) ?>" href="#" id="set-cover-image"><?php _e( 'Set Cover image' ) ?></a> |
			<a title="<?php esc_attr_e( 'Remove Cover image' ) ?>" href="#" id="remove-cover-image" style="<?php echo ( ! $image_id ? 'display:none;' : '' ); ?>"><?php _e( 'Remove cover image' ) ?></a>
		</p>
<script type="text/javascript">
jQuery(document).ready(function($) {

// save the send_to_editor handler function
window.send_to_editor_default = window.send_to_editor;

jQuery('#set-cover-image').click(function(){

// replace the default send_to_editor handler function with our own
window.send_to_editor = window.attach_image1;
tb_show('', 'media-upload.php?post_id=<?php echo $post->ID ?>&amp;type=image&amp;TB_iframe=true');

return false;
});

jQuery('#remove-cover-image').click(function() {

	jQuery('#upload_cover_image_id').val('');
	jQuery('img').attr('src', '');
	jQuery(this).hide();

	return false;
});

// handler function which is invoked after the user selects an image from the gallery popup.
// this function displays the image and sets the id so it can be persisted to the post meta
window.attach_image1 = function(html) {

// 	// 	// turn the returned image html into a hidden image element so we can easily pull the relevant attributes we need
jQuery('#cover').append('<div id="temp_image">' + html + '</div>');

var img = jQuery('#temp_image').find('img');

imgurl   = img.attr('src');
imgclass = img.attr('class');
imgid    = parseInt(imgclass.replace(/\D/g, ''), 10);

jQuery('#upload_cover_image_id').val(imgid);
jQuery('#remove-cover-image').show();

jQuery('img#cover_image').attr('src', imgurl);
try{tb_remove();}catch(e){};
jQuery('#temp_image').remove();

// restore the send_to_editor handler function
window.send_to_editor = window.send_to_editor_default;

}

});
</script>
</div>




<?php
}

   public function add_bank_inforamtion_meta_boxes(){
        add_meta_box(
			'wp_restaurant_bank_inforamtion_meta_box', //id
			'Bank Information', //name
			array($this,'restaurant_bank_information_meta_box_display'), //display function
			'wp_restaurant', //post type
			'normal', //location
			'default' //priority
		);



   }
   public function restaurant_bank_information_meta_box_display($post) {

    // collect bank information
   	$bank_name = get_post_meta($post->ID,'bank_name',true);
   	$bank_code = get_post_meta($post->ID,'bank_code',true);
   	$account_name = get_post_meta($post->ID,'account_name',true);
   	$account_number = get_post_meta($post->ID,'account_number',true);
   	$swift_code = get_post_meta($post->ID,'swift_code',true);
   	$branch_name = get_post_meta($post->ID,'branch_name',true);
   	$branch_code = get_post_meta($post->ID,'branch_code',true);
   	$preferred_mode = get_post_meta($post->ID,'preferred_mode',true);
   	?>

   	<div class="inside bankinformation" style="margin: 15px 0 0 0;">
   		<div class="row">
   			<div class="col-sm-6" >
   				<label>Bank Name</label>
   				<fieldset><input type="text" style="width:80%" name="bank_name" value="<?php echo $bank_name;?>"></fieldset>
   			</div>	
   			<div class="col-sm-6">
   				<label>Bank Code</label>
   				<fieldset><input type="text" name="bank_code" id="bank_code" value="<?php echo $bank_code;?>" style="width:80%"></fieldset>
   			</div>	
   			<div class="col-sm-6" >
   				<label>Account Name</label>
   				<fieldset><input type="text" style="width:80%" name="account_name" value="<?php echo $account_name;?>"></fieldset>
   			</div>	
   			<div class="col-sm-6" >
   				<label>Account Number</label>
   				<fieldset>
   					<input type="text" name="account_number" id="account_number" value="<?php echo $account_number;?>" style="width:80%">
   				</fieldset>
   			</div>
   			<div class="col-sm-6" >
   				<label>Swift Code</label>
   				<fieldset><input type="text" name="swift_code" style="width:80%" value="<?php echo $swift_code;?>"></fieldset>
   			</div>	
   			<div class="col-sm-6" >
   				<label>Branch Name</label>
   				<fieldset>
   					<input type="text" name="branch_name" id="branch_name" value="<?php echo $branch_name;?>" style="width:80%">
   				</fieldset>
   			</div>	
   			<div class="col-sm-6" >
   				<label>Branch Code</label>
   				<fieldset><input type="text" name="branch_code" id="branch_code" style="width:80%" value="<?php echo $branch_code;?>"></fieldset>
   			</div>	
   			<div class="col-sm-6" >
   				<label>Preferred Mode</label>
   				<fieldset>
   					<input type="text" name="preferred_mode" id="preferred_mode" value="<?php echo $preferred_mode;?>" style="width:80%">
   				</fieldset>
   			</div>	 
   		</div>	
   	</div>  	


   <?php  }

   public function save_bank_information($post_id){
   	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
   		return;
   	}

   	if ( wp_is_post_revision( $post_id ) ) {
   		return;
   	}

    //save bank information values
   	$bank_fields = array(
   		'post_id' => $post_id, 
   		'bank_name' => trim($_POST['bank_name']),
   		'bank_code' => trim($_POST['bank_code']), 
   		'account_name' => trim($_POST['account_name']), 
   		'account_number' => trim($_POST['account_number']),
   		'swift_code' => trim($_POST['swift_code']),
   		'branch_name' => trim($_POST['branch_name']),
   		'branch_code' => trim($_POST['branch_code']),
   		'preferred_mode' => trim($_POST['preferred_mode'])

   	);

   	foreach ( $bank_fields as $key=>$value ) {
   		if ( array_key_exists( $key, $bank_fields ) ) {
   			$success = update_post_meta( $post_id, $key, sanitize_text_field($value) );
   		}
   	}

   }
    
     // add restataurant type meta box
   public function add_restaurant_types_meta_boxes() {
   	add_meta_box(
          'wp_restaurant_types_meta_box', //id
          'Restaurant Types', //name
          array($this,'restaurant_types_meta_box_display'), //display function
          'wp_restaurant', //post type
          'side', //location
          'default' //priority
         );

   }

public function restaurant_types_meta_box_display($post) {
   	global $post;
    // get all the restaurant types 
   	$all_restaurant_types = get_terms( array('taxonomy' => 'restaurant_type', 'hide_empty' => 0) ); 

    // get all restaurant types assigned to a post
   	$ids = get_post_meta($post->ID,'wp_restaurant_type', true); 
    // HTML
   	echo '<div id="taxonomy-post_tag" class="categorydiv">';
   	echo '<input type="hidden" name="tax_input[restaurant_type][]" value="0" />';
   	echo '<ul>';
   	foreach( $all_restaurant_types as $restaurant_type ){
        // unchecked by default
   		$checked = "";
        // if an ID of a tag in the loop is in the array of assigned post tags - then check the checkbox
   		if ( !empty($ids) && in_array( $restaurant_type->term_id, $ids ) ) {
   			$checked = " checked='checked'";
   		}

   		$id = 'post_tag-' . $restaurant_type->term_id;
   		echo "<li id='{$id}'>";
   		echo "<label><input type='checkbox' name='wp_restaurant_type[]' id='in-wp_restaurant_types_meta_box'". $checked ." value='$restaurant_type->term_id' /> $restaurant_type->name</label><br />";
   		echo "</li>";
   	}
       echo '</ul></div>'; // end HTML
}

public function save_restaurant_types_information($post_id){
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( wp_is_post_revision( $post_id ) ) {
		return;
	}


	update_post_meta( $post_id, 'wp_restaurant_type', $_POST['wp_restaurant_type']	 );
}

// add restataurant categories meta box
public function add_restaurant_categories_meta_boxes() {
	add_meta_box(
        'wp_restaurant_categories_meta_box', //id
        'Restaurant Categories', //name
        array($this,'restaurant_categories_meta_box_display'), //display function
        'wp_restaurant', //post type
        'side', //location
        'default' //priority
       );

}


public function restaurant_categories_meta_box_display($post){
	global $post;
    // get all the restaurant categories 
	$all_restaurant_categories = get_terms( array('taxonomy' => 'restaurant_category', 'hide_empty' => 0) ); 

    // get all restaurant types assigned to a post
	$ids = get_post_meta($post->ID,'wp_restaurant_category', true); 
   // HTML
	echo '<ul id="restaurant_category-tabs" class="category-tabs">';
	echo '<li class="tabs"><a href="#restaurant_category-all">All Restaurant Category</a></li>';
	echo '<li class="hide-if-no-js"><a href="#restaurant_category-pop">Most Used</a></li>';
	echo  '</ul>';

	echo '<div id="taxonomy-restaurant_category" class="categorydiv">';
	echo '<div id="restaurant_category-all" class="tabs-panel">';
	echo '<div id="taxonomy-post_tag" class="categorydiv">';
	echo '<input type="hidden" name="tax_input[restaurant_category][]" value="0" />';
	echo '<ul>';
	foreach( $all_restaurant_categories as $restaurant_category ){
        // unchecked by default
		$checked = "";
        // if an ID of a tag in the loop is in the array of assigned post tags - then check the checkbox
		if ( !empty($ids) && in_array( $restaurant_category->term_id, $ids ) ) {
			$checked = " checked='checked'";
		}

		$id = 'post_tag-' . $restaurant_category->term_id;
		echo "<li id='{$id}'>";
		echo "<label><input type='checkbox' name='wp_restaurant_category[]' id='in-wp_restaurant_types_meta_box'". $checked ." value='$restaurant_category->term_id' /> $restaurant_category->name</label><br />";
		echo "</li>";
	} 
   echo '</ul></div></div></div>'; // end HTML
}

// add restaurant user meta box
public function add_restaurant_user_meta_boxes() {
	add_meta_box(
        'wp_restaurant_author_meta_box', //id
        'Restaurant Owner', //name
        array($this,'restaurant_author_meta_box_display'), //display function
        'wp_restaurant', //post type
        'side', //location
        'high' //priority
      );
}
   
public function restaurant_author_meta_box_display($post){
	global $post;
	$blogusers = get_users();
	$post_author_id = get_post_field( 'post_author', $post);
	wp_nonce_field( basename( __FILE__ ), 'restaurant_user_metabox_nonce' ); ?>

	<select name="post_author_update" id="post_author_override" class="">
		<?php
		foreach ($blogusers as  $user) { ?>
			<option value="<?php echo $user->ID; ?>" <?php echo ($user->data->ID == $post_author_id ? 'selected': ''); ?> ><?php echo $user->display_name ? $user->display_name : $user->user_nicename;?></option>
		<?php } ?>
	</select>

<?php }


	public function save_restaurant_user_information($post_id) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		// if ( wp_is_post_autosave( $post_id ) ) {
			return;
		}

		// Add nonce for security and authentication.
		if ( 
			!isset( $_POST['restaurant_user_metabox_nonce'] ) 
			|| !wp_verify_nonce( $_POST['restaurant_user_metabox_nonce'], basename( __FILE__ ) ) 
		) {
			return $post_id;
		}

		if ( wp_is_post_revision( $post_id ) ) {
			return;
		}

        $callbackCouter++;
        if ($callbackCouter > 5) {
            return;
        }

        $post_author_id = get_post_field( 'post_author', $post);
        $postDataAuthorId = intval($_POST['post_author_update']);
	    
        if ($post_author_id != $postDataAuthorId) {
            global $wpdb;
            $queryFlag = $wpdb->update(
                $wpdb->prefix .'posts',
                array(
                    'post_author' => $_POST['post_author_update'],
                ),
                array(
                    'ID' => $post_id,
                )
            );
        }
    }

    
    // add restaurant order meta box
public function add_restaurant_order_meta_boxes() {
	add_meta_box(
         'wp_restaurant_order_meta_box', //id
         'Restaurant Order', //name
         array($this,'restaurant_order_meta_box_display'), //display function
         'wp_restaurant', //post type
         'side', //location
         'default' //priority
        );

}

public function restaurant_order_meta_box_display($post){
	global $post;
	$order = get_post_meta($post->ID,'wp_restaurant_list_no', true); 
	?>
	<fieldset>
		<input type="text" name="wp_restaurant_list_no" style="width:80%" value="<?php echo $order;?>">
	</fieldset>


<?php }

public function save_restaurant_order_information($post_id){
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( wp_is_post_revision( $post_id ) ) {
		return;
	}

	update_post_meta( $post_id, 'wp_restaurant_list_no', $_POST['wp_restaurant_list_no']);
}

    
public function save_restaurant_categories_information($post_id){
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( wp_is_post_revision( $post_id ) ) {
		return;
	} 

	update_post_meta( $post_id, 'wp_restaurant_category', $_POST['wp_restaurant_category']);
}



public function add_location_meta_boxes() {
	add_meta_box(
      'wp_location_meta_box', //id
      'Restaurant Information', //name
      array($this,'restaurant_meta_box_display'), //display function
      'wp_restaurant', //post type
      'normal', //location
      'default' //priority
    );
	add_meta_box( 'restaurant_hours_of_operation', __( 'Hours of Operation', 'wp_restaurant_meta_box' ), array( $this, 'restaurant_hours_of_operation' ), 'wp_locations', 'side', 'low' );
}


public function restaurant_meta_box_display($post) {
    //set nonce field
	wp_nonce_field('wp_location_nonce', 'wp_location_nonce_field');

    //collect variables
	$wp_restaurant_phone = get_post_meta($post->ID,'wp_restaurant_phone',true);
	// $wp_restaurant_delivery_fee = get_post_meta($post->ID,'wp_restaurant_delivery_fee',true);
    $wp_restaurant_delivery = get_post_meta($post->ID,'wp_restaurant_delivery',true);
	$wp_restaurant_email = get_post_meta($post->ID,'wp_restaurant_email',true);
	$wp_restaurant_address = get_post_meta($post->ID,'wp_restaurant_address',true);
	$wp_restaurant_min_order = get_post_meta($post->ID,'wp_restaurant_minimum_order_amount',true);
	$wp_restaurant_featured = get_post_meta($post->ID,'wp_restaurant_featured',true);
	$wp_restaurant_delivery_area = get_post_meta($post->ID,'wp_restaurant_delivery_pincodes',true);
	$wp_restaurant_closing_date = get_post_meta($post->ID,'wp_restaurant_closing_date',true);
	$wp_restaurant_delivery_days = get_post_meta($post->ID,'wp_restaurant_delivery_days',true);	
	$wp_restaurant_opening_days = get_post_meta($post->ID,'wp_restaurant_opening_days',true);	
	$wp_restaurant_pickup_days = get_post_meta($post->ID,'wp_restaurant_pickup_days',true);






    // collect social media information
	$wp_restaurant_website = get_post_meta($post->ID,'wp_restaurant_website',true);
	$wp_restaurant_tagline = get_post_meta($post->ID,'wp_restaurant_tagline',true);
	$wp_restaurant_facebook = get_post_meta($post->ID,'wp_restaurant_facebook',true);
	$wp_restaurant_instagram = get_post_meta($post->ID,'wp_restaurant_instagram',true);
	$wp_restaurant_lead_time= get_post_meta($post->ID,'wp_restaurant_lead_time',true);




	?>
	<div class="field-container">
		<?php 
        //before main form elementst hook
		do_action('wp_location_admin_form_start'); 
		?>
	</div>
	<div class="inside">
		<div class="row">
			<div class="col-sm-6" >
				<label>Address</label>
				<fieldset>
					<textarea name="wp_restaurant_address" id="wp_restaurant_address" rows="4" cols="50"style="width:80%;height: inherit;">
						<?php echo $wp_restaurant_address; ?>
					</textarea>
				</fieldset>

			</div>
			<div class="col-sm-6" >
				<label>Minimum Order Amount</label>
				<fieldset><input type="number" name="wp_restaurant_min_order" min="1" value="<?php echo $wp_restaurant_min_order; ?>"style="width:80%;height:38px;"></fieldset>

			</div>

			<div class="col-sm-6" >
				<label>Lead Time</label>
				<fieldset>
					<select style="width: 80%" name="wp_restaurant_lead_time">
						<?php for($i=1;$i<=10;$i++) { ?>
							<option value="<?php echo $i*12; ?>" 
								<?php
								if($wp_restaurant_lead_time == $i*12 ){
									echo "selected";

								}
								?>
								>

								<?php echo $i*12; ?> Hrs</option>	
							<?php } ?>
						</select>
					</fieldset>

				</div>

				<div class="col-sm-6" >
					<label>Closing dates</label>
					<fieldset>

						<input type="hidden" id="wp_restaurant_closing_date_hide"   placeholder="Select the closing dates"  value="<?php echo $wp_restaurant_closing_date?>">
						<input type="text" id="wp_restaurant_closing_date" name="wp_restaurant_closing_date"  placeholder="Select the closing dates" class="" autocomplete="true" <?php echo $wp_restaurant_closing_date?>>
					</fieldset>
					<script type="text/javascript">
						jQuery(document).ready(function($){
							$("#wp_restaurant_closing_date").multiDatesPicker();
						});
					</script>

				</div>

				<div class="col-sm-6" >
					<label>Restaurant Email</label>
					<fieldset><input name="wp_restaurant_email" id="wp_restaurant_email" value="<?php echo $wp_restaurant_email;?>" type="text" style="width:80%"></fieldset>
				</div> 

				<div class="col-sm-6" >
					<!-- <label>Restaurant Delivery Fees</label>
					<fieldset><input type="text" name="wp_restaurant_delivery_fees" style="width:80%" 
						value="<?php //echo $wp_restaurant_delivery_fee;?>"></fieldset> -->
                    <fieldset class="delivery-fee-section">
                        <label>Delivery Fee <small class="sgd-text">( in <b>SGD</b> )</small></label>
                        <div class="field row first-delivery-fee">
                            <div class="col-md-5">
                                <sub>Price Greater Than</sub>
                                <input type="text" class="delivery-conditional-price"  name="wp_restaurant_delivery[conditional_price][]" value="0" readonly />
                            </div>
                            <div class="col-md-5">
                                <sub>Then Applicable Delivery Fee</sub>
                                <input type="text" class="delivery-fee"  name="wp_restaurant_delivery[applicable_fee][]" value="<?php echo (isset($wp_restaurant_delivery['applicable_fee']) ? $wp_restaurant_delivery['applicable_fee'][0] : '') ?>" />
                            </div>
                            <div class="col-md-2 delivery-action-button">
                                <button type="button" class="add-delivery-condition" tabindex="-1">+</button>
                            </div>
                        </div>

                        <div class="rest-delivery-fees">
                        <?php
                        if (isset($wp_restaurant_delivery['conditional_price'])) {
                            foreach ($wp_restaurant_delivery['conditional_price'] as $key => $deliveryCondition) {
                                if (!$key) {
                                    continue;
                                }

                                $deliveryContent .= '
                                    <div class="field row first-delivery-fee">
                                        <div class="col-md-5">
                                            <sub>Price Greater Than</sub>
                                            <input type="text" class="delivery-conditional-price"  name="wp_restaurant_delivery[conditional_price][]" value="'. $deliveryCondition .'" />
                                        </div>
                                        <div class="col-md-5">
                                            <sub>Then Applicable Delivery Fee</sub>
                                            <input type="text" class="delivery-fee"  name="wp_restaurant_delivery[applicable_fee][]" value="'. $wp_restaurant_delivery['applicable_fee'][$key] .'" />
                                        </div>
                                        <div class="col-md-2 delivery-action-button">
                                            <button type="button" class="remove-delivery-condition" tabindex="-1">-</button>
                                            <button type="button" class="add-delivery-condition" tabindex="-1">+</button>
                                        </div>
                                    </div>
                                ';
                            }
                            echo $deliveryContent;
                        }
                        ?>
                        </div>

                        <script type="text/javascript">
                            jQuery(document.body).ready(function($){
                                $(".delivery-fee-section").on("click", ".add-delivery-condition", function(){
                                    $(".rest-delivery-fees").append(`
                                        <div class="field row first-delivery-fee">
                                            <div class="col-md-5">
                                                <sub>Price Greater Than</sub>
                                                <input type="text" class="delivery-conditional-price"  name="wp_restaurant_delivery[conditional_price][]" value="" />
                                            </div>
                                            <div class="col-md-5">
                                                <sub>Then Applicable Delivery Fee</sub>
                                                <input type="text" class="delivery-fee"  name="wp_restaurant_delivery[applicable_fee][]" value="" />
                                            </div>
                                            <div class="col-md-2 delivery-action-button">
                                                <button type="button" class="remove-delivery-condition" tabindex="-1">-</button>
                                                <button type="button" class="add-delivery-condition" tabindex="-1">+</button>
                                            </div>
                                        </div>
                                    `);
                                });

                                $(".delivery-fee-section").on("click", ".remove-delivery-condition", function(){
                                    $(this).parent().parent().remove();
                                });
                            });
                        </script>
                    </fieldset>
				</div> 
				<div class="col-sm-6" >
						<label>Phone</label>
						<fieldset>
							<input type="text" name="wp_restaurant_phone" id="wp_restaurant_phone" value="<?php echo $wp_restaurant_phone;?>" style="width:80%">
						</fieldset>

					</div>

					<div class="col-sm-6" >
						<label>Restaurant Delivery Pincodes</label>
						<fieldset><input type="text" name="wp_restaurant_delivery_area" value ="<?php echo $wp_restaurant_delivery_area ?>" style="width:80%"></fieldset>

					</div> 

					<div class="col-sm-6" >
						<label>Website</label>
						<fieldset><input type="text" name="wp_restaurant_website" style="width:80%" value="<?php echo $wp_restaurant_website;?>"></fieldset>

					</div> 

					<div class="col-sm-6" >
						<label>Tagline</label>
						<fieldset>
							<input type="text" name="wp_restaurant_tagline" style="width:80%" value="<?php echo $wp_restaurant_tagline;?>">
						</fieldset>

					</div>

<div class="col-sm-6" >
	<label>Twitter username</label>
	<fieldset>
		<input type="text" name="wp_restaurant_twitter" style="width:80%" value="<?php 
		echo $wp_restaurant_twitter;?>">
	</fieldset>

</div>

<div class="col-sm-6" >
	<label>Facebook username</label>
	<fieldset><input type="text" name="wp_restaurant_facebook" style="width:80%" value="<?php echo $wp_restaurant_facebook;?>"></fieldset>

</div>

<div class="col-sm-6" >
	<label>Instagram username</label>
	<fieldset>
		<input type="text" name="wp_restaurant_instagram" style="width:80%" value="<?php echo $wp_restaurant_instagram;?>">
	</fieldset>

</div>

<div class="col-sm-12" >
	<label>Opening Hours</label>
	<table class="table-responsive">
		<?php
		$weekDays = array(
			'monday' => 'Monday',
			'tuesday' => 'Tuesday',
			'wednesday' => 'Wednesday',
			'thursday' => 'Thursday',
			'friday' => 'Friday',
			'saturday' => 'Saturday',
			'sunday' => 'Sunday',
			'public' => 'Public holidays',
		);
		$hourDropDownOptions = array(
			'0' => '12 AM',
			'1' => '1 AM',
			'2' => '2 AM',
			'3' => '3 AM',
			'4' => '4 AM',
			'5' => '5 AM',
			'6' => '6 AM',
			'7' => '7 AM',
			'8' => '8 AM',
			'9' => '9 AM',
			'10' => '10 AM',
			'11' => '11 AM',
			'12' => '12 PM',
			'13' => '1 PM',
			'14' => '2 PM',
			'15' => '3 PM',
			'16' => '4 PM',
			'17' => '5 PM',
			'18' => '6 PM',
			'19' => '7 PM',
			'20' => '8 PM',
			'21' => '9 PM',
			'22' => '10 PM',
			'23' => '11 PM',
		);


		?>
		<tbody>

			<tr>
				<?php	 
				foreach($weekDays as $dayKey => $dayText){ 
					if (isset($wp_restaurant_opening_days[$dayKey])) { ?>
						<td>
							<span style="font-weight: bold;"><?php echo $dayText;?></span>
							<?php  
							foreach ($wp_restaurant_opening_days[$dayKey]['from'] as $key => $deliveryFrom) {


								?>
								<div class="time-wrapper"style="padding-top: 10px;"> <span>From</span>:
									<select name="wp_restaurant_opening_days[<?php echo $dayKey;?>][from][]" class="from-hours"> 

										<?php

										foreach ($hourDropDownOptions as $hourKey => $hourText) {
											$selected = '';
											if ($deliveryFrom == $hourKey) {
												$selected = 'selected';
											} ?>
											<option value="<?php echo $hourKey;?>"  <?php echo $selected;?> ><?php echo $hourText;?></option>

										<?php    }

										?>	


									</select>	

									<br> To:
									<select name="wp_restaurant_opening_days[<?php echo $dayKey;?>][to][]" class="to-hours">;
										<?php
										foreach ($hourDropDownOptions as $hourKey => $hourText) {
											$selected = '';
											if ($wp_restaurant_opening_days[$dayKey]['to'][$key] == $hourKey) {
												$selected = 'selected';
											}?>

											<option value="<?php echo $hourKey;?>" <?php echo $selected;?>><?php echo $hourText;?></option>
										<?php  	}   


										?>

									</select> 


								</div> 	
							<?php } 



							?>


							<td>


							<?php  } 
						}  

						?> 

					</tr>  	

					<tbody>
					</table>




				</div>	

				<div class="col-sm-12" >
					<label>Delivery Hours</label>
					<table class="table-responsive">
						<?php
						$weekDays = array(
							'monday' => 'Monday',
							'tuesday' => 'Tuesday',
							'wednesday' => 'Wednesday',
							'thursday' => 'Thursday',
							'friday' => 'Friday',
							'saturday' => 'Saturday',
							'sunday' => 'Sunday',
							'public' => 'Public holidays',
						);
						$hourDropDownOptions = array(
							'0' => '12 AM',
							'1' => '1 AM',
							'2' => '2 AM',
							'3' => '3 AM',
							'4' => '4 AM',
							'5' => '5 AM',
							'6' => '6 AM',
							'7' => '7 AM',
							'8' => '8 AM',
							'9' => '9 AM',
							'10' => '10 AM',
							'11' => '11 AM',
							'12' => '12 PM',
							'13' => '1 PM',
							'14' => '2 PM',
							'15' => '3 PM',
							'16' => '4 PM',
							'17' => '5 PM',
							'18' => '6 PM',
							'19' => '7 PM',
							'20' => '8 PM',
							'21' => '9 PM',
							'22' => '10 PM',
							'23' => '11 PM',
						);


						?>
						<tbody>

							<tr>
								<?php	 
								foreach($weekDays as $dayKey => $dayText){ 
									if (isset($wp_restaurant_opening_days[$dayKey])) { ?>
										<td>
											<span style="font-weight: bold;"><?php echo $dayText;?></span>
											<?php  
											foreach ($wp_restaurant_delivery_days[$dayKey]['from'] as $key => $deliveryFrom) {

												?>
												<div class="time-wrapper"style="padding-top: 10px;"> <span>From</span>:
													<select name="wp_restaurant_delivery_days[<?php echo $dayKey;?>][from][]" class="from-hours"> 

														<?php

														foreach ($hourDropDownOptions as $hourKey => $hourText) {
															$selected = '';
															if ($deliveryFrom == $hourKey) {
																$selected = 'selected';
															} ?>
															<option value="<?php echo $hourKey;?>"  <?php echo $selected;?> ><?php echo $hourText;?></option>

														<?php    }

														?>	


													</select>	

													<br> To:
													<select name="wp_restaurant_delivery_days[<?php echo $dayKey;?>][to][]" class="to-hours">;
														<?php
														foreach ($hourDropDownOptions as $hourKey => $hourText) {
															$selected = '';
															if ($wp_restaurant_delivery_days[$dayKey]['to'][$key] == $hourKey) {
																$selected = 'selected';
															}?>

															<option value="<?php echo $hourKey;?>" <?php echo $selected;?>><?php echo $hourText;?></option>
														<?php  	}   


														?>

													</select> 


												</div> 	
											<?php } 



											?>


											<td>


											<?php  }  
										} 

										?> 

									</tr>  	

									<tbody>
									</table>

								</div>

								<div class="col-sm-12" >
									<label>Pick-up Hours</label>
									<table class="table-responsive">
										<?php
										$weekDays = array(
											'monday' => 'Monday',
											'tuesday' => 'Tuesday',
											'wednesday' => 'Wednesday',
											'thursday' => 'Thursday',
											'friday' => 'Friday',
											'saturday' => 'Saturday',
											'sunday' => 'Sunday',
											'public' => 'Public holidays',
										);
										$hourDropDownOptions = array(
											'0' => '12 AM',
											'1' => '1 AM',
											'2' => '2 AM',
											'3' => '3 AM',
											'4' => '4 AM',
											'5' => '5 AM',
											'6' => '6 AM',
											'7' => '7 AM',
											'8' => '8 AM',
											'9' => '9 AM',
											'10' => '10 AM',
											'11' => '11 AM',
											'12' => '12 PM',
											'13' => '1 PM',
											'14' => '2 PM',
											'15' => '3 PM',
											'16' => '4 PM',
											'17' => '5 PM',
											'18' => '6 PM',
											'19' => '7 PM',
											'20' => '8 PM',
											'21' => '9 PM',
											'22' => '10 PM',
											'23' => '11 PM',
										);


										?>
										<tbody>

											<tr>
												<?php	 
												foreach($weekDays as $dayKey => $dayText){ 
													if (isset($wp_restaurant_opening_days[$dayKey])) { ?>
														<td>
															<span style="font-weight: bold;"><?php echo $dayText;?></span>
															<?php  
															foreach ($wp_restaurant_pickup_days[$dayKey]['from'] as $key => $deliveryFrom) {
																?>
																<div class="time-wrapper"style="padding-top: 10px;"> <span>From</span>:
																	<select name="wp_restaurant_pickup_days[<?php echo $dayKey;?>][from][]" class="from-hours"> 

																		<?php

																		foreach ($hourDropDownOptions as $hourKey => $hourText) {
																			$selected = '';
																			if ($deliveryFrom == $hourKey) {
																				$selected = 'selected';
																			} ?>
																			<option value="<?php echo $hourKey;?>"  <?php echo $selected;?> ><?php echo $hourText;?></option>

																		<?php    }

																		?>	


																	</select>	

																	<br> To:
																	<select name="wp_restaurant_pickup_days[<?php echo $dayKey;?>][to][]" class="to-hours">;
																		<?php
																		foreach ($hourDropDownOptions as $hourKey => $hourText) {
																			$selected = '';
																			if ($wp_restaurant_pickup_days[$dayKey]['to'][$key] == $hourKey) {
																				$selected = 'selected';
																			}?>

																			<option value="<?php echo $hourKey;?>" <?php echo $selected;?>><?php echo $hourText;?></option>
																		<?php  	}   


																		?>

																	</select> 


																</div> 	
															<?php } 



															?>


															<td>


															<?php  }   
														}

														?> 

													</tr>  	

													<tbody>
													</table>


												</div>	
												<div class="col-sm-12" >
													<?php
													global $post;
													$post_id = $post->ID; 
													$user_id = $post->post_author; 

													?>
													<form method="get">

														<input type="hidden" name="user_id" value="<?php echo $user_id;?> ">
														<a class="dot" href="<?php echo site_url();?>/submit-restaurant/?user_id=<?php echo $user_id;?>" user_id="<?php echo $user_id;?>" target="_blank">Edit On Frontend</a><br />
														<a href="<?php echo site_url("restaurant-dashboard/?post_id={$post_id}&user_id={$user_id}");?>" target="_blank">View On Frontend</a>

													</form>

												</div>	


											</div>	

										</div>	
										<?php 
                                       //after main form elementst hook
										do_action('wp_location_admin_form_end'); 
										?>
									</div>
									<?php 
								}
	
	public function save_location($post_id) {
		if(!isset($_POST['wp_location_nonce_field'])) {
			return $post_id;
		}
		//verify nonce
		if(!wp_verify_nonce($_POST['wp_location_nonce_field'], 'wp_location_nonce')) {
			return $post_id;
		}
		//check for autosave
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post_id;
		}
		$post_id=$_POST['ID'];
        
		$wp_restaurant_phone = isset($_POST['wp_restaurant_phone']) ? sanitize_text_field($_POST['wp_restaurant_phone']) : '';
		$wp_restaurant_email = isset($_POST['wp_restaurant_email']) ? sanitize_text_field($_POST['wp_restaurant_email']) : '';
		$wp_restaurant_address = isset($_POST['wp_restaurant_address']) ? sanitize_text_field($_POST['wp_restaurant_address']) : '';
		$wp_restaurant_featured = isset($_POST['wp_restaurant_featured']) ? sanitize_text_field($_POST['wp_restaurant_featured']) : '';
		$wp_restaurant_min_order = isset($_POST['wp_restaurant_min_order']) ? sanitize_text_field($_POST['wp_restaurant_min_order']) : '';
		// $wp_restaurant_delivery_fee = isset($_POST['wp_restaurant_delivery_fees']) ? sanitize_text_field($_POST['wp_restaurant_delivery_fees']) : '';
        $wp_restaurant_delivery = isset($_POST['wp_restaurant_delivery']) ? $_POST['wp_restaurant_delivery'] : [];
		$wp_restaurant_delivery_area = isset($_POST['wp_restaurant_delivery_area']) ? sanitize_text_field($_POST['wp_restaurant_delivery_area']) : '';
		$wp_restaurant_website = isset($_POST['wp_restaurant_website']) ? sanitize_text_field($_POST['wp_restaurant_website']) : '';
		$wp_restaurant_tagline = isset($_POST['wp_restaurant_tagline']) ? sanitize_text_field($_POST['wp_restaurant_tagline']) : '';
		$wp_restaurant_twitter = isset($_POST['wp_restaurant_twitter']) ? sanitize_text_field($_POST['wp_restaurant_twitter']) : '';
		$wp_restaurant_facebook = isset($_POST['wp_restaurant_facebook']) ? sanitize_text_field($_POST['wp_restaurant_facebook']) : '';
		$wp_restaurant_instagram = isset($_POST['wp_restaurant_instagram']) ? sanitize_text_field($_POST['wp_restaurant_instagram']) : '';
		$wp_restaurant_lead_time = isset($_POST['wp_restaurant_lead_time']) ? sanitize_text_field($_POST['wp_restaurant_lead_time']) : '';
		$wp_restaurant_closing_date = isset($_POST['wp_restaurant_closing_date']) ? sanitize_text_field($_POST['wp_restaurant_closing_date']) : '';
		$wp_restaurant_list_img = isset($_POST['wp_restaurant_list_img']) ? sanitize_text_field($_POST['wp_restaurant_list_img']) : '';
		$wp_restaurant_gallery = isset($_POST['wp_restaurant_gallery']) ? sanitize_text_field($_POST['wp_restaurant_gallery']) : '';
		  
		
		$delivery_fields = array(
	    	'wp_restaurant_delivery_days' => $_POST['wp_restaurant_delivery_days']
	     );

		foreach ( $delivery_fields as $key => $value ) {
              if ( array_key_exists( $key, $delivery_fields ) ) {
                    update_post_meta( $post_id, $key, $value );
                  }
           }

         $opening_fields = array(
            ///'post_id' => $post_id, 
	    	'wp_restaurant_opening_days' => $_POST['wp_restaurant_opening_days']
	     );
		foreach ( $opening_fields as $key => $value ) {
              if ( array_key_exists( $key, $opening_fields ) ) {
                    update_post_meta( $post_id, $key, $value );
 

               }
          }

          $pickup_fields = array(
            ///'post_id' => $post_id, 
	    	'wp_restaurant_pickup_days' => $_POST['wp_restaurant_pickup_days']
	     );
		foreach ( $pickup_fields as $key => $value ) {
              if ( array_key_exists( $key, $pickup_fields ) ) {
                    update_post_meta( $post_id, $key, $value );
 

               }
          }  




		
		//update phone, memil and address fields
		update_post_meta($post_id, 'wp_restaurant_phone', $wp_restaurant_phone);
		update_post_meta($post_id, 'wp_restaurant_email', $wp_restaurant_email);
		update_post_meta($post_id, 'wp_restaurant_address', $wp_restaurant_address);
		update_post_meta($post_id, 'wp_restaurant_featured', $wp_restaurant_featured);
		update_post_meta($post_id, 'wp_restaurant_minimum_order_amount', $wp_restaurant_min_order);
		// update_post_meta($post_id, 'wp_restaurant_delivery_fee', $wp_restaurant_delivery_fee);
        update_post_meta($post_id, 'wp_restaurant_delivery', $wp_restaurant_delivery);
		update_post_meta($post_id, 'wp_restaurant_delivery_pincodes', $wp_restaurant_delivery_area);
		update_post_meta($post_id, 'wp_restaurant_website', $wp_restaurant_website);
		update_post_meta($post_id, 'wp_restaurant_tagline', $wp_restaurant_tagline);
		update_post_meta($post_id, 'wp_restaurant_twitter', $wp_restaurant_twitter);
		update_post_meta($post_id, 'wp_restaurant_facebook', $wp_restaurant_facebook);
		update_post_meta($post_id, 'wp_restaurant_instagram', $wp_restaurant_instagram);
		update_post_meta($post_id, 'wp_restaurant_lead_time', $wp_restaurant_lead_time);
		update_post_meta($post_id, 'wp_restaurant_closing_date', $wp_restaurant_closing_date);
		update_post_meta( $post_id, 'wp_restaurant_list_img', $wp_restaurant_list_img ); 
		update_post_meta( $post_id, 'wp_restaurant_gallery', array($wp_restaurant_gallery) );

		//search for our trading hour data and update
		foreach($_POST as $key => $value) {
			//if we found our trading hour data, update it
			if(preg_match('/^wp_restaurant_delivery_days_/', $key)) {
				update_post_meta($post_id, $key, $value);
			}
		}


		
		//search for our trading hour data and update
		foreach($_POST as $key => $value) {
			//if we found our trading hour data, update it
			if(preg_match('/^wp_location_trading_hours_/', $key)) {
				update_post_meta($post_id, $key, $value);
			}
		}
		
		//location save hook 
		//used so you can hook here and save additional post fields added via 'wp_location_meta_data_output_end' or 'wp_location_meta_data_output_end'
		do_action('wp_location_admin_save',$post_id);
	}
	public function crf_show_extra_profile_fields( $user ) {
		$selectedRestaurant = get_the_author_meta( 'selected_restaurant', $user->ID );
		?>
		<h3><?php esc_html_e( 'Restaurant Information', 'crf' ); ?></h3>
		<table class="form-table">
			<tr>
				<th><label for="selected_restaurant"><?php esc_html_e( 'User Related Restaurants', 'crf' ); ?></label></th>
				<td>
					<input type="hidden" id="selected_restaurant"
				       name="selected_restaurant"
				       value="<?php echo esc_attr( $selectedRestaurant ); ?>"
				       class="regular-text"
					/>
				</td>
			</tr>
		</table>
		<?php
		$args = array(
	        'post_type' => 'wp_restaurant',// your post type,
	        'orderby' => 'post_date',
	        'order' => 'DESC',   
		);
		$the_query = new WP_Query($args);
		if($the_query->have_posts()):
			$restaurant.=  '<ul>';
			
			$selectedRestaurantData=explode(',',$selectedRestaurant);
		   	while($the_query->have_posts()): 
		   		$the_query->the_post();
		  		$checked=0;
		    	for($i=0;$i<sizeof($selectedRestaurantData);$i++) {
			    	$id =$selectedRestaurantData[$i];
			    	if($id==get_the_ID()) {
			    		$checked++;
			    	}
		    	}
		    	if($checked>0) {
		    		$checked="checked";
		    	} else {
		    		$checked="";
		    	}
		    	$restaurant.='<li style="width:20%;float:left;">';
		    	$restaurant.=  "
		    		<a href='".get_the_permalink()."'>
		    		<div>".get_the_post_thumbnail(null,array( 100, 100))."</div>
	    		";
		    	$restaurant.=  "<h3>".get_the_title()."</h3>";
		     	$restaurant.= "
			     	</a>
			     	<input  id='postid'  class='mycheckbox  slectthecheckbox'  type='checkbox' ".$checked." value='".get_the_ID()."'>
			     	</li>
		     	";
			endwhile;
			$restaurant.=  '</ul>';
		endif;
		echo $restaurant;
	}
	public function crf_update_profile_fields( $user_id ) {
		if ( ! current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}
		if ( ! empty( $_POST['selected_restaurant'] ) ) {
			update_user_meta( $user_id, 'selected_restaurant', ( $_POST['selected_restaurant'] ) );
		}
	}
 	
 	public function loadOptimizeMapModal() {
		$modal = '
			<div class="container">
				<div class="modal fade" id="myModal" role="dialog">
				    <div class="modal-dialog  modal-lg">
				      	<div class="modal-content">
					        <div class="modal-header">
					          	<h4 class="modal-title">Map Header</h4>
					        </div>
				        	<div class="modal-body">
							    <div id="panel"> 
							    	<div id="color-palette"></div> 
							    	<div>
								        <input type="button" id="delete-button" value="Delete Selected Shape">
								        <input type="button" id="submit" value="Selected Shape" data-dismiss="modal">
										<input type="text" id="latlng" value="">
								    </div>
								    <div id="curpos"></div>
								    <div id="cursel"></div>
								    <div id="note">
								    	<small>Note: markers can be selected, but are not graphically indicated; can be deleted, but cannot have their color changed.</small>
							    	</div>
						    	</div>
						    	
						    	<input id="pac-input" style="width:30% !important"  type="text" placeholder="Search Box">
						    	<div id="map" style="height:500px">A</div>
								<div class="modal-footer">
					          		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        	</div>
					        </div>
					    </div> 
				    </div>
			  	</div>
			</div>
		';
		return $modal;
	}
 	
 	public function load_map_modal() {
		$modal = '
			<div class="container">
				<div class="modal fade" id="myModal" role="dialog">
				    <div class="modal-dialog  modal-lg">
				      	<div class="modal-content">
					        <div class="modal-header">
					          	<h4 class="modal-title">Map Header</h4>
					        </div>
				        	<div class="modal-body">
							    <script type="text/javascript">
							      	var drawingManager;
							      	var selectedShape;
							      	var colors = ["#1E90FF", "#FF1493", "#32CD32", "#FF8C00", "#4B0082"];
							      	var selectedColor;
							      	var colorButtons = {};
							      	function clearSelection() {
							        	if (selectedShape) {
								          	if (typeof selectedShape.setEditable == "function") {
								            	selectedShape.setEditable(false);
								          	}
								          	selectedShape = null;
							        	}
							        	curseldiv.innerHTML = "<b>cursel</b>:";
							      	}
							      	function updateCurSelText(shape) {
							        	posstr = "" + selectedShape.position;
							        	if (typeof selectedShape.position == "object") {
							          		posstr = selectedShape.position.toUrlValue();
							        	}
							        	pathstr = "" + selectedShape.getPath;
							        	if (typeof selectedShape.getPath == "function") {
								          	pathstr = "[ ";
								          	for (var i = 0; i < selectedShape.getPath().getLength(); i++) {
								            	// .toUrlValue(5) limits number of decimals, default is 6 but can do more
								            	pathstr += selectedShape.getPath().getAt(i).toUrlValue() + " , ";
								          	}
								          	pathstr += "]";
							        	}
							        	bndstr = "" + selectedShape.getBounds;
							        	cntstr = "" + selectedShape.getBounds;
							        	if (typeof selectedShape.getBounds == "function") {
								          	var tmpbounds = selectedShape.getBounds();
								          	cntstr = "" + tmpbounds.getCenter().toUrlValue();
								          	bndstr = "[NE: " + tmpbounds.getNorthEast().toUrlValue() + " SW: " + tmpbounds.getSouthWest().toUrlValue() + "]";
							        	}
							        	cntrstr = "" + selectedShape.getCenter;
							        	
							        	if (typeof selectedShape.getCenter == "function") {
							          		cntrstr = "" + selectedShape.getCenter().toUrlValue();
							        	}
							        	radstr = "" + selectedShape.getRadius;
							        	
							        	if (typeof selectedShape.getRadius == "function") {
							          		radstr = "" + selectedShape.getRadius();
							        	}
							        	curseldiv.innerHTML = "<b>cursel</b>: " + selectedShape.type + " " + selectedShape + "; <i>pos</i>: " + posstr + " ; <i>path</i>: " + pathstr + " ; <i>bounds</i>: " + bndstr + " ; <i>Cb</i>: " + cntstr + " ; <i>radius</i>: " + radstr + " ; <i>Cr</i>: " + cntrstr ;
							        	document.getElementById("latlng").value=pathstr;
							      	}
							      	function setSelection(shape, isNotMarker) {
							        	clearSelection();
							        	selectedShape = shape;
							        	if (isNotMarker)
							          		shape.setEditable(true);
							        	selectColor(shape.get("fillColor") || shape.get("strokeColor"));
							        	updateCurSelText(shape);
							      	}
							      	function deleteSelectedShape() {
							        	if (selectedShape) {
							          		selectedShape.setMap(null);
							        	}
							      	}
							      	function selectColor(color) {
							        	selectedColor = color;
							        	
							        	for (var i = 0; i < colors.length; ++i) {
								          	var currColor = colors[i];
								          	colorButtons[currColor].style.border = currColor == color ? "2px solid #789" : "2px solid #fff";
							        	}
							        	// Retrieves the current options from the drawing manager and replaces the
							        	// stroke or fill color as appropriate.
							        	var polylineOptions = drawingManager.get("polylineOptions");
							        	polylineOptions.strokeColor = color;
							        	drawingManager.set("polylineOptions", polylineOptions);
							        	var rectangleOptions = drawingManager.get("rectangleOptions");
							        	rectangleOptions.fillColor = color;
							        	drawingManager.set("rectangleOptions", rectangleOptions);
							        	var circleOptions = drawingManager.get("circleOptions");
							        	circleOptions.fillColor = color;
							        	drawingManager.set("circleOptions", circleOptions);
							        	var polygonOptions = drawingManager.get("polygonOptions");
							        	polygonOptions.fillColor = color;
							        	drawingManager.set("polygonOptions", polygonOptions);
							      	}
							      	function setSelectedShapeColor(color) {
							        	if (selectedShape) {
								          	if (selectedShape.type == google.maps.drawing.OverlayType.POLYLINE) {
								            	selectedShape.set("strokeColor", color);
								          	} else {
								            	selectedShape.set("fillColor", color);
								          	}
							        	}
							      	}
							      	function makeColorButton(color) {
							        	var button = document.createElement("span");
							        	button.className = "color-button";
							        	button.style.backgroundColor = color;
							        	google.maps.event.addDomListener(button, "click", function() {
								          	selectColor(color);
								          	setSelectedShapeColor(color);
							        	});
							        	return button;
							      	}
							       	function buildColorPalette() {
							         	var colorPalette = document.getElementById("color-palette");
							         	
							         	for (var i = 0; i < colors.length; ++i) {
								           	var currColor = colors[i];
								           	var colorButton = makeColorButton(currColor);
								           	colorPalette.appendChild(colorButton);
								           	colorButtons[currColor] = colorButton;
							         	}
							         	selectColor(colors[0]);
							       	}
							      	/////////////////////////////////////
							      	var map; //= new google.maps.Map(document.getElementById("map"), {
							      	// these must have global refs too!:
							      	var placeMarkers = [];
							      	var input;
							      	var searchBox;
							      	var curposdiv;
							      	var curseldiv;
							      	function deletePlacesSearchResults() {
							        	for (var i = 0, marker; marker = placeMarkers[i]; i++) {
							          		marker.setMap(null);
							        	}
							        	placeMarkers = [];
							        	input.value = ""; // clear the box too
							      	}
							      	/////////////////////////////////////
							      	function initialize() {
							        	map = new google.maps.Map(document.getElementById("map"), { //var
								          	zoom: 6,//10,
								          	center: new google.maps.LatLng(1.302570,103.834690),
								          	mapTypeId: google.maps.MapTypeId.ROADMAP,
								          	disableDefaultUI: false,
								          	zoomControl: true
							        	});
							        	curposdiv = document.getElementById("curpos");
							        	curseldiv = document.getElementById("cursel");
							        	var polyOptions = {
								          	strokeWeight: 0,
								          	fillOpacity: 0.45,
								          	editable: true
							        	};
							        	// Creates a drawing manager attached to the map that allows the user to draw
							        	// markers, lines, and shapes.
							        	drawingManager = new google.maps.drawing.DrawingManager({
							          	drawingMode: google.maps.drawing.OverlayType.POLYGON,
								          	markerOptions: {
								            	draggable: true,
								            	editable: true,
								          	},
								          	polylineOptions: {
								            	editable: true
								          	},
								          	rectangleOptions: polyOptions,
								          	circleOptions: polyOptions,
								          	polygonOptions: polyOptions,
								          	map: map
							        	});
							        	google.maps.event.addListener(drawingManager, "overlaycomplete", function(e) {
							          	//~ if (e.type != google.maps.drawing.OverlayType.MARKER) {
							            	var isNotMarker = (e.type != google.maps.drawing.OverlayType.MARKER);
							            	// Switch back to non-drawing mode after drawing a shape.
							            	drawingManager.setDrawingMode(null);
							            	// Add an event listener that selects the newly-drawn shape when the user
							            	// mouses down on it.
							            	var newShape = e.overlay;
							            	newShape.type = e.type;
							            	google.maps.event.addListener(newShape, "click", function() {
							              		setSelection(newShape, isNotMarker);
							            	});
							            	google.maps.event.addListener(newShape, "drag", function() {
							              		updateCurSelText(newShape);
							            	});
							            	google.maps.event.addListener(newShape, "dragend", function() {
							              		updateCurSelText(newShape);
							            	});
							            	setSelection(newShape, isNotMarker);
							          	//~ }// end if
							        	});
							        	// Clear the current selection when the drawing mode is changed, or when the
							        	// map is clicked.
							        	google.maps.event.addListener(drawingManager, "drawingmode_changed", clearSelection);
							        	google.maps.event.addListener(map, "click", clearSelection);
							        	google.maps.event.addDomListener(document.getElementById("delete-button"), "click", deleteSelectedShape);
							        	buildColorPalette();
								        
							         	input = /** @type {HTMLInputElement} */( //var
							            	document.getElementById("pac-input"));
								        
							        	var DelPlcButDiv = document.createElement("div");
							        	//~ DelPlcButDiv.style.color = "rgb(25,25,25)"; // no effect?
							        	DelPlcButDiv.style.backgroundColor = "#fff";
							        	DelPlcButDiv.style.cursor = "pointer";
							        	DelPlcButDiv.innerHTML = "DEL";
							        	map.controls[google.maps.ControlPosition.TOP_RIGHT].push(DelPlcButDiv);
							        	google.maps.event.addDomListener(DelPlcButDiv, "click", deletePlacesSearchResults);
							        	
							        	searchBox = new google.maps.places.SearchBox( //var
							          		/** @type {HTMLInputElement} */(input)
						          		);
							        	// Listen for the event fired when the user selects an item from the
							        	// pick list. Retrieve the matching places for that item.
							        	google.maps.event.addListener(searchBox, "places_changed", function() {
								          	var places = searchBox.getPlaces();
								          	
								          	if (places.length == 0) {
								            	return;
								          	}
								          	for (var i = 0, marker; marker = placeMarkers[i]; i++) {
								            	marker.setMap(null);
								          	}
								          	// For each place, get the icon, place name, and location.
								          	placeMarkers = [];
								          	var bounds = new google.maps.LatLngBounds();
								          	for (var i = 0, place; place = places[i]; i++) {
								            	var image = {
									              	url: place.icon,
									              	size: new google.maps.Size(71, 71),
									              	origin: new google.maps.Point(0, 0),
									              	anchor: new google.maps.Point(17, 34),
									              	scaledSize: new google.maps.Size(25, 25)
								            	};
								            	// Create a marker for each place.
								            	var marker = new google.maps.Marker({
									              	map: map,
									              	icon: image,
									              	title: place.name,
									              	position: place.geometry.location
								            	});
								            	placeMarkers.push(marker);
								            	bounds.extend(place.geometry.location);
							          		}
								          	map.fitBounds(bounds);
							        	});
							        	// Bias the SearchBox results towards places that are within the bounds of the
							        	// current map"s viewport.
							        	google.maps.event.addListener(map, "bounds_changed", function() {
								          	var bounds = map.getBounds();
								          	searchBox.setBounds(bounds);
								          	curposdiv.innerHTML = "<b>curpos</b> Z: " + map.getZoom() + " C: " + map.getCenter().toUrlValue();
							        	}); //////////////////////
							         	var geocoder = new google.maps.Geocoder;
							        	var infowindow = new google.maps.InfoWindow;
							         	document.getElementById("submit").addEventListener("click", function() {
							          		geocodeLatLng(geocoder, map, infowindow);
							        	});
							      	}
							      	google.maps.event.addDomListener(window, "load", initialize);
							      	function geocodeLatLng(geocoder, map, infowindow) {
							        	var input = document.getElementById("latlng").value;
							        	var latlngStr1 = input.split(" , ");
							        	/*console.log(latlngStr1);*/
							        	var latlngStr ="";
							        	var zipstr ="";
							        	for(var i=0; i<latlngStr1.length-1;i++) {
								        	latlngStrs = latlngStr1[i].replace("[ ","");
								        	latlngStr = latlngStrs.split(",", 2);
								        	console.log(latlngStr);
								        	var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
								        	geocoder.geocode({"location": latlng}, function(results, status) {
								          		if (status === "OK") {
									            	if (results[0]) {
										              	map.setZoom(11);
										              	var marker = new google.maps.Marker({
										                	position: latlng,
										                	map: map
										              	});
										              	console.log(results[0].address_components);
										              	zip =results[0].address_components[results[0].address_components.length-1];
										              	if(isNaN(zip.long_name)) {
										                 	zip =results[0].address_components[0];
										              	}
										              	zipstr =zip.long_name+","+zipstr;
										              	$("#wp_restaurant_delivery_area").val(zipstr);
										              	infowindow.setContent(results[0].formatted_address);
										              	infowindow.open(map, marker);
									            	} else {
								              			window.alert("No results found");
								            		}
									          	} else {
									            	window.alert("Geocoder failed due to: " + status);
									          	}
							        		});
							      		}
							    	}
							    </script> 
							    <div id="panel"> 
							    	<div id="color-palette"></div> 
							    	<div>
								        <input type="button" id="delete-button" value="Delete Selected Shape">
								        <input type="button" id="submit" value="Selected Shape" data-dismiss="modal">
										<input type="text" id="latlng" value="">
								    </div>
								    <div id="curpos"></div>
								    <div id="cursel"></div>
								    <div id="note">
								    	<small>Note: markers can be selected, but are not graphically indicated; can be deleted, but cannot have their color changed.</small>
							    	</div>
						    	</div>
						    	
						    	<input id="pac-input" style="width:30% !important"  type="text" placeholder="Search Box">
						    	<div id="map" style="height:500px">A</div>
								<div class="modal-footer">
					          		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        	</div>
					        </div>
					    </div> 
				    </div>
			  	</div>
			</div>
		';
		return $modal;
	}
	
	public function submit_modal() {
        ob_start();
        include(WYZ_PLUGIN_ROOT_PATH . 'termsAndConditions.txt');
        $termsAndCondtionsContent = ob_get_clean();

 		$modal = '
		  	<div class="modal" id="termsAndCondtions" role="dialog">
		    	<div class="modal-dialog  modal-lg">
			      	<div class="modal-content">
			        	<div class="modal-header">
			          	<h4 class="modal-title">Terms & Conditions</h4>
		        	</div>
		        	<div class="modal-body">
						<div class="term-condition"> 
							'. $termsAndCondtionsContent .'
					        <div class="modal-footer">
					          	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        </div>
				      	</div> 
			      	</div>
				</div> 
			</div>
		';
		
		return $modal;
	}

	/**
	 * Add Menu Taxonomy
	 */
	public function add_menu_taxonomy() {
		$labels = array(
			'name'                       => _x( 'Restaurant Menus', 'Taxonomy General Name', 'wyzchef' ),
			'singular_name'              => _x( 'Restaurant Menu', 'Taxonomy Singular Name', 'wyzchef' ),
			'menu_name'                  => __( 'Restaurant Menus', 'wyzchef' ),
			'all_items'                  => __( 'All Restaurant Menus', 'wyzchef' ),
			'new_item_name'              => __( 'New Restaurant Menu', 'wyzchef' ),
			'add_new_item'               => __( 'Add New Menu', 'wyzchef' ),
			'edit_item'                  => __( 'Edit Menu', 'wyzchef' ),
			'update_item'                => __( 'Update Menu', 'wyzchef' ),
			'view_item'                  => __( 'View Menu', 'wyzchef' ),
			'add_or_remove_items'        => __( 'Add or remove menus', 'wyzchef' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'wyzchef' ),
			'popular_items'              => __( 'Popular Menus', 'wyzchef' ),
			'search_items'               => __( 'Search Menus', 'wyzchef' ),
			'not_found'                  => __( 'Not Found', 'wyzchef' ),
			'no_terms'                   => __( 'No menus', 'wyzchef' ),
			'items_list'                 => __( 'Menus list', 'wyzchef' ),
			'items_list_navigation'      => __( 'Menus list navigation', 'wyzchef' ),
		);
		
		$args = array(
			'labels'                     => $labels,
			'public'                     => true,
			'show_ui'                    => true,
			'show_in_nav_menus'          => true,
			'hierarchical'               => true,
			'show_admin_column'          => false,
		    'show_in_quick_edit'         => false,
		    'meta_box_cb'                => false,
		);
		register_taxonomy( 'restaurant_menu', array( 'wp_restaurant' ), $args );
	}
  
    /**
     * Add a form field in the new category page
     */

    public function wyz_chef_orders(){
    	$page_title = 'WYZ Orders';   $menu_title = 'WYZ Orders';
    	$capability = 'read';
    	$menu_slug  = 'wyz-chef-orders';   
    	$function   = array($this,'wyz_chef_orders_page');   
    	$icon_url   = WYZ_PLUGIN_ROOT_URL . 'assets/images/payment.png';   
    	$position   = 24;    
    	add_menu_page( $page_title, $menu_title, $capability, $menu_slug,  $function, $icon_url, $position );
    }

    public function wyz_chef_orders_page() {
    	global $wpdb; 
    	?>
    	<style type = "text/css">
		  	@media print {
		  		#adminmenumain, #wpadminbar, .wrap > h1, 
		  		.welcome-panel .dish-single-item, 
				.modal-header, .modal-footer, 
				#wpfooter {
					 display: none !important;
		     	}

		     	#wpbody-content, .welcome-panel {
				    border: 0;
				    height: 0;
				    margin: 0;
				    padding: 0;
				}

				.modal.in .modal-dialog {
					margin-top: 0 !important;
				}
		  	}
		</style>

        <div class="wrap">
          	<h1>Orders</h1>
    	    <div id="Order" class="welcome-panel">
    	      	<div class="row">
    	      	<?php
		        	$orderTable    = $wpdb->prefix . 'wyz_restaurant_order';
		        	$deliveryTable = $wpdb->prefix . 'wyz_restaurant_order_delivery_details';
		        	$order_details = $wpdb->get_results("SELECT o.*, d.delivery_date FROM $orderTable AS o INNER JOIN $deliveryTable AS d ON o.`id` = d.`order_id` ORDER BY `created_at` DESC", ARRAY_A);
       
		        	if(count($order_details) > 0) {
		          		foreach ($order_details as $order) {
		                	$user_info = get_userdata($order['user_id']);
		                	?>
		              		<div class="col-md-4 col-sm-6 col-xs-12">
		                		<div class="dish-single-item">
					               	<span>Order Id: #<?php echo $order['id'];?></span><br>
					               	<span>Total Amount: $<?php echo $order['total'];?></span><br>
					               	<?php echo $user_info->first_name .  " " . $user_info->last_name . "<br>"; ?>
								   	Delivery Address: <?php echo $this->delivery_address($order['id']);?><br>
								   	<?php echo $this->delivery_date($order['id'])."<br>"; ?>
					               	Created At: <?php echo date('F d, Y h:i A', strtotime($order['created_at']))."<br>"; ?>
					            	<span class="completed-<?php echo $order['id']; ?>">Status: 
					            		<?php if($order['status'] == 0) { ?>
											<button type="button" data-restro = "<?php echo $order['restaurant_id'];?>"data-user = "<?php echo $order['user_id'];?>" data-status="2" data-id="<?php echo $order['id'];?>" class="btn btn-primary reject rejected-<?php echo $order['id'];?>">Decline</button>
		                  
		                					<button type="button" data-restro = "<?php echo $order['restaurant_id'];?>"data-user = "<?php echo $order['user_id'];?>" data-status="1" data-id="<?php echo $order['id'];?>" class="btn btn-primary approve approved-<?php echo $order['id'];?>">Accept</button>
			       						<?php } elseif ($order['status'] == 1) { ?>
								           	<span data-id="<?php echo $order['id'];?>" class="complete">Confirmed</span>
								            <button type="button" data-status="3" data-id="<?php echo $order['id'];?>" class="btn btn-primary complete completed-<?php echo $order['id'];?>">Mark As Complete</button>
								        <?php } elseif ($order['status'] == 2) {
								           	echo "Rejected";
								        } elseif ($order['status'] == 3) {
								           echo "Completed";
								        } ?>
								    </span>
		 
						         	<div class="order-btn">
					                    <style> .order-btn .modal.in {opacity: 1; } </style> 
					                    <a href="javascript:void(0)" class="orders-popup-order"data-toggle="modal" data-target="#order-det-<?php echo $order['id'];?>">View Order</a>
					         		</div> 
				            	</div>
				            </div>

				            <div class="invoice-popup">
				                <div class="modal fade" id="order-det-<?= $order['id'] ?>" tabindex="-1">
				                  <div class="modal-dialog" role="document">
				                    <div class="modal-content">
				                      <div class="modal-header invoice_header">
				                        <button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close">
				                          <span aria-hidden="true">&times;</span>
				                        </button>
				                      </div>
				                      <div class="modal-body" id="content">
				                               
				                          <div id="wpPUSResult">
				                            <div class="resultWrapper">
				                              <div class="header_wrapper">
				                                <table class="header_table">
				                                  <tbody>
				                                    <tr>
				                                      <td class="dateTableData">
				                                        <span><img src="<?= WYZ_PLUGIN_ROOT_URL ?>assets/images/wyzchef.png" style="height: auto;"></span><br />
				                                      </td>
				                                      <td style=text-align:left >
				                                        <span>HELP: +65 8891 3599</span>
				                                      </td>
				                                      <td>
				                                      <span>EMAIL: order@wyzchef.com</span>
				                                      </td>
				                                    </tr>
				                                  </tbody>
				                                </table>
				                              </div>
				                              <?= $this->dish_details($order) ?>
				                            </div>
				                          </div>
				                     </div>
				                     <div id="editor"></div>
				                      <div class="modal-footer">
				                        <div class="clearfix"></div>
				                        <button type="button" class="btn btn-secondary pdf_download download-order-info">Print Order</button>
				                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				                      </div>
				                    </div>
				                  </div>
				                </div>
			                </div>
		        		<?php }
		          	} 
		        ?>
    			</div>
			</div>
		</div>

		<script type="text/javascript">
            jQuery(document).on("click", ".complete", function(e) {
             
             	var id = jQuery(this).attr("data-id");
             	var status = jQuery(this).attr("data-status");

             	jQuery.ajax({
	                type: "POST",
	                url: "<?php echo admin_url("admin-ajax.php");?>",
	                data: { 
	                    action: "wyz_complete_order",
	                    status:status, 
	                    id:id
	                },
			        success: function(result) {
			            jQuery(".complete-"+id).hide();
			            jQuery("span.completed-"+id).html(result);
			        },
			        error: function(result) {
			            alert("error");
			        }
		        });
            });

            jQuery(document).on("click", ".approve", function(e){
             	var id = jQuery(this).attr("data-id");
             	var status = jQuery(this).attr("data-status");
             	var restro_id = jQuery(this).attr("data-restro");
             	var user_id = jQuery(this).attr("data-user");
	            jQuery.ajax({
	                type: "POST",
	                url: "<?php echo admin_url("admin-ajax.php");?>",
	                data: { 
	                    action: "wyz_approve_order",
	                    restro_id:restro_id,
	                    user_id:user_id,
	                    status:status, 
	                    id:id
	                   
	                },
			        success: function(result) {
			           jQuery(".approved-"+id).hide();
			           jQuery(".rejected-"+id).hide();
			           jQuery("span.completed-"+id).html(result);
			        },
			        error: function(result) {
			            alert("error");
			        }
			    });
            });

            jQuery(document).on("click", ".reject", function(e){
             	var id = jQuery(this).attr("data-id");
             	var status = jQuery(this).attr("data-status");
             	var restro_id = jQuery(this).attr("data-restro");
             	var user_id = jQuery(this).attr("data-user");
	            jQuery.ajax({
	                type: "POST",
	                url: "<?php echo admin_url("admin-ajax.php");?>",
	                data: { 
	                    action: "wyz_decline_order",
	                    restro_id:restro_id,
	                    user_id:user_id,
	                    status:status, 
	                    id:id
	                   
	                },
			        success: function(result) {
			           jQuery(".approved-"+id).hide();
			           jQuery(".rejected-"+id).hide();
			           jQuery("span.completed-"+id).append(result);
			        },
			        error: function(result) {
			            alert("Some error occured. Please refresh the page");
			        }
			    });
            });

	     	jQuery(document.body).on("click", ".download-order-info", function(){
		        window.print();

		        return false;
			});
		</script>
   	<?php }

     /**
     * Add a form field in the new category page
     */
    public function addMenuImage( $taxonomy ) { ?>
     	<div class="form-field term-group">
	       	<label for="wyz-menu-taxonomy-image-id"><?php _e( 'Image', 'showcase' ); ?></label>
	       	<input type="hidden" id="wyz-menu-taxonomy-image-id" name="wyz-menu-taxonomy-image-id" class="custom_media_url" value="" />
	       	<div id="menu-image-wrapper"></div>
	       	<p>
	         	<input type="button" class="button button-secondary showcase_tax_media_button" id="showcase_tax_media_button" name="showcase_tax_media_button" value="<?php _e( 'Add Image', 'showcase' ); ?>" />
	         	<input type="button" class="button button-secondary showcase_tax_media_remove" id="showcase_tax_media_remove" name="showcase_tax_media_remove" value="<?php _e( 'Remove Image', 'showcase' ); ?>" />
	       	</p>
     	</div>
   	<?php }
    /**
     * Edit the form field
     */
    public function editMenuImage( $term, $taxonomy ) { ?>
      	<tr class="form-field term-group-wrap">
        	<th scope="row">
          		<label for="wyz-menu-taxonomy-image-id"><?php _e( 'Image', 'showcase' ); ?></label>
        	</th>
        	<td>
          	<?php $imageId = get_term_meta( $term->term_id, 'wyz-menu-taxonomy-image-id', true ); ?>
          	<input type="hidden" id="wyz-menu-taxonomy-image-id" name="wyz-menu-taxonomy-image-id" value="<?php echo esc_attr( $imageId ); ?>">
          	<div id="menu-image-wrapper">
            	<?php if( $imageId ) {
              		echo wp_get_attachment_image( $imageId, 'thumbnail' );
            	} ?>
          	</div>
          	<p>
            	<input type="button" class="button button-secondary showcase_tax_media_button" id="showcase_tax_media_button" name="showcase_tax_media_button" value="<?php _e( 'Update Image', 'showcase' ); ?>" />
            	<input type="button" class="button button-secondary showcase_tax_media_remove" id="showcase_tax_media_remove" name="showcase_tax_media_remove" value="<?php _e( 'Remove Image', 'showcase' ); ?>" />
          	</p>
        	</td>
      	</tr>
   	<?php }
    /**
     * Save/Update the form field value
     */
    public function saveMenuImage( $term_id, $tt_id ) {
	$term_id = intval($term_id);
	if( isset( $_POST['wyz-menu-taxonomy-image-id'] ) && '' !== $_POST['wyz-menu-taxonomy-image-id'] ) {
        	update_term_meta( $term_id, 'wyz-menu-taxonomy-image-id', intval( $_POST['wyz-menu-taxonomy-image-id'] ) );
      	} else {
        	update_term_meta( $term_id, 'wyz-menu-taxonomy-image-id', '' );
      	}
    }
    /**
     * Add related column header
     * 
     * $columns     Array
     */
    public function relatedMenuColumnHeader( $columns ) 
    {
    	$columns["restaurant_menu_image"] = __('Image', 'wyzchef');
        return $columns;
    }
    /**
     * Add related column selected content
     * 
     * $value       Int|Null
     * $columnName  String
     * $termId      Int
     */
    public function relatedMenuColumnContent( $value, $columnName, $termId ) 
    {
    	if ("restaurant_menu_image" === $columnName) {
    		$imageId = get_term_meta( $termId, 'wyz-menu-taxonomy-image-id', true );
    		if( $imageId ) {
          		echo wp_get_attachment_image( $imageId, 'thumbnail' );
        	}
        }
    }
   	/**
     * Load media library scripts
     */
   	public function loadMenuMedia() {
    	if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != 'restaurant_menu' ) {
    	  	return;
    	}
    	wp_enqueue_media();
   	}
 
    /**
     * Enqueue custom scripts
     */
    public function loadMenuJS() {
     	if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != 'restaurant_menu' ) {
       		return;
     	} ?>
     	<script type="text/javascript">
	     	jQuery(document).ready( function($) {
		       	_wpMediaViewsL10n.insertIntoPost = '<?php _e( "Insert", "showcase" ); ?>';
		       	function ct_media_upload(button_class) {
		         	var _custom_media = true, _orig_send_attachment = wp.media.editor.send.attachment;
		         	$('body').on('click', button_class, function(e) {
			           	var button_id = '#'+$(this).attr('id');
			           	var send_attachment_bkp = wp.media.editor.send.attachment;
			           	var button = $(button_id);
			           	_custom_media = true;
			           	wp.media.editor.send.attachment = function(props, attachment) {
			             	if( _custom_media ) {
				               	$('#wyz-menu-taxonomy-image-id').val(attachment.id);
				               	$('#menu-image-wrapper').html('<img class="custom_media_image" src="" 	style="margin:0;padding:0;max-height:100px;float:none;" />');
				               	$( '#menu-image-wrapper .custom_media_image' ).attr( 'src',attachment.url ).css( 'display','block' );
			             	} else {
			               		return _orig_send_attachment.apply( button_id, [props, attachment] );
			             	}
			           	}
			           	wp.media.editor.open(button); return false;
		         	});
		       	}
		       	ct_media_upload('.showcase_tax_media_button.button');
		       	$('body').on('click','.showcase_tax_media_remove',function() {
		         	$('#wyz-menu-taxonomy-image-id').val('');
		         	$('#menu-image-wrapper').html('<img class="custom_media_image" src="" 	style="margin:0;padding:0;max-height:100px;float:none;" />');
		       	});
		       	
		       	$(document).ajaxComplete(function(event, xhr, settings) {
		       		if(typeof settings.data !== 'undefined') {
		       			// console.log('settings.data: ', settings.data);
		       			
			         	var queryStringArr = settings.data.split('&');
			         	if( $.inArray('action=add-tag', queryStringArr) !== -1 ) {
				           	var xml = xhr.responseXML;
				           	$response = $(xml).find('term_id').text();
				           	if($response!="") {
				             	// Clear the thumb image
				             	$('#menu-image-wrapper').html('');
				           	}
			          	}
		          	}
	        	});
	      	});
    	</script>
   	<?php 
   	}

	/**
	 * Add Dish Tags Taxonomy
	 */
	public function add_dish_tags() {
		$labels = array(
			'name'                       => _x( 'Dish Tags', 'Taxonomy General Name', 'wyzchef' ),
			'singular_name'              => _x( 'Dish Tag', 'Taxonomy Singular Name', 'wyzchef' ),
			'menu_name'                  => __( 'Dish Tags', 'wyzchef' ),
			'all_items'                  => __( 'All Dish Tags', 'wyzchef' ),
			'new_item_name'              => __( 'New Dish Tag', 'wyzchef' ),
			'add_new_item'               => __( 'Add New Tag', 'wyzchef' ),
			'edit_item'                  => __( 'Edit Tag', 'wyzchef' ),
			'update_item'                => __( 'Update Tag', 'wyzchef' ),
			'view_item'                  => __( 'View Tag', 'wyzchef' ),
			'add_or_remove_items'        => __( 'Add or remove tags', 'wyzchef' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'wyzchef' ),
			'popular_items'              => __( 'Popular Tags', 'wyzchef' ),
			'search_items'               => __( 'Search Tags', 'wyzchef' ),
			'not_found'                  => __( 'Not Found', 'wyzchef' ),
			'no_terms'                   => __( 'No Tags', 'wyzchef' ),
			'items_list'                 => __( 'Tags list', 'wyzchef' ),
			'items_list_navigation'      => __( 'Tags list navigation', 'wyzchef' ),
		);
		
		$args = array(
			'labels'                     => $labels,
			'public'                     => true,
			'show_ui'                    => true,
			'show_in_nav_menus'          => true,
			'hierarchical'               => false,
			'show_admin_column'          => false,
		    'show_in_quick_edit'         => false,
		    'meta_box_cb'                => false,
		);
		register_taxonomy( 'restaurant_tag', array( 'wp_restaurant' ), $args );
	}
  
    /**
     * Add a form field in the new category page
     */
    public function addCategoryImage( $taxonomy ) { ?>
     	<div class="form-field term-group">
	       	<label for="wyz-category-image-id"><?php _e( 'Image', 'showcase' ); ?></label>
	       	<input type="hidden" id="wyz-category-image-id" name="wyz-category-image-id" class="custom_media_url" value="" />
	       	<div id="category-image-wrapper"></div>
	       	<p>
	         	<input type="button" class="button button-secondary showcase_tax_media_button" id="showcase_tax_media_button" name="showcase_tax_media_button" value="<?php _e( 'Add Image', 'showcase' ); ?>" />
	         	<input type="button" class="button button-secondary showcase_tax_media_remove" id="showcase_tax_media_remove" name="showcase_tax_media_remove" value="<?php _e( 'Remove Image', 'showcase' ); ?>" />
	       	</p>
     	</div>
   	<?php }

    /**
     * Edit the form field
     */
    public function editCategoryImage( $term, $taxonomy ) { ?>
      	<tr class="form-field term-group-wrap">
        	<th scope="row">
          		<label for="wyz-category-image-id"><?php _e( 'Image', 'showcase' ); ?></label>
        	</th>
        	<td>
          	<?php $imageId = get_term_meta( $term->term_id, 'wyz-category-image-id', true ); ?>
          	<input type="hidden" id="wyz-category-image-id" name="wyz-category-image-id" value="<?php echo esc_attr( $imageId ); ?>">
          	<div id="category-image-wrapper">
            	<?php if( $imageId ) {
              		echo wp_get_attachment_image( $imageId, 'thumbnail' );
            	} ?>
          	</div>
          	<p>
            	<input type="button" class="button button-secondary showcase_tax_media_button" id="showcase_tax_media_button" name="showcase_tax_media_button" value="<?php _e( 'Update Image', 'showcase' ); ?>" />
            	<input type="button" class="button button-secondary showcase_tax_media_remove" id="showcase_tax_media_remove" name="showcase_tax_media_remove" value="<?php _e( 'Remove Image', 'showcase' ); ?>" />
          	</p>
        	</td>
      	</tr>
   	<?php }

    /**
     * Save/Update the form field value
     */
    public function saveCategoryImage( $term_id, $tt_id ) {
		$term_id = intval($term_id);
		if( isset( $_POST['wyz-category-image-id'] ) && '' !== $_POST['wyz-category-image-id'] ) {
        	update_term_meta( $term_id, 'wyz-category-image-id', intval( $_POST['wyz-category-image-id'] ) );
      	} else {
        	update_term_meta( $term_id, 'wyz-category-image-id', '' );
      	}
    }

    /**
     * Add related column header
     * 
     * $columns     Array
     */
    public function relatedCategoryColumnHeader( $columns ) {
    	$columns["restaurant_category_image"] = __('Image', 'wyzchef');
        return $columns;
    }

    /**
     * Add related column selected content
     * 
     * $value       Int|Null
     * $columnName  String
     * $termId      Int
     */
    public function relatedCategoryColumnContent( $value, $columnName, $termId ) {
    	if ("restaurant_category_image" === $columnName) {
    		$imageId = get_term_meta( $termId, 'wyz-category-image-id', true );
    		if( $imageId ) {
          		echo wp_get_attachment_image( $imageId, 'thumbnail' );
        	}
        }
    }

   	/**
     * Load media library scripts
     */
   	public function loadCategoryMedia() {
    	if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != 'restaurant_category' ) {
    	  	return;
    	}
    	wp_enqueue_media();
   	}
 
    /**
     * Enqueue custom scripts
     */
    public function loadCategoryJS() {
     	if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != 'restaurant_category' ) {
       		return;
     	} ?>
     	<script type="text/javascript">
	     	jQuery(document).ready( function($) {
		       	_wpMediaViewsL10n.insertIntoPost = '<?php _e( "Insert", "showcase" ); ?>';
		       	function ct_media_upload(button_class) {
		         	var _custom_media = true, _orig_send_attachment = wp.media.editor.send.attachment;
		         	$('body').on('click', button_class, function(e) {
			           	var button_id = '#'+$(this).attr('id');
			           	var send_attachment_bkp = wp.media.editor.send.attachment;
			           	var button = $(button_id);
			           	_custom_media = true;
			           	wp.media.editor.send.attachment = function(props, attachment) {
			             	if( _custom_media ) {
				               	$('#wyz-category-image-id').val(attachment.id);
				               	$('#category-image-wrapper').html('<img class="custom_media_image" src="" 	style="margin:0;padding:0;max-height:100px;float:none;" />');
				               	$( '#category-image-wrapper .custom_media_image' ).attr( 'src',attachment.url ).css( 'display','block' );
			             	} else {
			               		return _orig_send_attachment.apply( button_id, [props, attachment] );
			             	}
			           	}
			           	wp.media.editor.open(button); return false;
		         	});
		       	}
		       	ct_media_upload('.showcase_tax_media_button.button');
		       	$('body').on('click','.showcase_tax_media_remove',function() {
		         	$('#wyz-category-image-id').val('');
		         	$('#category-image-wrapper').html('<img class="custom_media_image" src="" 	style="margin:0;padding:0;max-height:100px;float:none;" />');
		       	});
		       	
		       	$(document).ajaxComplete(function(event, xhr, settings) {
		       		if(typeof settings.data !== 'undefined') {
		       			// console.log('settings.data: ', settings.data);
		       			
			         	var queryStringArr = settings.data.split('&');
			         	if( $.inArray('action=add-tag', queryStringArr) !== -1 ) {
				           	var xml = xhr.responseXML;
				           	$response = $(xml).find('term_id').text();
				           	if($response!="") {
				             	// Clear the thumb image
				             	$('#category-image-wrapper').html('');
				           	}
			          	}
		          	}
	        	});
	      	});
    	</script>
   	<?php 
   	}
  
    /**
     * Add coupon sub menu for admin area
     */
    public function wyzchef_coupons_menu(){
        $parent_menu = 'wyz-chef-orders';
        $page_title = 'Coupons';
        $menu_title = 'Coupons';
        $capability = 'read';
        $menu_slug  = 'wyzchef-coupons';
        $call_back_function   = array( $this, 'wyzchef_coupons' );
        
        add_submenu_page(
            $parent_menu, 
            $page_title, 
            $menu_title, 
            $capability, 
            $menu_slug, 
            $call_back_function
        );
    }
  
    /**
     * Add coupon page for admin area
     */
    public function wyzchef_coupons(){
        include(WYZ_PLUGIN_ROOT_PATH . 'admin/coupons.php');
    }
  
    /**
     * Add coupon sub menu for admin area
     */
    public function wyzchef_add_coupon_menu(){
        $parent_menu = 'wyz-chef-orders';
        $page_title = 'Add Coupon';
        $menu_title = 'Add Coupon';
        $capability = 'read';
        $menu_slug  = 'wyzchef-add-coupon';
        $call_back_function   = array( $this, 'wyzchef_add_coupon' );
        
        add_submenu_page(
            $parent_menu, 
            $page_title, 
            $menu_title, 
            $capability, 
            $menu_slug, 
            $call_back_function
        );
    }
  
    /**
     * Add coupon page for admin area
     */
    public function wyzchef_add_coupon(){
        include(WYZ_PLUGIN_ROOT_PATH . 'admin/add_coupon.php');
    }

    /**
     * Save Coupon Data
     * @var $_POST
     * 
     * @return null
     */
    public function wyzchef_save_coupon() {
        if(session_id() == '')  {
            session_start();
        }

        $msgArray = array(
            'success' => array(
                'status' => false,
                'msg' => '',
            ), 
            'error' => array(
                'status' => false,
                'msg' => '',
            ), 
        );

        $current_user = wp_get_current_user();

        if (isset($_POST['coupon']) && !empty($_POST['coupon'])) {
            $couponData = $_POST['coupon'];
        } else {
            $msgArray['error']['msg'] = 'Coupon data cannot empty.';
            $msgArray['error']['status'] = true;

            $_SESSION["msgArray"] = $msgArray;

            wp_safe_redirect( wp_get_referer() );
        }

        global $wpdb;
        if (!isset($couponData['id']) || empty($couponData['id'])) {
            $wpdb->insert(
                $wpdb->prefix .'wyz_restaurant_order_coupons',
                $dataArray = array(
                    'code' => $couponData['code'],
                    'description' => $couponData['description'],
                    'type' => $couponData['type'],
                    'value' => $couponData['value'],
                    'status' => $couponData['status'],
                    'expiry' => $couponData['expiry'],
                )
            );
            $couponData['id'] = $wpdb->insert_id;
        } else {
            $wpdb->update(
                $wpdb->prefix .'wyz_restaurant_order_coupons',
                array(
                    'code' => $couponData['code'],
                    'description' => $couponData['description'],
                    'type' => $couponData['type'],
                    'value' => $couponData['value'],
                    'status' => $couponData['status'],
                    'expiry' => $couponData['expiry'],
                ),
                array(
                    'id' => $couponData['id'],
                )
            );
        }

        $msgArray['success']['msg'] = 'Coupon data saved';
        $msgArray['success']['status'] = true;

        $_SESSION["msgArray"] = $msgArray;

        wp_safe_redirect( wp_get_referer()."&id={$couponData[id]}" );
    }

    /**
     * Delete Coupon Data
     * @var $_POST
     * 
     * @return null
     */
    public function wyzchef_delete_coupon() {
        if(session_id() == '')  {
            session_start();
        }

        $msgArray = array(
            'success' => array(
                'status' => false,
                'msg' => '',
            ), 
            'error' => array(
                'status' => false,
                'msg' => '',
            ), 
        );

        $current_user = wp_get_current_user();

        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $couponId = $_GET['id'];
        } else {
            $msgArray['error']['msg'] = 'Invalid Coupon data.';
            $msgArray['error']['status'] = true;

            $_SESSION["msgArray"] = $msgArray;

            wp_safe_redirect( wp_get_referer() );
        }

        global $wpdb;
        $queryFlag = $wpdb->delete(
            $wpdb->prefix .'wyz_restaurant_order_coupons',
            array(
                'id' => $couponId,
            )
        );

        $msgArray['success']['msg'] = 'Coupon data deleted';
        $msgArray['success']['status'] = true;

        $_SESSION["msgArray"] = $msgArray;

        wp_safe_redirect( wp_get_referer() );
    }

    private function uploadBase64DataImage($imgData = '', $fileName = '') 
    {
        if (preg_match('/^data:image\/(\w+);base64,/', $imgData, $imgType)) {
            // Check the valid image extensions
            $imgType = strtolower($imgType[1]);

            // Decode the base64 and check for valid
            $imgData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $imgData));
            if (false === $imgData) {
                die('Decode the base64 content failed.');
            }

            $uploadDir = wp_upload_dir();
            $uploadPath = str_replace( '/', DIRECTORY_SEPARATOR, $uploadDir['path'] ) . DIRECTORY_SEPARATOR;

            if (empty($fileName)) {
            	$hashedFileName = (microtime(true)*10000) .'.'. $imgType;
            } else {
            	$hashedFileName = $fileName .'-'. (microtime(true)*10000) .'.'. $imgType;
            }
            
            $imageUpload = file_put_contents( $uploadPath . $hashedFileName, $imgData );

            // Check function to handle upload
            if( !function_exists( 'wp_handle_sideload' ) ) {
                require_once( ABSPATH . 'wp-admin/includes/file.php' );
            }

            // Array based on $_FILE as seen in PHP file uploads
            $file = array(
                'name'     => $hashedFileName,
                'type'     => "image/{$imgType}",
                'tmp_name' => $uploadPath . $hashedFileName,
                'size'     => filesize($uploadPath . $hashedFileName),
                'error'    => 0,
            );

            $overrides = array(
                // Tells WordPress to not look for the POST form
                // fields that would normally be present as
                // we downloaded the file from a remote server, so there
                // will be no form fields
                // Default is true
                'test_form' => false,
            );

            // Move the temporary file into the uploads directory
            $fileObj = wp_handle_sideload( $file, $overrides );

            if ( !empty( $fileObj['error'] ) ) {
                die('Some internal server error occured during upload.');
            } else {
                $fileBaseName = basename($fileObj['file']);
                list($croppedImgWidth, $croppedImgHeight) = getimagesize($uploadPath . $fileBaseName);
                
                $file = $fileObj['file'];
                $attachment = array(
                    'post_mime_type' => $fileObj['type'],
                    'post_title' => preg_replace('/\.[^.]+$/', '', $fileBaseName),
                    'post_content' => '',
                    'post_status' => 'inherit',
                    'guid' => $uploadDir['url'] . '/' . $fileBaseName
                );
                
                $attachId = wp_insert_attachment( $attachment, $file, 289 );

                // Check function to generate attachment metadata
                if( !function_exists( 'wp_generate_attachment_metadata' ) ) {
                    require_once(ABSPATH . 'wp-admin/includes/image.php');
                }
                $attachData = wp_generate_attachment_metadata( $attachId, $file );
                wp_update_attachment_metadata( $attachId, $attachData );

                return [
                    'attachId' => $attachId,
                    'attachUrl' => $uploadDir['url'] . '/' . $fileBaseName,
                    'attachName' => $fileBaseName,
                ];
            }
        } else {
            die('Did not match data URI with image data.');
        }
    }

    public function saveRestaurantData() 
    {
		if( isset( $_POST['wyzchef_submit_user_meta_nonce'] ) && wp_verify_nonce( $_POST['wyzchef_submit_user_meta_nonce'], 'wyzchef_submit_user_meta_form_nonce') ) {
			global $wpdb;
			$restaurantId = 0;
			$categoryIDs = $_POST['wp_restaurant_type'];

			if (!isset($_POST['wp_restaurant_delivery_everywhere'])) {
				$_POST['wp_restaurant_delivery_everywhere'] = '';
			}

            if (!isset($_POST['terms_conditions'])) {
                $_POST['terms_conditions'] = '1';
            }

            if (isset($_POST['restaurantId']) && $_POST['restaurantId']) {
            	$_POST['wp_restaurant_list_no'] = get_post_meta($_POST['restaurantId'], 'wp_restaurant_list_no', true);

				$restaurant = array(
					'ID'    		=> $_POST['restaurantId'],
					'post_title'    => $_POST['title'],
					'post_content'  => $_POST['description'],
					'post_type'     => 'wp_restaurant',
					'post_category' => ($categoryIDs),
				);

				$restaurantId = wp_update_post($restaurant);
			} else {
				$restaurant = array(
					'post_title'    => $_POST['title'],
					'post_content'  => $_POST['description'],
					'post_status'   => 'draft',
					'post_type'     => 'wp_restaurant',
					'post_category' => ($categoryIDs),
				);

				$restaurantId = wp_insert_post($restaurant);
			}

            $includeImageLib = false;
            for($i=0; $i < sizeof($_FILES['wp_restaurant_gallery']['name']); $i++) {
				$_FILES['image']['name']   = $_FILES['wp_restaurant_gallery']['name'][$i];
				$_FILES['image']['tmp_name']  = $_FILES['wp_restaurant_gallery']['tmp_name'][$i];
				if($_FILES['image']['name']!='') {
					$upload = wp_upload_bits( $_FILES['image']['name'], null, file_get_contents( $_FILES['image']['tmp_name'] ) );
				    $wp_filetype = wp_check_filetype( basename( $upload['file'] ), null );
				    $wp_upload_dir = wp_upload_dir();
				    $attachment = array(
				        'guid'			 	=> $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $upload['file'] ),
				        'post_mime_type' 	=> $wp_filetype['type'],
				        'post_title' 	 	=> preg_replace('/\.[^.]+$/', '', basename( $upload['file'] )),
				        'post_content' 	 	=> '',
				        'post_status' 	 	=> 'inherit',
			        	'post_parent' 	 	=> $restaurantId,
				    );

				    $attach_id = wp_insert_attachment( $attachment, $upload['file'], $restaurantId );
				    if(!$includeImageLib) {
				    	require_once(ABSPATH . 'wp-admin/includes/image.php');
				    	$includeImageLib = true;
				    }
				    
				    $attach_data = wp_generate_attachment_metadata( $attach_id, $upload['file'] );
				    wp_update_attachment_metadata( $attach_id, $attach_data );
				    update_post_meta( $restaurantId, 'wp_restaurant_gallery', $attach_id );
				    $gallarimgID[] = $attach_id;
				}
			}
			if(isset($gallarimgID) && !empty($gallarimgID)) {
				$_POST['wp_restaurant_gallery']= $gallarimgID;
			}

			$_FILES['image']['name']   = $_FILES['wp_restaurant_logo']['name'];
			$_FILES['image']['tmp_name']  = $_FILES['wp_restaurant_logo']['tmp_name'];
			if($_FILES['image']['name']!='') {
				$upload = wp_upload_bits( $_FILES['image']['name'], null, file_get_contents( $_FILES['image']['tmp_name'] ) );
			    $wp_filetype = wp_check_filetype( basename( $upload['file'] ), null );
			    $wp_upload_dir = wp_upload_dir();
			    $attachment = array(
			        'guid'			 	=> $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $upload['file'] ),
			        'post_mime_type' 	=> $wp_filetype['type'],
			        'post_title' 	 	=> preg_replace('/\.[^.]+$/', '', basename( $upload['file'] )),
			        'post_content' 	 	=> '',
			        'post_status' 	 	=> 'inherit',
			        'post_parent' 	 	=> $restaurantId,
			    );

			    $attach_id = wp_insert_attachment( $attachment, $upload['file'], $restaurantId );
			    if(!$includeImageLib) {
			    	require_once(ABSPATH . 'wp-admin/includes/image.php');
			    	$includeImageLib = true;
			    }

			    $attach_data = wp_generate_attachment_metadata( $attach_id, $upload['file'] );
			    wp_update_attachment_metadata( $attach_id, $attach_data );
			    update_post_meta( $restaurantId, '_thumbnail_id', $attach_id );
			    $logomgID = $attach_id;
			}
			if(!empty($logomgID)) {
				$_POST['wp_restaurant_logo']= $logomgID;
			}

			$_FILES['image']['name']   = $_FILES['wp_restaurant_list_img']['name'];
			$_FILES['image']['tmp_name']  = $_FILES['wp_restaurant_list_img']['tmp_name'];
			if($_FILES['image']['name']!='') {
				$upload = wp_upload_bits( $_FILES['image']['name'], null, file_get_contents( $_FILES['image']['tmp_name'] ) );
			    $wp_filetype = wp_check_filetype( basename( $upload['file'] ), null );
			    $wp_upload_dir = wp_upload_dir();
			    $attachment = array(
			        'guid'			 	=> $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $upload['file'] ),
			        'post_mime_type' 	=> $wp_filetype['type'],
			        'post_title' 	 	=> preg_replace('/\.[^.]+$/', '', basename( $upload['file'] )),
			        'post_content' 	 	=> '',
			        'post_status' 	 	=> 'inherit',
			        'post_parent' 	 	=> $restaurantId,
			    );

			    $attach_id = wp_insert_attachment( $attachment, $upload['file'], $restaurantId );
			    if(!$includeImageLib) {
			    	require_once(ABSPATH . 'wp-admin/includes/image.php');
			    	$includeImageLib = true;
			    }

			    $attach_data = wp_generate_attachment_metadata( $attach_id, $upload['file'] );
			    wp_update_attachment_metadata( $attach_id, $attach_data );
			    update_post_meta( $restaurantId, 'wp_restaurant_list_img', $attach_id );
			    $restaurantListImg = $attach_id;
			}
			if(!empty($restaurantListImg)) {
				$_POST['wp_restaurant_list_img']= $restaurantListImg;
			}

			foreach ($_POST as $key => $value) {
				if(
					($key != 'title')
					&& ($key != 'description')
					&& ($key != 'postID')
					&& ($key != 'restaurant_services')
					&& ($key != 'menu')
				) {
				    update_post_meta($restaurantId, $key, $value);
				}
			}

			### Category Post Relation ###
			// if(is_array($categoryIDs)) {
			// 	for($k=0; $k<sizeof($categoryIDs); $k++) {
			// 		$wpdb->query( $wpdb->prepare("INSERT IGNORE INTO {$wpdb->prefix}term_relationships (object_id, term_taxonomy_id) VALUES ( %d, %d)",array($restaurantId, $categoryIDs[$k])));
			// 	}
			// }
			wp_set_post_categories($restaurantId, $categoryIDs );

			$deletedOptions = trim($_POST['deletedOptions']);
			if ($deletedOptions) {
				$wpdb->query("
					DELETE FROM {$wpdb->prefix}wyz_restaurant_service_options 
					WHERE id IN($deletedOptions)
				");
			}

			$deletedServices = trim($_POST['deletedServices']);
			if ($deletedServices) {
				$wpdb->query("
					DELETE FROM {$wpdb->prefix}wyz_restaurant_services 
					WHERE id IN($deletedServices)
				");
			}

			$restaurantServices = json_decode(str_replace("\\", "",$_POST['restaurant_services']));
			if ($restaurantServices) {
				foreach ($restaurantServices as $key1 => $service) {
					if (empty($service)) {
						continue;
					}

					if (empty($service->id)) {
						$wpdb->insert(
							"{$wpdb->prefix}wyz_restaurant_services",
							array(
								'type' => $service->type,
								'quantityRequired' => $service->quantityRequired,
								'question' => htmlspecialchars(str_replace('\\', '', $service->question), ENT_QUOTES),
								'restaurant_id' => $restaurantId,
							)
						);

						$serviceId = $wpdb->insert_id;
						
						foreach ($service->options as $key2 => $option) {
							$wpdb->insert(
								"{$wpdb->prefix}wyz_restaurant_service_options",
								array(
									'name' => htmlspecialchars(str_replace('\\', '', $option->name), ENT_QUOTES),
									'price' => $option->price,
									'tax' => $option->tax,
									'total' => $option->total,
									'service_id' => $serviceId,
								)
							);
						}
					} else {
						$wpdb->update(
							"{$wpdb->prefix}wyz_restaurant_services",
							array(
								'type' => $service->type,
								'quantityRequired' => $service->quantityRequired,
								'question' => htmlspecialchars(str_replace('\\', '', $service->question), ENT_QUOTES),
							),
							array(
								'id' => $service->id
							)
						);
						
						foreach ($service->options as $key2 => $option) {
							if (empty($option->id)) {
								$wpdb->insert(
									"{$wpdb->prefix}wyz_restaurant_service_options",
									array(
										'name' => htmlspecialchars(str_replace('\\', '', $option->name), ENT_QUOTES),
										'price' => $option->price,
										'tax' => $option->tax,
										'total' => $option->total,
										'service_id' => $service->id,
									)
								);

							} else {
								$wpdb->update(
									"{$wpdb->prefix}wyz_restaurant_service_options",
									array(
										'name' => htmlspecialchars(str_replace('\\', '', $option->name), ENT_QUOTES),
										'price' => $option->price,
										'tax' => $option->tax,
										'total' => $option->total,
										'service_id' => $service->id,
									),
									array(
										'id' => $option->id
									)
								);
							}
						}
					}
				}
			}

			if (!empty($_POST['menu_change']) && isset($_POST['menu_dishes'])) {
				// $menuDishes = str_replace('\\', "", $_POST['menu_dishes']);
				$menuDishes = stripcslashes($_POST['menu_dishes']);
				$menuDishes = json_decode( $menuDishes, true);

				if (count($menuDishes['deleteMenuId'])) {
					foreach ($menuDishes['deleteMenuId'] as $key => $menuId) {
	                    $selectQuery = "
	                        SELECT dm.id FROM `{$wpdb->prefix}wyz_restaurant_dish_modifiers` dm
	                        INNER JOIN `{$wpdb->prefix}wyz_restaurant_dishes` d ON dm.dish_id = d.id
	                        WHERE d.menu_id = {$menuId} AND d.restaurant_id = {$restaurantId}
	                    ";
	                    $dishModifierIds = $wpdb->get_col($selectQuery);

	                    foreach ($dishModifierIds as $key => $modifierId) {
	                        $wpdb->delete(
	                            $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
	                            array(
	                                'modifier_id' => $modifierId,
	                            )
	                        );
	                    }

	                    $wpdb->delete(
	                        $wpdb->prefix .'wyz_restaurant_dish_modifiers',
	                        array(
	                            'dish_id' => $dish['deletedishId'],
	                        )
	                    );

						$wpdb->delete(
							$wpdb->prefix .'wyz_restaurant_dishes',
							array(
								'menu_id' => $menuId,
								'restaurant_id' => $restaurantId,
							)
						);
					}
				}

				if (count($menuDishes['menus'])) {
					foreach ($menuDishes['menus'] as $menuId => $menu) {
						if (count(($menu['deletedishId']))) {
							foreach ($menu['deletedishId'] as $key => $dishId) {
								$selectQuery = "
	                                SELECT dm.id FROM `{$wpdb->prefix}wyz_restaurant_dish_modifiers` dm
	                                INNER JOIN `{$wpdb->prefix}wyz_restaurant_dishes` d ON dm.dish_id = d.id
	                                WHERE d.id = {$dishId}
	                            ";
	                            $dishModifierIds = $wpdb->get_col($selectQuery);

	                            foreach ($dishModifierIds as $key => $modifierId) {
	                                $wpdb->delete(
	                                    $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
	                                    array(
	                                        'modifier_id' => $modifierId,
	                                    )
	                                );
	                            }

	                            $wpdb->delete(
	                                $wpdb->prefix .'wyz_restaurant_dish_modifiers',
	                                array(
	                                    'dish_id' => $dishId,
	                                )
	                            );

								$wpdb->delete(
									$wpdb->prefix .'wyz_restaurant_dishes',
									array(
										'id' => $dishId,
									)
								);
							}
						}

						if (count(($menu['dishes']))) {
							foreach ($menu['dishes'] as $key => $dish) {
								if (!empty($dish['image']) && !filter_var($dish['image'], FILTER_VALIDATE_URL)) {
									$dishImgObj = $this->uploadBase64DataImage($dish['image']);
									$dish['image'] = $dishImgObj['attachUrl'];
								}

								if (!isset($dish['id']) || empty($dish['id'])) {
									$wpdb->insert(
										$wpdb->prefix .'wyz_restaurant_dishes',
										array(
											'name' => htmlspecialchars(str_replace('\\', '', $dish['name']), ENT_QUOTES),
											'description' => htmlspecialchars(str_replace('\\', '', $dish['description']), ENT_QUOTES, 'UTF-8'),
											'image' => $dish['image'],
											'price' => $dish['price'],
		                                    'tax' => $dish['tax'],
											'min_quantity' => $dish['min_quantity'],
											'min_guest_serve' => $dish['min_guest_serve'],
											'max_guest_serve' => $dish['max_guest_serve'],
											'lead_time' => $dish['lead_time'],
											'tags' => implode(',', $dish['tags']).'',
		                                    'having_modifiers' => $dish['having_modifiers'],
											'menu_id' => $dish['menu_id'],
											'restaurant_id' => $restaurantId,
											'status' => 1,
										)
									);
		                            $dishId = $wpdb->insert_id;

		                            if (isset($dish['modifiers']) && !empty($dish['modifiers'])) {
		                                foreach ($dish['modifiers'] as $key => $modifier) {
		                                    $wpdb->insert(
		                                        $wpdb->prefix .'wyz_restaurant_dish_modifiers',
		                                        array(
		                                            'prompt_text' => htmlspecialchars(str_replace('\\', '', $modifier['prompt_text']), ENT_QUOTES),
		                                            'modifier_type' => $modifier['modifier_type'],
		                                            'min' => $modifier['min'],
		                                            'max' => $modifier['max'],
		                                            'dish_id' => $dishId,
		                                        )
		                                    );
		                                    $modifierId = $wpdb->insert_id;

		                                    if (isset($modifier['items']) && !empty($modifier['items'])) {
		                                        foreach ($modifier['items'] as $key => $item) {
		                                            $modifierItemId = $wpdb->insert(
		                                                $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
		                                                array(
		                                                    'text' => $item['text'],
		                                                    'price' => $item['price'],
		                                                    'tax' => $item['tax'],
		                                                    'total' => $item['total'],
		                                                    'modifier_id' => $modifierId,
		                                                )
		                                            );
		                                        }
		                                    }
		                                }
		                            }
								} else {
									$wpdb->update(
										$wpdb->prefix .'wyz_restaurant_dishes',
										array(
											'name' => htmlspecialchars($dish['name'], ENT_QUOTES, 'UTF-8'),
											'description' => htmlspecialchars($dish['description'], ENT_QUOTES, 'UTF-8'),
											'image' => $dish['image'],
											'price' => $dish['price'],
		                                    'tax' => $dish['tax'],
											'min_quantity' => $dish['min_quantity'],
											'min_guest_serve' => $dish['min_guest_serve'],
											'max_guest_serve' => $dish['max_guest_serve'],
											'lead_time' => $dish['lead_time'],
											'tags' => implode(',', $dish['tags']).'',
		                                    'having_modifiers' => $dish['having_modifiers'],
											'menu_id' => $dish['menu_id'],
											'restaurant_id' => $restaurantId,
											'status' => 1,
										),
										array(
											'id' => $dish['id'],
										)
									);

		                            if (isset($dish['delete_modifiers']) && !empty($dish['delete_modifiers'])) {
		                                foreach ($dish['delete_modifiers'] as $key => $deleteModifier) {
		                                    $wpdb->delete(
		                                        $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
		                                        array(
		                                            'modifier_id' => $deleteModifier,
		                                        )
		                                    );

		                                    $wpdb->delete(
		                                        $wpdb->prefix .'wyz_restaurant_dish_modifiers',
		                                        array(
		                                            'id' => $deleteModifier,
		                                        )
		                                    );
		                                }
		                            }

		                            if (isset($dish['modifiers']) && !empty($dish['modifiers'])) {
		                                foreach ($dish['modifiers'] as $key => $modifier) {
		                                    if (!isset($modifier['id']) || empty($modifier['id'])) {
		                                        $wpdb->insert(
		                                            $wpdb->prefix .'wyz_restaurant_dish_modifiers',
		                                            array(
		                                                'prompt_text' => htmlspecialchars(str_replace('\\', '', $modifier['prompt_text']), ENT_QUOTES),
		                                                'modifier_type' => $modifier['modifier_type'],
		                                                'min' => $modifier['min'],
		                                                'max' => $modifier['max'],
		                                                'dish_id' => $dish['id'],
		                                            )
		                                        );
		                                        $modifierId = $wpdb->insert_id;
		                                    } else {
		                                        $wpdb->update(
		                                            $wpdb->prefix .'wyz_restaurant_dish_modifiers',
		                                            array(
		                                                'prompt_text' => htmlspecialchars(str_replace('\\', '', $modifier['prompt_text']), ENT_QUOTES),
		                                                'modifier_type' => $modifier['modifier_type'],
		                                                'min' => $modifier['min'],
		                                                'max' => $modifier['max'],
		                                            ),
		                                            array(
		                                                'id' => $modifier['id'],
		                                            )
		                                        );
		                                        $modifierId = $modifier['id'];
		                                    }

		                                    if (isset($modifier['delete_modifier_items']) && !empty($modifier['delete_modifier_items'])) {
		                                        foreach ($modifier['delete_modifier_items'] as $key => $deleteModifierItem) {
		                                            $wpdb->delete(
		                                                $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
		                                                array(
		                                                    'id' => $deleteModifierItem,
		                                                )
		                                            );
		                                        }
		                                    }

		                                    if (isset($modifier['items']) && !empty($modifier['items'])) {
		                                        foreach ($modifier['items'] as $key => $item) {
		                                            if (!isset($item['id']) || empty($item['id'])) {
		                                                $wpdb->insert(
		                                                    $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
		                                                    array(
		                                                        'text' => $item['text'],
			                                                    'price' => $item['price'],
			                                                    'tax' => $item['tax'],
			                                                    'total' => $item['total'],
		                                                        'modifier_id' => $modifierId,
		                                                    )
		                                                );
		                                            } else {
		                                                $wpdb->update(
		                                                    $wpdb->prefix .'wyz_restaurant_dish_modifier_items',
		                                                    array(
		                                                        'text' => $item['text'],
			                                                    'price' => $item['price'],
			                                                    'tax' => $item['tax'],
			                                                    'total' => $item['total'],
		                                                    ),
		                                                    array(
		                                                        'id' => $item['id'],
		                                                    )
		                                                );
		                                            }
		                                        }
		                                    }
		                                }
		                            }
								}
							}
						}
					}
				}
			}
			
			wp_safe_redirect( wp_get_referer() );
		} else {
			wp_safe_redirect( get_home_url() );
		}
		exit;
	}
}

include(WYZ_PLUGIN_ROOT_PATH . 'wp_add_ajax.php');
$wp_simple_locations = new wyz_chef;

function wyz_add_footer_styles() {
    wp_enqueue_style('responsive', WYZ_PLUGIN_ROOT_URL .'assets/css/responsive.css');
}
add_action( 'get_footer', 'wyz_add_footer_styles' );


function wyz_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
  }
  add_filter('upload_mimes', 'wyz_mime_types', 20);