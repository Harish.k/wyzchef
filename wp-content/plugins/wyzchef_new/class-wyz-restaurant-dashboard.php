<?php 
Class restaurant_dashborad{
  public function __construct(){
    add_action('init',  array($this,'Restaurantdetail'));
  }

  public function Restaurantdetail($id){
    // $id."Restauran Detail";
    $post = get_post($id);
    $pageID = '';
    if(isset($_REQUEST['page_id'])){
      $pageID = $_REQUEST['page_id'];
    }
    $current_user = wp_get_current_user();

    $wp_contact_person_name = get_post_meta($id, 'wp_contact_person_name',true);
    $wp_restaurant_email = get_post_meta($id, 'wp_restaurant_email',true);
    $wp_restaurant_phone = get_post_meta($id, 'wp_restaurant_phone',true);
    $wp_restaurant_address = get_post_meta($id, 'wp_restaurant_address',true);
    $wp_restaurant_type = get_post_meta($id, 'wp_restaurant_type',true);
    $wp_restaurant_category = get_post_meta($id, 'wp_restaurant_category',true);
    $wp_restaurant_logo = get_post_meta($id, 'wp_restaurant_logo',true);
    $wp_restaurant_gallery = get_post_meta($id, 'wp_restaurant_gallery',true);
    $wp_restaurant_website = get_post_meta($id, 'wp_restaurant_website',true);
    $wp_restaurant_tagline = get_post_meta($id, 'wp_restaurant_tagline',true);
    $wp_restaurant_facebook = get_post_meta($id, 'wp_restaurant_facebook',true);
    $wp_restaurant_instagram = get_post_meta($id, 'wp_restaurant_instagram',true);
    // $wp_restaurant_twitter = get_post_meta($id, 'wp_restaurant_twitter',true);
    $wp_restaurant_opening_days = get_post_meta($id, 'wp_restaurant_opening_days',true);
    $wp_restaurant_delivery_days = get_post_meta($id, 'wp_restaurant_delivery_days',true);
    $wp_restaurant_pickup_days = get_post_meta($id, 'wp_restaurant_pickup_days',true);
    $wp_restaurant_closing_date = get_post_meta($id, 'wp_restaurant_closing_date',true);

    // $wp_restaurant_delivery_days = get_post_meta($id, 'wp_restaurant_delivery_days',true);
    // $wp_restaurant_delivery_hours = get_post_meta($id, 'wp_restaurant_delivery_hours',true);
    // $wp_restaurant_price_range = get_post_meta($id, 'wp_restaurant_price_range',true);
    // $wp_restaurant_video = get_post_meta($id, 'wp_restaurant_video',true);
    
    $wp_restaurant_minimum_order_amount = get_post_meta($id, 'wp_restaurant_minimum_order_amount',true);
    $wp_restaurant_lead_time = get_post_meta($id, 'wp_restaurant_lead_time',true);
    $wp_restaurant_delivery_fee = get_post_meta($id, 'wp_restaurant_delivery_fee',true);
    $pickup_option = get_post_meta($id, 'pickup_option',true);
    $wp_restaurant_delivery_area = get_post_meta($id, 'wp_restaurant_delivery_pincodes',true);
    
    $bank_name = get_post_meta($id, 'bank_name',true);
    $bank_code = get_post_meta($id, 'bank_code',true);
    $account_name = get_post_meta($id, 'account_name',true);
    $account_number = get_post_meta($id, 'account_number',true);
    $swift_code = get_post_meta($id, 'swift_code',true);
    $branch_name = get_post_meta($id, 'branch_name',true);
    $branch_code = get_post_meta($id, 'branch_code',true);
    $preferred_mode = get_post_meta($id, 'preferred_mode',true);

    $weekDays = array(
        'monday' => 'Monday',
        'tuesday' => 'Tuesday',
        'wednesday' => 'Wednesday',
        'thursday' => 'Thursday',
        'friday' => 'Friday',
        'saturday' => 'Saturday',
        'sunday' => 'Sunday',
        'public' => 'Public holidays',
    );

    $hourDropDownOptions = array(
        '0' => '12 AM',
        '1' => '1 AM',
        '2' => '2 AM',
        '3' => '3 AM',
        '4' => '4 AM',
        '5' => '5 AM',
        '6' => '6 AM',
        '7' => '7 AM',
        '8' => '8 AM',
        '9' => '9 AM',
        '10' => '10 AM',
        '11' => '11 AM',
        '12' => '12 PM',
        '13' => '1 PM',
        '14' => '2 PM',
        '15' => '3 PM',
        '16' => '4 PM',
        '17' => '5 PM',
        '18' => '6 PM',
        '19' => '7 PM',
        '20' => '8 PM',
        '21' => '9 PM',
        '22' => '10 PM',
        '23' => '11 PM',
    );

    if (isset($wp_restaurant_gallery[0])) {
        $bgCoverImg = wp_get_attachment_url($wp_restaurant_gallery[0]);
    } else {
        $bgCoverImg = WYZ_PLUGIN_ROOT_URL .'assets/images/default-restaurant-cover.png';
    }

    if (isset($wp_restaurant_logo) && !empty($wp_restaurant_logo)) {
        $restaurantLogo = wp_get_attachment_thumb_url($wp_restaurant_logo);
    } else {
        $restaurantLogo = WYZ_PLUGIN_ROOT_URL . 'assets/images/default-logo.png';
    }

    $html = '
            <div class="first-section">
                <div class="bg-img">
                    <img src="'. $bgCoverImg .'">
                    <div class="resto_logo">
                        <img src="'. $restaurantLogo .'" alt="'.$post->post_title.'">
                    </div>
                </div>
            </div>
            <div class="second-section">
                <div class="row pad-bot">
                    <div class="col-md-2 img-banner"></div>
                    <div class="col-md-8 business-content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="resto-business-head">
                                    <p>'.$post->post_title.'</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="tag-list"> 
                            ';

                            if (isset($wp_restaurant_type) && !empty($wp_restaurant_type)) {
                                for ($i=0; $i < count($wp_restaurant_type); $i++) {
                                    $type = get_term($wp_restaurant_type[$i]);
                                    $html .='<span>'. $type->name .'</span>';
                                }
                            }

                            $html .='
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 end-img">
                ';

                if (isset($wp_restaurant_category) && !empty($wp_restaurant_category)) {
                    for ($j=0; $j < count($wp_restaurant_category); $j++) {
                        $category = get_term($wp_restaurant_category[$j]);
                        $imageId = get_term_meta( $category->term_id, 'wyz-category-image-id', true );
                        if( $imageId ) {
                            $categoryImage = wp_get_attachment_thumb_url( $imageId, 'thumbnail' );
                        } else {
                            $categoryImage = WYZ_PLUGIN_ROOT_URL . 'assets/images/restaurant.svg';
                        }
                        $html .='<img src="'. $categoryImage .'" alt="'.$category->name.'" />';
                    }
                }

                $html .='
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9 resto-add">
                        <p>
                            <span class="add_line1">'. $wp_restaurant_address .'</span>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class=" content-para">
                            '. $post->post_content .'
                            <!-- <a href="'. site_url('submit-restaurant') .'" class="modify-button">Edit</a> -->
                        </div>
                    </div>
                    <div class="col-md-3 contact-person">
                        <p>Contact-person</p> 
                        <span class="contact_name">'. $wp_contact_person_name .'</span> 
                        <span class="contact_mail">'. $wp_restaurant_email .'</span>
                        <span class="contact_number">'. $wp_restaurant_phone .'</span>
                        <ul class="social-list">
                            <li>
                                <a href="'. $wp_restaurant_website .'">
                                    <img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/web.png" alt="website">
                                </a>
                            </li>
                            <li>
                                <a href="'. $wp_restaurant_facebook .'">
                                    <img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/fb.png" alt="facebook">
                                </a>
                            </li>
                            <li>
                                <a href="'. $wp_restaurant_instagram .'">
                                    <img src="'. WYZ_PLUGIN_ROOT_URL .'assets/images/instagram.png" alt="instagram">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="timings">
                <ul class="nav nav-tabs" role="tablist">
                  <li role="tab" class="active" data-tab="tab-1"><a href="javascript:void(0);" aria-controls="home" role="tab" data-toggle="tab">Opening Hours</a>
                  </li>
                  <li role="tab" data-tab="tab-2" ><a href="javascript:void(0);" aria-controls="profile" role="tab" data-toggle="tab">Delivery Hours</a>
                  </li>
                  <li role="tab" data-tab="tab-3"><a href="javascript:void(0);" aria-controls="messages" role="tab" data-toggle="tab">PICK-UP HOURS</a>
                  </li>
                  <li role="tab" data-tab="tab-4"><a href="javascript:void(0);" aria-controls="settings" role="tab" data-toggle="tab">HOLIDAYS</a>
                  </li>
                </ul>
                        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab-1">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="custom-select-box-contain submit-timings-tabs">
                    <fieldset>
                        <div class="field required-field">
                            <table class="table-responsive">
                                <tr>
                        ';

                        foreach ($weekDays as $dayKey => $dayText) {
                            $html .= ' 
                                <td>
                                    <span>'. $dayText .'</span>
                                    <div class="time-container">
                                    ';

                                    if (!empty($wp_restaurant_opening_days && isset($wp_restaurant_opening_days[$dayKey]))) {
                                        foreach ($wp_restaurant_opening_days[$dayKey]['from'] as $key => $openFrom) {
                                            $html .= '
                                                <div class="time-wrapper"> From:
                                                    <select name="wp_restaurant_opening_days['. $dayKey .'][from][]" class="from-hours">
                                                ';

                                                    foreach ($hourDropDownOptions as $hourKey => $hourText) {
                                                        $selected = '';
                                                        if ($openFrom == $hourKey) {
                                                            $selected = 'selected';
                                                        }
                                                        
                                                        $html .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
                                                    }

                                                $html .= '
                                                    </select>
                                                    <br> To:
                                                    <select name="wp_restaurant_opening_days['. $dayKey .'][to][]" class="to-hours">
                                                ';

                                                    foreach ($hourDropDownOptions as $hourKey => $hourText) {
                                                        $selected = '';
                                                        if ($wp_restaurant_opening_days[$dayKey]['to'][$key] == $hourKey) {
                                                            $selected = 'selected';
                                                        }
                                                        
                                                        $html .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
                                                    }

                                                $html .= '
                                                    </select>
                                                    <hr> 
                                                </div>
                                            ';
                                        }
                                    }

                                    $html .= ' 
                                    </div>
                                </td>
                            ';
                        }
                        
                        $html .= ' 
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="tab-content">
          <div role="tabpanel" class="tab-pane" id="tab-2">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="custom-select-box-contain submit-timings-tabs">

                    <fieldset>
                        <div class="field required-field">
                            <table class="table-responsive">
                                <tr>
                        ';

                        foreach ($weekDays as $dayKey => $dayText) {
                            $html .= ' 
                                <td>
                                    <span>'. $dayText .'</span>
                                    <div class="time-container">
                                    ';

                                    if (!empty($wp_restaurant_delivery_days) && isset($wp_restaurant_delivery_days[$dayKey])) {
                                        foreach ($wp_restaurant_delivery_days[$dayKey]['from'] as $key => $deliveryFrom) {
                                            $html .= '
                                                <div class="time-wrapper"> From:
                                                    <select name="wp_restaurant_delivery_days['. $dayKey .'][from][]" class="from-hours">
                                                ';

                                                    foreach ($hourDropDownOptions as $hourKey => $hourText) {
                                                        $selected = '';
                                                        if ($deliveryFrom == $hourKey) {
                                                            $selected = 'selected';
                                                        }
                                                        
                                                        $html .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
                                                    }

                                                $html .= '
                                                    </select>
                                                    <br> To:
                                                    <select name="wp_restaurant_delivery_days['. $dayKey .'][to][]" class="to-hours">
                                                ';

                                                    foreach ($hourDropDownOptions as $hourKey => $hourText) {
                                                        $selected = '';
                                                        if ($wp_restaurant_delivery_days[$dayKey]['to'][$key] == $hourKey) {
                                                            $selected = 'selected';
                                                        }
                                                        
                                                        $html .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
                                                    }

                                                $html .= '
                                                    </select>
                                                    <hr> 
                                                </div>
                                            ';
                                        }
                                    }

                                    $html .= ' 
                                    </div>
                                </td>
                            ';
                        }
                        
                        $html .= ' 
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="tab-content">
          <div role="tabpanel" class="tab-pane" id="tab-3">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="custom-select-box-contain submit-timings-tabs">

                    <fieldset>
                        <div class="field required-field">
                            <table class="table-responsive">
                                <tr>
                        ';

                        foreach ($weekDays as $dayKey => $dayText) {
                            $html .= ' 
                                <td>
                                    <span>'. $dayText .'</span>
                                    <div class="time-container">
                                    ';

                                    if (!empty($wp_restaurant_pickup_days) && isset($wp_restaurant_pickup_days[$dayKey])) {
                                        foreach ($wp_restaurant_pickup_days[$dayKey]['from'] as $key => $pickupFrom) {
                                            $html .= '
                                                <div class="time-wrapper"> From:
                                                    <select name="wp_restaurant_pickup_days['. $dayKey .'][from][]" class="from-hours">
                                                ';

                                                    foreach ($hourDropDownOptions as $hourKey => $hourText) {
                                                        $selected = '';
                                                        if ($pickupFrom == $hourKey) {
                                                            $selected = 'selected';
                                                        }
                                                        
                                                        $html .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
                                                    }

                                                $html .= '
                                                    </select>
                                                    <br> To:
                                                    <select name="wp_restaurant_pickup_days['. $dayKey .'][to][]" class="to-hours">
                                                ';

                                                    foreach ($hourDropDownOptions as $hourKey => $hourText) {
                                                        $selected = '';
                                                        if ($wp_restaurant_pickup_days[$dayKey]['to'][$key] == $hourKey) {
                                                            $selected = 'selected';
                                                        }
                                                        
                                                        $html .= '<option value="'. $hourKey .'" '. $selected .'>'. $hourText .'</option>';
                                                    }

                                                $html .= '
                                                    </select>
                                                    <hr> 
                                                </div>
                                            ';
                                        }
                                    }

                                    $html .= ' 
                                    </div>
                                </td>
                            ';
                        }
                        
                        $html .= '
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="tab-content">
          <div role="tabpanel" class="tab-pane" id="tab-4">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="custom-select-box-contain submit-timings-tabs">
                <fieldset>
                    <div class="field required-field">
                        <p>'.$wp_restaurant_closing_date.'</p>
                    </div>
                </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
';
        /*$html .= '
            <!-- timings div-->
            <div class="img-gallery">
                <div class="gallery-heading">
                    <h3>Image Gallery</h3>
                </div>
                <div class="thumbnail-div">
            ';

            if(is_array($wp_restaurant_gallery)) {
                for($i=0; $i < sizeof($wp_restaurant_gallery); $i++) {
                    $html .= '<div class="column"><img src="'. wp_get_attachment_url($wp_restaurant_gallery[$i]) .'" alt="'. $post->post_title .'" style="width:100%" onclick="myFunction(this);"></div>';
                }
            }

            $html .= '
                </div>
                <div class="img-show"> <span onclick="this.parentElement.style.display=\'none\'" class="closebtn">&times;</span>
                    <img id="expandedImg" style="width:100%; height:100%">
                    <div id="imgtext"></div>
                </div>
            </div>
            <!-- img-gallery -->
        ';*/
        
    return $html;
  }
} 
?>