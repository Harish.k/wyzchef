<?php
$userid       = get_current_user_id();
$args = array(
        'user_id' => $userid,
        'number' => 10, // how many comments to retrieve
        'status' => 'approve'
        );

$comments = get_comments( $args );
?>

<div class="row"> 
	<?php
         if ( $comments )
    {
        foreach ( $comments as $c )
        { ?>
        <div class="col-md-6 col-sm-6 col-xs-12">
        	<div class="row dish-single-item review-list">
              <div class="col-md-12 col-xs-12">   
<h6><a href="<?php echo get_comment_link( $c->comment_ID );?> ">
             <?php
             $review = get_comment_meta( $c->comment_ID, 'wyz_title', true );
          echo get_the_title($c->comment_post_ID).": ".$review;
             ?></a></h6>

                        <div class="rating-star"><?php
                                    $return = '';
										$ratings =  get_comment_meta( $c->comment_ID, 'wyz_rating', true );
										$off_rating = 5 - $ratings;
										//echo $off_rating;die(); 
                                        for ($i=1; $i <=$ratings ; $i++) { 
                                        	$return	 .= "<span class='fa fa-star'></span>";
                                           }
                                         for ( $i = 1; $i <= $off_rating; $i++ ) {
							                  $a	= $i + $ratings;
							                  $return .= "<span class='fa fa-star-o'></span>";
						                }

                                       echo $return;
                        ?></div>
                        <div class="Reviews-description"><?php echo $c->comment_content;?></div>
                  </div>
			

        <h4>Posted on: <?php echo date('F d, Y',strtotime($c->comment_date));?></h4>
        </div> 
        </div>
       <?php }

    } 
    else { echo
      "<div class='no-reviews'>
         <h1>You haven't written any review yet!</h1>
          <img src='". WYZ_PLUGIN_ROOT_URL."assets/images/Review-catering.svg'>
      </div>";
   }
    
   ?>
</div>
