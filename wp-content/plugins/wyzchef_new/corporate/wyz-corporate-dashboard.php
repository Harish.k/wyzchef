<?php
//$current_user = wp_get_current_user();
//if (!isset($current_user->user_login) || empty($current_user->user_login)) {
//    return '
//        <div class="login-required">
//            <h1>You need to login to access this page</h1>
//        </div>
//    ';
//}

$dir = plugin_dir_url( WYZ_RESTAURANT_PLUGIN_FILE );
wp_enqueue_style( 'front-css',$dir. 'assets/fonts/font-awesome.min.css');
wp_enqueue_style('admin-console',$dir. 'assets/css/customer-console.css');
// wp_enqueue_style('responsive',$dir. 'assets/css/responsive.css', ['admin-console']);


// wp_enqueue_style('datetimepicker-css', plugin_dir_url(__FILE__). 'css/bootstrap-datetimepicker.css');

//wp_enqueue_script( 'jquery-js',$dir. 'js/jquery.min.js');
// wp_enqueue_script('datetime-moment-js', plugin_dir_url(__FILE__). 'js/moment.min.js');
// wp_enqueue_script('datetimepicker-js', plugin_dir_url(__FILE__). 'js/bootstrap-datetimepicker.js');
  /* wp_enqueue_script( 'front-map-script', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyARteWOCnYMfmc4hP7xyZpLVulT7fG_Cn4&libraries=places,drawing');*/

$pluginURL = WYZ_PLUGIN_ROOT_URL;

global $wpdb;
$user_id = get_current_user_id();

ob_start();
include(WYZ_PLUGIN_ROOT_PATH . 'corporate/wyz-profile-section.php');
$profileSectionData = ob_get_clean();

ob_start();
include(WYZ_PLUGIN_ROOT_PATH . 'corporate/wyz-order-section.php');
$orderSectionData = ob_get_clean();

ob_start();
include(WYZ_PLUGIN_ROOT_PATH . 'corporate/wyz-reviews-section.php');
$reviewsSectionData = ob_get_clean();

ob_start();
include(WYZ_PLUGIN_ROOT_PATH . 'corporate/wyz-preferences-section.php');
$preferencesSectionData = ob_get_clean();


ob_start();
include(WYZ_PLUGIN_ROOT_PATH . 'corporate/wyz-rewards-section.php');
$rewardsSectionData = ob_get_clean();


ob_start();
include(WYZ_PLUGIN_ROOT_PATH . 'corporate/wyz-calender-section.php');
$calenderSectionData = ob_get_clean();

$listingPage = home_url('caterers');

$sidetabbing = '
    <div class="icon-tab '. ((!isset($_GET['active-tab']) || $_GET['active-tab'] == 'Profile') ? 'active' : '') .'">
      <img class="image-default" src="'. $pluginURL .'assets/images/my-profile-tab.svg">
      <img class="image-hover" src="'. $pluginURL .'assets/images/my-profile-tab-catering.svg">
      <input type="button" class="tablinks" value="Profile">
    </div>

    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Calendar') ? 'active' : '') .'">
      <img class="image-default" src="'. $pluginURL .'assets/images/calender.svg">
      <img class="image-hover" src="'. $pluginURL .'assets/images/calendar-tab-catering.svg">
      <input type="button" class="tablinks" value="Calendar">
    </div>

    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Preferences') ? 'active' : '') .'">
      <img class="image-default" src="'. $pluginURL .'assets/images/menu-tab.svg">
      <img class="image-hover" src="'. $pluginURL .'assets/images/preferences-tab-catering.svg">
      <input type="button" class="tablinks" value="Preferences">
    </div>

    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Orders') ? 'active' : '') .'">
       <img class="image-default" src="'. $pluginURL .'assets/images/orders-tab.svg">
       <img class="image-hover" src="'. $pluginURL .'assets/images/order-tab-catering.svg">
      <input type="button" class="tablinks" value="Orders">
    </div>

    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Rewards') ? 'active' : '') .'">
      <img class="image-default" src="'. $pluginURL .'assets/images/reward-tab.svg">
       <img class="image-hover" src="'. $pluginURL .'assets/images/rewards-tab-catering.svg">
      <input type="button" class="tablinks" value="Rewards">
    </div>

    <div class="icon-tab '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Reviews') ? 'active' : '') .'">
      <img class="image-default" src="'. $pluginURL .'assets/images/reviews-tab.svg">
       <img class="image-hover" src="'. $pluginURL .'assets/images/review-tab-catering.svg">
      <input type="button" class="tablinks" value="Reviews">
    </div>
';

$tabbingContent = '
    <!-- Profile tab -->
    <div id="Profile" class="tabcontent '. ((!isset($_GET['active-tab']) || $_GET['active-tab'] == 'Profile') ? 'active' : '') .'">
        '. $profileSectionData .'
    </div> 
    <!-- Profile tab -->


    <!--Calendar tab -->
    <div id="Calendar" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Calendar') ? 'active' : '') .'">
          <!--'. $calenderSectionData .'  -->
          *Coming soon
    </div> <!--Calendar tab -->
  

    <!--Prefrences tab -->
    <div id="Preferences" class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Preferences') ? 'active' : '') .'">
        '. $preferencesSectionData .'  
    </div> <!--Prefrences tab -->

  
    <!--Invoice tab -->
      <div id="Orders" style="min-height: 586px; " class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Orders') ? 'active' : '') .'">
        <div class="dashboard-container">
         '. $orderSectionData .'
        </div>
      </div>
    <!--Invoice tab -->

    <!--Rewards tab -->
      <div id="Rewards" style="min-height: 586px; " class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Rewards') ? 'active' : '') .'">
        <div class="dashboard-container">
        '. $rewardsSectionData .'
        </div>
      </div>
    <!--Rewards tab -->

    <!--Reviews tab -->
      <div id="Reviews" style="min-height: 586px; " class="tabcontent '. ((isset($_GET['active-tab']) && $_GET['active-tab'] == 'Reviews') ? 'active' : '') .'">
        <div class="dashboard-container">
        '. $reviewsSectionData .'
        </div>
      </div>
    <!--Reviews tab -->
';

$corporateDashboardHTML = <<<CorporateDashboard
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="{$pluginURL}assets/css/bootstrap-datetimepicker.css" />

<!-- <link rel="stylesheet" type="text/css" href="https://uicdn.toast.com/tui.time-picker/latest/tui-time-picker.css" />
<link rel="stylesheet" type="text/css" href="https://uicdn.toast.com/tui.date-picker/latest/tui-date-picker.css" />
<link rel="stylesheet" type="text/css" href="{$pluginURL}assets/css/tui-calendar.css" />
<link rel="stylesheet" type="text/css" href="{$pluginURL}assets/css/default.css" />
<link rel="stylesheet" type="text/css" href="{$pluginURL}assets/css/icons.css" /> -->

<style>
    .icon-tab.active .image-hover {
        display: block;
    }

    .icon-tab.active .tablinks {
        color: #0ab9b1 !important;
    }

    .tabcontent {
        display: none;
    }

    .tabcontent.active {
        display: block;
    }
</style>

<script type="text/javascript">document.body.classList.add("wyzchef-admin-console");</script>
<div class="container-fluid filter-container">
    <div class="container filter-head"> 
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARteWOCnYMfmc4hP7xyZpLVulT7fG_Cn4&libraries=places,drawing"></script>
        <form action="{$listingPage}" method="get" class="header-filter-form" >
            <table  style="max-width:450px;float:left;"><tr>
            <td><span class="filter-tags input-group date" id="filter_date" ><i class="input-group-addon" id="filter_calender"></i><input type="text" class="datetimepicker" placeholder="Date" name="delivery_date" value=""></span></td>
            <!-- <div class="form-group">
                <div class="input-group date" id="filter_date">
                    <input type="text" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div> -->
            <!-- <td><span class="maplocatiopn-icon filter-tags"><i id="filter_location" button-0"></i><input type="text" placeholder="Location" class="input-0" name="delivery_location" value="" id="locationTextField" autocomplete="off"></span></td> -->
            <td><button type="submit" class="filter-tags filter-button filter-order-button"><i id="dine_icon"></i> Order</button></td>
            <!-- <td><a href="#"><span class="filter-tags filter-button filter-order-button"><i id="dine_icon"></i>Order</span></a></td> -->
            </tr>
         </table>
        </form>
    </div>
</div>


<div class="admin-panel">
  <div class="side-menu-button">
      <span class="one"></span>
      <span class="two"></span>
      <span class="three"></span>
  </div>
  <div class="tab admin-console-menu">
    <!-- Sidebar Tabbing -->
        {$sidetabbing}
    <!-- Sidebar Tabbing -->
  </div>

    <!-- Tabbing Content -->
        {$tabbingContent}
    <!-- Tabbing Content -->

<!-- Dashboard submit button -->
<div class="copyrights-sec">
  <p><span><img src="{$pluginURL}assets/images/copyr-logo.png" alt/></span><span>for corporations 2019 &copy;</span>
  </p>
</div>
<!-- copyrights-sec -->

<script type="text/javascript" src="{$pluginURL}assets/js/moment.min.js"></script>
<script type="text/javascript" src="{$pluginURL}assets/js/bootstrap-datetimepicker.js"></script>


<script type="text/javascript">
    function init() {
        var input = document.getElementById("locationTextField");
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, "load", init);
</script>

<script src="https://richardcornish.github.io/jquery-geolocate/js/jquery-geolocate.min.js"></script>
<script type="text/javascript">
    (function($, google) {
        $(".button-0, #filter_location").on("click", function(e) {
            $(".input-0").geolocate();
            e.preventDefault();
            // Map (not part of plugin)
            navigator.geolocation.getCurrentPosition(function(position) {
                // drawMap({
                //     lat: position.coords.latitude,
                //     lng: position.coords.longitude
                // });
            });
        });
    })(jQuery, google);
</script>

<!-- <script type="text/javascript" src="https://uicdn.toast.com/tui.code-snippet/latest/tui-code-snippet.min.js"></script>
<script type="text/javascript" src="https://uicdn.toast.com/tui.time-picker/latest/tui-time-picker.min.js"></script>
<script type="text/javascript" src="https://uicdn.toast.com/tui.date-picker/latest/tui-date-picker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chance/1.0.13/chance.min.js"></script>
<script type="text/javascript" src="{$pluginURL}assets/js/tui-calendar.js"></script>
<script type="text/javascript" src="{$pluginURL}assets/js/calendars.js"></script>
<script type="text/javascript" src="{$pluginURL}assets/js/schedules.js"></script>

<script type="text/javascript" class="code-js">
  // register templates
  const templates = {
    popupIsAllDay: function() {
      return 'All Day';
    },
    popupStateFree: function() {
      return 'Free';
    },
    popupStateBusy: function() {
      return 'Busy';
    },
    titlePlaceholder: function() {
      return 'Subject';
    },
    locationPlaceholder: function() {
      return 'Location';
    },
    startDatePlaceholder: function() {
      return 'Start date';
    },
    endDatePlaceholder: function() {
      return 'End date';
    },
    popupSave: function() {
      return 'Save';
    },
    popupUpdate: function() {
      return 'Update';
    },
    popupDetailDate: function(isAllDay, start, end) {
      var isSameDate = moment(start).isSame(end);
      var endFormat = (isSameDate ? '' : 'YYYY.MM.DD ') + 'hh:mm a';

      if (isAllDay) {
        return moment(start).format('YYYY.MM.DD') + (isSameDate ? '' : ' - ' + moment(end).format('YYYY.MM.DD'));
      }

      return (moment(start).format('YYYY.MM.DD hh:mm a') + ' - ' + moment(end).format(endFormat));
    },
    popupDetailLocation: function(schedule) {
      return 'Location : ' + schedule.location;
    },
    popupDetailUser: function(schedule) {
      return 'User : ' + (schedule.attendees || []).join(', ');
    },
    popupDetailState: function(schedule) {
      return 'State : ' + schedule.state || 'Busy';
    },
    popupDetailRepeat: function(schedule) {
      return 'Repeat : ' + schedule.recurrenceRule;
    },
    popupDetailBody: function(schedule) {
      return 'Body : ' + schedule.body;
    },
    popupEdit: function() {
      return 'Edit';
    },
    popupDelete: function() {
      return 'Delete';
    }
  };

  var cal = new tui.Calendar('#calendar', {
    defaultView: 'month',
    template: templates,
    useCreationPopup: true,
    useDetailPopup: true
  });
</script>
<script src="{$pluginURL}assets/js/default.js"></script> -->

CorporateDashboard;
echo $corporateDashboardHTML;
