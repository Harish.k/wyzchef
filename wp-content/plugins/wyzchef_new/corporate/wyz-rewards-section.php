<?php $rewardPoints  = intval(get_user_meta($user_id, "reward_points", true )); ?>
<!-- rewards top section -->
<div class="redeem-section">
  <div class="row">
    <div class="col-md-3">
      <div class="admin-form-container">
        <div class="redeem-box">
          <h2>You have earned</h2>
          <div class="redeem-circle">
            <div id="wrapper" class="center">
              <svg class="progress blue noselect" data-progress="<?php echo $rewardPoints; ?>" x="0px" y="0px" viewBox="0 0 80 80">
                <path class="track" d="M5,40a35,35 0 1,0 70,0a35,35 0 1,0 -70,0" />
                <path class="fill" d="M5,40a35,35 0 1,0 70,0a35,35 0 1,0 -70,0" />
                <text class="value redeem-text" x="50%" y="55%">0</text>
                <text class="value hero-text" x="50%" y="65%">Hero Points</text>
              </svg>
            </div>
          <button class="redeem-now">REDEEM NOW</button>
        </div>
      </div>  
    </div>
  </div>
    <div class="col-md-6"></div>
    <div class="col-md-3">
      <div class="admin-form-container">
      <h2>How it works</h2>
      <div clas="row">
        <div class="col-md-12">
          <p>For Every $1 you order in food and drink, you  get 1 WYZ coin! When you reach 3,000 coins, you will receive a special gift basket full of delicious products !</p>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
<!-- rewards top section -->



<!-- Refer colleague form -->
<form id="rewads-referal-form" action="<?php echo $postDataUrl; ?>" method="POST" class="form-horizontal admin-submission-form" >
    <div class="admin-content-box">       
        <div class="admin-form-container rewards-refer">
           <h3 class="hero-text"><span><img src="<?php echo $pluginURL; ?>assets/images/coins.png"></span>Refer WYZchef to a colleague!</h3>
           <h4>Refer WYZ to someone and get 100 Hero points once this person place his first order!</h4>
                <div class="form-group">
                  <label class="control-label col-sm-4 col-xs-12" for="refer_colleague_name">Colleague's Name<span>*</span></label>
                  <div class="col-sm-8 col-xs-12">
                    <input type="text" class="form-control" id="refer_colleague_name" name="refer_colleague_name" placeholder="Your colleague name" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4 col-xs-12" for="refer_colleague_email">Email<span>*</span></label>
                  <div class="col-sm-8 col-xs-12">
                    <input type="text" class="form-control" id="refer_colleague_email" name="refer_colleague_email" placeholder="Enter your colleague email" required>
                  </div>
                </div>
                <div class="form-group message-box">
                  <label class="control-label col-sm-4 col-xs-12" for="refer_message">Message</label>
                  <div class="col-sm-8 col-xs-12">
                    <textarea class="form-control" id="refer_message" name="refer_message" placeholder="Enter your message" rows="4"></textarea>
                  </div>
                </div>
                <div class="form-group"> 
                   <button type="submit" class="btn btn-default admin-save">SEND</button>
                </div>
                <input type="hidden" name="action" value="send_corporate_refer"> 
         </div> <!-- admin-form-container -->
    </div> <!--admin-content-box-->
</form> <!-- Refer colleague form -->

<!-- Refer social media section -->
<!--
   <div class="admin-form-container rewards-refer">
      <div class="row">
        <div class="col-md-12">
          <div class="share-redeem">
          <h3 class="hero-text"><span><img src="<?php echo $pluginURL; ?>assets/images/coins.png"></span>Get 10 Hero Points by sharing WYZ on social media !</h3>
            <ul class="list-inline list-unstyle">
              <li>
                <h4>Instagram</h4>
                <div class="social-div">
                    <div class="social-content">
                        <img src="<?php echo $pluginURL; ?>assets/images/social-bg.png" alt="wyz-insta"/>
                    </div>
                    <div class="redeem-overlay">
                      <a  href="https://www.instagram.com/wyzchef">Share</a>
                    </div>
                </div>
              </li>
              <li>
                <h4>Facebook</h4>
                <div class="social-div">
                      <div class="social-content">
                          <img src="<?php echo $pluginURL; ?>assets/images/social-bg.png" alt="wyz-facebook"/>
                      </div>
                      <div class="redeem-overlay">
                      <a  href="https://www.facebook.com/gotowyzchef">Share</a>
                    </div>
                  </div>
              </li>
              <li>
                <h4>LinkedIn</h4>
                <div class="social-div">
                    <div class="social-content">
                        <img src="<?php echo $pluginURL; ?>assets/images/social-bg.png" alt="wyz-linkedIn"/>
                    </div>
                    <div class="redeem-overlay">
                      <a  href="">Share</a>
                    </div>
                </div>
              </li>
              <li>
                <h4>Twitter</h4>
                <div class="social-div">
                      <div class="social-content">
                          <img src="<?php echo $pluginURL; ?>assets/images/social-bg.png" alt="wyz-twitter"/>
                      </div>
                      <div class="redeem-overlay">
                      <a  href="https://www.twitter.com/wyzchef">Share</a>
                    </div>
                  </div>
              </li>
            </ul>
          </div>
        </div>
      </div>  
    </div>
-->
<!-- Refer social media section -->

<!-- Complete your Profile -->
<div class="rewards-refer">
  <div class="row">
    <div class="col-md-6">
      <div class="admin-form-container">
        <h3 class="hero-text"><span><img src="<?php echo $pluginURL; ?>assets/images/coins.png"></span>Get 100 WYZ coins!</h3>
        <p>Tell us more about you and what are your preferences when ordering catering for your office and get more personalized experience!
          Fill <span class="other_links" id="reward-profile">My Profile</span> and <span class="other_links" id="reward-preference">My Preferences</span> categories 100%!
        </p>
        </div>
    </div> 
    <div class="col-md-6">
      <div class="admin-form-container">
      <h3 class="hero-text"><span><img src="<?php echo $pluginURL; ?>assets/images/coins.png"></span>Get 100 WYZ coins by reviewing!</h3>
      <p>After each order, leave a review to the caterer or restaurant and unlock 100 WYZ coins per review.</p>
      <a href="javascript:void(0);" id="rewards-review">Review</a>
    </div>
    </div>
  </div>
</div>
<!-- Complete your Profile -->


<script>
    var referDetails = {
        'refer_colleague_name': 'Steve Chang',
        'refer_colleague_email': 'steve@xyz.com',
        'refer_message': 'Hello, I use WYZchef to order food at my office. You should try them, they are great. And plus, they will give you a 10% DISCOUNT on your first order.'
    };

    for(let key in referDetails) {
      // console.log('Key: ', key);
      document.getElementById(key).value = referDetails[key];
    }

    jQuery('.redeem-now').on('click', function(){
        let data = {
            'action': 'sendRedeemRequest',
            'userId': <?php echo $user_id; ?>
        };

        jQuery.ajax({
            url: "<?php echo admin_url('admin-ajax.php');?>",
            type: "POST",
            dataType: "json",
            data: data,
            success: function(response) {
                alert(response.msg);
                if (parseInt(response.rewardPoints)) {
                    jQuery('.progress').attr('data-progress', parseInt(response.rewardPoints));
                    setRewardPoints();
                }
            },
            error: function(error) {
                console.log('error', error);
            }
        });
    });

    var forEach = function (array, callback, scope) {
      for (var i = 0; i < array.length; i++) {
        callback.call(scope, i, array[i]);
      }
    };

    function setRewardPoints(){
        var max = -219.99078369140625;
        forEach(document.querySelectorAll('.progress'), function (index, value) {
            percent = value.getAttribute('data-progress');
            value.querySelector('.value').innerHTML = percent;
            percent = percent > 3000 ? 3000 : percent;
            value.querySelector('.fill').setAttribute('style', 'stroke-dashoffset: ' + ((3000 - percent) / 3000) * max);
        });
    }

    window.onload = setRewardPoints;
</script>