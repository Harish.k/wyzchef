<!-- Admin Personal Detail form -->
<?php
    // do_action('save_user_form'); 
    if(session_id() == '')  {
        session_start();
    }

    $postDataUrl = admin_url('admin-post.php');

    // Removing session data
    if(isset($_SESSION["msgArray"])){
        echo "<pre>Success! <strong>{$_SESSION[msgArray][success][msg]}</strong></pre>";
        unset($_SESSION["msgArray"]);
    }

    $profilePic = get_user_meta($current_user->ID, 'profile_pic_url', true);
    if (empty($profilePic)) {
        $profilePic = WYZ_PLUGIN_ROOT_URL + 'assets/images/admin-profile-pic.png';
    }
?>
<form id="admin-details-form" action="<?php echo $postDataUrl; ?>" method="POST" class="form-horizontal admin-submission-form" enctype="multipart/form-data">
    <div class="admin-content-box">
        <div class="first-section">
            <div class="bg-img">
                <img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/admin-banner-image.png">
                <div class="resto_logo" for="profile-pic">
                    <img id="profile-pic-preview" src="<?php echo $profilePic; ?>" alt=""> 
                    <input type="file" name="profile_pic" class="user-thumb" id="profile-pic">
                    <span class="fa fa-camera"></span>
               </div>
            </div>
        </div>
        <div class="admin-form-container">
            <div class="admin-info-top">
                <h2><span id="user_full_name"></span>,&nbsp;Office hero</h2>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 col-xs-12" for="first_name">First Name<span>*</span></label>
              <div class="col-sm-8 col-xs-12">
                <input type="text" class="form-control" name="userData[first_name]" id="first_name" placeholder="First Name" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 col-xs-12" for="last_name">Last Name<span>*</span></label>
              <div class="col-sm-8 col-xs-12"> 
                <input type="text" class="form-control" name="userData[last_name]" id="last_name" placeholder="Last Name" required>
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-sm-4 col-xs-12" for="email">Corporate Email<span>*</span></label>
              <div class="col-sm-8 col-xs-12"> 
                <input type="email" class="form-control" name="userData[email]" id="email" placeholder="Corporate Email" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 col-xs-12" for="phone">Phone Number<span>*</span></label>
              <div class="col-sm-8 col-xs-12"> 
                <input type="phone" class="form-control" name="userData[User_phone]" id="User_phone" placeholder="Phone Number" required>
              </div>
            </div>
            <div class="form-group bday-gift-text">
              <label class="control-label col-sm-4 col-xs-12" for="birth_date">Date of Birth</label>
              <div class="col-sm-8 col-xs-12"> 
                <input type="date" class="form-control" name="userData[birth_date]" id="birth_date" placeholder="Date of Birth">
              </div>
                <span class="bday-text">Special gift on your birthday!</span>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4 col-xs-12" for="gender">Gender</label>
              <div class="col-sm-8 col-xs-12"> 
                <select name="userData[gender]" id="gender">
                  <option value="female">Female</option>
                  <option value="male">Male</option>
                </select>
              </div>
            </div>
            <!-- <div class="form-group">
                <a href="#" class="button" data-toggle="modal" data-target="#password_modal">Change Password</a>
            </div> -->
            <div class="form-group"> 
               <button type="submit" name="save_data" class="btn btn-default admin-save">SAVE</button>
            </div>
            <input type="hidden" name="action" value="save_corporate_profile"> 
        </div> <!-- admin-form-container -->
    </div>  <!--admin-content-box-->
<!-- </form> -->  <!-- Admin Personal Detail form -->

<!-- Company's Detail form -->
<!-- <form id="company-details-form" action="<?php // echo $postDataUrl; ?>" method="POST" class="form-horizontal admin-submission-form" > -->
    <div class="admin-content-box">       
        <div class="admin-form-container">
           <h2>My Company</h2>
                <div class="form-group">
                  <label class="control-label col-sm-4 col-xs-12" for="company_name">Company name<span></span></label>
                  <div class="col-sm-8 col-xs-12">
                    <input type="text" class="form-control" name="userData[company_name]" id="company_name" placeholder="Name of your company">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4 col-xs-12" for="company_size">Company Size<span></span></label>
                  <div class="col-sm-8 col-xs-12">
                    <input type="text" class="form-control" name="userData[company_size]" id="company_size" placeholder="What is the size of your company">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-4 col-xs-12" for="company_postal">Postal Code<span></span></label>
                  <div class="col-sm-8 col-xs-12">
                    <input type="text" class="form-control" name="userData[company_postal]" id="company_postal" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4 col-xs-12" for="company_address">Address<span></span></label>
                  <div class="col-sm-8 col-xs-12">
                    <input type="text" class="form-control" name="userData[company_address]" id="company_address" placeholder="Company's address">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4 col-xs-12" for="floor_building">Unit number (Eg. #12-318)<span></span></label>
                  <div class="col-sm-8 col-xs-12">
                    <div class="unit-one-div">
                         <span class="hash-unitnumber" >#</span><input type="text" class="form-control" name="userData[floor_building]" id="floor_building" class="unit_floor" placeholder="Enter the floor and building"><span class="dash-unitnumber">–</span>
                    </div>
                   <div class="unit-two-div">
                        <input type="text" class="form-control" name="userData[unit_number]" id="unit_number" class="unit_number" placeholder="Enter the floor and building">
                   </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4 col-xs-12" for="position">Position</label>
                  <div class="col-sm-8 col-xs-12">
                    <input type="text" class="form-control" name="userData[position]" id="position" placeholder="Enter your Position">
                  </div>
                </div>
                <div class="form-group"> 
                   <button type="submit" class="btn btn-default admin-save">SAVE</button>
                </div>
                <!-- <input type="hidden" name="action" value="save_corporate_company">  -->
         </div> <!-- admin-form-container -->
    </div> <!--admin-content-box-->
</form> <!-- Company's Detail form -->

<!-- Change Password Pop-up  -->
<div class="modal" id="password_modal" aria-hidden="true">
    <div class="modal-header">
        <h2>Change Password <span class="extra-title muted"></span></h2>
    </div>
    <div class="modal-body form-horizontal">
        <form class="update-pwd" name="update-pwd">
            <div class="control-group">
                <label for="current_password" class="control-label">Current Password</label>
                <div class="controls">
                    <input type="password" name="current_password">
                </div>
            </div>
            <div class="control-group">
                <label for="new_password" class="control-label">New Password</label>
                <div class="controls">
                    <input type="password" name="new_password">
                </div>
            </div>
            <div class="control-group">
                <label for="confirm_password" class="control-label">Confirm Password</label>
                <div class="controls">
                    <input type="password" name="confirm_password">
                </div>
            </div>      
        </form>
    </div>
    <div class="modal-footer">
        <button href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button href="#" class="btn btn-primary" id="password_modal_save">Save changes</button>
    </div>
</div>
<!-- Change Password Pop-up  -->

<!-- Refer colleague form -->
<form id="referal-form" action="<?php echo $postDataUrl; ?>" method="POST" class="form-horizontal admin-submission-form" >
    <div class="admin-content-box">       
        <div class="admin-form-container">
           <h2>Refer WYZ to a colleague</h2>
                <div class="form-group">
                  <label class="control-label col-sm-4 col-xs-12" for="colleague_name">Colleague's Name<span>*</span></label>
                  <div class="col-sm-8 col-xs-12">
                    <input type="text" class="form-control" id="colleague_name" name="colleague_name" placeholder="Your colleague name" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4 col-xs-12" for="colleague_email">Email<span>*</span></label>
                  <div class="col-sm-8 col-xs-12">
                    <input type="text" class="form-control" id="colleague_email" name="colleague_email" placeholder="Enter your colleague email" required>
                  </div>
                </div>
                <div class="form-group message-box">
                  <label class="control-label col-sm-4 col-xs-12" for="message">Message</label>
                  <div class="col-sm-8 col-xs-12">
                    <textarea class="form-control" id="message" name="refer_message" placeholder="Enter your message" rows="4"></textarea>
                  </div>
                </div>
                <div class="form-group"> 
                   <button type="submit" class="btn btn-default admin-save">SEND</button>
                </div>
                <input type="hidden" name="action" value="send_corporate_refer"> 
         </div> <!-- admin-form-container -->
    </div> <!--admin-content-box-->
</form> <!-- Refer colleague form -->

<form id="suggestion-form" action="<?php echo $postDataUrl; ?>" method="POST" class="form-horizontal admin-submission-form" >
    <div class="admin-content-box">       
        <div class="admin-form-container">
           <h2>Suggest a restaurant or caterer that you like</h2>
                <div class="form-group">
                  <label class="control-label col-sm-4 col-xs-12" for="restaurant_name">Restaurant's Name</label>
                  <div class="col-sm-8 col-xs-12">
                    <input type="text" class="form-control" id="restaurant_name" name="restaurant_name" placeholder="Enter restaurant name" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4 col-xs-12" for="website">Website</label>
                  <div class="col-sm-8 col-xs-12">
                    <input type="text" class="form-control" id="website" name="website" placeholder="Enter restaurant's website" required>
                  </div>
                </div>
                <div class="form-group message-box">
                  <label class="control-label col-sm-4 col-xs-12" for="comment">Comment</label>
                  <div class="col-sm-8 col-xs-12">
                    <textarea class="form-control" id="comment" name="comment_msg" placeholder="Your comments on the restaurant" rows="4"></textarea>
                  </div>
                </div>
                <div class="form-group"> 
                   <button type="submit" class="btn btn-default admin-save">SEND</button>
                </div>
                <input type="hidden" name="action" value="send_corporate_suggestion"> 
         </div> <!-- admin-form-container -->
    </div> <!--admin-content-box-->
</form> <!-- Refer colleague form -->

<?php
    $userData['first_name'] = get_user_meta($current_user->ID, 'first_name', true);
    $userData['last_name'] = get_user_meta($current_user->ID, 'last_name', true);
    $userData['email'] = $current_user->user_email;
    $userData['User_phone'] = get_user_meta($current_user->ID, 'User_phone', true);
    $userData['birth_date'] = get_user_meta($current_user->ID, 'birth_date', true);
    $userData['gender'] = get_user_meta($current_user->ID, 'gender', true);
    if (empty($userData['gender'])) {
        $userData['gender'] = 'male';
    }

    $userData['company_name'] = get_user_meta($current_user->ID, 'company_name', true);
    $userData['company_size'] = get_user_meta($current_user->ID, 'company_size', true);
    $userData['company_address'] = get_user_meta($current_user->ID, 'company_address', true);
    $userData['floor_building'] = get_user_meta($current_user->ID, 'floor_building', true);
    $userData['position'] = get_user_meta($current_user->ID, 'position', true);
    $userData['company_postal'] = get_user_meta($current_user->ID, 'company_postal', true);
    $userData['unit_number'] = get_user_meta($current_user->ID, 'unit_number', true);
    
    $userData['colleague_name'] = '';
    $userData['colleague_email'] = '';
    $userData['message'] = 'Refer this restaurant to your loved once.';
    $userData['restaurant_name'] = '';
    $userData['website'] = '';
    $userData['comment'] = '';
?>

<script type="text/javascript">
    var cDashboard = <?php echo json_encode($userData); ?>
    /*var cDashboard = {
        'first_name': 'Shubhanshu',
        'last_name': 'Gupta',
        'email': 'shubhanshu.kumar2@mail.vinove.com',
        'phone': '9876543210',
        'birth_date': "1991-09-24",
        'gender': 'male',
        'company_name': 'Vinove Softwares & Services',
        'company_size': '500',
        'company_address': 'H-221, Noida Sec-63',
        'floor_building': 'Ground Floor',
        'position': 'Software Developer',
        'colleague_name': '',
        'colleague_email': '',
        'message': '',
        'restaurant_name': '',
        'website': '',
        'comment': ''
    };*/

    for(let key in cDashboard) {
      // console.log('Key: ', key);
      document.getElementById(key).value = cDashboard[key];
    }

    document.getElementById('user_full_name').innerText = cDashboard.first_name +' '+ cDashboard.last_name;

    jQuery(document.body).ready(function(){
        jQuery('#profile-pic').on('change', function() {
            let countFiles = jQuery(this)[0].files.length;
            let imgPath = jQuery(this)[0].value;
            let extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            let fileSize = this.files[0].size;
            
            if ((fileSize > 1024000)) {
                alert("Image size must less than 1MB");
            } else if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                if (typeof(FileReader) != "undefined") {
                    for (let i = 0; i < countFiles; i++) {
                        let reader = new FileReader();
                        reader.onload = function(e) {
                            jQuery('#profile-pic-preview').attr('src', e.target.result);  
                        }
                
                        reader.readAsDataURL(jQuery(this)[0].files[i]);
                    }
                } else {
                    console.log("This browser does not support FileReader.");
                }
            } else {
                console.log("Please select only images");
            }
        });
    });
</script>