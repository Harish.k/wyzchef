<?php
global $wpdb;
$userDetails    = wp_get_current_user();
$orderTable    = $wpdb->prefix . 'wyz_restaurant_order';
$deliveryTable = $wpdb->prefix . 'wyz_restaurant_order_delivery_details';
$userid       = get_current_user_id();


wp_enqueue_style('comment-rating-styles', WYZ_PLUGIN_ROOT_URL. 'assets/css/rating.css');

function get_delivery_fees($order_id){
  global $wpdb;
  $ordertable = $wpdb->prefix . 'wyz_restaurant_order';
  $sql = "SELECT `delivery_fees` FROM $ordertable WHERE id = {$order_id}";
  $delivery_fees = $wpdb->get_row($sql, ARRAY_A);
  return $delivery_fees['delivery_fees'];

}

function get_tax($order_id){
  global $wpdb;
  $ordertable = $wpdb->prefix . 'wyz_restaurant_order';
  $sql = "SELECT `gst` FROM $ordertable WHERE id = {$order_id}";
  $tax = $wpdb->get_row($sql, ARRAY_A);
  return $tax['gst'];
}

function get_restaurant_name($dish_id){
  global $wpdb;
  $dishesTable = $wpdb->prefix . 'wyz_restaurant_dishes';       
  $restaurant_id = $wpdb->get_results("SELECT DISTINCT `restaurant_id` FROM $dishesTable WHERE id = ".$dish_id,ARRAY_A);
  return $restaurant_id[0]['restaurant_id'];

}

function get_order_rating($order_id){
  global $wpdb;
  $ordertable = $wpdb->prefix . 'wyz_restaurant_order';
  $sql = "SELECT `order_rating` FROM $ordertable WHERE id = {$order_id}";
  $order_rating = $wpdb->get_row($sql, ARRAY_A);
  return $order_rating['order_rating'];

}


//pending orders for users  
$order_details_pending = $wpdb->get_results("SELECT ord.id, ord.user_id, ord.total, ord.status, ord.created_at, ord.delivery_fees, ord.gst, delivery.delivery_date FROM $orderTable AS ord INNER JOIN $deliveryTable AS delivery ON ord.`id` = delivery.`order_id` WHERE delivery.delivery_date > CURRENT_DATE() AND ord.user_id = '".$userid."' AND ord.status = '0' ORDER BY ord.id DESC");

//confirmed orders for users  
$order_details_confirmed = $wpdb->get_results("SELECT ord.id, ord.user_id, ord.total, ord.status, ord.created_at, ord.delivery_fees, ord.gst, delivery.delivery_date FROM $orderTable AS ord INNER JOIN $deliveryTable AS delivery ON ord.`id` = delivery.`order_id` WHERE ord.user_id = '".$userid."' AND ord.status = '1' ORDER BY ord.id DESC");

//completed orders for users  
$order_details_completed = $wpdb->get_results("SELECT ord.id, ord.user_id, ord.total, ord.status,
  ord.restaurant_id,  ord.order_rating, ord.created_at, ord.delivery_fees, ord.gst, delivery.delivery_date FROM $orderTable AS ord INNER JOIN $deliveryTable AS delivery ON ord.`id` = delivery.`order_id` WHERE ord.user_id = '".$userid."' AND ord.status = '3' ORDER BY ord.id DESC");

//past orders for users 
$order_details_past = $wpdb->get_results("SELECT ord.id, ord.user_id, ord.total, ord.status, ord.created_at, ord.delivery_fees, ord.gst, delivery.delivery_date FROM $orderTable AS ord LEFT JOIN $deliveryTable AS delivery ON ord.`id` = delivery.`order_id` WHERE delivery.delivery_date < CURRENT_DATE() AND ord.user_id = '".$userid."'");
?>

<style type = "text/css">
    .order-btn .modal.in {
        opacity: 1;
    }

    .order-review-wrapper {
        border: 0;
        height: 0 !important;
        padding: 0;
        position: absolute;
        width: 0;
    }

    @media print {
        .modal-header, .whatsappme {
            display: none;
        }
        .modal.in .modal-dialog{
		    	margin-top:10px !important;
		    }

        #main-header, .container-fluid, #main-footer, .side-menu-button, .admin-console-menu, .pending-orders h3, .dish-single-item, .btn-secondary, .copyrights-sec {
            display: none !important;
        }

        .dashboard-container .pending-orders {
            border: none;
        }
    }
</style>

<script type="text/javascript">
    var orderObj = {};
</script>

<?php if(count($order_details_pending) > 0){ ?>
  <div class="row pending-orders">
    <h3>Pending Orders</h3>
    <div class="col-md-12">
      <div class="row">
        <?php foreach ($order_details_pending as $order) { ?>
            <script type="text/javascript">
                orderObj[<?= $order->id ?>] = <?= json_encode($order) ?>
            </script>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="dish-single-item">
              <span>Order Id: #<?php echo $order->id; ?></span><br>
              <span>Total Amount: $<?php echo $order->total."<br>"; ?></span>

              <?php echo $this->delivery_address($order->id); ?><br>
              <?php echo $this->delivery_date($order->id); ?><br>
              <p>Created At: <?php echo date('F d, Y h:i A',strtotime($order->created_at))."<br>"; ?></p><br>
              <span>Status: Pending</span>

              <div class="order-btn">
              <a href="#order-detail" class="orders-popup-order" data-order-id="<?= $order->id ?>">View Order</a>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
<?php }
// end of pending ordersz

if(count($order_details_confirmed) > 0){ ?>
  <div class="row pending-orders">
    <h3>Confirmed Orders</h3>
    <div class="col-md-12">
      <div class="row">
        <?php foreach ($order_details_confirmed as $order) { ?>
            <script type="text/javascript">
                orderObj[<?= $order->id ?>] = <?= json_encode($order) ?>
            </script>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="dish-single-item">
              <span>Order Id: #<?php echo $order->id; ?></span><br>
              <span>Total Amount: $<?php echo $order->total."<br>"; ?></span>

              <?php echo $this->delivery_address($order->id)."<br>"; ?>
              <?php echo $this->delivery_date($order->id)."<br>"; ?>
              <p>Created At: <?php echo date('F d, Y h:i A',strtotime($order->created_at))."<br>"; ?></p><br>
              <span>Status: Confirmed</span>

              <!-- Place event data -->
              <span class="addtocalendar atc-style-blue">
                <var class="atc_event">
                <var class="atc_date_start"><?php echo date('F d, Y h:i A',strtotime($this->delivery_date($order->id))); ?></var>
                <!--<var class="atc_date_end">2015-05-04 18:00:00</var>-->
                <var class="atc_timezone">Asia/Singapore</var>
                <var class="atc_title">Event with catering from WYZchef</var>
                <var class="atc_location"><?php echo $this->delivery_address($order->id); ?></var>
                <!--<var class="atc_organizer"></var>-->
                <var class="atc_organizer_email"><?php echo $this->get_delivery_email($order->id); ?></var>
                </var>
              </span>
            
              <div class="order-btn">
              <a href="#order-detail" class="orders-popup-order" data-order-id="<?= $order->id ?>">View Order</a>
              </div>
            </div>
          </div>

        <?php } ?>
      </div>
    </div>
  </div>

<script type="text/javascript">(function () {
    if (window.addtocalendar)if(typeof window.addtocalendar.start == "function")return;
    if (window.ifaddtocalendar == undefined) { 
        window.ifaddtocalendar = 1;
        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
        s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
        s.src = '<?php echo WYZ_PLUGIN_ROOT_URL;?>/assets/js/atc.min.js';
        //s.src = 'https://addtocalendar.com/atc/1.5/atc.min.js';                                      
        var h = d[g]('body')[0];h.appendChild(s); 
    }})();
</script>

<?php }

// Completed orders
if(count($order_details_completed) > 0){ ?>
  <div class="row pending-orders">
    <h3>Completed Orders</h3>
    <div class="col-md-12">
      <div class="row">
        <?php foreach ($order_details_completed as $order) {
          $user_info = get_userdata($order->user_id);
          $user_full_name = $user_info->user_firstname . ' ' . $user_info->user_lastname;
          $user_email = $user_info->user_email;
        ?>
        <script type="text/javascript">
            orderObj[<?= $order->id ?>] = <?= json_encode($order) ?>
        </script>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="dish-single-item">
              <span>Order Id: #<?php echo $order->id; ?></span><br>
              <span>Total Amount: $<?php echo $order->total."<br>"; ?></span>

              <?php echo $this->delivery_address($order->id)."<br>"; ?>
              <?php echo $this->delivery_date($order->id)."<br>"; ?>
              <p>Created At: <?php echo date('F d, Y h:i A',strtotime($order->created_at))."<br>"; ?></p><br>
              <p class="show-ratings-<?php echo esc_html($order->id); ?>"><p>
                <?php if($order->status == 3 && $order->order_rating == "" ){ ?>
                  <p class="give-review-<?php echo esc_html($order->id); ?>"><a href="#" class="order-give-review" data-order-id="<?= $order->id ?>" data-restaurant-id="<?= $order->restaurant_id ?>"><?php esc_html_e('GIVE REVIEW', 'textdomain'); ?></a></p>
                <?php } else { ?>
                  <p class="show-ratings-<?php echo esc_html($order->id); ?>">
                    <?php 
                    $return = '';
                    $ratings =  get_order_rating($order->id);
                    $off_rating = 5 - $ratings;

                    for ($i=1; $i <=$ratings ; $i++) { 
                      $return  .= "<span class='fa fa-star'></span>";
                    }
                    for ( $i = 1; $i <= $off_rating; $i++ ) {
                      $a  = $i + $ratings;
                      $return .= "<span class='fa fa-star-o'></span>";
                    }
                    echo $return;
                    ?>
                  </p>
                <?php } ?>

                <span>Status: Completed</span>

                <div class="order-btn">
                  <a href="#order-detail" class="orders-popup-order" data-order-id="<?= $order->id ?>">View Order</a>
                </div>
            </div>
        </div>
    <?php } ?>
      </div>
   </div>
</div>

<div class="dish-single-item order-review-wrapper">
    <div class="modal fade menu-order-detail order-review" id="order-review-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3><?php _e("Rate And Write A Review" , "textdomain"); ?></h3>
            <button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="order-detail-inner">
              <div class="reviews-holder">
                <form action="" id="write_a_comment" method ="post">
                  <div class="add-new-review-holder add-new-review">
                    <div class="row">
                        <div class="wyz-add-review-data">
                          <div class="wyz-added-review-string" style="display:none;">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">

                          </div>
                       </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="rating-restaurant">
                          <div class="col-md-12 form-group form-rate">
                            <h3>Rating:</h3>
                            <fieldset class="rating">
                              <input type="radio" id="star5" name="rating" class="rating"  value="5" />
                              <label class = "full" for="star5" title="Awesome - 5 stars"></label>

                              <input type="radio" id="star4" name="rating" class="rating" value="4" />
                              <label class = "full" for="star4" title="Pretty good - 4 stars"></label>

                              <input type="radio" id="star3" name="rating" class="rating" value="3" />
                              <label class = "full" for="star3" title="Meh - 3 stars"></label>

                              <input type="radio" id="star2" name="rating" class="rating" value="2" />
                              <label class = "full" for="star2" title="Kinda bad - 2 stars"></label>

                              <input type="radio" id="star1" name="rating" class="rating" value="1" />
                              <label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                            </fieldset>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-element">
                          <i class="icon-edit2"></i>
                          <input type="hidden" name="order_id" id="order_id" value="">
                          <input type="hidden" name="restaurant_id" id="restaurant_id" value="">

                          <input type="text" placeholder="<?php echo esc_html__('Title of your review *', 'textdomain'); ?>" name="review_title" id="review_title" value="">
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-height">
                        <div class="form-element">
                          <i class="icon-user4"></i>
                          <input type="text" placeholder="<?php echo esc_html__('Name *', 'textdomain'); ?>" name="user_full_name" id="user_full_name" value="<?php echo esc_html($user_full_name); ?>" <?php echo esc_html($user_full_name) != '' ? 'disabled="disabled"' : ''; ?>>
                          <input type="hidden" name="review_user_id" id="review_user_id" value="<?php echo esc_html($order->user_id); ?>">
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-height">
                        <div class="form-element">
                          <i class="icon-envelope3"></i>
                          <input type="text" placeholder="<?php echo esc_html__('Email *', 'textdomain'); ?>" name="review_email_address" id="review_email_address" value="<?php echo esc_html($user_email); ?>" <?php echo esc_html($user_email) != '' ? 'disabled="disabled"' : ''; ?>>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-element">
                          <i class="icon-message"></i>
                          <textarea placeholder="<?php echo esc_html__('Tell about your experience or leave a tip for others', 'textdomain'); ?>" cols="30" rows="10" name="review_description" id="review_description" maxlength=""></textarea>
                        </div>
                      </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="form-element send_review_holder">
                        <input type="hidden" id="parent_review_id" name="parent_review_id" value="" >
                        <input type="button" name="send_your_review" id ="write_a_review"value="<?php _e("Submit Review" , "textdomain"); ?>">
                        <span class="ajax-message"></span>
                      </div>
                    </div>
                </div>    
                </div>
                </div>
                </form>
                <div id="comment_message_box"></div>
              </div>
                    <!-- script for rating review -->
                    <script type="text/javascript">
                        jQuery("#write_a_review").click( function() {
                            if (jQuery('#review_title').length && jQuery('#review_title').val() != '') {
                                var review_title = jQuery('#review_title').val();
                            } 

                            if (jQuery('#restaurant_id').length && jQuery('#restaurant_id').val() != '') {
                                var restaurant_id = jQuery('#restaurant_id').val();
                            }
                            //order id
                            if (jQuery('#order_id').length && jQuery('#order_id').val() != '') {
                                var order_id = jQuery('#order_id').val();
                            }

                            if (jQuery('#review_user_id').length && jQuery('#review_user_id').val() != '') {
                                var review_user_id = jQuery('#review_user_id').val();
                            }

                            if (jQuery('#user_full_name').length && jQuery('#user_full_name').val() != '') {
                                var user_full_name = jQuery('#user_full_name').val();
                            }
                            if (jQuery('#review_email_address').length && jQuery('#review_email_address').val() != '') {
                                var review_email_address = jQuery('#review_email_address').val();
                            } 
                            if (jQuery('.rating').length && jQuery('[name="rating"]:checked').val() != '') {
                                var restaurant_rating = jQuery('[name="rating"]:checked').val();
                            } 
                            if (jQuery('#review_description').length && jQuery('#review_description').val() != '') {
                                var review_description = jQuery('#review_description').val();
                            } 
                            jQuery.ajax({
                                type: "POST",
                                dataType: 'json', 
                                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                                data: {
                                    'action': 'wyz_post_user_review',
                                    'comment_post_ID': restaurant_id,
                                    'comment_order_ID': order_id,
                                    'comment_user_id': review_user_id,
                                    'review_title' : review_title,
                                    'comment_author': user_full_name,
                                    'review_author_email': review_email_address,
                                    'comment_content': review_description,
                                    'rating': restaurant_rating
                                },
                                success: function(data) {
                                    jQuery('#comment_message_box').removeAttr('class').attr('class', '');
                                    jQuery('#comment_message_box').addClass(data.class);
                                    jQuery('#comment_message_box').html(data.Msg);
                                    jQuery('#order-review-modal').modal('hide');
                                    jQuery('p.give-review-'+order_id).hide();
                                    jQuery('p.show-ratings-'+order_id).html(data.ratings);
                                }
                            });
                        });
                    </script>
                    <!-- end of this-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php }
//end of completed orders

// past orders
if(count($order_details_past) > 0){ ?>

  <div class="row pending-orders">
    <h3>Past Orders</h3>
    <div class="col-md-12">
      <div class="row">
        <?php
        foreach ($order_details_past as $order) { ?>
            <script type="text/javascript">
                orderObj[<?= $order->id ?>] = <?= json_encode($order) ?>
            </script>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="dish-single-item">
              <span>Order Id: #<?php echo $order->id; ?></span><br>
              <span>Total Amount: $<?php echo $order->total."<br>"; ?></span>

              <?php echo $this->delivery_address($order->id)."<br>"; ?>
              <?php echo $this->delivery_date($order->id)."<br>"; ?>
              <p>Created At: <?php echo date('F d, Y h:i A',strtotime($order->created_at))."<br>"; ?></p><br>
              <span>Status: <?php 
              if($order->status == 0){
                echo "pending";

              }elseif ($order->status == 1) {
                echo "Confirmed";
              }

              elseif ($order->status == 2) {
                echo "Rejected";
              }?>

            </span>
            <div class="order-btn">
              <a href="#order-detail" class="orders-popup-order" data-order-id="<?= $order->id ?>">View Order</a>
              </div>
            </div>
          </div>

        <?php }?>
      </div>
    </div>
  </div>

<?php } ?>

<!-- Order Invoice popup for PDF -->
<div class="invoice-popup">
    <div class="modal fade" id="order_invoice" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header invoice_header">
                    <button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="content">
                    <div id="wpPUSResult">
                      <div class="resultWrapper">
                        <div class="header_wrapper">
                          <table class="header_table">
                            <tbody>
                              <tr>
                                <td class="dateTableData">
                                  <span><img src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/images/wyzchef.png"></span><br />
                                </td>
                                <td style=text-align:left >
                                  <span>HELP: +65 8891 3599</span>
                                </td>
                                <td>
                                <span>EMAIL: order@wyzchef.com</span>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div class="order-details-info"></div>
                      </div>
                    </div>
                </div>
                <div id="editor"></div>
                <div class="modal-footer">
                    <div class="clearfix"></div>
                    <button type="button" class="btn btn-secondary pdf_download download-order-info">Print Order</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
<script type="text/javascript">
    jQuery(document.body).on("click", ".download-order-info", function(){
        window.print();

        return false;
    });

    jQuery('.order-give-review').on("click", function(){
        let orderId = jQuery(this).attr('data-order-id');
        let orderRestaurantId = jQuery(this).attr('data-restaurant-id');
        jQuery('#order_id').val(orderId);
        jQuery('#restaurant_id').val(orderRestaurantId);

        jQuery('.rating').prop('checked', false);
        jQuery('#review_title').val('');
        jQuery('#review_description').val('');

        jQuery('#order-review-modal').modal();

        return false;
    });

    jQuery('.orders-popup-order').on("click", function(){
        let orderId = jQuery(this).attr('data-order-id');
        jQuery('.order-id').text(orderId);

        jQuery.ajax({
            type: "POST",
            dataType: 'json', 
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            data: {
                'action': 'orderDishDetails',
                'order': JSON.stringify(orderObj[orderId])
            },
            success: function(data) {
                jQuery('.order-details-info').html(data['html']);
                jQuery('#order_invoice').modal();
            }
        });

        return false;
    });

    // var pdf = new jsPDF();
     
    // jQuery('#cmd').click(function () {
    //     pdf.addHTML(jQuery('#content')[0], function () {
    //         pdf.save('order-invoice.pdf');
    //         pdf.internal.scaleFactor = 10;     
    //     });
    // });
</script>
