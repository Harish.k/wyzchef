<?php
$prefrenceTable    = $wpdb->prefix . 'wyz_user_preferences';
$prefrences = $wpdb->get_row("SELECT * FROM $prefrenceTable WHERE `user_id` = $user_id",ARRAY_A);

$dietary_restriction_array = explode(",",$prefrences['dietary_restriction']);

$nationalities_array = explode(",",$prefrences['nationalities']);

$cuisines = explode(",",$prefrences['restaurant_types']);

$servicesy = explode(",",$prefrences['services']);

?>

<link rel='stylesheet' href='<?php echo $pluginURL; ?>assets/css/select-2min.css' />

<div class="admin-form-container">
    <form id="user-preference-form" class="" name="" action="" method="POST">
        <h2>Company food preferences and priorities</h2>
        <div class="preferrence-content">
            <div class="row">
                <div class="col-md-12">
                    <h4><span><img src="<?php echo $pluginURL; ?>assets/images/preferred-type-of-cuisine-catering.svg" alt="dine icon"></span>Preferred type of cuisine</h4>
                    <ul class="inline-display-list mid-1-check cuisines-container"></ul>
                </div>
            </div> 
        </div> <!-- preferrence-content -->

<!--<div class="preferrence-content">
<div class="row">
<div class="col-md-12">
<h4><span><img src="<?php //echo $pluginURL; ?>assets/images/dine_ico.png" alt="dine icon"></span>Preferred type of meals</h4>
<ul class="inline-display-list mid-1-check meals-container"></ul>
</div>
</div>
</div> <!-- preferrence-content -->

<div class="preferrence-content services-preferred">
    <h4><span><img src="<?php echo $pluginURL; ?>assets/images/type-of-services-catering-for-offices.svg" alt="dine icon"></span>Preferred type of services</h4>
    <div class="row">
        <div class="col-md-12">
            <ul class="inline-display-list mid-1-check services-container"></ul>
        </div>
    </div>
</div> <!-- preferrence-content -->
<div class="preferrence-content">
    <div class="row">
        <div class="col-md-6">
            <h4><span><img src="<?php echo $pluginURL; ?>assets/images/money-bag-with-dollar-symbol.svg" alt="budget icon"></span>Average budget / pax</h4>
            <fieldset>
                <div class="field ">
                    <select name="budget_select" class="budget_select" id="budget_select" form-control="">
                        <option value="null">Select your budget</option>
                        <option value="Less than $10/pax" <?php if($prefrences['budget']== "Less than $10/pax" && !empty($prefrences['budget']))  { echo "selected"; }?>>Less than $10/pax</option>
                        <option value="$11-$20/pax" <?php if($prefrences['budget']== "$11-$20/pax" && !empty($prefrences['budget']))  { echo "selected"; }?>>$11-$20/pax</option>
                        <option value="$21-$35/pax" <?php if($prefrences['budget']== "$21-$35/pax" && !empty($prefrences['budget']))  { echo "selected"; }?>>$21-$35/pax</option>
                        <option value="$36-$50/pax" <?php if($prefrences['budget']== "$36-$50/pax" && !empty($prefrences['budget']))  { echo "selected"; }?>>$36-$50/pax</option>
                        <option value="More than $50/pax" <?php if($prefrences['budget']== "More than $50/pax" && !empty($prefrences['budget']))  { echo "selected"; }?>>More than $50/pax</option>
                    </select>
                </div>
            </fieldset>
        </div> <!-- col-md-6 div -->
        <div class="col-md-6">
            <h4><span><img src="<?php echo $pluginURL; ?>assets/images/dieatary-restrictions-for-catering.svg" alt="select icon"></span>Any dietary restriction ?</h4>
            <fieldset>
                <div class="field ">
                    <select name="dietary_restriction[]" id="dietary_restriction" data-style="btn-default" placeholder="Select" class="selectpicker form-control" multiple data-max-options="2">
                        <option value="Halal" <?php if (in_array("Halal", $dietary_restriction_array))
                        { echo "selected";}?>>Halal</option>
                        <option value="Vegetarian" <?php if (in_array("Vegetarian", $dietary_restriction_array))
                        { echo "selected";}?>>Vegetarian</option>
                        <option value="Vegan" <?php if (in_array("Vegan", $dietary_restriction_array))
                        { echo "selected";}?>>Vegan</option>
                        <option value="Gluten Free" <?php if (in_array("Gluten Free", $dietary_restriction_array))
                        { echo "selected";}?> >Gluten Free</option>
                        <option value="Nut Free" <?php if (in_array("Nut Free", $dietary_restriction_array))
                        { echo "selected";}?>>Nut Free</option>
                        <option value="Low Sugar" <?php if (in_array("Low Sugar", $dietary_restriction_array))
                        { echo "selected";}?>>Low Sugar</option>
                    </select>
                </div>
            </fieldset>
        </div> <!-- col-md-6 div -->
    </div> <!-- row -->
    <div class="row">
        <div class="col-md-6">
            <h4><span><img src="<?php echo $pluginURL; ?>assets/images/priorities-for-catering-order.svg" alt="dine icon"></span>Select your first 3 priorities</h4>
            <fieldset>
                <div class="field priorities_fields">
                    <select name="first_priority" id="first_priority" form-control="">
                        <option value="null">First priority</option>
                        <option value="Price fits my budget" <?php if($prefrences['first_priority']== "Price fits my budget" && !empty($prefrences['first_priority']))  { echo "selected"; }?>>Price fits my budget</option>
                        <option value="Can place last minute order" <?php if($prefrences['first_priority']== "Can place last minute order" && !empty($prefrences['first_priority']))  { echo "selected"; }?>>Can place last minute order</option>
                        <option value="Food Quality" <?php if($prefrences['first_priority']== "Food Quality" && !empty($prefrences['first_priority']))  { echo "selected"; }?>>Food Quality</option>
                        <option value="Presentation of the food" <?php if($prefrences['first_priority']== "Presentation of the food" && !empty($prefrences['first_priority']))  { echo "selected"; }?>>Presentation of the food</option>
                        <option value="Dietary restriction" <?php if($prefrences['first_priority']== "ietary restriction" && !empty($prefrences['first_priority']))  { echo "selected"; }?>>Dietary restriction</option>
                        <option value="Big portion size" <?php if($prefrences['first_priority']== "Big portion size" && !empty($prefrences['first_priority']))  { echo "selected"; }?>>Big portion size</option>
                        <option value="Other" <?php if($prefrences['first_priority']== "Other" && !empty($prefrences['first_priority']))  { echo "selected"; }?>>Other</option>
                    </select>
                    <select name="second_priority" id="second_priority" form-control="">
                        <option value="null">Second priority</option>
                        <option value="Price fits my budget" <?php if($prefrences['second_priority']== "Price fits my budget" && !empty($prefrences['second_priority']))  { echo "selected"; }?>>Price fits my budget</option>
                        <option value="Can place last minute order" <?php if($prefrences['second_priority']== "Can place last minute order" && !empty($prefrences['second_priority']))  { echo "selected"; }?>>Can place last minute order</option>
                        <option value="Food Quality" <?php if($prefrences['second_priority']== "Food Quality" && !empty($prefrences['second_priority']))  { echo "selected"; }?>>Food Quality</option>
                        <option value="Presentation of the food" <?php if($prefrences['second_priority']== "Presentation of the food" && !empty($prefrences['second_priority']))  { echo "selected"; }?>>Presentation of the food</option>
                        <option value="Dietary restriction" <?php if($prefrences['second_priority']== "Dietary restriction" && !empty($prefrences['second_priority']))  { echo "selected"; }?>>Dietary restriction</option>
                        <option value="Big portion sizel" <?php if($prefrences['second_priority']== "Big portion sizel" && !empty($prefrences['second_priority']))  { echo "selected"; }?>>Big portion size</option>
                        <option value="Other" <?php if($prefrences['second_priority']== "Other" && !empty($prefrences['second_priority']))  { echo "selected"; }?>>Other</option>
                    </select>
                    <select name="third_priority" id="third_priority" form-control="">
                        <option value="null">Third priority</option>
                        <option value="Price fits my budget" <?php if($prefrences['third_priority']== "Price fits my budget" && !empty($prefrences['third_priority']))  { echo "selected"; }?>>Price fits my budget</option>
                        <option value="Can place last minute order" <?php if($prefrences['third_priority']== "Can place last minute order" && !empty($prefrences['third_priority']))  { echo "selected"; }?>>Can place last minute order</option>
                        <option value="Food Quality" <?php if($prefrences['third_priority']== "Food Quality" && !empty($prefrences['third_priority']))  { echo "selected"; }?>>Food Quality</option>
                        <option value="Presentation of the food" <?php if($prefrences['third_priority']== "Presentation of the food" && !empty($prefrences['third_priority']))  { echo "selected"; }?>>Presentation of the food</option>
                        <option value="Dietary restriction" <?php if($prefrences['third_priority']== "Dietary restriction" && !empty($prefrences['third_priority']))  { echo "selected"; }?>>Dietary restriction</option>
                        <option value="Big portion size" <?php if($prefrences['third_priority']== "Big portion size" && !empty($prefrences['third_priority']))  { echo "selected"; }?>>Big portion size</option>
                        <option value="Other" <?php if($prefrences['third_priority']== "Other" && !empty($prefrences['third_priority']))  { echo "selected"; }?>>Other</option>
                    </select>
                </div>
            </fieldset>
        </div> <!-- col-md-6 div -->
        <div class="col-md-6">
            <h4><span><img src="<?php echo $pluginURL; ?>assets/images/nationalities-of-the-team-catering.svg" alt="dine icon"></span>Ethnicities within your team</h4>
            <fieldset>
                <div class="field ">
                    <?php
                    $myfile = fopen(WYZ_PLUGIN_ROOT_PATH . 'corporate/countries.txt', "r") or die("Unable to open file!");
                    $countries = fread($myfile,filesize(WYZ_PLUGIN_ROOT_PATH . 'corporate/countries.txt'));
                    $nationalities = explode(PHP_EOL, $countries);
                    ?>
                    <select name="nationalities[]" id="nationalities" data-style="btn-default" class="selectpicker form-control" multiple data-max-options="2">
                        <?php
                        foreach ($nationalities as $key => $nationality) {?>
                            <option value="<?php echo $nationality; ?>" <?php if (in_array($nationality, $nationalities_array))
                            { echo "selected";}?> ><?php echo $nationality; ?></option>
                        <?php } ?>

                    </select>
                </div>
            </fieldset>
        </div> <!-- col-md-6 div -->
    </div> <!-- row -->
</div> <!-- preferrence-content -->
<div class="preferrence-content">
    <input type="hidden" name="" id="user_id" data-id="<?php echo get_current_user_id(); ?>" value="<?php echo get_current_user_id(); ?>" />
</div> 
<div class="preferrence-content">
    <button type="button" name="prefrence_save_data" class="btn btn-default prefrence-save admin-save">SAVE</button>
</div>  

</form>
<div class="preferrence-content" >
    <div class="success" style="
    padding: 10px;
    text-align: center;
    background-color: #ef7844;
    color: #fff;
    font-weight: bold;
    display:none;
    "></div>
</div>
</div> <!-- admin-form-container -->
<?php
$restaurant_type = get_terms('restaurant_type', array( 'hide_empty' => 0 ));
$customPostTypeDetails = array();
if (count($restaurant_type) > 0) {
    foreach ($restaurant_type as $keyTerm => $valueTerm) {
        $checked =  in_array( $valueTerm->term_id, $cuisines ) ? true : false;
        $customPostTypeDetails['cuisines'][] = array(
            'text' => $valueTerm->name,
            'value' => $valueTerm->term_id,
            'checked' => $checked,
        );
    }
}

$myfile = fopen(WYZ_PLUGIN_ROOT_PATH . 'corporate/services.txt', "r") or die("Unable to open file!");
$services = fread($myfile,filesize(WYZ_PLUGIN_ROOT_PATH . 'corporate/services.txt'));
$skuList = explode(PHP_EOL, $services);
$x=0;
foreach ($skuList as $key => $valueTerm) {

    $val = explode(',', $valueTerm);

    for($i=0;$i<count($val); $i++){

        $checked =  in_array($customPostTypeDetails['services'][$x]['value'], $servicesy ) ? true : false;
        $customPostTypeDetails['services'][$x]['text'] = $val[0];
        $customPostTypeDetails['services'][$x]['value'] = $val[1];
        $customPostTypeDetails['services'][$x]['checked'] = $checked;
        $customPostTypeDetails['services'][$x]['img'] = $pluginURL.$val[2];


    }
    $x++;     
}

fclose($myfile);

// $customPostTypeDetails['services'] = array(
//     array(
//         'text' => 'Mini Buffet',
//         'value' => 1,
//         'checked' => false,
//         'img' => "$pluginURL/assets/images/preferred-services-1.png"
//     ),
//     array(
//         'text' => 'Mee Mian',
//         'value' => 2,
//         'checked' => false,
//         'img' => "$pluginURL/assets/images/preferred-services-2.png"
//     ),
//     array(
//         'text' => 'Dim Sum',
//         'value' => 3,
//         'checked' => false,
//         'img' => "$pluginURL/assets/images/preferred-services-3.png"
//     ),
//     array(
//         'text' => 'Hot Pot',
//         'value' => 4,
//         'checked' => false,
//         'img' => "$pluginURL/assets/images/preferred-services-4.png"
//     ),
//     array(
//         'text' => 'Stir Fried Tofu',
//         'value' => 5,
//         'checked' => false,
//         'img' => "$pluginURL/assets/images/preferred-services-5.png"
//     ),
//     array(
//         'text' => 'Fried rice',
//         'value' => 6,
//         'checked' => false,
//         'img' => "$pluginURL/assets/images/preferred-services-6.png"
//     )
// ); 

//echo "<pre>";
//print_r($customPostTypeDetails);

$restaurant_json = json_encode($customPostTypeDetails);

?>
<script type="text/javascript">
    var preferenceObj = <?php echo $restaurant_json; ?>;
//console.log(preferenceObj);

/*var preferenceObj = {
'cuisines': [
{
'text': 'American',
'value': 1,
'checked': false,
'img': ''
},
{
'text': 'Chinese',
'value': 2,
'checked': true,
'img': ''
},
{
'text': 'Indian',
'value': 3,
'checked': true,
'img': ''
},
{
'text': 'Japanese',
'value': 4,
'checked': false,
'img': ''
},
],
'meals': [
{
'text': 'Dumplings',
'value': 1,
'checked': false,
'img': ''
},
{
'text': 'Mee Mian',
'value': 2,
'checked': true,
'img': ''
},
{
'text': 'Dim Sum',
'value': 3,
'checked': true,
'img': ''
},
{
'text': 'Hot Pot',
'value': 4,
'checked': false,
'img': ''
},
{
'text': 'Stir Fried',
'value': 5,
'checked': false,
'img': ''
},
{
'text': 'Fried rice',
'value': 6,
'checked': true,
'img': ''
},
{
'text': 'Kimchi',
'value': 7,
'checked': false,
'img': ''
},
],
'services': [
{
'text': 'Mini Buffet',
'value': 1,
'checked': false,
'img': '<?php echo $pluginURL; ?>assets/images/preferred-services-1.png'
},
{
'text': 'Mee Mian',
'value': 2,
'checked': true,
'img': '<?php echo $pluginURL; ?>assets/images/preferred-services-2.png'
},
{
'text': 'Dim Sum',
'value': 3,
'checked': true,
'img': '<?php echo $pluginURL; ?>assets/images/preferred-services-3.png'
},
{
'text': 'Hot Pot',
'value': 4,
'checked': false,
'img': '<?php echo $pluginURL; ?>assets/images/preferred-services-4.png'
},
{
'text': 'Stir Fried Tofu',
'value': 5,
'checked': true,
'img': '<?php echo $pluginURL; ?>assets/images/preferred-services-5.png'
},
{
'text': 'Fried rice',
'value': 6,
'checked': false,
'img': '<?php echo $pluginURL; ?>assets/images/preferred-services-6.png'
},
]
};*/




for(let key in preferenceObj) {
    for (let i = 0; i < preferenceObj[key].length; i++) {
        let liObj = preferenceObj[key][i];
        jQuery('.' + key + '-container').append('\
            <li class="chkinput">\
            <label>\
            '+ (liObj.img ? '<img src="'+ liObj.img +'" alt="" />' : '') +'\
            <input  name="'+ key +'[]" type="checkbox" value="'+ liObj.value +'"  '+ (liObj.checked ? 'checked' : '') +'><span><i>'+ liObj.text +'</i></span>\
            </label>\
            </li>\
            ');
    }
}
</script>

<?php
wp_enqueue_script('select2js', WYZ_PLUGIN_ROOT_URL .'assets/js/select-2min.js');
?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.selectpicker').select2();
    }); 
</script>
<script type="text/javascript">
    jQuery(document).on("click", ".prefrence-save", function(e){
        var user_id = jQuery("#user_id").attr("data-id"); 
        var budget = jQuery('.budget_select').val();
        var dietary_restriction = jQuery('#dietary_restriction').val();
        var first_priority = jQuery('#first_priority').val();
        var second_priority = jQuery('#second_priority').val();
        var third_priority = jQuery('#third_priority').val();
        var nationalities = jQuery('#nationalities').val();
        var cuisines =[];
        var services =[]; 
        jQuery('input[name="cuisines[]"]:checked').each(function(){
            cuisines.push(jQuery(this).val());
        });
        jQuery('input[name="services[]"]:checked').each(function(){
            services.push(jQuery(this).val());

        });



        jQuery.ajax({
                type: "POST",
                url: "<?php echo admin_url('admin-ajax.php');?>",
                data: { 
                action: 'prefrence_save_data', // < note use of 'this' here
                cuisines: cuisines,
                services: services,
                budget: budget,
                dietary_restriction:dietary_restriction,
                first_priority: first_priority,
                second_priority: second_priority,
                third_priority:third_priority,
                nationalities:nationalities,
                user_id: user_id
                },
                success: function(result) {
                    if(result.success == true){
                        jQuery('.success').show();  
                        jQuery('.success').html(result.data);
                    }
                },
                error: function(result) {
                    alert('error');
                }
         });

    });   

</script>