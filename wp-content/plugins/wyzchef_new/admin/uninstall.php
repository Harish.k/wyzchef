<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * WYZ Restaurant uninstall hook
 */
register_uninstall_hook( WYZ_RESTAURANT_PLUGIN_FILE, 'wyz_restaurant_uninstall' );

/**
 * WYZ Restaurant uninstallation
 */
function wyz_restaurant_uninstall()
{
	global $wpdb;

	$pluginName = 'wyz-restaurant/wyz-restaurant.php';

	if (is_plugin_active($pluginName)) {
		return;
	}


}
?>