<?php
    // do_action('save_user_form'); 
    if(session_id() == '')  {
        session_start();
    }

    $adminUrl = admin_url('admin.php');
    $addCouponUrl = admin_url('admin.php?page=wyzchef-add-coupon');
    $postDataUrl = admin_url('admin-post.php');

    // Removing session data
    if(isset($_SESSION["msgArray"])){
        echo "<pre>Success! <strong>{$_SESSION[msgArray][success][msg]}</strong></pre>";
        unset($_SESSION["msgArray"]);
    }

    $nonce = wp_create_nonce( 'wyzchef-coupons' );

    global $wpdb;
    $sqlQuery = "
        SELECT *
        FROM {$wpdb->prefix}wyz_restaurant_order_coupons as c
    ";

    $coupons = $wpdb->get_results($sqlQuery);
?>
<div class="wrap">
    <h1 class="wp-heading-inline">Coupons</h1>
    <a href="<?php echo $addCouponUrl; ?>" class="page-title-action">Add New</a>
    <hr class="wp-header-end">

    <?php if (count($coupons)) { ?>
    <form id="posts-filter" method="get">
        <input type="hidden" name="coupon_status" class="coupon_status_page" value="all">
        <input type="hidden" name="page" class="page" value="wyzchef-add-coupon">
        <input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo $nonce; ?>">
        <input type="hidden" name="_wp_http_referer" value="<?php echo $addCouponUrl; ?>">

        <h2 class="screen-reader-text">Coupons list</h2>
        <table class="wp-list-table widefat fixed striped posts">
            <thead>
                <tr>
                    <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <a href="#" onclick="return false;"><span>Coupon Code</span></a>
                    </th>
                    <th scope="col" id="coupon-description" class="manage-column column-description">
                        <span class="coupon-description" title="Coupon Description">
                            <span class="screen-reader-text">Description</span>
                        </span>
                    </th>
                    <th scope="col" id="date" class="manage-column column-date sortable asc">
                        <a href="#" onclick="return false;"><span>Expiry Date</span></a>
                    </th>
                </tr>
            </thead>
            <tbody id="the-list">
                <?php foreach ($coupons as $key => $coupon) { ?>
                <tr id="coupon-<?php echo $coupon->id; ?>" class="iedit author-other level-0 coupon-<?php echo $coupon->id; ?> type-wp_restaurant status-publish hentry">
                    <td class="title column-title has-row-actions column-primary page-title" data-colname="Coupon Code">
                        <strong><a class="row-title" href="<?php echo $addCouponUrl ."&id={$coupon->id}"; ?>" aria-label="<?php echo $coupon->code; ?>"><?php echo $coupon->code; ?></a></strong>
                        <div class="row-actions">
                            <span class="edit"><a href="<?php echo $addCouponUrl ."&id={$coupon->id}"; ?>" aria-label="<?php echo $coupon->code; ?>">Edit</a> | </span><span class="trash"><a href="<?php echo "{$postDataUrl}?id={$coupon->id}&action=wyzchef_delete_coupon"; ?>" class="submitdelete" aria-label="Delete <?php echo $coupon->code; ?>">Delete</a></span>
                        </div>
                        <button type="button" class="toggle-row">
                            <span class="screen-reader-text">Show more details</span>
                        </button>
                    </td>
                    <td class="description column-description" data-colname="description">
                        <div class="post-com-count-wrapper">
                            <span aria-hidden="true">—</span>
                            <span class="screen-reader-text"><?php echo $coupon->description; ?></span>
                            <span class="post-com-count post-com-count-pending post-com-count-no-pending">
                                <span class="comment-count comment-count-no-pending" aria-hidden="true"><?php echo $coupon->description; ?></span>
                                <span class="screen-reader-text"><?php echo $coupon->description; ?></span>
                            </span>
                        </div>
                    </td>
                    <td class="date column-date" data-colname="Date">Expired
                        <br><abbr title="<?php echo $coupon->expiry; ?>"><?php echo $coupon->expiry; ?></abbr>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </form>
    <?php } else { ?>
        <h3>There is coupon yet. Please add.</h3>
    <?php } ?>
    <br class="clear">
</div>
<div class="clear"></div>