<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/*
if ( is_admin() ){
	add_action('admin_menu', 'wyz_restaurant_admin_menu');
}

function wyz_restaurant_admin_menu()
{
	$dish_menu_position = 4;

	add_menu_page('WYZ Restaurant', 'WYZ Restaurant', 'manage_options', 'wyz-restaurant', 'wyz_restaurant', WYZ_PLUGIN_ROOT_URL . 'assets/images/restaurant.svg', $dish_menu_position);
	add_submenu_page( 'wyz-restaurant', 'Restaurant', 'Restaurant', 'manage_options', 'wyz-restaurant', 'wyz_restaurant', WYZ_PLUGIN_ROOT_URL . 'assets/images/restaurant.svg');
    add_submenu_page( 'wyz-restaurant', 'Menu', 'Menu', 'manage_options', 'wyz-restaurant-menu', 'wyz_restaurant_menu');
    add_submenu_page( 'wyz-restaurant', 'Dishes', 'Dishes', 'manage_options', 'wyz-restaurant-dishes', 'wyz_restaurant_dishes');
}

function wyz_restaurant()
{
	$restaurant_flag = 0;

	if ( isset($_GET['action']) && $_GET['action']=='create_restaurant' )
	{
		include(dirname( __FILE__ ) . '/create-restaurant.php');
		$restaurant_flag = 1;
	}

	if ( $restaurant_flag == 0 )
	{
		header('location:'.site_url().'/wp-admin/edit.php?post_type=wp_restaurant');
		// require( dirname( __FILE__ ) . '/list-restaurant.php' );
	}
}

function wyz_restaurant_menu()
{
	$menu_flag = 0;

	if ( isset($_GET['action']) && $_GET['action']=='create-menu' )
	{
		include(dirname( __FILE__ ) . '/create-menu.php');
		$menu_flag = 1;
	}

	if ( $menu_flag == 0 )
	{
		require( dirname( __FILE__ ) . '/list-menu.php' );
	}
}

function wyz_restaurant_dishes()
{
	$dish_flag = 0;

	if ( isset($_GET['action']) && $_GET['action']=='create_dish' )
	{
		include(dirname( __FILE__ ) . '/create-dish.php');
		$dish_flag = 1;
	}

	if ( $dish_flag == 0 )
	{
		require( dirname( __FILE__ ) . '/list-dish.php' );
	}
}
*/

if(is_admin()){
	add_action('admin_enqueue_scripts', 'wyz_restaurant_add_style_script');

	function wyz_restaurant_add_style_script()
	{
		wp_enqueue_script('jquery');

		wp_register_script( 'wyz_restaurant_custom_script', WYZ_PLUGIN_ROOT_URL .'assets/js/wyz-custom-script.js' );
		wp_enqueue_script( 'wyz_restaurant_custom_script' );

		$translation_array = array(
			'admin_ajax' => admin_url( 'admin-ajax.php' )
		);
		wp_localize_script('wyz_restaurant_custom_script', 'restaurant', $translation_array);

		wp_register_style('wyz_restaurant_bootstrap', WYZ_PLUGIN_ROOT_URL .'assets/css/bootstrap4.0.0.min.css');
		wp_enqueue_style('wyz_restaurant_bootstrap');

		wp_register_style('wyz_restaurant_style', WYZ_PLUGIN_ROOT_URL .'assets/css/dish-styles.css');
		wp_enqueue_style('wyz_restaurant_style');
	}
}
