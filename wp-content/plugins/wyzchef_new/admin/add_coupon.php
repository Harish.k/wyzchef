<?php
    // do_action('save_user_form'); 
    if(session_id() == '')  {
        session_start();
    }

    // $adminUrl = admin_url('admin.php');
    $addCouponUrl = admin_url('admin.php?page=wyzchef-add-coupon');
    $postDataUrl = admin_url('admin-post.php');

    // Removing session data
    if(isset($_SESSION["msgArray"])){
        echo "<pre>Success! <strong>{$_SESSION[msgArray][success][msg]}</strong></pre>";
        unset($_SESSION["msgArray"]);
    }

    $nonce = wp_create_nonce( 'wyzchef-add-coupon' );
    $currentUser = wp_get_current_user();

    if (isset($_GET['id']) && !empty($_GET['id'])) {
        global $wpdb;
        $sqlQuery = "
            SELECT *
            FROM {$wpdb->prefix}wyz_restaurant_order_coupons as c  
            WHERE c.id = {$_GET[id]}
        ";

        $couponData = $wpdb->get_row($sqlQuery);
    } else {
        $couponData = new stdclass();
        $couponData->id = '';
        $couponData->code = '';
        $couponData->description = '';
        $couponData->type = '';
        $couponData->value = '';
        $couponData->status = 1;
        $couponData->expiry = '';
    }
?>
<link rel="stylesheet" href="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/css/bootstrap-datetimepicker.css" >
<style type="text/css">
    .coupon-inputs-container fieldset {
        margin: 8px auto;
    }
</style>

<div class="wrap">
    <h1 class="wp-heading-inline">Coupons</h1>
    <a href="<?php echo $addCouponUrl; ?>" class="page-title-action">Add New</a>
    <hr class="wp-header-end">

    <form id="coupon-form" method="post" name="coupon-form" action="<?php echo $postDataUrl; ?>" >
        <input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo $nonce; ?>">
        <input type="hidden" id="form-action" name="action" value="wyzchef_save_coupon">
        <input type="hidden" name="page" class="page" value="wyzchef-add-coupon">
        <input type="hidden" id="user-id" name="user_id" value="<?php echo $currentUser->ID; ?>">
        <input type="hidden" id="coupon-id" name="coupon[id]" value="<?php echo $couponData->id; ?>">

        <div class="coupon-inputs-container">
            <fieldset>
                <label>Coupon Code</label>
                <div class="field">
                    <input type="text" name="coupon[code]" placeholder="Enter the Coupon Code" class="" value="<?php echo $couponData->code; ?>" />
                </div>
            </fieldset>
            <fieldset>
                <label>Coupon Description</label>
                <div class="field">
                    <textarea name="coupon[description]" placeholder="Enter the Coupon Description" class=""><?php echo $couponData->description; ?></textarea>
                </div>
            </fieldset>
            <fieldset>
                <label>Coupon type</label>
                <div class="field">
                    <select name="coupon[type]">
                        <option value="0">Fix Discount</option>
                        <option value="1" <?php echo ($couponData->type == 1 ? 'selected' : ''); ?> >Percentage</option>
                    </select>
                </div>
            </fieldset>
            <fieldset>
                <label>Coupon amount/percentage</label>
                <div class="field">
                    <input type="number" name="coupon[value]" placeholder="Enter the Coupon amount/percentage" class="" value="<?php echo $couponData->value; ?>" />
                </div>
            </fieldset>
            <fieldset>
                <label>Coupon status</label>
                <div class="field">
                    <select name="coupon[status]">
                        <option value="1">Publish</option>
                        <option value="0" <?php echo ($couponData->status == 0 ? 'selected' : ''); ?> >Draft</option>
                    </select>
                </div>
            </fieldset>
            <fieldset>
                <label>Coupon expiry</label>
                <div class="field">
                    <input type="text" class="date-picker hasDatepicker" style="" name="coupon[expiry]" id="expiry_date" placeholder="YYYY-MM-DD" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" value="<?php echo $couponData->expiry; ?>" />
                </div>
            </fieldset>
            <fieldset>
                <button type="submit" class="button button-primary save"><?php echo $_GET['id'] ? 'Update' : 'Save'; ?></button>
            </fieldset>
        </div>
    </form>
</div>

<script src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/js/moment.min.js"></script>
<script src="<?php echo WYZ_PLUGIN_ROOT_URL; ?>assets/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript">
    $(function() {
        $('#expiry_date').datetimepicker({format: 'YYYY-MM-DD'});
    });
</script>