<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * WYZ Restaurant activation hook
 */
register_activation_hook( WYZ_RESTAURANT_PLUGIN_FILE ,'wyz_restaurant_install');


/**
 * WYZ Restaurant installation
 */
function wyz_restaurant_install()
{
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    global $wpdb;

	//add_option('wyz','');

	/**
	 * Table $wpdb->prefix_wyz_restaurant_menu
	 */
	$wyz_restaurant_menu = $wpdb->get_results('SHOW TABLE STATUS WHERE name="wyz_restaurant_menu"');

	if(count($wyz_restaurant_menu) > 0){
		$wpdb->query("RENAME TABLE wyz_restaurant_menu TO ".$wpdb->prefix."wyz_restaurant_menu");
	}else{
		$wyz_restaurant_menu_exist = $wpdb->get_results('SHOW TABLE STATUS WHERE name="'.$wpdb->prefix.'wyz_restaurant_menu"');

		if(count($wyz_restaurant_menu_exist) == 0){
			$query_wyz_restaurant_menu = "CREATE TABLE IF NOT EXISTS  ".$wpdb->prefix."wyz_restaurant_menu (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`title` varchar(200) NOT NULL,
			`icon` text NOT NULL,
			`status` enum('0','1') NOT NULL,
			`created_at` datetime NOT NULL,
			`modified_at` datetime NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1";

			$wpdb->query($query_wyz_restaurant_menu);
			$wpdb->query("INSERT INTO ".$wpdb->prefix."wyz_restaurant_menu (`id`, `title`, `icon`, `status`, `created_at`, `modified_at`) VALUES (NULL, 'Menu1', 'icon-1', '1', '2019-05-07 00:00:00', '2019-05-07 00:00:00'), (NULL, 'Menu-2',  'icon-2', '1', '2019-05-07 00:00:00', '2019-05-07 00:00:00');");
		}
	}

	/**
	 * Table $wpdb->prefix_wyz_restaurant_dishes
	 */
	$wyz_restaurant_dishes = $wpdb->get_results('SHOW TABLE STATUS WHERE name="wyz_restaurant_dishes"');

	if(count($wyz_restaurant_dishes) > 0){
		$wpdb->query("RENAME TABLE wyz_restaurant_dishes TO ".$wpdb->prefix."wyz_restaurant_dishes");
	}else{
		$wyz_restaurant_dishes_exist = $wpdb->get_results('SHOW TABLE STATUS WHERE name="'.$wpdb->prefix.'wyz_restaurant_dishes"');

		if(count($wyz_restaurant_dishes_exist) == 0){
			$query_wyz_restaurant_dishes = "CREATE TABLE IF NOT EXISTS  ".$wpdb->prefix."wyz_restaurant_dishes (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
				  `description` text COLLATE utf8_unicode_ci NOT NULL,
				  `image` longtext COLLATE utf8_unicode_ci NOT NULL,
				  `price` DECIMAL(10,2) NOT NULL,
				  `tax` FLOAT NOT NULL DEFAULT '7',
				  `min_quantity` int(11) NOT NULL,
				  `min_guest_serve` int(11) NOT NULL,
				  `max_guest_serve` int(11) NOT NULL,
				  `lead_time` int(11) NOT NULL,
				  `tags` text COLLATE utf8_unicode_ci NOT NULL,
				  `having_modifiers` INT NOT NULL DEFAULT '0',
				  `menu_id` int(11) NOT NULL,
				  `restaurant_id` int(11) NOT NULL,
				  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
				  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1";

			$wpdb->query($query_wyz_restaurant_dishes);
        }
	}

	/**
	 * Table $wpdb->prefix_wyz_restaurant_order
	 */
	$wyz_restaurant_order = $wpdb->get_results('SHOW TABLE STATUS WHERE name="wyz_restaurant_order"');

	if(count($wyz_restaurant_order) > 0){
		$wpdb->query("RENAME TABLE wyz_restaurant_order TO ".$wpdb->prefix."wyz_restaurant_order");
	}else{
		$wyz_restaurant_order_exist = $wpdb->get_results('SHOW TABLE STATUS WHERE name="'.$wpdb->prefix.'wyz_restaurant_order"');

		if(count($wyz_restaurant_order_exist) == 0){
			$query_wyz_restaurant_order = "CREATE TABLE IF NOT EXISTS  ".$wpdb->prefix."wyz_restaurant_order (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` int(11) NOT NULL,
			`payment_status` enum('0','1','2') NOT NULL,
			`total` int(11) NOT NULL,
			`status` enum('0','1','2','3') NOT NULL,
			`created_at` datetime NOT NULL,
			`delivery_fees` int(11) NOT NULL,
			`restaurant_id` varchar(200) NOT NULL,
			`order_rating` int(11) DEFAULT NULL,
			`gst` int(11) DEFAULT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1";

			$wpdb->query($query_wyz_restaurant_order);
		}
	}

	/**
	 * Table $wpdb->prefix_wyz_restaurant_order_details
	 */
	$wyz_restaurant_order_details = $wpdb->get_results('SHOW TABLE STATUS WHERE name="wyz_restaurant_order_details"');

	if(count($wyz_restaurant_order_details) > 0){
		$wpdb->query("RENAME TABLE wyz_restaurant_order_details TO ".$wpdb->prefix."wyz_restaurant_order_details");
	}else{
		$wyz_restaurant_order_details_exist = $wpdb->get_results('SHOW TABLE STATUS WHERE name="'.$wpdb->prefix.'wyz_restaurant_order_details"');

		if(count($wyz_restaurant_order_details_exist) == 0){
			$query_wyz_restaurant_order_details = "CREATE TABLE IF NOT EXISTS  ".$wpdb->prefix."wyz_restaurant_order_details (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`order_id` int(11) NOT NULL,
			`dish_id` int(11) NOT NULL,
			`size` varchar(10) NOT NULL,
			`quantity` int(11) NOT NULL,
			`line_total` FLOAT(11) NOT NULL,
			`created_at` datetime NOT NULL,
			`modifier` longtext NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1";

			$wpdb->query($query_wyz_restaurant_order_details);
		}
	}
    /** 
     * Table for storing the coupon codes
     */
    $wyz_restaurant_order_coupons_exist = $wpdb->get_results('SHOW TABLE STATUS WHERE name="'.$wpdb->prefix.'wyz_restaurant_order_coupons"');
    if(count($wyz_restaurant_order_coupons_exist) == 0){
        $query_wyz_restaurant_order_coupons = "CREATE TABLE IF NOT EXISTS  ".$wpdb->prefix."wyz_restaurant_order_coupons ( 
        `id` INT NOT NULL AUTO_INCREMENT ,
        `code` VARCHAR(100) NOT NULL ,
        `description` TEXT NULL ,
        `type` TINYINT NOT NULL ,
        `value` INT NOT NULL ,
        `status` INT NOT NULL ,
        `expiry` DATE NOT NULL ,
        PRIMARY KEY (`id`)) ENGINE = InnoDB"; 
        
        $wpdb->query($query_wyz_restaurant_order_coupons);
	}
	/**
	 * Table $wpdb->prefix_wyz_restaurant_order_coupon_mapping
	 */
	$wyz_restaurant_order_coupon_mapping = $wpdb->get_results('SHOW TABLE STATUS WHERE name="wyz_restaurant_order_coupon_mapping"');

	if(count($wyz_restaurant_order_coupon_mapping) > 0){
		$wpdb->query("RENAME TABLE wyz_restaurant_order_coupon_mapping TO ".$wpdb->prefix."wyz_restaurant_order_coupon_mapping");
	}else{
		$wyz_restaurant_order_coupon_mapping_exist = $wpdb->get_results('SHOW TABLE STATUS WHERE name="'.$wpdb->prefix.'wyz_restaurant_order_coupon_mapping"');

		if(count($wyz_restaurant_order_coupon_mapping_exist) == 0){
			$query_wyz_restaurant_order_coupon_mapping = "CREATE TABLE IF NOT EXISTS  ".$wpdb->prefix."wyz_restaurant_order_coupon_mapping (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`order_id` int(11) NOT NULL,
			`coupon_code` varchar(100) NOT NULL,
			`amount` int(11) NOT NULL,
			`status` enum('0','1') NOT NULL DEFAULT '1',
			`created_at` datetime NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1";

			$wpdb->query($query_wyz_restaurant_order_coupon_mapping);
		}
	}

	/**
	 * Table $wpdb->prefix_wyz_restaurant_order_delivery_details
	 */
	$wyz_restaurant_order_delivery_details = $wpdb->get_results('SHOW TABLE STATUS WHERE name="wyz_restaurant_order_delivery_details"');

	if(count($wyz_restaurant_order_delivery_details) > 0){
		$wpdb->query("RENAME TABLE wyz_restaurant_order_delivery_details TO ".$wpdb->prefix."wyz_restaurant_order_delivery_details");
	}else{
		$wyz_restaurant_order_delivery_details_exist = $wpdb->get_results('SHOW TABLE STATUS WHERE name="'.$wpdb->prefix.'wyz_restaurant_order_delivery_details"');

		if(count($wyz_restaurant_order_delivery_details_exist) == 0){
			$query_wyz_restaurant_order_delivery_details = "CREATE TABLE IF NOT EXISTS  ".$wpdb->prefix."wyz_restaurant_order_delivery_details (
    			`id` int(11) NOT NULL AUTO_INCREMENT,
    			`user_id` int(11) NOT NULL,
    			`order_id` int(11) NOT NULL,
    			`street` varchar(100) NOT NULL,
    			`floor` varchar(100) DEFAULT NULL,
    			`company` varchar(100) DEFAULT NULL,
    			`postal_code` varchar(100) DEFAULT NULL,
    			`city` varchar(100) DEFAULT NULL,
    			`instructions` text DEFAULT NULL,
    			`created_at` datetime NOT NULL,
    			`delivery_date` datetime NULL,
    			`delivery_phone` varchar(100) DEFAULT NULL,
    			`people` int(11) NOT NULL,
    			`email` VARCHAR(320) NOT NULL,
    			PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1";


           $wpdb->query($query_wyz_restaurant_order_delivery_details);


		}
	}

    $wyz_restaurant_dish_modifiers_exist = $wpdb->get_results('SHOW TABLE STATUS WHERE name="'.$wpdb->prefix.'wyz_restaurant_dish_modifiers"');
    if(count($wyz_restaurant_dish_modifiers_exist) == 0){
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}wyz_restaurant_dish_modifiers` ( 
                `id` INT NOT NULL AUTO_INCREMENT , 
                `prompt_text` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
                `modifier_type` INT NOT NULL , 
                `min` INT NULL , 
                `max` INT NULL , 
                `dish_id` INT NOT NULL , 
                PRIMARY KEY (`id`)
            ) ENGINE = InnoDB
        ");
    }
    
    $wpdb->query("
        CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}wyz_restaurant_dish_modifier_items` ( 
            `id` INT NOT NULL AUTO_INCREMENT , 
            `text` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
            `price` FLOAT NOT NULL , 
            `tax` FLOAT NOT NULL , 
            `total` FLOAT NOT NULL , 
            `modifier_id` INT NOT NULL , 
            PRIMARY KEY (`id`)
        ) ENGINE = InnoDB
    ");

    $table_name = $wpdb->prefix . 'wyz_user_preferences';
    $sql = "CREATE TABLE IF NOT EXISTS  ".$table_name."  (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `user_id` int(11) NOT NULL,
        `dietary_restriction` text COLLATE utf8_unicode_ci,
        `meals` text COLLATE utf8_unicode_ci,
        `services` text COLLATE utf8_unicode_ci,
        `budget` varchar(200) COLLATE utf8_unicode_ci ,
        `restaurant_types` text COLLATE utf8_unicode_ci,
        `first_priority` varchar(200) COLLATE utf8_unicode_ci,
        `second_priority` varchar(200) COLLATE utf8_unicode_ci,
        `third_priority` varchar(200) COLLATE utf8_unicode_ci,
        `nationalities` text COLLATE utf8_unicode_ci,
        PRIMARY KEY (`id`)  
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
    dbDelta($sql);

    $table_name = $wpdb->prefix . 'wyz_restaurant_services';
    $sql = "CREATE TABLE IF NOT EXISTS `{$table_name}` ( 
		`id` INT NOT NULL AUTO_INCREMENT , 
		`type` TINYINT NOT NULL , 
		`quantityRequired` TINYINT NOT NULL , 
		`question` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
		`restaurant_id` INT NOT NULL , 
		PRIMARY KEY (`id`), 
		INDEX `service_restaurant_id` (`restaurant_id`)
	) ENGINE = InnoDB;";
    dbDelta($sql);

    $table_name = $wpdb->prefix . 'wyz_restaurant_service_options';
    $sql = "CREATE TABLE IF NOT EXISTS `{$table_name}` ( 
		`id` INT NOT NULL AUTO_INCREMENT , 
		`name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
		`price` FLOAT NOT NULL DEFAULT '0' , 
		`tax` FLOAT NOT NULL DEFAULT '7' , 
		`total` FLOAT NOT NULL DEFAULT '0' , 
		`service_id` INT NOT NULL , 
		PRIMARY KEY (`id`), 
		INDEX `option_service_id` (`service_id`)
	) ENGINE = InnoDB;";
    dbDelta($sql);

    $table_name = $wpdb->prefix . 'wyz_restaurant_order_services';
    $sql = "CREATE TABLE IF NOT EXISTS `{$table_name}` ( 
		`id` INT NOT NULL AUTO_INCREMENT , 
		`order_id` INT NOT NULL ,  
		`service_id` INT NOT NULL ,  
		`quantity` INT NOT NULL ,  
		`base_price` FLOAT NOT NULL DEFAULT '0' , 
		`total_price` FLOAT NOT NULL DEFAULT '0' , 
		`options` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL , 
		PRIMARY KEY (`id`), 
		INDEX `order_id` (`order_id`), 
		INDEX `order_service_id` (`service_id`) 
	) ENGINE = InnoDB;";
    dbDelta($sql);
}

/**
 * Restaurant details page display
 */
add_filter('single_template', 'restaurant_details');

function restaurant_details($single)
{
	global $post;

	/* Checks for single template by post type */
	if ( $post->post_type == 'wp_restaurant' ) {

		if ( file_exists( plugin_dir_path(__FILE__) . '../frontend/wyz-restaurant-details.php' ) ) {
			return plugin_dir_path(__FILE__) . '../frontend/wyz-restaurant-details.php';
		}
	}

	return $single;
}

/**
 * Restaurant details page display
 */
add_filter('page_template', 'checkout_page');

function checkout_page($single)
{
	global $post;

	/* Checks for single template by post type */
	if ( $post->post_title == 'Checkout' ) {
		add_action('wp_footer', 'wyzAddMainClass');

		if ( file_exists( plugin_dir_path(__FILE__) . '../frontend/wyz-restaurant-checkout.php' ) ) {
			return plugin_dir_path(__FILE__) . '../frontend/wyz-restaurant-checkout.php';
		}
	}

	return $single;
}

add_action( 'admin_post_nopriv_checkout_login','checkout_login' );

function checkout_login()
{
	if($_POST["action"] == "checkout_login") {
		$referrer 						= $_POST['redirectLink']; 
		$credentials['user_login'] 		= $_POST['email'];
		$credentials['user_password'] 	= $_POST['password'];
	    $user 							= wp_signon($credentials);
	    if($user->errors)
	    	exit( wp_redirect( add_query_arg('login', 'failed', $referrer) ) );
	    else
	    	exit( wp_redirect( $referrer ) );
	}
}

add_action( 'admin_post_nopriv_checkout_register','checkout_register' );
add_action( 'admin_post_checkout_register','checkout_register' );

function checkout_register()
{

	$exists 	= email_exists( $_POST["email"] );
	$referrer 	= $_POST['redirectLink'];
	if (get_current_user_id() <= 0) {

			if ( $exists ) {
		    	exit( wp_redirect( add_query_arg(array(
						    'email' => 'registered',
						    'id' 	=> $_POST["email"],
							), $referrer) ) );
			}
			else {
			    $userdata = array(
				    'user_login'  	=>  $_POST['email'],
				    'user_email'  	=>  $_POST['email'],
				    'user_nicename' =>	$_POST['first_name'],
				    'user_url'    	=>  "",
				    'user_pass'   	=>  $_POST['password'] 
				);

				$user_id = wp_insert_user( $userdata ) ;
				if($user_id){
					 update_user_meta( $user_id, 'first_name', $_POST['first_name'] );
                     update_user_meta( $user_id, 'last_name', $_POST['last_name'] );
		             update_user_meta( $user_id, 'User_phone', $_POST['mobile'] );


				}
				
				if (! is_wp_error( $user_id ) ) {
					$credentials['user_login'] 		= $_POST['email'];
					$credentials['user_password'] 	= $_POST['password'];
				    $user 							= wp_signon($credentials);
					$redirect_to 					= !empty( $_POST['redirectLink'] ) ? $_POST['redirectLink'] : 'login?checkemail=registered';
					exit( wp_redirect( add_query_arg(array(
						    'tab' => 'delivery'
					), $redirect_to) ) );
				}
				else {
					$redirect_to = !empty( $_POST['redirectLink'] ) ? $_POST['redirectLink'] : 'login?checkemail=registered';
					wp_safe_redirect( $redirect_to );
					exit();
				}
			}
	}
	else {
		    update_user_meta( get_current_user_id(), 'first_name', $_POST['first_name'] );
            update_user_meta( get_current_user_id(), 'last_name', $_POST['last_name'] );
		    update_user_meta( get_current_user_id(), 'User_phone', $_POST['mobile'] );

   
			$redirect_to = !empty( $_POST['redirectLink'] ) ? $_POST['redirectLink'] : 'login?checkemail=registered';
				exit( wp_redirect( add_query_arg(array(
					    'tab' => 'delivery',
					    'email' => $_POST['email'],
				), $redirect_to,$userEmail) ) );
	}
	
}

add_action( 'admin_post_nopriv_checkout_delivery','redirect_cart' );
add_action( 'admin_post_checkout_delivery','checkout_delivery' );

function redirect_cart()
{
	$referrer = $_POST['redirectLink'];
	exit( wp_redirect( add_query_arg('login', 'failed', $referrer) ) );
}

function checkout_delivery()
{
	global $wpdb;
	$gst = 0;
	$total = 0;
	$totalBasePrice = 0;
	// $order_date = '';

	$userId 	= get_current_user_id();
	$unit_address = implode(",",$_POST['unit_address']);
	// $cartItems 	= $_SESSION["cart_item"];
	$cart 	= json_decode(str_replace('\\', '', $_POST['cart']));
	$restro_id = $cart->restaurantId;
	$order_date = $_POST['delivery_date'];
	$cartData = $cart->cartData;
	$deliveryObj = $cartData->deliveryObj;
	$cartServiceItems = $cartData->cartServiceItems;
	$cartItems = $cartData->cartItems;

	foreach ($cartItems as $key => $dish) {
		$totalBasePrice += $dish->totalBasePrice;
		$gst += $dish->totalGstPrice - $dish->totalBasePrice;
		$total += $dish->totalGstPrice;
	}

	$deliveryGST = 0;
    $deliveryfee = 0;
	for($i = (count($deliveryObj->conditional_price) - 1); $i >= 0; $i--) {
		if ($total > $deliveryObj->conditional_price[$i]) {
			$deliveryfee = floatval($deliveryObj->applicable_fee[$i] ? $deliveryObj->applicable_fee[$i] : 0);
            $deliveryGST = 0.07*$deliveryfee;
            $deliveryfee = 1.07*$deliveryfee;
			break;
		}
	}
    $gst += $deliveryGST;
	$total += $deliveryfee;

	foreach ($cartServiceItems as $key => $service) {
		$totalBasePrice += $service->totalBasePrice;
		$gst += $service->totalGstPrice - $service->totalBasePrice;
		$total += $service->totalGstPrice;
	}

	/*foreach ($cartItems as $key => $cartItem) {
        // $restro_id = $_SESSION["cart_item"][$key]['restro_id'];
        // $order_date = $_SESSION["cart_item"][$key]["delivery_date"];

        if (isset($_SESSION['coupon_applied']) && $_SESSION['coupon_applied']) {
            $total += ($cartItem['total_amount'] + $cartItem['tax'] + $cartItem['total_modifier_tax']);
            $gst += ($cartItem['tax'] + $cartItem['total_modifier_tax']);
        } else {
    		$total += ($cartItem["amount"] + $cartItem["tax"]);
    		$gst += $cartItem["tax"];
        }
	}*/

	/*if($order_date == ''){
		$delivery_date = $_POST['delivery_date'];
		$delivery_date = date('Y-m-d H:i:s', strtotime($delivery_date));

	} else{
		$delivery_date = date('Y-m-d H:i:s', strtotime($order_date));
	}*/
	$delivery_date = date('Y-m-d H:i:s', strtotime($order_date));
    $creation_date = current_time('mysql', false);

	/*$delivery_fees = get_post_meta($restro_id, "wp_restaurant_delivery_fee", true);
	$total += $delivery_fees;*/

	$tableName     = $wpdb->prefix . 'wyz_restaurant_order';
    $success = $wpdb->insert($tableName, array(
		"user_id" 		=> $userId,
		"payment_status"=> 0,
		"total" 		=> $total,
		"status" 		=> 0,
		"created_at" 	=> $creation_date,
		"delivery_fees" => $deliveryfee,
		"restaurant_id" => $restro_id,
		"gst"           => $gst,
	));

	if($success) {
		$orderId 				= $wpdb->insert_id;
		$orderDetailsTable 		= "{$wpdb->prefix}wyz_restaurant_order_details";
		$deliveryDetailsTable 	= "{$wpdb->prefix}wyz_restaurant_order_delivery_details";
		$orderServicesTable 	= "{$wpdb->prefix}wyz_restaurant_order_services";

		foreach ($cartItems as $key => $cartItem) {
            /*if (!empty($cartItem['radioValues']) && !empty($cartItem['checkboxValues'])) {
                $cartItem['modifier'] = array_merge($cartItem['radioValues'], $cartItem['checkboxValues'] );
            } elseif (!empty($cartItem['radioValues'])) {
                $cartItem['modifier'] = $cartItem['radioValues'];
            } elseif (!empty($cartItem['checkboxValues'])) {
                $cartItem['modifier'] = $cartItem['checkboxValues'];
            } else {
                $cartItem['modifier'] = [];
            }

            if (isset($_SESSION['coupon_applied']) && $_SESSION['coupon_applied']) {
                $cartItem['amount'] = $cartItem['total_amount'];
            }*/

			$wpdb->insert($orderDetailsTable, array(
				"order_id" 	=> $orderId,
				"dish_id" 	=> $cartItem->dish->id,
				"size" 		=> '',
				"quantity" 	=> $cartItem->quantity,	
				"line_total" => $cartItem->totalGstPrice,
				"created_at" => $creation_date,
				"modifier" => maybe_serialize($cartItem->modifiers),
			));
		}

		foreach ($cartServiceItems as $key => $service) {
			$wpdb->insert($orderServicesTable, array(
				"order_id" 	=> $orderId,
				"service_id" => $service->serviceId,
				"quantity" 	=> $service->quantity,	
				"base_price" => $service->totalBasePrice,
				"total_price" => $service->totalGstPrice,
				"options" => maybe_serialize($service->serviceOptions),
			));
		}

		$deliveryDetailsData = array(
            "user_id"       => $userId,
            "order_id"      => $orderId,
            "street"        => $_POST['street_address'],
            "floor"         => $unit_address,
            "company"       => $_POST['company_name'],  
            "postal_code"   => $_POST['postcode'],
            "city"          => $_POST['city'],
            "instructions"  => $_POST['additional_instructions'],
            "created_at"    => $creation_date,
            "delivery_date" => $delivery_date,
            "delivery_phone"=> $_POST['mobile'],
            "people"        => $_POST['people'],
            "email"         => $_POST['email']

        );
        $wpdb->insert($deliveryDetailsTable, $deliveryDetailsData);

        if (isset($_SESSION['coupon_applied']) && $_SESSION['coupon_applied']) {
            $orderCouponMappingData = array(
                "order_id"      => $orderId,
                "coupon_code"   => $_SESSION['coupon_data']->code,
                "amount"        => $_SESSION['coupon_amount'],
                "status"        => 1,  
                "created_at"    => $creation_date,
            );
            $orderCouponMappingTable = $wpdb->prefix . "wyz_restaurant_order_coupon_mapping";
            $wpdb->insert($orderCouponMappingTable, $orderCouponMappingData);
        }

        // get admin email
        // $admin_email = get_bloginfo('admin_email');

        // get user info
        $user_info = get_userdata( $userId );
        $user_Email = $user_info->user_email;
        $userFirstName  = get_user_meta($userId, "first_name", true );
        $userLastName  = get_user_meta($userId, "last_name", true );
        $rewardPoints  = floatval(get_user_meta($userId, "reward_points", true ));
        update_user_meta($userId, "reward_points", ($rewardPoints+$totalBasePrice) );

        $delivery_email = $_POST['email'];

        // Restaurant Name
        $restaurant_name = get_the_title($restro_id);
        $restaurant_emails = get_post_meta($restro_id, "wp_restaurant_email" , true);

        // get PAX
        $pax = $_POST['people'];

        // Delivery Date
        // $delivery_date = $results[0]->delivery_date;

        // $user_info = get_userdata( $results[0]->user_id );

        // Get User Email
        // $user_Email = $user_info->data->user_email;
        
        // $delivery_email = $results[0]->email;
        
        //echo $delivery_email."<br>".$user_Email;


        //for admin mail
        $adminEmail = get_option('admin_email');
        if( !empty($adminEmail) ){
            $subject = 'New Order Placed for admin';

            ob_start();
            include(WYZ_PLUGIN_ROOT_PATH . '/frontend/admin-email.php');
            $email_content = ob_get_contents();
            ob_end_clean();
            $headers = array(
                'Content-Type: text/html; charset=UTF-8',
                'From: WYZchef <contact@wyzchef.com>',
            );
            // add_filter('wp_mail_from', function($admin_email) {
            //     return 'contact@wyzchef.com';
            // });
            // add_filter('wp_mail_from_name', function($admin_email) {
            //     return 'WYZchef';
            // });

            $adminMail = wp_mail( $adminEmail, $subject, $email_content, $headers );
        }

        //for User mail
        if( !empty($user_Email) ){
            $to = $user_Email;
            $subject = 'New Order Placed for user';
            ob_start();
            include(WYZ_PLUGIN_ROOT_PATH . '/frontend/user-email.php');
            $email_content = ob_get_contents();
            ob_end_clean();
            $headers = array(
                'Content-Type: text/html; charset=UTF-8',
                'From: WYZchef <contact@wyzchef.com>',
            );
            $userMail = wp_mail( $to, $subject, $email_content, $headers );
            // echo '<pre>Debug userMail: '; print_r( $userMail ); echo '</pre>';
        }


        // //for  different delivery email
        if($delivery_email != $user_Email){
            $to = $delivery_email;
            // $subject = 'New Order Placed';
            // $body .= 'Order Id: '.$orderId;
            // $body .= 'Delivery Address: '.$results[0]->company."&nbsp".$results[0]->street."&nbsp".$results[0]->floor;
            // $body .= 'Total: '.$total;
            // $headers = array('Content-Type: text/html; charset=UTF-8');
            // wp_mail( $to, $subject, $body, $headers );

            $subject = "New Order Placed by {$userFirstName} {$userLastName}";
            ob_start();
            include(WYZ_PLUGIN_ROOT_PATH . '/frontend/user-email.php');
            $email_content = ob_get_contents();
            ob_end_clean();
            $headers = array(
                'Content-Type: text/html; charset=UTF-8',
                'From: WYZchef <contact@wyzchef.com>',
            );
            $deliveryMail = wp_mail( $to, $subject, $email_content, $headers );
            // echo '<pre>Debug deliveryMail: '; print_r( $deliveryMail ); echo '</pre>';
        }


        //for restaurant owner
        if( !empty($restaurant_emails) ){
        	$emails = explode(',', $restaurant_emails);
            $emails = array_map('trim', $emails);
            $emails = array_filter($emails);
            $to = array_unique($emails);
            
            $subject = 'New Order Placed for Restaurant';
            ob_start();
            include(WYZ_PLUGIN_ROOT_PATH . '/frontend/restaurant-email.php');
            $email_content = ob_get_contents();
            ob_end_clean();
            // $headers = array('Content-Type: text/html; charset=UTF-8');
            $headers = array(
                'Content-Type: text/html; charset=UTF-8',
                'From: WYZchef <contact@wyzchef.com>',
            );
            $restaurantMail = wp_mail( $to, $subject, $email_content, $headers );
            // echo '<pre>Debug restaurantMail: '; print_r( $restaurantMail ); echo '</pre>';
        }

        /* \Stripe\Stripe::setApiKey("sk_test_W0UoNCBFLVNxmJYoe1k1MEHz00IO2I69Re");
        if ( empty($CustId)) {
            $desc = 'email: '.$user_Email;
            $customer = \Stripe\Customer::create([
                "description" => $desc,
                "email" => $user_Email,
                "source" => "tok_visa" // obtained with Stripe.js
            ]);
            $CustId = $customer->id;
            update_user_meta( $userDetails->data->ID, 'stripe_cust_id', $CustId );
        }
        else {
        }
        $invoiceItem = \Stripe\InvoiceItem::create([
            "customer" => $CustId,
            "amount" => $results[0]->total*100,
            "currency" => "sgd",
            "description" => "Total food value including tax and delivery"
        ]);
        $invoice = \Stripe\Invoice::create([
            "customer" => $CustId,
            "auto_advance" => true,
            "collection_method" => "send_invoice",
            "days_until_due" => "10"
        ]);*/

		$redirect_to = !empty( $_POST['redirectLink'] ) ? $_POST['redirectLink'] : home_url('login?checkemail=registered');
        
        session_destroy();

        wp_redirect( add_query_arg(array(
            'tab'       => 'confirmation',
            'order_id'  => $orderId
        ), $redirect_to) ); exit;
	} else {
		$redirect_to = !empty( $_POST['redirectLink'] ) ? $_POST['redirectLink'] : home_url('login?checkemail=registered');
		exit( wp_redirect( add_query_arg(array(
			'tab' 	=> 'delivery',
			'error' => 1
		), $redirect_to) ) );
	}
}

