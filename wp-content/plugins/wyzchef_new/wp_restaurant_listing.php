<?php


Class Restaurant_Listing {
	public function __construct(){
		//add_action( 'init', array($this,'lsiting') );

		add_action( 'wp_ajax_getRestaurants', array( $this, 'getRestaurants' ) ); 
        add_action( 'wp_ajax_nopriv_getRestaurants', array( $this, 'getRestaurants' ) );
	}

	/* Listing  for Featured Restaurant*/
	public function featuredLsiting() { 
		wp_enqueue_style('cumtom-ui', plugin_dir_url(__FILE__). 'css/custom.css');
	    $restaurant="";
	    $current_category = get_queried_object(); 
		$args = array(
		        'post_type' => 'wp_restaurant',
		        'meta_query' => 
		        array(
			        array(
			          'key' => 'wp_restaurant_featured',
			          'value' => '1',
			        )
			    ),
		        'orderby' => 'post_date',
		        'order' => 'DESC',   
			);
			$the_query = new WP_Query($args);
			$restaurant.=  '<section class="featuredListing">
								<h4 class="label featuredlabel">You may also enjoy</h4>
		    					<div class="row featuredRow">
		    					<div class="MultiCarousel" data-items="1,2,3,4" data-slide="1" id="MultiCarousel"  data-interval="1000">
		            				<div class="MultiCarousel-inner">';
		                  			while($the_query->have_posts()): $the_query->the_post();
						     			$restaurant.='<div class="item">
						                    		  <div class="pad15">';
						     			$restaurant.="<a href='".get_the_permalink()."'><div>".get_the_post_thumbnail(null,'medium')."</div>";
						    			$restaurant.=  "<label>".get_the_title()."</label>";
										$restaurant.= "</a>";
		                  				$restaurant.= " </div>
		                							    </div>";
		                			endwhile;	
		            				$restaurant.='</div>
		            				<button class="btn btn-primary leftLst"><</button>
		            				<button class="btn btn-primary rightLst">></button>
		        					</div></div></section>';
		    $restaurant .=$this->sliderJS();
			return $restaurant;
	}


	/* Listing  for Admin Restaurant*/
	public function userSelectedlsiting(){ 
		$current_user = wp_get_current_user();
		$userId= $current_user->id;
		if($userId==''){
			$userId=1;
		}
		$selectRestaurant = get_usermeta($userId,'selected_restaurant');
		$dataRow = explode(",",$selectRestaurant);
		
		wp_enqueue_style('cumtom-ui', plugin_dir_url(__FILE__). 'css/custom.css');
		wp_enqueue_script('ajax-js', plugin_dir_url(__FILE__). 'ja-demo-ajax-code.js');
	    $restaurant="";
	    $styleWidth=" style='width: 31.33%;'";
	    $current_category = get_queried_object(); 
		$args = array(
	        'post_type' => 'wp_restaurant',
	        'post__in'=>$dataRow,
	        'orderby' => 'post_date',
	        'order' => 'DESC',
	        'posts_per_page'=>3,   
		);

		$the_query = new WP_Query($args);
		if($the_query->have_posts()):
		    while($the_query->have_posts()): $the_query->the_post();
		    	$restaurant.='<li'.$styleWidth.'>';
		    	$restaurant.=  "<a href='".get_the_permalink()."'>".get_the_post_thumbnail(null,'medium');
		    	$restaurant.=  "<h6 class='postLabel'>".get_the_title()."</h6>";
		     	$restaurant.= "</a></li>";
			endwhile;
			
		endif;

		return $restaurant;
	}

	/**
     * This function call through AJAX
     * to get the restaurants
     */
    public function getRestaurants($offset = 0, $limit = 10) {
    	global $wpdb;
    	$selectQuery = array(
    		'p.ID',
			'p.post_title',
			'p.post_name',
			'p.guid',
			'p.post_author',
			'pmln.meta_value as listingNumber',
            'pm.meta_value as logoImageId',
			'pm7.meta_value as avgRating',
    	);
    	$joinQuery = '';
    	$whereQuery = '';
    	$dateTime = '';
        $requestedData = [];
    	parse_str($_POST['formData'], $formData);

		// $dateQuery = '
		// 	LEFT JOIN 
		// 	'.$wpdb->prefix.'postmeta as pm1 
		// 	ON (p.ID = pm1.post_id AND pm1.meta_key = "wp_restaurant_closing_date") 
		// 	LEFT JOIN 
		// 	'.$wpdb->prefix.'postmeta as pm2 
		// 	ON (p.ID = pm2.post_id AND pm2.meta_key = "wp_restaurant_delivery_days") 
		// ';
    	if (isset($formData['headcount']) && !empty($formData['headcount'])) {
            $requestedData[] = "headcount=" . urlencode($formData['headcount']);
        }

    	if (isset($formData['delivery_date']) && !empty($formData['delivery_date'])) {
            $requestedData[] = "delivery_date=" . urlencode($formData['delivery_date']);
    		$currentDateTime = strtotime("now");
            $dateTime = strtotime($formData['delivery_date']);
            $differenceInHours = ($dateTime - $currentDateTime)/3600;
    		$date = date('m/d/Y', $dateTime);
		 
    		$selectQuery[] = 'pm1.meta_value as closingDates';
    		$selectQuery[] = 'pm2.meta_value as deliveryDays';
            $selectQuery[] = 'pmlt.meta_value as leadTime';

    		$dateQuery = '
				INNER JOIN 
				'.$wpdb->prefix.'postmeta as pm1 
				ON (p.ID = pm1.post_id AND pm1.meta_key = "wp_restaurant_closing_date") 
				INNER JOIN 
					'.$wpdb->prefix.'postmeta as pm2 
				ON (p.ID = pm2.post_id AND pm2.meta_key = "wp_restaurant_delivery_days") 
                INNER JOIN 
                    '.$wpdb->prefix.'postmeta as pmlt 
                ON (p.ID = pmlt.post_id AND pmlt.meta_key = "wp_restaurant_lead_time") 
			';

			$whereQuery .= ' AND pm1.meta_value NOT LIKE "%'. $date .'%" ';
            $whereQuery .= ' AND (pmlt.meta_value ="" OR pmlt.meta_value < '. $differenceInHours .')';
    	} /*else {
    		$dateTime = strtotime('now');
    	}*/
    	$joinQuery .= $dateQuery;

    	if (isset($formData['restaurant_service']) && !empty($formData['restaurant_service'])) {
    		$joinQuery .= '
	    		INNER JOIN 
					'.$wpdb->prefix.'postmeta as pm8 
				ON (p.ID = pm8.post_id AND pm8.meta_key = "wp_restaurant_service") 
			';

			$whereQuery .= ' AND (';
			foreach ($formData['restaurant_service'] as $key => $service) {
				if (!$key) {
					$whereQuery .= 'pm8.meta_value LIKE "%'.$service.'%"';
				} else {
					$whereQuery .= 'OR pm8.meta_value LIKE "%'.$service.'%"';
				}
			}
			$whereQuery .= ') ';
    	}
    	
    	if (isset($formData['restaurant_type']) && !empty($formData['restaurant_type'])) {
    		$joinQuery .= '
	    		INNER JOIN 
					'.$wpdb->prefix.'postmeta as pm3 
				ON (p.ID = pm3.post_id AND pm3.meta_key = "wp_restaurant_type") 
			';

			$whereQuery .= ' AND (';
			foreach ($formData['restaurant_type'] as $key => $type) {
				if (!$key) {
					$whereQuery .= 'pm3.meta_value LIKE "%'.$type.'%"';
				} else {
					$whereQuery .= 'OR pm3.meta_value LIKE "%'.$type.'%"';
				}
			}
			$whereQuery .= ') ';
    	}

    	if (isset($formData['restaurant_category']) && !empty($formData['restaurant_category'])) {
    		$joinQuery .= '
	    		INNER JOIN 
					'.$wpdb->prefix.'postmeta as pm4 
				ON (p.ID = pm4.post_id AND pm4.meta_key = "wp_restaurant_category") 
			';

			$whereQuery .= ' AND (';
			foreach ($formData['restaurant_category'] as $key => $category) {
				if (!$key) {
					$whereQuery .= 'pm4.meta_value LIKE "%'.$category.'%"';
				} else {
					$whereQuery .= 'OR pm4.meta_value LIKE "%'.$category.'%"';
				}
			}
			$whereQuery .= ') ';
    	}

    	if (isset($formData['restaurant_occasion']) && !empty($formData['restaurant_occasion'])) {
    		$joinQuery .= '
	    		INNER JOIN 
					'.$wpdb->prefix.'postmeta as pm9 
				ON (p.ID = pm9.post_id AND pm9.meta_key = "wp_restaurant_occasion") 
			';

			$whereQuery .= ' AND (';
			foreach ($formData['restaurant_occasion'] as $key => $occasion) {
				if (!$key) {
					$whereQuery .= 'pm9.meta_value LIKE "%'.$occasion.'%"';
				} else {
					$whereQuery .= 'OR pm9.meta_value LIKE "%'.$occasion.'%"';
				}
			}
			$whereQuery .= ') ';
    	}

    	if (
    		isset($formData['headcount']) && 
    		!empty($formData['headcount']) &&
    		isset($formData['budget_select']) && 
    		!empty($formData['budget_select'])
    	) {
    		$orderBudget = $formData['headcount'] * $formData['budget_select'];
    		$joinQuery .= '
	    		INNER JOIN 
					'.$wpdb->prefix.'postmeta as pm10 
				ON (p.ID = pm10.post_id AND pm10.meta_key = "wp_restaurant_minimum_order_amount") 
			';

			$whereQuery .= ' AND pm10.meta_value < '. $orderBudget .' ';
    	}

    	if (isset($formData['delivery_location']) && !empty($formData['delivery_location'])) {
            $requestedData[] = "delivery_location=" . urlencode($formData['delivery_location']);
    		$joinQuery .= '
	    		INNER JOIN 
					'.$wpdb->prefix.'postmeta as pm5 
				ON (p.ID = pm5.post_id AND pm5.meta_key = "wp_restaurant_delivery_pincodes") 
                LEFT JOIN 
                    '.$wpdb->prefix.'postmeta as pm6 
                ON (p.ID = pm6.post_id AND pm6.meta_key = "wp_restaurant_delivery_everywhere") 
			';

			$whereQuery .= ' AND pm5.meta_value LIKE "%'.$formData['delivery_location'].'%" ';
            $whereQuery .= ' OR pm6.meta_value = 1 ';
    	}

    	$avgQuery = '
    		LEFT JOIN 
				'.$wpdb->prefix.'postmeta as pm7 
			ON (p.ID = pm7.post_id AND pm7.meta_key = "wyz_restaurant_avg_rating") 
    	';
    	if (isset($formData['restaurant_avg_rating']) && ($formData['restaurant_avg_rating'] > 0)) {
			$avgQuery = '
				INNER JOIN 
					'.$wpdb->prefix.'postmeta as pm7 
				ON (p.ID = pm7.post_id AND pm7.meta_key = "wyz_restaurant_avg_rating") 
			';

			$whereQuery .= ' AND pm7.meta_value >= '.$formData['restaurant_avg_rating'].' ';
    	}
    	$joinQuery .= $avgQuery;

    	if (
    		isset($formData['search_restaurant']) 
    		&& !empty($formData['search_restaurant'])
    		&& ($searchText = trim($formData['search_restaurant']))
    	) {
            $requestedData[] = "search_restaurant=" . urlencode($searchText);
    		$joinQuery .= '
                LEFT JOIN 
                    '.$wpdb->prefix.'wyz_restaurant_dishes as rd 
                ON (p.ID = rd.restaurant_id) 
			';

			$whereQuery .= ' AND (';
			$whereQuery .= ' p.post_title LIKE "%'.$searchText.'%" ';
			$whereQuery .= ' OR rd.name LIKE "%'.$searchText.'%" ';
			$whereQuery .= ' ) ';
    	}

    	if (isset($formData['restaurant_menu']) && !empty($formData['restaurant_menu'])) {
    		$joinQuery .= '
	    		INNER JOIN 
					'.$wpdb->prefix.'wyz_restaurant_dishes as rd 
				ON (p.ID = rd.restaurant_id)  
			';

			$whereQuery .= ' AND rd.menu_id IN ('. implode(',', $formData['restaurant_menu']) .') ';
    	}

    	$sqlQuery = '
    		SELECT DISTINCT
    			'. implode(',', $selectQuery) .'
			FROM '.$wpdb->prefix.'posts as p 
			LEFT JOIN 
				'.$wpdb->prefix.'postmeta as pmln 
			ON (p.ID = pmln.post_id AND pmln.meta_key = "wp_restaurant_list_no") 
            LEFT JOIN 
                '.$wpdb->prefix.'postmeta as pm 
            ON (p.ID = pm.post_id AND pm.meta_key = "wp_restaurant_list_img") 
			'. $joinQuery .' 
    		WHERE 
    			p.post_type = "wp_restaurant" 
    			AND p.post_status = "publish" 
    			'. $whereQuery .'
    		ORDER BY listingNumber DESC LIMIT '. $formData['offset'] .', '. $formData['limit'] .'
		'; 

    	$restaurants = $wpdb->get_results($sqlQuery);

		$listingHTML = '';
		if(count($restaurants)){
	    	$day = strtolower(date('l', $dateTime));
	    	$timeHour = intval(date('H', $dateTime));
	    	// $hourMinutes = date('H:i A', $dateTime);

            if (count($requestedData)) {
                $requestedData = '?' . implode('&', $requestedData);
            } else {
                $requestedData = '';
            }

		   	foreach ($restaurants as $key => $restaurant) {
		   		$allowDeliveryFlag = true;
		   		if (!empty($dateTime)) {
		   			$deliveryDays = unserialize($restaurant->deliveryDays);
			   		if(isset($deliveryDays[$day])) {
			   			$deliverySlots = count($deliveryDays[$day]['from']);
			   			if (1 === $deliverySlots) {
			   				if (
			   					($timeHour >= $deliveryDays[$day]['from'][0])
		   						&& ($timeHour < $deliveryDays[$day]['to'][0])
			   				) {
			   					$allowDeliveryFlag = true;
			   				} else {
			   					$allowDeliveryFlag = false;
			   				}
			   			} else {
			   				if (
			   					(
			   						($timeHour >= $deliveryDays[$day]['from'][0])
			   						&& ($timeHour < $deliveryDays[$day]['to'][0])
			   					) || (
			   						($timeHour >= $deliveryDays[$day]['from'][1])
			   						&& ($timeHour < $deliveryDays[$day]['to'][1])
			   					)
			   				) {
			   					$allowDeliveryFlag = true;
			   				} else {
			   					$allowDeliveryFlag = false;
			   				}
			   			}
				   	}
		   		}

                if ($allowDeliveryFlag) {
                    $imgSrc = wp_get_attachment_image_src($restaurant->logoImageId, 'medium_large');
                    if (!isset($imgSrc[0])) {
                        $imgSrc[0] = WYZ_PLUGIN_ROOT_URL . 'assets/images/default-restaurant-list-img.png';
                    }
			   		$listingHTML .= '<li>';
                    $listingHTML .= "<a class='list-bg-img' href='".get_the_permalink($restaurant->ID).$requestedData."' style='background-image: url({$imgSrc[0]})' />";

                    // for category 
                    $categoryHTML = "";
                    $wp_restaurant_category = get_post_meta($restaurant->ID, 'wp_restaurant_category', true);
                    if (isset($wp_restaurant_category) && !empty($wp_restaurant_category)) {
                        for ($j=0; $j < count($wp_restaurant_category); $j++) {
                            $category = get_term($wp_restaurant_category[$j]);
                            if ($category->slug == 'halal') {
                            	$imageId = get_term_meta( $category->term_id, 'wyz-category-image-id', true );
	                            if( $imageId ) {
	                                $categoryImage = wp_get_attachment_thumb_url( $imageId, 'thumbnail' );
	                            } else {
	                                $categoryImage = WYZ_PLUGIN_ROOT_URL . 'assets/images/restaurant.svg';
	                            }
	                            $categoryHTML .= "<span class='halal_img'><img src='{$categoryImage}' alt='{$category->name}'></span>";
                            }
                        }
                    }

					// $listingHTML .= "<div class='listing-overlay-box'>
					// 					<div class='list-overlay'>	
					// 						<a href='#' data-href='".get_the_permalink($restaurant->ID).$requestedData."' class='view-restaurant'><span>View Menu</span></a>
					// 						<ul>{$categoryHTML}</ul>
					// 					</div>
                    //                 </div>";
                    // $listingHTML .= "<a href='".get_the_permalink($restaurant->ID).$requestedData."'><img style='width:100%' data-img-id='{$restaurant->logoImageId}' src='{$imgSrc[0]}'/>";
			    	// $listingHTML .= "<a href='".get_the_permalink($restaurant->ID)."'><img style='width:100%' width='300'  height='300' src='".wp_get_attachment_thumb_url($restaurant->logoImageId)."'/>";
					$listingHTML .= "<div class='restaurantContentbox'><div class='row'>
										<a href='".get_the_permalink($restaurant->ID).$requestedData."'>
										<!-- <div class='col-md-3'>
										<div class='match-box'>97%<small>MATCH</small></div>
										</div> -->";
					$listingHTML .= "<div class='col-md-12'>";

					$foodTypes = array();
					$foodTypeString = "";
					$shortType = " ";
                    $wp_restaurant_type = get_post_meta($restaurant->ID, 'wp_restaurant_type',true);
                    if (isset($wp_restaurant_type) && !empty($wp_restaurant_type)) {
                        for ($i=0; $i < count($wp_restaurant_type); $i++) {
                            $type = get_term($wp_restaurant_type[$i]);
                            $foodTypes[] = $type->name;
                        }
                        $foodTypeString = implode(' | ', $foodTypes);
					    $shortType = $foodTypeString;
					    if( strlen($foodTypeString ) > 40 ){
						    $shortType = substr($foodTypeString, 0, 40)."...";
					    }

                    }
                    $listingHTML .= "<p><small>". $shortType ."</small></p>";
                    
					// $occasionHTML = "";
					// $occasionArray = array();
                    // $wp_restaurant_occasion = get_post_meta($restaurant->ID, 'wp_restaurant_occasion', true);
                    // if (!empty($wp_restaurant_occasion)) {
                        // for ($s=0; $s < count($wp_restaurant_occasion); $s++) {
                            // $occasion = get_term($wp_restaurant_occasion[$s]);
		                    // $occasionArray[] = $occasion->name;
                        // }
                        // 
                        // $occasionHTML = implode(' | ', $occasionArray);
                    // }

					
					$title = $restaurant->post_title;
			    	$listingHTML .= '<h6 title="'.$title.'" class="postLabel">'.$title.'</h6>';

			    	$listingHTML .= "</div>";
			    	$listingHTML .= "<div class='col-md-12'>";

					$serviceArray = array();
					$serviceHTML = "";
					$shortService = " ";
                    $wp_restaurant_service = get_post_meta($restaurant->ID, 'wp_restaurant_service', true);
                    if (!empty($wp_restaurant_service)) {
                        for ($s=0; $s < count($wp_restaurant_service); $s++) {
                            $service = get_term($wp_restaurant_service[$s]);
		                    $serviceArray[] = $service->name;
                        }
                        
                        $serviceHTML = implode(' | ', $serviceArray);
                        $shortService = $serviceHTML;
					    if( strlen($serviceHTML ) > 40 ){
						    $shortService = substr($serviceHTML, 0, 40)."...";
					    }
                    }

					$listingHTML .= "<p class='caterer-list-info'><small>{$shortService}</small>";

					$minOrderAmount = get_post_meta($restaurant->ID, 'wp_restaurant_minimum_order_amount',true);
					if ($minOrderAmount) {
						$minOrderAmount = '$'. $minOrderAmount;
					} else {
						$minOrderAmount = 'N/A';
					}

					$restaurantLeadTime = get_post_meta($restaurant->ID, 'wp_restaurant_lead_time',true);

					$listingHTML .="<span class='fa fa-dollar'></span><span>Min. {$minOrderAmount}</span><span class='fa fa-clock-o'></span><span>Notice:{$restaurantLeadTime}h</span>";

					if (!empty($restaurant->avgRating) && ($restaurant->avgRating > 1)) {
                        // $listingHTML .= "<span class='fa fa-star'><span>{$restaurant->avgRating}</span></span>";

                        $reviews = new Wyz_Chef_Reviews();
						$listingHTML .= $reviews->wyz_comment_rating_get_average_ratings($restaurant->ID);
					}

					$halalcertified = WYZ_PLUGIN_ROOT_URL . 'assets/images/certifi-2.png';
					$listingHTML .= "{$categoryHTML}";
					$listingHTML .="</p>";
					
			     	$listingHTML .= "</div></a></div></div>";
			     	$listingHTML .= "</a>";
                    $listingHTML .= "</li>";
			   	}
		   	}
		}

		echo json_encode([
        	// 'msg' => 'Getting response from list function is done.', 
        	'sqlQuery' => preg_replace('!\s+!', ' ', $sqlQuery), 
            'differenceInHours' => $differenceInHours, 
        	// 'dateTime' => $dateTime, 
        	// 'restaurants' => $restaurants, 
        	// 'formData' => $formData,
        	'restaurantsHTML' => $listingHTML, 
            // 'avgRating' => $restaurant->avgRating, 
        	// 'day' => $day, 
        	// 'date' => $date, 
        	// 'time' => $timeHour, 
        	// 'deliveryDays' => unserialize($restaurant->deliveryDays), 
        ]); die;
    }

	/* Listing  Of Restaurants*/
	public function lsiting($sideFilter=''){
	   	$restaurant = "";
	    if($sideFilter == ''){
	    	//$restaurant .=  $this->Filters();
	    }

		$width = "100%";
		
		$restaurant .="<div id='loadRequest'>";
		$restaurant.='<section style="width:'.$width.'" class="restauranList">';
		
		$restaurant .="
			<style>
				#restaurants-listing-loader {
				    background-color: #fbfbfb;
				    opacity: 0.5;
				    padding-bottom: 20px;
				    text-align: center; 
					z-index: 1;
					height: 104px;
					position: absolute;
					top:100%;
					width: 100%;
				}

				#restaurants-listing-loader img {
					background: transparent;
					position: absolute;
    				top: 20px;
				}
			</style>
			<div class='row'>
			<div class='col-lg-2 col-md-3 col-sm-3'>
				<div class='list_side_filter'>
					". $this->Filters() ."
	
				</div>
			</div>

			<div class='col-lg-10 col-md-9 col-sm-9'>
				<div class='difrentListing'>
					<div class='row'>
						<div class='col-md-8 col-sm-12 catering_warning_message'>
						</div>
					</div>
						<ul id='restaurants-listing'></ul>
						<div id='restaurants-listing-loader'>
							<div class='loader-wrapper'>
								<img src='". WYZ_PLUGIN_ROOT_URL ."assets/images/loading.gif' />
							</div>
						</div>
					</div>
				</div>
			</div>

			<style>
				/*body {
					display: none !important;
				}*/
			</style>
		";
		// <p><span class='fa fa-exclamation-triangle'></span>Please provide <span>Delivery Date</span> for us to check the availability of the caterer <span>!</span></p>

		$restaurant.='</section>';
		$restaurant .="</div>";
		
		$formData = array();
        if (isset($_REQUEST['headcount']) && !empty(isset($_REQUEST['headcount']))) {
            $formData[] = 'headcount='. urlencode($_REQUEST['headcount']);
        }

        if (isset($_REQUEST['delivery_date']) && !empty(isset($_REQUEST['delivery_date']))) {
            $formData[] = 'delivery_date='. urlencode($_REQUEST['delivery_date']);
        }

        // if (isset($_REQUEST['delivery_location']) && !empty(isset($_REQUEST['delivery_location']))) {
        //     $formData[] = 'delivery_location='. urlencode($_REQUEST['delivery_location']);
        // }
        
        if (isset($_REQUEST['cuisines'])) {
            $cuisines = explode(',', $_REQUEST['cuisines']);
            foreach ($cuisines as $key => $cuisine) {
                $formData[] = 'restaurant_type[]='. urlencode($cuisine);
            }
        }
        
        // if (isset($_REQUEST['categories'])) {
        //     $categories = explode(',', $_REQUEST['categories']);
        //     foreach ($categories as $key => $category) {
        //         $formData[] = 'restaurant_category[]='. urlencode($category);
        //     }
        // }
        
        // if (isset($_REQUEST['rating'])) {
        //     $formData[] = 'restaurant_avg_rating='. urlencode($_REQUEST['rating']);
        // }

        if (isset($_REQUEST['services'])) {
            $services = explode(',', $_REQUEST['services']);
            foreach ($services as $key => $service) {
                $formData[] = 'restaurant_service[]='. urlencode($service);
            }
        }
        
        if (isset($_REQUEST['occasions'])) {
            $occasions = explode(',', $_REQUEST['occasions']);
            foreach ($occasions as $key => $occasion) {
                $formData[] = 'restaurant_occasion[]='. urlencode($occasion);
            }
        }
        
        if (isset($_REQUEST['budget'])) {
            $formData[] = 'budget_select='. urlencode($_REQUEST['budget']);
        }

        // $formData[] = 'offset=0';
        // $formData[] = 'limit=10';

        $formData = implode('&', $formData);
        // echo '<pre>Debug formData: '; print_r( $formData ); echo '</pre>'; die;
        // $formData .= '&offset=0';

        $restaurant .='
			<script type="text/javascript" >
                var lockCalling = true, offset = 0, limit = 10;
				function getRestaurantsList(data) {
					jQuery.post(aj_ajax_demo.ajax_url, data, function(response) {
						// console.log("response: ", response);
                        // console.log("offset: ", offset);
                        if(offset) {
                            jQuery("#restaurants-listing").append(response.restaurantsHTML);
                        } else {
                            jQuery("#restaurants-listing").html(response.restaurantsHTML);
                        }

                        if(response.restaurantsHTML == "") {
                            lockCalling = true;
                        } else {
                            lockCalling = false;
                        }

                        jQuery("#restaurants-listing-loader").hide();
					}, "json");
				}

				jQuery(document).ready(function($) {
					let selectedFormData = "";
					let preSelected = "'. $formData .'";
					if(preSelected) {
						selectedFormData = preSelected;
					} else {
						/*let filterParent = $(".side-filter-form");
						let checkBoxes = $(\'input[type="checkbox"]:checked\', filterParent).serialize();
						let textBoxes = $(\'input[type="text"]\', filterParent).serialize();
						let searchBox = $(\'input.search-restaurant\').serialize();
						selectedFormData = checkBoxes+"&"+textBoxes+"&"+searchBox;*/

						let headerFilterForm = $(".header-filter-form");
                        let sideFilterForm = $(".side-filter-form");
						// let checkBoxesArray = $(\'input[type="checkbox"]:checked\', this).serializeArray();
						// let textBoxesArray = $(\'input[type="text"]\', this).serializeArray();
						// let selectedFormData = $.merge(checkBoxesArray, textBoxesArray);

						let checkBoxes = $(\'input[type="checkbox"]:checked\', sideFilterForm).serialize();
						let textBoxes = $(\'input[type="text"]\', sideFilterForm).serialize();
						let serializeHeaderFormData = headerFilterForm.serialize();
						selectedFormData = checkBoxes+"&"+textBoxes+"&"+serializeHeaderFormData;
					}
                    selectedFormData += "&offset="+ offset +"&limit="+ limit;

					getRestaurantsList({
						"action": "getRestaurants",
                        "formData": selectedFormData
					});

                    $("#restaurants-listing").on("click", ".view-restaurant", function(){
                        let dataHref = $(this).attr("data-href");
                        // if(-1 === dataHref.indexOf("delivery_date")) {
                        //     alert("Please provide delivery date for us to check the availability of the caterer")

                        //     return false;
                        // }

                        window.location.href = dataHref;

                        return false;
                    });

                    $(document.body).on("submit", ".side-filter-form, .header-filter-form", function(e){
                    	return false;
                	});

					$(document.body).on("change", ".side-filter-form", function(e){
						$("#restaurants-listing-loader").show();
                        offset = 0;
                        limit = 10;
                        /*let filterParent = $(".side-filter-form");
						// let checkBoxesArray = $(\'input[type="checkbox"]:checked\', this).serializeArray();
						// let textBoxesArray = $(\'input[type="text"]\', this).serializeArray();
						// let selectedFormData = $.merge(checkBoxesArray, textBoxesArray);

						let checkBoxes = $(\'input[type="checkbox"]:checked\', filterParent).serialize();
						let textBoxes = $(\'input[type="text"]\', filterParent).serialize();
						// let searchBox = $(\'input.search-restaurant\').serialize();
						// let selectedFormData = checkBoxes+"&"+textBoxes+"&"+searchBox;
						let selectedFormData = checkBoxes+"&"+textBoxes;*/

						let headerFilterForm = $(".header-filter-form");
                        let sideFilterForm = $(".side-filter-form");
						// let checkBoxesArray = $(\'input[type="checkbox"]:checked\', this).serializeArray();
						// let textBoxesArray = $(\'input[type="text"]\', this).serializeArray();
						// let selectedFormData = $.merge(checkBoxesArray, textBoxesArray);

						let checkBoxes = $(\'input[type="checkbox"]:checked\', sideFilterForm).serialize();
						let textBoxes = $(\'input[type="text"]\', sideFilterForm).serialize();
						let serializeHeaderFormData = headerFilterForm.serialize();
						let selectedFormData = checkBoxes+"&"+textBoxes+"&"+serializeHeaderFormData;
                        selectedFormData += "&offset="+ offset +"&limit="+ limit;
						
						// let formatedValue = e.date.format("YYYY-MM-DD HH:mm:00"); 
						// console.log(formatedValue);

						// console.log(selectedFormData);

						getRestaurantsList({
							"action": "getRestaurants",
							"formData": selectedFormData
						});
					});

                    $(window).scroll(function() {
                        // console.log("$(window).scrollTop() : ", $(window).scrollTop());
                        // console.log("Difference : ", ($("#restaurants-listing").height() - $(window).height()));
                        if(
                            !lockCalling 
                            && (
                                $(window).scrollTop() >= ($("#restaurants-listing").height() - $(window).height())
                            )
                        ) {
                        	$("#restaurants-listing-loader").show();
                            lockCalling = true;

                            offset = offset + limit;
                            limit = 10;
                            /*let filterParent = $(".side-filter-form");

                            let checkBoxes = $(\'input[type="checkbox"]:checked\', filterParent).serialize();
                            let textBoxes = $(\'input[type="text"]\', filterParent).serialize();
							let searchBox = $(\'input.search-restaurant\').serialize();
							let selectedFormData = checkBoxes+"&"+textBoxes+"&"+searchBox;*/

                            let headerFilterForm = $(".header-filter-form");
	                        let sideFilterForm = $(".side-filter-form");
							// let checkBoxesArray = $(\'input[type="checkbox"]:checked\', this).serializeArray();
							// let textBoxesArray = $(\'input[type="text"]\', this).serializeArray();
							// let selectedFormData = $.merge(checkBoxesArray, textBoxesArray);

							let checkBoxes = $(\'input[type="checkbox"]:checked\', sideFilterForm).serialize();
							let textBoxes = $(\'input[type="text"]\', sideFilterForm).serialize();
							let serializeHeaderFormData = headerFilterForm.serialize();
							let selectedFormData = checkBoxes+"&"+textBoxes+"&"+serializeHeaderFormData;

                            selectedFormData += "&offset="+ offset +"&limit="+ limit;

                            // console.log(selectedFormData);

                            getRestaurantsList({
                                "action": "getRestaurants",
                                "formData": selectedFormData
                            });
                        }

                        // if($(window).scrollTop() == $(document).height() - $(window).height()) {
                        //     alert("scrollTop == difference b/w document & window");
                        // }
                    });

					$(".header-filter-form").on("click", ".list-header-submit-btn", function(e){
						$("#restaurants-listing-loader").show();
                        offset = 0;
                        limit = 10;
                        let headerFilterForm = $(".header-filter-form");
                        let sideFilterForm = $(".side-filter-form");
						// let checkBoxesArray = $(\'input[type="checkbox"]:checked\', this).serializeArray();
						// let textBoxesArray = $(\'input[type="text"]\', this).serializeArray();
						// let selectedFormData = $.merge(checkBoxesArray, textBoxesArray);

						let checkBoxes = $(\'input[type="checkbox"]:checked\', sideFilterForm).serialize();
						let textBoxes = $(\'input[type="text"]\', sideFilterForm).serialize();
						let serializeHeaderFormData = headerFilterForm.serialize();
						let selectedFormData = checkBoxes+"&"+textBoxes+"&"+serializeHeaderFormData;
						// let selectedFormData = checkBoxes+"&"+textBoxes;
                        selectedFormData += "&offset="+ offset +"&limit="+ limit;
						
						// let formatedValue = e.date.format("YYYY-MM-DD HH:mm:00"); 
						// console.log(formatedValue);

						console.log(selectedFormData);

						getRestaurantsList({
							"action": "getRestaurants",
							"formData": selectedFormData
						});
					});
				});
			</script>
		';
		
		return $restaurant;
	}

	/* Predefined Filters (Location, Date,Time)*/
	public function Filters() {
		wp_enqueue_style('datetimepicker-css', plugin_dir_url(__FILE__). 'assets/css/bootstrap-datetimepicker.min.css');

		wp_enqueue_style('jquery-ui', plugin_dir_url(__FILE__). 'assets/css/jquery-ui.css');
		wp_enqueue_style('bootstrap-css-slider', plugin_dir_url(__FILE__). 'assets/css/bootstrap-slider.min.css');
		// wp_enqueue_style('cumtom-ui', plugin_dir_url(__FILE__). 'css/custom.css');

		wp_enqueue_script('jquery-ui', plugin_dir_url(__FILE__). 'assets/js/jquery-ui.js');
		wp_enqueue_script('bootstrap-slider-js', plugin_dir_url(__FILE__). 'assets/js/bootstrap-slider.min.js');
		wp_enqueue_script('datetime-moment-js', plugin_dir_url(__FILE__). 'assets/js/moment.min.js');
		wp_enqueue_script('datetimepicker-js', plugin_dir_url(__FILE__). 'assets/js/bootstrap-datetimepicker.js');

		wp_enqueue_style('location-icon', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css');

		// $Addressdata = $this->get_meta_values('wp_restaurant_address','wp_restaurant');
		$deliveryDate = $headcount = '';
		if (isset($_REQUEST['headcount'])) {
			$headcount = $_REQUEST['headcount'];
		}

		if (isset($_REQUEST['delivery_date'])) {
			$deliveryDate = $_REQUEST['delivery_date'];
		}
		
		// if (isset($_REQUEST['delivery_location'])) {
		// 	$location = $_REQUEST['delivery_location'];
		// }
        
        $checkedServices = array();
        if (isset($_REQUEST['services'])) {
            $checkedServices = explode(',', $_REQUEST['services']);
        }
        
        $checkedCuisines = array();
        if (isset($_REQUEST['cuisines'])) {
            $checkedCuisines = explode(',', $_REQUEST['cuisines']);
        }
        
        $categories = array();
        if (isset($_REQUEST['categories'])) {
            $categories = explode(',', $_REQUEST['categories']);
        }
        
        $rating = 0;
        // if (isset($_REQUEST['rating'])) {
        //     $rating = $_REQUEST['rating'];
        // }
        
        $budget = '';
        if (isset($_REQUEST['budget'])) {
            $budget = $_REQUEST['budget'];
        }
        
        $checkedOccasions = array();
        if (isset($_REQUEST['occasions'])) {
            $checkedOccasions = explode(',', $_REQUEST['occasions']);
        }

		$services = get_terms([
		  	'taxonomy' => 'restaurant_service', 
			'hide_empty' => false,
			'childless' => true,
			'orderby' => 'parent',
		]);

		$typeOfServices = '';
		if(count($services)) {
			$typeOfServices .= '
				<div class="">
					<div class="options-choices common-choices common-radio">
						<h4><span><img src="" alt=""></span>Type of  Services</h4>
						<ul class="inline-display-list">
			';

			foreach ($services as $service) {
				$checked = '';
            	if ( in_array($service->term_id, $checkedServices) ) {
            		$checked = 'checked';
            	}

            	$typeOfServices .= '
            		<li>
						<label><input name="restaurant_service[]" type="checkbox" value="'. $service->term_id .'" '. $checked .'><span>'. $service->name .'</span></label>
					</li>
            	';
			}

			$typeOfServices .= '
						</ul>
						<span class="more">+ See more</span>
						<span class="less">- See less</span>
					</div>
				</div>
			';
		}

		$types = get_terms([
		  	'taxonomy' => 'restaurant_type', 
		  	'hide_empty' => false,
		]);

		$typeOfCuisines = '';
		if(count($types)) {
			$typeOfCuisines .= '
				<div>
					<div class="options-choices common-choices common-radio">
						<h4><span><img src="" alt=""></span>Type of Cuisine</h4>
						<ul class="inline-display-list">
			';

			foreach ($types as $type) {
				$checked = '';
                if ( in_array($type->term_id, $checkedCuisines) ) {
                    $checked = 'checked';
                }

            	$typeOfCuisines .= '
            		<li>
						<label><input name="restaurant_type[]" type="checkbox" value="'. $type->term_id .'" '. $checked .'><span>'. $type->name .'</span></label>
					</li>
            	';
			}

			$typeOfCuisines .= '
						</ul>
						<span class="more">+ See more</span>
						<span class="less">- See less</span>
					</div>
				</div>
			';
		}

		$occasions = get_terms([
		  	'taxonomy' => 'restaurant_occasion', 
			'hide_empty' => false,
			'childless' => true,
			'orderby' => 'parent',
		]);

		$typeOfOccasion = '';
		if(count($occasions)) {
			$typeOfOccasion .= '
				<div>
					<div class="options-choices common-choices common-radio">
						<h4><span><img src="" alt=""></span>Type of Occasion</h4>
						<ul class="inline-display-list">
			';

			foreach ($occasions as $occasion) {
				$checked = '';
                if ( in_array($occasion->term_id, $checkedOccasions) ) {
                    $checked = 'checked';
                }

            	$typeOfOccasion .= '
            		<li>
						<label><input name="restaurant_occasion[]" type="checkbox" value="'. $occasion->term_id .'" '. $checked .'><span>'. $occasion->name .'</span></label>
					</li>
            	';
			}

			$typeOfOccasion .= '
						</ul>
						<span class="more">+ See more</span>
						<span class="less">- See less</span>
					</div>
				</div>
			';
		}

		$restaurantCategories = get_terms([
		  	'taxonomy' => 'restaurant_category', 
		  	'hide_empty' => false,
		]);

		$halalCategoryObj = get_term_by( 'slug', 'halal', 'restaurant_category' );

		$dietaryRestrictions = '';
		if(count($restaurantCategories)) {
			$dietaryRestrictions .= '
				<div class="options-choices common-choices common-radio dietary-section">
					<h4><span><img src="" alt=""></span>Dietary Restriction</h4>
					<ul class="inline-display-list">
			';

			foreach ($restaurantCategories as $category) {
				$checked = '';
            	if ( in_array($category->term_id, $categories) ) {
                    $checked = 'checked';
                }

            	$dietaryRestrictions .= '
            		<li>
						<label><input name="restaurant_category[]" type="checkbox" value="'. $category->term_id .'" '. $checked .'><span>'. $category->name .'</span></label>
					</li>
            	';
			}

			$dietaryRestrictions .= '
					</ul>
						<span class="more">+ See more</span>
						<span class="less">- See less</span>
				</div>
			';
		}
					// <table  style="max-width:450px; float:left;"><tr>
					// <td><span class="filter-tags input-group date" id="filter_date" ><i class="input-group-addon" id="filter_calender"></i><input type="text" class="datetimepicker" placeholder="Date" name="delivery_date" value="'.$date.'" readonly></span></td>
					// <td><span class="maplocatiopn-icon filter-tags"><i id="filter_location" button-0"></i><input type="text" placeholder="Location" class="input-0" name="delivery_location" value="'.$location.'" id="locationTextField" autocomplete="off"></span></td>
					// <td><span class="filter-tags filter-button"><i id="filter_icon"></i>Filter</span></td>
					// </tr></table>
		$filters = '
			<div class="container-fluid filter-container"><div class="container filter-head"> <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARteWOCnYMfmc4hP7xyZpLVulT7fG_Cn4&libraries=places,drawing">
					</script>

					<form action="" method="get" class="header-filter-form" >
					<div class="list-head-filter-bar">
						<div class="col-md-12">
							<h2>Order Catering for your corporate event</h2>
							<div class="container-fluid filter-container">
								<div class="container filter-head new_layout">
										<table>
											<tr>
												<td>
													<label>WHEN</label>
													<span class="filter-tags input-group date" id="filter_date" >
														<i class="input-group-addon" id="filter_calender"></i>
														<input type="text" class="datetimepicker" placeholder="Date" name="delivery_date" value="'. $deliveryDate .'">
													</span>
												</td>
												<td style="width:200px">
													<label>HEADCOUNT</label>
													<span class="filter-tags input-group list-mob-filters" id="head_counts" >
														<i class="input-group-addon" id="head_count"></i>
														<input type="text" placeholder="Guest Number" class="mob-filter-input" name="headcount" value="'. $headcount .'">
													</span>
												</td>
												<td>
													<label>BUDGET</label>
													<span class="filter-tags input-group list-mob-filters" id="price_pax" >
														<i class="input-group-addon" id="head_price"></i>
														<select name="budget_select" class="budget_select mob-filter-input" id="budget_select" form-control="">
															<option class="mob-show" value=""></option>
															<option value="">Price per pax</option>
															<option value="10">Less than $10/pax</option>
															<option value="20">$11-$20/pax</option>
															<option value="35">$21-$35/pax</option>
															<option value="50">$36-$50/pax</option>
															<option value="1000">More than $50/pax</option>
														</select>
													</span>
												</td>
												<td>
												<button type="submit" class="list-header-submit-btn">Search</button>
												</td>
											</tr>
										</table>
								</div>
							</div>
						</div>
					</div>
					</form>
					

					<form action="" method="get" class="side-filter-form" >
					<!-- FILTER BOX -->
					<div class="filter-box">
						<span class="mobile-filters">Apply Filters</span>
						<span class="close_mob_filters fa fa-close"></span>

						<div class="row">

							<div class="dishes-search">
								<input type="text" class="search-restaurant" name="search_restaurant" placeholder="Search Caterer" />
								<button class="search-restaurant-button"><span class="detail-search-icon fa fa-search"></span></button>
							</div>

							<div class="block_box">
								<span>View Halal-Certified Only</span>
								<div class="onoffswitch">
									<input type="checkbox" name="restaurant_category[]" class="onoffswitch-checkbox" id="myonoffswitch-1" value="'. $halalCategoryObj->term_id .'">
									<label class="onoffswitch-label" for="myonoffswitch-1">
										<div class="onoffswitch-inner"></div>
										<div class="onoffswitch-switch"></div>
									</label>
								</div>
							</div> 
							<span class="inner-reset-button">Clear filters</span>
							<ul id="list-page-filter-list"></ul>
						
							<div class="super-category-bar">

								'. $typeOfServices .'

							</div>
							'. $typeOfCuisines .'
							'. $typeOfOccasion .'
							
							<div>
								<div class="range">
									<h4><span><img src="" alt=""></span>RATING</h4>
									<span class="range-value"></span>
							  		<input id="ex5" name="restaurant_avg_rating" type="text" data-slider-min="0" data-slider-max="5" data-slider-step="1" data-slider-value="'. $rating .'"/>
							  		<span class="range-value last-range">5</span>
							  	</div>
	
							 									 
								'. $dietaryRestrictions .'

								
							</div>
						</div>
					
					</div>
				</form>
                <span class="mobile-cart-box">
                    <i class="fa fa-shopping-cart"></i>
                    <i class="cart-number"></i>
                </span>
			</div>
			</div>    

			<script type="text/javascript">
                var slider;
				jQuery(document).ready(function($){
					$("#budget_select").val('. $budget .');

                    $(".header-filter-form-reset").on("click", function(){
                        let parentContainer = $(this).parent().parent();
                        $("input[type=\'checkbox\']", parentContainer).prop("checked", false);
                        $(".side-filter-form").trigger("change");
                    });

					// With JQuery
					jQuery("#ex5").slider();

					// Without JQuery
					slider = new Slider("#ex5");

					jQuery("#destroyEx5Slider").click(function() {

					  // With JQuery
					  jQuery("#ex5").slider("destroy");

					  // Without JQuery
					  slider.destroy();
					});


					/*jQuery("#ex6").slider();

					// Without JQuery
					var slider6 = new Slider("#ex6");

					jQuery("#destroyEx6Slider").click(function() {

					  // With JQuery
					  jQuery("#ex6").slider("destroy");

					  // Without JQuery
					  slider.destroy();
					});*/
				});
			</script>             
		';
		$filters .= $this->loadInternalJS();
		$filters .= $this->loadMapJS();

		return $filters;
	}

	/* Get Meta Values*/

	function get_meta_values( $key = '', $type = 'post', $status = 'publish' ) {

	    global $wpdb;

	    if( empty( $key ) )
	        return;

	    $r = $wpdb->get_col( $wpdb->prepare( "
	        SELECT pm.meta_value FROM {$wpdb->postmeta} pm
	        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
	        WHERE pm.meta_key = '%s' 
	        AND p.post_status = '%s' 
	        AND p.post_type = '%s'
	    GROUP BY pm.meta_value", $key, $status, $type ) );

	    return $r;
	}


	/* Get Side Filters Data*/

	public function customFilters(){
		global $wpdb;
		$query= "SELECT meta_key FROM `wp_postmeta` WHERE `meta_key`  ='wp_restaurant_minimum_order_amount' OR `meta_key` = 'wp_restaurant_price_range' GROUP BY meta_key";
		$customFields = $wpdb->get_col($wpdb->prepare($query,'','publish'));
		return $customFields;
	} 


	/* HTML For Side Filters*/

	public function sideFilter(){
		$dataRestaurantType = get_terms( array(
		    'taxonomy' => 'restaurant_type',
		    'hide_empty' => false,
		) );
		$dataRestaurantCategory = get_terms( array(
		    'taxonomy' => 'restaurant_category',
		    'hide_empty' => false,
		) );
		
		$sideFilter .="<section class='sideFilter'>";
		$sideFilter .="<form id='sideFilterForm' class='sideFilterForm' action='javascript:void(0);' method='POST'>";
				
				$sideFilter .="<div  class='accordion' ><h4>Restaurant Type</h4><ul>";
		foreach($dataRestaurantType as $value){
			$sideFilter .= "<li class='sidefilters'><input type='checkbox'  name='wp_restaurant_type[]' class='myfilters' value='".$value->term_id."'>".$value->name."</li>";	
		}
		$sideFilter.="</ul></div>";
		$sideFilter .="<div class='accordion'><h4>Restaurant Category</h4><ul>";
		foreach($dataRestaurantCategory as $value){
			$sideFilter .= "<li class='sidefilters'><input type='checkbox' name='wp_restaurant_type[]' class='myfilters' value='".$value->term_id."'>".$value->name."</li>";	
		}
		$sideFilter .="</ul></div>";
		$customFields =$this->customFilters();
		foreach($customFields as $row){
			
			$sideFilter .="<div class='accordion' style='width: 100%;float: left;'><h4>".str_replace(array("wp_restaurant_","_")," ",$row)."</h4><ul>";
			$metaValues = $this->get_meta_values($row,'wp_restaurant');
			
				foreach($metaValues as $mvals){
					if($mvals!='')
					$sideFilter .= "<li class='sidefilters'><input type='checkbox' value='".$mvals."' class='myfilters' name='".$row."[]'>".$mvals."</li>";
				}		
			$sideFilter .="</ul></div>";
		}
			if($_REQUEST['date']!=''){
				$dt = strtotime($_REQUEST['date']);
				$day = date("l", $dt);
				$days= strtolower(str_replace('day', '', $day));
			}
			if($_REQUEST['time']!=''){
				if(strpos($_REQUEST['time'],"PM")==true){
					$time = round($_REQUEST['time'])+12;
				}else{
					$time = round($_REQUEST['time']);
				}
				if($time==24){
					$time ="0";
				}
				if($time<10){
					$time ="0".$time;
				}
			}
		$sideFilter .="<input type='hidden' name='wp_restaurant_address[]' value='".$_REQUEST['location']."'>
					<input type='hidden' name='wp_restaurant_delivery_days[]' value='".$days."'>
				<input type='hidden' name='wp_restaurant_delivery_hours[]' value=".$time.">";
		$sideFilter .="</form>";
		$sideFilter.='</section>
						<script>
						  $( function() {
						    $( ".accordion" ).accordion({
						      collapsible: true
						    });
						  } );
						</script>';
		return $sideFilter; 
	}

	/*Load JS For Clock Picker */

	public function loadInternalJS(){
		$data='
			<script>
				document.body.classList.add("list-page");
			
				$(".inner-reset-button").on("click", function(){
					let parentContainer = $(this).siblings();
					$("input[type=\'checkbox\']", parentContainer).prop("checked", false);
					$(".side-filter-form").trigger("change");
				});

			</script>
			
		';

		return $data;
	}
		public function loadMapJS(){
			$data = '
			        <script>
			            function init() {
			                var input = document.getElementById("locationTextField");
			                var autocomplete = new google.maps.places.Autocomplete(input);
			            }
						google.maps.event.addDomListener(window, "load", init);
			        </script>
	        
					<script src="https://richardcornish.github.io/jquery-geolocate/js/jquery-geolocate.min.js"></script>
					<script>
					    (function($, google) {
					        $(".button-0, #filter_location").on("click", function(e) {
					            $(".input-0").geolocate();
					            e.preventDefault();
					            // Map (not part of plugin)
					            navigator.geolocation.getCurrentPosition(function(position) {
					                drawMap({
					                    lat: position.coords.latitude,
					                    lng: position.coords.longitude
					                });
					            });
					        });
					    })(jQuery, google);
					</script>';
        return $data;
	}

	public function sliderJS(){
		$data ='<script type="text/javascript">
				    jQuery(document).ready(function () {
				    var itemsMainDiv = (".MultiCarousel");
				    var itemsDiv = (".MultiCarousel-inner");
				    var itemWidth = "";

				    jQuery(".leftLst, .rightLst").click(function () {
				        var condition = jQuery(this).hasClass("leftLst");
				        if (condition)
				            click(0, this);
				        else
				            click(1, this)
				    });

				    ResCarouselSize();
				    jQuery(window).resize(function () {
				        ResCarouselSize();
				    });

				    //this function define the size of the items
				    function ResCarouselSize() {
				        var incno = 0;
				        var dataItems = ("data-items");
				        var itemClass = (".item");
				        var id = 0;
				        var btnParentSb = "";
				        var itemsSplit = "";
				        var sampwidth = jQuery(itemsMainDiv).width();
				        var bodyWidth = jQuery("body").width();
				        jQuery(itemsDiv).each(function () {
				            id = id + 1;
				            var itemNumbers = jQuery(this).find(itemClass).length;
				            btnParentSb = jQuery(this).parent().attr(dataItems);
				            itemsSplit = btnParentSb.split(",");
				            jQuery(this).parent().attr("id", "MultiCarousel" + id);


				            if (bodyWidth >= 1200) {
				                incno = itemsSplit[3];
				                itemWidth = sampwidth / incno;
				            }
				            else if (bodyWidth >= 992) {
				                incno = itemsSplit[2];
				                itemWidth = sampwidth / incno;
				            }
				            else if (bodyWidth >= 768) {
				                incno = itemsSplit[1];
				                itemWidth = sampwidth / incno;
				            }
				            else {
				                incno = itemsSplit[0];
				                itemWidth = sampwidth / incno;
				            }
				            jQuery(this).css({ "transform": "translateX(0px)", "width": itemWidth * itemNumbers });
				            jQuery(this).find(itemClass).each(function () {
				                jQuery(this).outerWidth(itemWidth);
				            });

				            jQuery(".leftLst").addClass("over");
				            jQuery(".rightLst").removeClass("over");

				        });
				    }
				    //this function used to move the items
				    function ResCarousel(e, el, s) {
				        var leftBtn = (".leftLst");
				        var rightBtn = (".rightLst");
				        var translateXval = "";
				        var divStyle = jQuery(el + " " + itemsDiv).css("transform");
				        var values = divStyle.match(/-?[\d\.]+/g);
				        var xds = Math.abs(values[4]);
				        if (e == 0) {
				            translateXval = parseInt(xds) - parseInt(itemWidth * s);
				            jQuery(el + " " + rightBtn).removeClass("over");

				            if (translateXval <= itemWidth / 2) {
				                translateXval = 0;
				                jQuery(el + " " + leftBtn).addClass("over");
				            }
				        }
				        else if (e == 1) {
				            var itemsCondition = jQuery(el).find(itemsDiv).width() - jQuery(el).width();
				            translateXval = parseInt(xds) + parseInt(itemWidth * s);
				            jQuery(el + " " + leftBtn).removeClass("over");

				            if (translateXval >= itemsCondition - itemWidth / 2) {
				                translateXval = itemsCondition;
				                jQuery(el + " " + rightBtn).addClass("over");
				            }
				        }
				        jQuery(el + " " + itemsDiv).css("transform", "translateX(" + -translateXval + "px)");
				    }

				    //It is used to get some elements from btn
				    function click(ell, ee) {
				        var Parent = "#" + jQuery(ee).parent().attr("id");
				        var slide = jQuery(Parent).attr("data-slide");
				        ResCarousel(ell, Parent, slide);
				    }

				});
			</script>';
		return $data;
	}
}


/*Ajax Filtering*/

$wp_simple_locations = new Restaurant_Listing;
add_action( 'wp_ajax_nopriv_aj_ajax_demo_get_filter', 'aj_ajax_demo_filter' );
add_action( 'wp_ajax_aj_ajax_demo_get_filter', 'aj_ajax_demo_filter' ); 
 function aj_ajax_demo_filter() {
	$wp_simple_locations = new Restaurant_Listing;
	echo $wp_simple_locations->lsiting($_REQUEST['data']);
	exit;
    
}


